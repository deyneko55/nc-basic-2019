<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Sending Email with Freemarker HTML Template Example</title>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>

    <!-- use the font -->
    <style>
        body {
            font-family: 'Roboto', sans-serif;
            font-size: 58px;
        }
    </style>
</head>
<body style="margin: 0; padding: 0;">

    <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
        <tr>
            <td align="center" bgcolor="#7fffd4" style="padding: 40px 0 30px 0;">
<#--                <img src="cid:attachementFilename.png" style="display: block;" />-->
                <p><h3>Greetings from NetCracker Training Center</h3></p>
            </td>
        </tr>
        <tr>
            <td bgcolor="#b0c4de" style="padding: 40px 30px 40px 30px;">
                <p><h1>Dear ${name},</h1></p>
<#--                <p>Sending <i>test message</i> <b> with different </b> <u> decor.</u></p>-->
                <p>${text}</p>
                <p><b>With best regards, NetCracker Training Center!</b></p>
            </td>
        </tr>
        <tr>
            <td bgcolor="silver" style="padding: 30px 30px 30px 30px;">
                <p>${signature}</p>
                <p>${location}</p>
            </td>
        </tr>
    </table>

</body>
</html>