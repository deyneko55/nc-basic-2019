package edu.netcracker.backend.utils;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;

public class ExcelBuilder {

    private static final int DEFAULT_COLUMN_WIDTH = 35;
    private static final float HEIGHT_IN_POINTS = 30;

    public static XSSFSheet createSheet(Workbook workbook, String sheetName) {
        XSSFSheet sheet = (XSSFSheet) workbook.createSheet(sheetName);
        sheet.setDefaultColumnWidth(DEFAULT_COLUMN_WIDTH);
        return sheet;
    }

    public static XSSFRow[] createRows(XSSFSheet sheet, Integer rowsCount) {
        XSSFRow[] rows = new XSSFRow[rowsCount];
        for (int rowIndex = 0; rowIndex < rowsCount; rowIndex++) {
            rows[rowIndex] = sheet.createRow(rowIndex);
        }
        return rows;
    }

    public static XSSFCell createCell(XSSFRow row, int index) {
        return row.createCell(index);
    }

    public static void setFont(Workbook workbook, XSSFRow row, String fontName,
                               HSSFColor.HSSFColorPredefined foregroundColor,
                               FillPatternType fillPatternType, boolean bold,
                               HSSFColor.HSSFColorPredefined fontColor) {

        XSSFCellStyle style = (XSSFCellStyle) workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName(fontName);
        style.setFillForegroundColor(foregroundColor.getIndex());
        style.setFillPattern(fillPatternType);
        font.setBold(bold);
        font.setColor(fontColor.getIndex());
        style.setFont(font);

        for (int i = 0; i < row.getLastCellNum(); i++) {
            row.getCell(i).setCellStyle(style);
        }
    }

    public static void processTitleRow(XSSFRow row, Integer columnIndex, String cellValue) {
        row.setHeightInPoints(HEIGHT_IN_POINTS);
        createCell(row, columnIndex).setCellValue(cellValue);
    }
}
