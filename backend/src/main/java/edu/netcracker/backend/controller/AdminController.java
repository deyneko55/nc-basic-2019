package edu.netcracker.backend.controller;

import edu.netcracker.backend.message.request.auth.EmailRequest;
import edu.netcracker.backend.message.request.group.GroupSetTrainerRequest;
import edu.netcracker.backend.message.request.news.NewsCreationRequest;
import edu.netcracker.backend.message.request.news.NewsDeleteRequest;
import edu.netcracker.backend.message.response.*;
import edu.netcracker.backend.model.GroupStatistic;
import edu.netcracker.backend.model.NewsPost;
import edu.netcracker.backend.model.NewsPostId;
import edu.netcracker.backend.service.GroupService;
import edu.netcracker.backend.service.NewsService;
import edu.netcracker.backend.security.JwtTokenProvider;
import edu.netcracker.backend.service.*;
import edu.netcracker.backend.service.impl.AttendanceReportService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.List;

@RestController
@Slf4j
@RequestMapping("/api/admin")
@CrossOrigin(origins = "http://localhost:4200")
public class AdminController {

    private GroupService groupService;
    private NewsService newsService;
    private JwtTokenProvider jwtTokenProvider;
    private EmailService emailService;
    private AttendanceReportService attendanceReportService;

    @Autowired
    public AdminController(GroupService groupService, NewsService newsService, JwtTokenProvider jwtTokenProvider,
                           EmailService emailService, AttendanceReportService attendanceReportService) {
        this.groupService = groupService;
        this.newsService = newsService;
        this.jwtTokenProvider = jwtTokenProvider;
        this.emailService = emailService;
        this.attendanceReportService = attendanceReportService;
    }

    @PutMapping(path = "/assign-trainer")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<MessageResponse> assignTrainerToTheGroup(@RequestBody GroupSetTrainerRequest trainerForGroup) {
        log.debug("Method was invoked");
        groupService.assignTrainerToTheGroup(trainerForGroup);
        return ResponseEntity.ok(new MessageResponse(HttpStatus.OK, "Trainer for group has been assigned"));
    }

    @PostMapping(path = "/news")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<NewsPostId> createNewsPost(@RequestBody NewsCreationRequest newsCreationRequest) {
        log.debug("Method was invoked");
        return new ResponseEntity<>(newsService.createNewsPost(newsCreationRequest.getHeader(), newsCreationRequest.getDescription(), newsCreationRequest.getContent()), HttpStatus.CREATED);
    }

    @PostMapping(value = "/upload/{newsId}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<MessageResponse> uploadNewsPhoto(@RequestParam(value = "picture") MultipartFile photo, @PathVariable("newsId") Integer newsId) {
        log.debug("Method was invoked");
        newsService.uploadNewPhoto(newsId, photo);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PatchMapping(path = "/news")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<NewsPost> updateNewsPost(@ModelAttribute @Valid NewsPost newsPost) {
        log.debug("Method was invoked");
        return new ResponseEntity<>(newsService.update(newsPost), HttpStatus.OK);
    }

    @DeleteMapping(path = "/news")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<Integer> deleteNewsPost(@RequestBody NewsDeleteRequest newsDeleteRequest) {
        log.debug("Method was invoked");
        return new ResponseEntity<>(newsService.delete(newsDeleteRequest.getNewsPostId()), HttpStatus.OK);
    }

    @PostMapping("/registration-link")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<MessageResponse> createRegistrationLink(@RequestBody EmailRequest emailRequest) {
        log.debug("Method was invoked");
        emailService.sendRegistrationMessage(emailRequest.getEmail(),
                jwtTokenProvider.generateMailRegistrationToken(emailRequest.getEmail()));
        return ResponseEntity.ok(new MessageResponse(HttpStatus.OK, "Registration link has been sent to email"));
    }

    @GetMapping("/group-statistics")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<List<GroupStatistic>> getGroupStatistic() {
        log.debug("Method was invoked");
        return new ResponseEntity<>(groupService.getGroupStatistics(), HttpStatus.OK);
    }

    @GetMapping("/download/attendance-report/{courseId}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ModelAndView downloadAttendanceReport(@PathVariable Integer courseId, Model model) {
        log.debug("Method was invoked");
        model.addAttribute("courseId", courseId);
        return new ModelAndView(attendanceReportService);
    }
}
