package edu.netcracker.backend.controller;

import edu.netcracker.backend.message.request.chat.ChatGroupFormRequest;
import edu.netcracker.backend.message.request.chat.ChatPrivateFormRequest;
import edu.netcracker.backend.message.request.chat.ChatSendMessageRequest;
import edu.netcracker.backend.message.response.ChatResponse;
import edu.netcracker.backend.service.ChatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/chats")
@CrossOrigin(origins = "http://localhost:4200")
public class ChatController {

    private ChatService chatService;

    @Autowired
    public ChatController(ChatService chatService) {
        this.chatService = chatService;
    }

    @MessageMapping("/{chatId}/messages/{userId}")
    public void getLastMessages(@DestinationVariable Integer chatId, @DestinationVariable Integer userId) {
        chatService.getLastMessagesFromChat(chatId, userId);
    }

    @PostMapping(path = "/new-group-chat")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<ChatResponse> createChatForGroup(@RequestBody ChatGroupFormRequest chatGroupFormRequest) {
        return ResponseEntity.ok().body(chatService.createChatForGroup(chatGroupFormRequest));
    }

    @PostMapping(path = "/new-private-chat")
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('MANAGER') or hasAuthority('TRAINER') or hasAuthority('EMPLOYEE')")
    public ResponseEntity<ChatResponse> createPrivateChat(@RequestBody @Valid ChatPrivateFormRequest chatPrivateFormRequest) {
        return ResponseEntity.ok().body(chatService.createPrivateChat(chatPrivateFormRequest));
    }

    @MessageMapping("/send")
    public void sendMessage(ChatSendMessageRequest message) {
        chatService.processMessage(message);
    }


    @GetMapping("/groups/{id}")
    public ChatResponse getByGroupId(@PathVariable("id") Integer groupId) {
        return new ChatResponse(chatService.getByGroupId(groupId));
    }

}
