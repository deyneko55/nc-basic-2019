package edu.netcracker.backend.controller;

import edu.netcracker.backend.message.request.news.NewsSetLiked;
import edu.netcracker.backend.message.request.news.NewsSetViewed;
import edu.netcracker.backend.message.request.news.SliderUpdateRequest;
import edu.netcracker.backend.message.response.MessageResponse;
import edu.netcracker.backend.model.NewsPost;
import edu.netcracker.backend.model.Slider;
import edu.netcracker.backend.service.NewsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Dmytro Vovk
 */

@RestController
@Slf4j
@RequestMapping("/api/news")
@CrossOrigin(origins = "http://localhost:4200")
public class NewsController {

    private NewsService newsService;

    @Autowired
    public NewsController(NewsService newsService) {
        this.newsService = newsService;
    }

    @GetMapping
    public ResponseEntity<List<NewsPost>> getAllNews(@RequestParam("userId") Integer userId) {
        log.debug("method was invoked");
        return new ResponseEntity<>(newsService.getAll(userId), HttpStatus.OK);
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<NewsPost> getCertainNewsPost(@PathVariable("id") Integer id,
                                                       @RequestParam("userId") Integer userId) {
        log.debug("method was invoked");
        return new ResponseEntity<>(newsService.getById(id, userId), HttpStatus.OK);
    }

    @PatchMapping("/set-viewed")
    public ResponseEntity<MessageResponse> setNewViewed(@RequestBody NewsSetViewed newsSetViewed) {
        log.debug("method was invoked");
        newsService.setNewsViewed(newsSetViewed.getNewsId(), newsSetViewed.getUserId());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PatchMapping("/set-like")
    public ResponseEntity<MessageResponse> setNewsLiked(@RequestBody NewsSetLiked newsSetLiked) {
        log.debug("method was invoked");
        newsService.setNewsLiked(newsSetLiked.getNewsId(), newsSetLiked.getUserId(), newsSetLiked.getIsLiked());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(path = "/sliders")
    public ResponseEntity<List<Slider>> getNewsFromSlider() {
        log.debug("method was invoked");
        return new ResponseEntity<>(newsService.getNewsFromSlider(), HttpStatus.OK);
    }

    @PatchMapping(path = "/sliders/update")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<MessageResponse> setNewsForSlider(@RequestBody SliderUpdateRequest sliderUpdateRequest) {
        log.debug("method was invoked");
        newsService.setNewsForSlider(sliderUpdateRequest.getSliderId(), sliderUpdateRequest.getNewsId());
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
