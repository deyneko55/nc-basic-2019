package edu.netcracker.backend.controller;

import edu.netcracker.backend.message.request.group.GroupDeleteRequest;
import edu.netcracker.backend.message.request.group.GroupFormRequest;
import edu.netcracker.backend.message.request.group.GroupSearchRequest;
import edu.netcracker.backend.message.request.group.GroupUpdateRequest;
import edu.netcracker.backend.message.request.user.UserSetToGroupRequest;
import edu.netcracker.backend.message.response.*;
import edu.netcracker.backend.service.GroupService;
import edu.netcracker.backend.service.impl.AttendanceReportService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author Dmytro Vovk
 */

@RestController
@Slf4j
@RequestMapping("/api/groups")
@CrossOrigin(origins = "http://localhost:4200")
public class GroupController {

    private GroupService groupService;
    private AttendanceReportService attendanceReportService;

    @Autowired
    public GroupController(GroupService groupService, AttendanceReportService attendanceReportService) {
        this.groupService = groupService;
        this.attendanceReportService = attendanceReportService;
    }

    @PostMapping(path = "/new-group")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<GroupResponse> createGroup(@Valid @RequestBody GroupFormRequest groupFormRequest) {
        log.debug("method was invoked");
        return new ResponseEntity<>(groupService.createGroup(groupFormRequest), HttpStatus.CREATED);
    }

    @PatchMapping
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('MANAGER') or hasAuthority('TRAINER')")
    public ResponseEntity<MessageResponse> editGroup(@Valid @RequestBody GroupUpdateRequest groupUpdateRequest) {
        log.debug("method was invoked");
        groupService.updateGroup(groupUpdateRequest);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<MessageResponse> deleteGroup(@RequestBody @Valid GroupDeleteRequest groupDeleteRequest) {
        log.debug("method was invoked");
        groupService.deleteGroup(groupDeleteRequest);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(path = "/{id}")
    @PreAuthorize("hasAuthority('EMPLOYEE') or hasAuthority('ADMIN') or hasAuthority('MANAGER') or hasAuthority('TRAINER')")
    public ResponseEntity<GroupWithTrainerResponse> getGroup(@PathVariable("id") @NotNull Integer groupId) {
        log.debug("method was invoked");
        return new ResponseEntity<>(groupService.getById(groupId), HttpStatus.OK);
    }

    @GetMapping(path = "/search")
    @PreAuthorize("hasAuthority('EMPLOYEE') or hasAuthority('ADMIN') or hasAuthority('MANAGER') or hasAuthority('TRAINER')")
    public ResponseEntity<List<GroupResponse>> searchByName(@RequestBody @Valid GroupSearchRequest groupSearchRequest) {
        log.debug("method was invoked");
        return new ResponseEntity<>(groupService.getGroupsByName(groupSearchRequest.getGroupName()), HttpStatus.OK);
    }

    @GetMapping(path = "/{id}/members")
    @PreAuthorize("hasAuthority('EMPLOYEE') or hasAuthority('ADMIN') or hasAuthority('MANAGER') or hasAuthority('TRAINER')")
    public ResponseEntity<List<UserResponse>> getAllMembers(@PathVariable("id") @NotNull Integer groupId) {
        log.debug("method was invoked");
        List<UserResponse> members = groupService.getGroupMembers(groupId);
        return new ResponseEntity<>(members, HttpStatus.OK);
    }

    @PostMapping(path = "/add-user")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<MessageResponse> addNewUserToGroup(@Valid @RequestBody UserSetToGroupRequest userSetToGroupRequest) {
        log.debug("method was invoked");
        groupService.addNewUserToGroup(userSetToGroupRequest.getUserId(), userSetToGroupRequest.getGroupId());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping
    @PreAuthorize("hasAuthority('EMPLOYEE') or hasAuthority('ADMIN') or hasAuthority('MANAGER') or hasAuthority('TRAINER')")
    public ResponseEntity<List<GroupResponse>> getAllGroups() {
        log.debug("method was invoked");
        return new ResponseEntity<>(groupService.getAllGroups(), HttpStatus.OK);
    }

    @GetMapping("/user-groups/{userId}")
    @PreAuthorize("hasAuthority('EMPLOYEE') or hasAuthority('ADMIN') or hasAuthority('MANAGER') or hasAuthority('TRAINER')")
    public ResponseEntity<List<GroupResponse>> getGroupsByUserId(@PathVariable("userId") Integer userId) {
        log.debug("method was invoked");
        return new ResponseEntity<>(groupService.getGroupsByUserId(userId), HttpStatus.OK);
    }

    @GetMapping("/group-mates/{userId}")
    @PreAuthorize("hasAuthority('EMPLOYEE') or hasAuthority('ADMIN') or hasAuthority('MANAGER') or hasAuthority('TRAINER')")
    public ResponseEntity<List<UserResponse>> getGroupMates(@PathVariable("userId") Integer userId) {
        log.debug("method was invoked");
        return new ResponseEntity<>(groupService.getGroupMates(userId), HttpStatus.OK);
    }

    @GetMapping("/attendance/{groupId}")
    @PreAuthorize("hasAuthority('EMPLOYEE') or hasAuthority('ADMIN') or hasAuthority('TRAINER') or hasAuthority('MANAGER')")
    public ResponseEntity<List<AttendanceDTO>> getAttendance(@PathVariable Integer groupId) {
        log.debug("method was invoked");
        return ResponseEntity.ok(groupService.getGroupAttendance(groupId));
    }

    @GetMapping("/download/attendance-report/{groupId}")
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('TRAINER') ")
    public ModelAndView downloadGroupAttendanceReport(@PathVariable Integer groupId, Model model) {
        log.debug("method was invoked");
        model.addAttribute("groupId", groupId);
        return new ModelAndView(attendanceReportService);
    }
}
