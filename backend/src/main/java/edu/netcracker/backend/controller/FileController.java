package edu.netcracker.backend.controller;

import edu.netcracker.backend.message.response.MessageResponse;
import edu.netcracker.backend.model.File;
import edu.netcracker.backend.service.FileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@Slf4j
@RequestMapping("/api/files")
@CrossOrigin(origins = "http://localhost:4200")
public class FileController {

    private FileService fileService;

    @Autowired
    public FileController(FileService fileService) {
        this.fileService = fileService;
    }

    @GetMapping(path = "/{fileId}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<File> getFile(@PathVariable("fileId") @NotNull Integer fileId) {
        log.debug("method was invoked");
        return new ResponseEntity<>(fileService.getFile(fileId), HttpStatus.OK);
    }

    @GetMapping(path = "/all")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<List<File>> getAllFiles() {
        log.debug("method was invoked");
        return new ResponseEntity<>(fileService.getAllFiles(), HttpStatus.OK);
    }

    @DeleteMapping(path = "/{fileId}")
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('TRAINER')")
    public ResponseEntity<MessageResponse> deleteFiles(@PathVariable("fileId") Integer fileId) {
        log.debug("method was invoked");
        fileService.deleteFile(fileId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(value = "/upload/{lessonId}")
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('TRAINER')")
    public ResponseEntity<MessageResponse> uploadFiles(@PathVariable Integer lessonId, @RequestParam MultipartFile file) {
        log.debug("method was invoked");
        fileService.uploadFile(lessonId, file);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/download/lesson-materials")
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('TRAINER')")
    public ResponseEntity<Resource> downloadLessonMaterials(@RequestParam Integer fileId) {
        log.debug("method was invoked");
        File file = fileService.getFile(fileId);
        return ResponseEntity.ok().contentType(MediaType.parseMediaType(file.getFileType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFileName() + "\"")
                .body(new ByteArrayResource(file.getLesson_file()));
    }
}
