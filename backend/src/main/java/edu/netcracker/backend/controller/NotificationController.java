package edu.netcracker.backend.controller;

import edu.netcracker.backend.model.Notification;
import edu.netcracker.backend.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Dmytro Vovk
 */

@RestController
@RequestMapping("/api/notifications")
@CrossOrigin(origins = "http://localhost:4200")
public class NotificationController {

    private NotificationService notificationService;

    @Autowired
    public NotificationController(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @GetMapping("/unread/{userId}")
    ResponseEntity<List<Notification>> getUnreadNotifications(@PathVariable("userId") Integer userId) {
        return new ResponseEntity<>(notificationService.getUnreadNotifications(userId), HttpStatus.OK);
    }

    @GetMapping("/{userId}")
    ResponseEntity<List<Notification>> getAllUserNotifications(@PathVariable("userId") Integer userId) {
        return new ResponseEntity<>(notificationService.getAllUserNotifications(userId), HttpStatus.OK);
    }

    @GetMapping("/last/{userId}")
    ResponseEntity<List<Notification>> getUserLastNotifications(@PathVariable("userId") Integer userId) {
        return new ResponseEntity<>(notificationService.getUserLastNotifications(userId), HttpStatus.OK);
    }

    @PatchMapping("/read")
    ResponseEntity markAllAsRead(@RequestBody Integer userId) {
        notificationService.markAllAsRead(userId);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
