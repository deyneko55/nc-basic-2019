package edu.netcracker.backend.controller;

import edu.netcracker.backend.message.request.course.*;
import edu.netcracker.backend.message.response.*;
import edu.netcracker.backend.model.CourseLevel;
import edu.netcracker.backend.model.CourseType;
import edu.netcracker.backend.service.CourseService;
import edu.netcracker.backend.service.NotificationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.List;

@RestController
@Slf4j
@RequestMapping("/api/courses")
@CrossOrigin(origins = "http://localhost:4200")
public class CourseController {

    private CourseService courseService;
    private NotificationService notificationService;

    @Autowired
    public CourseController(CourseService courseService, NotificationService notificationService) {
        this.courseService = courseService;
        this.notificationService = notificationService;
    }

    @PostMapping(path = "/new-course")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<CourseDTO> createCourse(@Valid @RequestBody CourseRequest courseRequest) {
        log.debug("Method was invoked");
        return new ResponseEntity<>(courseService.createCourse(courseRequest), HttpStatus.CREATED);
    }

    @PostMapping(path = "/new")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<CourseDTO> createNewCourse(@Valid @RequestBody CourseWithIdsFormRequest course) {
        log.debug("Method was invoked");
        return new ResponseEntity<>(courseService.addNewCourseByIds((course)), HttpStatus.CREATED);
    }

    @PatchMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<MessageResponse> editCourse(@Valid @RequestBody CourseRequest courseRequest) {
        log.debug("Method was invoked");
        courseService.editCourse(courseRequest);
        return ResponseEntity.ok(new MessageResponse(HttpStatus.OK, "Course has been edited"));
    }

    @DeleteMapping(path = "/{courseId}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<MessageResponse> deleteCourse(@PathVariable @NotNull Integer courseId) {
        log.debug("Method was invoked");
        courseService.deleteCourse(courseId);
        return ResponseEntity.ok(new MessageResponse(HttpStatus.OK, "Course has been removed"));
    }

    @GetMapping(path = "/search/{courseName}")
    @PreAuthorize("hasAuthority('EMPLOYEE') or hasAuthority('ADMIN') or hasAuthority('MANAGER') or hasAuthority('TRAINER')")
    public ResponseEntity<List<CourseDTO>> searchByName(@PathVariable @NotBlank @Size(max = 50) String courseName) {
        log.debug("Method was invoked");
        return new ResponseEntity<>(courseService.findCourseByName(courseName), HttpStatus.OK);
    }

    @GetMapping
    @PreAuthorize("hasAuthority('EMPLOYEE') or hasAuthority('ADMIN') or hasAuthority('MANAGER') or hasAuthority('TRAINER')")
    public ResponseEntity<List<CourseDTO>> getAllCourses() {
        log.debug("Method was invoked");
        return new ResponseEntity<>(courseService.getAllCourses(), HttpStatus.OK);
    }

    @GetMapping(path = "/active-courses")
    @PreAuthorize("hasAuthority('EMPLOYEE') or hasAuthority('ADMIN') or hasAuthority('MANAGER') or hasAuthority('TRAINER')")
    public ResponseEntity<List<CourseDTO>> getActiveCourses() {
        log.debug("Method was invoked");
        return new ResponseEntity<>(courseService.getActiveCourses(LocalDate.now()), HttpStatus.OK);
    }

    @GetMapping(path = "/user-courses/{userId}")
    @PreAuthorize("hasAuthority('EMPLOYEE') or hasAuthority('ADMIN') or hasAuthority('MANAGER') or hasAuthority('TRAINER')")
    public ResponseEntity<List<CourseDTO>> getCoursesByUserId(@PathVariable("userId") Integer userId) {
        log.debug("Method was invoked");
        return new ResponseEntity<>(courseService.getCoursesByUserId(userId), HttpStatus.OK);
    }

    @GetMapping(path = "/{id}")
    @PreAuthorize("hasAuthority('EMPLOYEE') or hasAuthority('ADMIN') or hasAuthority('MANAGER') or hasAuthority('TRAINER')")
    public ResponseEntity<CourseDTO> getCourse(@PathVariable("id") @NotNull Integer courseId) {
        log.debug("Method was invoked");
        return new ResponseEntity<>(courseService.findById(courseId), HttpStatus.OK);
    }

    @GetMapping(path = "/types")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<List<CourseType>> getAllCourseTypes() {
        log.debug("Method was invoked");
        return ResponseEntity.ok().body(courseService.getAllCourseTypes());
    }

    @GetMapping(path = "/levels")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<List<CourseLevel>> getAllLevelsOfCourse() {
        log.debug("Method was invoked");
        return ResponseEntity.ok().body(courseService.getAllLevelsOfCourse());
    }

    @GetMapping(path = "/{courseId}/groups")
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('MANAGER') or hasAuthority('TRAINER')")
    public ResponseEntity<List<GroupWithTrainerResponse>> getAllGroupsFromCourse(@PathVariable("courseId") @NotNull Integer courseId) {
        log.debug("Method was invoked");
        return ResponseEntity.ok().body(courseService.getGroupsWithTrainersFromCourse(courseId));
    }

    @GetMapping(path = "/{courseId}/users")
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('MANAGER') or hasAuthority('TRAINER')")
    public ResponseEntity<List<UserResponse>> getAllUsersFromCourse(@PathVariable("courseId") @NotNull Integer courseId) {
        log.debug("Method was invoked");
        return ResponseEntity.ok().body(courseService.getAllUsersFromCourse(courseId));
    }

    @GetMapping("/statistics/courseTypes/{courseTypeId}/users")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<List<CourseTypeStatsResponse>> getUsersByCourseType(@PathVariable @Max(50) Integer courseTypeId) {
        log.debug("Method was invoked");
        return new ResponseEntity<>(courseService.getUsersByCourseType(courseTypeId), HttpStatus.OK);
    }

    @GetMapping("/statistics/courseTypes/{courseTypeId}/groups")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<List<CourseTypeStatsResponse>> getGroupsByDifLevel(@PathVariable @Max(50) Integer courseTypeId) {
        log.debug("Method was invoked");
        return new ResponseEntity<>(courseService.getGroupsByCourseType(courseTypeId), HttpStatus.OK);
    }

    @PostMapping(path = "/bookcourse")
    @PreAuthorize("hasAuthority('EMPLOYEE') or hasAuthority('MANAGER') or hasAuthority('ADMIN')")
    public ResponseEntity<MessageResponse> bookCourse(@RequestBody BookCourseFormRequest bookCourseFormRequest) {
        log.debug("Method was invoked");
        courseService.bookCourse(bookCourseFormRequest.getUserId(), bookCourseFormRequest.getCourseId());
        return ResponseEntity.ok(new MessageResponse(HttpStatus.OK, "Course has been booked"));
    }

    @GetMapping(path = "/{courseId}/lessons")
    @PreAuthorize("hasAuthority('EMPLOYEE') or hasAuthority('ADMIN') or hasAuthority('MANAGER') or hasAuthority('TRAINER')")
    public ResponseEntity<List<LessonCreationResponse>> getAllLessonsFromCourse(@PathVariable("courseId") Integer courseId) {
        log.debug("Method was invoked");
        return new ResponseEntity<>(courseService.getAllLessonsFromCourse(courseId), HttpStatus.OK);
    }

    @GetMapping("/type/{courseTypeId}/levels")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<List<CourseLevel>> getLevelsOfCourseType(@PathVariable Integer courseTypeId) {
        log.debug("Method was invoked");
        return new ResponseEntity<>(courseService.getLevelsOfCourseType(courseTypeId), HttpStatus.OK);
    }

    @GetMapping(path = "/desiredcourses/{courseId}/users")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<List<UserResponse>> getAllUsersFromDesiredCourse(@PathVariable("courseId") Integer courseId) {
        log.debug("Method was invoked");
        return ResponseEntity.ok().body(courseService.getAllUsersFromDesiredCourse(courseId));
    }

    @PostMapping("/new-type")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<CourseTypeResponse> createNewCourseType(@RequestBody CourseTypeCreationRequest courseTypeCreationRequest) {
        log.debug("Method was invoked");
        return new ResponseEntity<>(courseService.createCourseType(courseTypeCreationRequest.getTypeName()), HttpStatus.CREATED);
    }

    @PostMapping("/new-difficulty-level")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<CourseDifficultyLevelResponse> createNewDifficultyLevel(@RequestBody CourseDifficultyLevelCreationRequest courseDifficultyLevelCreationRequest) {
        log.debug("Method was invoked");
        return new ResponseEntity<>(courseService.createDifficultyLevel(courseDifficultyLevelCreationRequest.getNameOfLevel()), HttpStatus.CREATED);
    }

    @PostMapping("/join-course-request")
    @PreAuthorize("hasAuthority('EMPLOYEE') or hasAuthority('MANAGER') or hasAuthority('TRAINER')")
    public ResponseEntity joinCourse(@RequestBody CourseJoinRequest courseJoinRequest) {
        log.debug("Method was invoked");
        notificationService.sendCourseJoinRequest(courseJoinRequest);
        return new ResponseEntity(HttpStatus.OK);
    }
}
