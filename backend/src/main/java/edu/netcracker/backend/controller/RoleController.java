package edu.netcracker.backend.controller;

import edu.netcracker.backend.model.Role;
import edu.netcracker.backend.service.RoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
@RequestMapping("/api/roles")
@CrossOrigin(origins = "http://localhost:4200")
public class RoleController {

    private RoleService roleService;

    @Autowired
    public RoleController(RoleService roleService) {
        this.roleService = roleService;
    }

    @GetMapping(path = "/{roleId}")
    public ResponseEntity<String> getRoleName(@PathVariable("roleId") Integer roleId) {
        log.debug("method was invoked");
        return new ResponseEntity<>(roleService.getRoleName(roleId), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<Role>> getAll() {
        log.debug("method was invoked");
        return new ResponseEntity<>(roleService.getAll(), HttpStatus.OK);
    }
}
