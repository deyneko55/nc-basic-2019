package edu.netcracker.backend.controller;

import edu.netcracker.backend.message.request.absencereason.AbsenceReasonFormRequest;
import edu.netcracker.backend.message.response.AbsenceReasonResponse;
import edu.netcracker.backend.service.AbsenceReasonService;
import edu.netcracker.backend.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
@RequestMapping("/api/absence-reasons")
@CrossOrigin(origins = "http://localhost:4200")
public class AbsenceReasonController {

    private UserService userService;
    private AbsenceReasonService absenceReasonService;

    @Autowired
    public AbsenceReasonController(UserService userService, AbsenceReasonService absenceReasonService) {
        this.userService = userService;
        this.absenceReasonService = absenceReasonService;
    }

    @PostMapping(path = "/new-absence-reason")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<AbsenceReasonResponse> addNewAbsenceReason(@RequestBody AbsenceReasonFormRequest absenceReasonFormRequest) {
        log.debug("method was invoked");
        return ResponseEntity.ok().body(userService.addNewAbsenceReason(absenceReasonFormRequest));
    }

    @GetMapping
    @PreAuthorize("hasAuthority('EMPLOYEE') or hasAuthority('ADMIN') or hasAuthority('MANAGER') or hasAuthority('TRAINER')")
    public ResponseEntity<List<AbsenceReasonResponse>> getAll() {
        log.debug("method was invoked");
        return ResponseEntity.ok(absenceReasonService.getAll());
    }
}
