package edu.netcracker.backend.controller;

import edu.netcracker.backend.message.request.lesson.*;
import edu.netcracker.backend.message.response.*;
import edu.netcracker.backend.model.PresenceStatus;
import edu.netcracker.backend.model.UserStatus;
import edu.netcracker.backend.service.LessonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@Slf4j
@RequestMapping("/api/lessons")
@CrossOrigin(origins = "http://localhost:4200")
public class LessonController {

    private LessonService lessonService;

    @Autowired
    public LessonController(LessonService lessonService) {
        this.lessonService = lessonService;
    }

    @PostMapping(path = "/new-lesson")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<LessonCreationResponse> createLesson(@Valid @RequestBody LessonFormRequest lessonFormRequest) {
        log.debug("method was invoked");
        return new ResponseEntity<>(lessonService.createLesson(lessonFormRequest), HttpStatus.CREATED);
    }

    @PatchMapping
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('MANAGER') or hasAuthority('TRAINER')")
    public ResponseEntity<MessageResponse> editLesson(@Valid @RequestBody LessonUpdateRequest lessonUpdateRequest) {
        log.debug("method was invoked");
        lessonService.updateLesson(lessonUpdateRequest);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PatchMapping("/cancel-lesson")
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('TRAINER')")
    public ResponseEntity<MessageResponse> cancelTheLesson(@RequestParam("lessonId") Integer lessonId,
                                                           @RequestParam("groupId") Integer groupId) {
        log.debug("method was invoked");
        lessonService.cancelTheLesson(lessonId, groupId);
        return ResponseEntity.ok(new MessageResponse(HttpStatus.OK, "Message has been sent to employees"));
    }

    @GetMapping(path = "/schedule/{groupId}")
    @PreAuthorize("hasAuthority('EMPLOYEE') or hasAuthority('ADMIN') or hasAuthority('MANAGER') or hasAuthority('TRAINER')")
    public ResponseEntity<List<LessonWithStatusResponse>> getAllLessonsOfGroup(@PathVariable("groupId") Integer groupId) {
        log.debug("method was invoked");
        return new ResponseEntity<>(lessonService.getAllLessons(groupId), HttpStatus.OK);
    }

    @GetMapping(path = "/{groupId}-{lessonId}")
    public ResponseEntity<LessonResponse> getGroupLesson(@PathVariable("lessonId") Integer lessonId,
                                                         @PathVariable("groupId") Integer groupId) {
        log.debug("method was invoked");
        return new ResponseEntity<>(lessonService.getGroupLesson(lessonId, groupId), HttpStatus.OK);
    }

    @PostMapping("/status")
    public ResponseEntity<MessageResponse> setUserStatus(@RequestBody LessonUserStatus lessonUserStatus) {
        log.debug("method was invoked");
        lessonService.setUserStatus(lessonUserStatus);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/status/{userId}-{lessonId}")
    public ResponseEntity<MessageResponse> deleteUserStatus(@PathVariable("userId") Integer userId,
                                                            @PathVariable("lessonId") Integer lessonId) {
        log.debug("method was invoked");
        lessonService.deleteUserStatus(userId, lessonId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/status/{userId}-{lessonId}")
    public ResponseEntity<UserStatus> getUserStatus(@PathVariable("userId") Integer userId,
                                                    @PathVariable("lessonId") Integer lessonId) {
        log.debug("method was invoked");
        return new ResponseEntity<>(lessonService.getUserStatus(userId, lessonId), HttpStatus.OK);
    }

    @PatchMapping(path = "/set-course")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<MessageResponse> updateCourseForLesson(@RequestBody LessonUpdateCourseRequest lessonUpdateCourseRequest) {
        log.debug("method was invoked");
        lessonService.updateCourseForLesson(lessonUpdateCourseRequest);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/files/{lessonId}")
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('MANAGER') or hasAuthority('TRAINER') or hasAuthority('EMPLOYEE')")
    public ResponseEntity<List<FileResponse>> getLessonFiles(@PathVariable("lessonId") Integer lessonId) {
        log.debug("method was invoked");
        return new ResponseEntity<>(lessonService.getLessonFiles(lessonId), HttpStatus.OK);
    }

    @PatchMapping(path = "/set-date-for-group")
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('MANAGER') or hasAuthority('TRAINER')")
    public ResponseEntity<MessageResponse> setDateForLessonOfGroup(
            @Valid @RequestBody LessonSetDateAndGroupRequest lessonOfGroup) {
        log.debug("method was invoked");
        lessonService.setDateForLessonOfGroup(lessonOfGroup);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/presence-statuses")
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('TRAINER')")
    public ResponseEntity<List<PresenceStatus>> getAllPresenceStatuses() {
        log.debug("method was invoked");
        return new ResponseEntity<>(lessonService.getAllPresenceStatuses(), HttpStatus.OK);
    }
}
