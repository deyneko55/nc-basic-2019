package edu.netcracker.backend.controller;

import edu.netcracker.backend.message.request.desiredtime.DesiredTimeRequest;
import edu.netcracker.backend.message.request.desiredtime.DesiredTimeUpdateRequest;
import edu.netcracker.backend.message.response.DesiredTimeResponse;
import edu.netcracker.backend.message.response.MessageResponse;
import edu.netcracker.backend.service.DesiredTimeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@Slf4j
@RequestMapping("/api/desiredschedules")
@CrossOrigin(origins = "http://localhost:4200")
public class DesiredTimeController {

    private DesiredTimeService desiredTimeService;

    @Autowired
    public DesiredTimeController(DesiredTimeService desiredTimeService) {
        this.desiredTimeService = desiredTimeService;
    }

    @PostMapping(path = "/settime")
    @PreAuthorize("hasAuthority('MANAGER') or hasAuthority('EMPLOYEE')")
    public ResponseEntity<DesiredTimeResponse> addDesiredTime(@Valid @RequestBody DesiredTimeRequest desiredTimeRequest) {
        log.debug("method was invoked");
        return new ResponseEntity<>(desiredTimeService.createDesiredTime(desiredTimeRequest), HttpStatus.OK);
    }

    @PatchMapping
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('MANAGER') or hasAuthority('EMPLOYEE')")
    public ResponseEntity<MessageResponse> editDesiredTime(@Valid @RequestBody DesiredTimeUpdateRequest desiredTimeUpdateRequest) {
        log.debug("method was invoked");
        desiredTimeService.updateDesiredTime(desiredTimeUpdateRequest);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<List<DesiredTimeResponse>> getAll() {
        log.debug("method was invoked");
        return new ResponseEntity<>(desiredTimeService.getAll(), HttpStatus.OK);
    }

    @GetMapping("/{userId}")
    public ResponseEntity<List<DesiredTimeResponse>> getUserDesiredTime(@Valid @PathVariable Integer userId) {
        log.debug("method was invoked");
        return new ResponseEntity<>(desiredTimeService.getUsersDesiredTime(userId), HttpStatus.OK);
    }

    @GetMapping(path = "/courses/{courseId}")
    public ResponseEntity<List<DesiredTimeResponse>> getAllDesiredTimeForCourse(@PathVariable("courseId") Integer courseId) {
        log.debug("method was invoked");
        return ResponseEntity.ok().body(desiredTimeService.getAllDesiredTimeForCourse(courseId));
    }

    @DeleteMapping(path = "/{desiredTimeId}")
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('EMPLOYEE')")
    public ResponseEntity<MessageResponse> deleteUserDesiredTime(@Valid @PathVariable("desiredTimeId") Integer desiredTimeId) {
        log.debug("method was invoked");
        desiredTimeService.deleteDesiredTime(desiredTimeId);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
