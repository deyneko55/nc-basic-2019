package edu.netcracker.backend.controller;

import edu.netcracker.backend.message.request.auth.SignUpByLinkRequest;
import edu.netcracker.backend.message.request.auth.SignUpRequest;
import edu.netcracker.backend.message.request.user.*;
import edu.netcracker.backend.message.response.*;
import edu.netcracker.backend.model.*;
import edu.netcracker.backend.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@Slf4j
@RequestMapping("/api/users")
@CrossOrigin(origins = "http://localhost:4200")
public class UserController {

    private AuthenticationService authenticationService;
    private UserService userService;
    private EmailService emailService;
    private ManagerService managerService;
    private TrainerService trainerService;
    private FeedbackService feedbackService;

    @Autowired
    public UserController(AuthenticationService authenticationService, UserService userService, EmailService emailService,
                          ManagerService managerService, TrainerService trainerService, FeedbackService feedbackService) {
        this.authenticationService = authenticationService;
        this.userService = userService;
        this.emailService = emailService;
        this.managerService = managerService;
        this.trainerService = trainerService;
        this.feedbackService = feedbackService;
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<UserResponse> getUser(@PathVariable("id") Integer userId) {
        log.debug("method was invoked");
        return new ResponseEntity<>(userService.getUserResponse(userId), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}/groups")
    public ResponseEntity<List<Group>> getUserGroups(@PathVariable("id") Integer userId) {
        log.debug("method was invoked");
        return new ResponseEntity<>(userService.getUserGroups(userId), HttpStatus.OK);
    }

    @PostMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<UserResponse> createUser(@Valid @RequestBody SignUpRequest signUpRequest) {
        log.debug("method was invoked");
        return new ResponseEntity<>(userService.createUser(signUpRequest), HttpStatus.CREATED);
    }

    @PatchMapping
    @PreAuthorize("hasAuthority('EMPLOYEE') or hasAuthority('ADMIN') or hasAuthority('MANAGER') or hasAuthority('TRAINER')")
    public ResponseEntity<UserResponse> editUser(@Valid @RequestBody UserEditRequest userEditRequest) {
        log.debug("method was invoked");
        userService.updateUser(userEditRequest);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PatchMapping(path = "/new-status")
    @PreAuthorize("hasAuthority('EMPLOYEE') or hasAuthority('ADMIN') or hasAuthority('MANAGER') or hasAuthority('TRAINER')")
    public ResponseEntity<MessageResponse> changeUserStatus(@Valid @RequestBody UserChangeStatusRequest userChangeStatusRequest) {
        log.debug("method was invoked");
        userService.changeUserStatus(userChangeStatusRequest);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping
    @PreAuthorize("hasAuthority('EMPLOYEE') or hasAuthority('ADMIN') or hasAuthority('MANAGER') or hasAuthority('TRAINER')")
    public ResponseEntity<MessageResponse> changePassword(@Valid @RequestBody ChangePasswordRequest changePasswordRequest) {
        log.debug("method was invoked");
        return ResponseEntity.ok().body(authenticationService.changePassword(changePasswordRequest));
    }

    @GetMapping
    @PreAuthorize("hasAuthority('EMPLOYEE') or hasAuthority('ADMIN') or hasAuthority('MANAGER') or hasAuthority('TRAINER')")
    public ResponseEntity<List<UserResponse>> getAllUsers() {
        log.debug("method was invoked");
        return new ResponseEntity<>(userService.getAllUsers(), HttpStatus.OK);
    }

    @PostMapping(path = "/search")
    public ResponseEntity<List<UserResponse>> searchUsers(@RequestBody UserSearchRequest userSearchRequest) {
        log.debug("method was invoked");
        return new ResponseEntity<>(userService.getByUsernameOrRole(userSearchRequest.getUsername(),
                userSearchRequest.getRoleIds()), HttpStatus.OK);
    }

    @PostMapping(path = "/registration")
    public ResponseEntity<UserResponse> signUpByRegistrationLink(@Valid @RequestBody SignUpByLinkRequest signUpRequest) {
        log.debug("method was invoked");
        UserResponse userResponse = userService.signUpByRegistrationLink(signUpRequest);
        userService.deleteRegistrationToken(signUpRequest.getRegistrationToken());
        return new ResponseEntity<>(userResponse, HttpStatus.OK);
    }

    @GetMapping("{userId}/absent-reason")
    public ResponseEntity<MessageResponse> sendAbsentReasonEmail(@PathVariable Integer userId,
                                                                 @RequestBody UserAbsentReason userAbsentReason) {
        log.debug("method was invoked");
        userService.specifyAbsentReason(userAbsentReason.getLessonId(), userId, userAbsentReason.getReason());
        emailService.sendAbsentReasonMessage(userService.getUsersSuperiorsEmails(userId), userAbsentReason.getReason());
        return new ResponseEntity<>(HttpStatus.OK);
    }


    @GetMapping("/{userId}/courses-and-groups")
    public ResponseEntity<List<UserGroupsAndCoursesResponse>> getUsersGroupsAndCourses(@PathVariable Integer userId) {
        log.debug("method was invoked");
        return new ResponseEntity<>(userService.getUsersGroupsAndCourses(userId), HttpStatus.OK);
    }

    @GetMapping("/passed-courses/{userId}")
    public ResponseEntity<List<UserCompletedCoursesResponse>> getUsersCompletedCourses(@PathVariable Integer userId) {
        log.debug("method was invoked");
        return new ResponseEntity<>(userService.getUsersCompletedCourses(userId), HttpStatus.OK);
    }

    @GetMapping("/manager/{managerId}/subordinates")
    public ResponseEntity<List<UserStatistic>> getUserStatistic(@PathVariable Integer managerId) {
        log.debug("method was invoked");
        return new ResponseEntity<>(managerService.getUserStatistic(managerId), HttpStatus.OK);
    }

    @GetMapping("/{userId}/chats")
    public ResponseEntity<List<Chat>> getUserChats(@PathVariable(value = "userId") Integer userId) {
        log.debug("method was invoked");
        List<Chat> userChats = userService.getUserChats(userId);
        return new ResponseEntity<>(userChats, HttpStatus.OK);
    }

    @DeleteMapping(path = "/{userId}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<MessageResponse> deleteUser(@PathVariable("userId") Integer userId) {
        log.debug("method was invoked");
        userService.deleteUserFromDB(userId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/lessons/{courseId}/{userId}")
    @PreAuthorize("hasAuthority('MANAGER') or hasAuthority('EMPLOYEE')")
    public ResponseEntity<List<UserLesson>> getUsersLessons(@PathVariable Integer userId,
                                                            @PathVariable Integer courseId) {
        log.debug("Method was invoked");
        return ResponseEntity.ok(userService.getUsersLessons(userId, courseId));
    }

    @GetMapping(path = "{userId}/trainers")
    public ResponseEntity<List<TrainerInfo>> getUserTrainers(@PathVariable Integer userId) {
        log.debug("Method was invoked");
        return ResponseEntity.ok().body(trainerService.getUserTrainer(userId));
    }

    @GetMapping("/attendance/{groupId}/{userId}")
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('TRAINER')")
    public ResponseEntity<List<AttendanceDTO>> getUserAttendance(@PathVariable Integer groupId,
                                                                 @PathVariable Integer userId) {
        log.debug("Method was invoked");
        return new ResponseEntity<>(userService.getUserAttendance(userId, groupId), HttpStatus.OK);
    }

    @GetMapping(path = "/{userId}/feedback")
    @PreAuthorize("hasAuthority('TRAINER') or hasAuthority('ADMIN') or hasAuthority('MANAGER')")
    public ResponseEntity<List<FeedbackAboutUser>> getFeedbackAboutUser(@PathVariable Integer userId) {
        log.debug("Method was invoked");
        return ResponseEntity.ok(feedbackService.getFeedbackAboutUser(userId));
    }
}