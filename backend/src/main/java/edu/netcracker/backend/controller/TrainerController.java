package edu.netcracker.backend.controller;

import edu.netcracker.backend.message.request.feedback.FeedbackFormRequest;
import edu.netcracker.backend.message.request.trainer.TrainerNewQualificationLevelRequest;
import edu.netcracker.backend.message.request.trainer.TrainerSetQualRequest;
import edu.netcracker.backend.message.response.*;
import edu.netcracker.backend.model.*;
import edu.netcracker.backend.service.FeedbackService;
import edu.netcracker.backend.service.TrainerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.util.List;

@RestController
@Slf4j
@RequestMapping("/api/trainers")
@CrossOrigin(origins = "http://localhost:4200")
public class TrainerController {

    private TrainerService trainerService;
    private FeedbackService feedbackService;

    @Autowired
    public TrainerController(TrainerService trainerService, FeedbackService feedbackService) {
        this.trainerService = trainerService;
        this.feedbackService = feedbackService;
    }


    @GetMapping(path = "/{trainerId}")
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('TRAINER')")
    public ResponseEntity<List<UserResponse>> getAllTrainerUsers(@PathVariable("trainerId") Integer trainerId) {
        log.debug("method was invoked");
        return new ResponseEntity<>(trainerService.getAllTrainerUsers(trainerId), HttpStatus.OK);
    }

    @GetMapping(path = "{trainerId}/info")
    public ResponseEntity<TrainerInfo> getTrainerById(@PathVariable Integer trainerId) {
        log.debug("method was invoked");
        return ResponseEntity.ok(trainerService.getTrainerById(trainerId));
    }

    @GetMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<List<UserResponse>> getAllTrainers() {
        log.debug("method was invoked");
        return ResponseEntity.ok().body(trainerService.getAllTrainers());
    }

    @PostMapping(path = "/new-feedback")
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('TRAINER')")
    public ResponseEntity<FeedbackResponse> writeFeedbackAboutUser(@Valid @RequestBody FeedbackFormRequest feedbackFormRequest) {
        log.debug("method was invoked");
        return new ResponseEntity<>(feedbackService.createFeedback(feedbackFormRequest), HttpStatus.CREATED);
    }

    @GetMapping(path = "/search/{trainerLastName}")
    public ResponseEntity<List<UserResponse>> getTrainerByLastName(@PathVariable("trainerLastName") @Size(max = 30) String lastName) {
        log.debug("method was invoked");
        return ResponseEntity.ok().body(trainerService.getTrainerByLastName(lastName));
    }

    @GetMapping(path = "/levels")
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('TRAINER')")
    public ResponseEntity<List<TrainerQualificationLevel>> getQualificationLevels() {
        log.debug("method was invoked");
        return new ResponseEntity<>(trainerService.getAllQualificationLevels(), HttpStatus.OK);
    }

    @PutMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<TrainerQualification> setQualificationLevel(@Valid @RequestBody TrainerSetQualRequest setQualRequest) {
        log.debug("method was invoked");
        return new ResponseEntity<>(trainerService.setTrainerQualification(setQualRequest), HttpStatus.OK);
    }

    @PatchMapping(path = "/updatequalification")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<TrainerQualification> updateQualificationLevel(@Valid @RequestBody TrainerSetQualRequest setQualRequest) {
        log.debug("method was invoked");
        return new ResponseEntity<>(trainerService.updateQualification(setQualRequest), HttpStatus.OK);
    }

    @GetMapping("/show-levels")
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('TRAINER')")
    public ResponseEntity<List<Trainer>> getTrainersWithLevels() {
        log.debug("method was invoked");
        return new ResponseEntity<>(trainerService.getTrainersWithLevels(), HttpStatus.OK);
    }

    @GetMapping("/{trainerId}/groups")
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('TRAINER')")
    public ResponseEntity<List<GroupResponse>> getTrainerGroups(@PathVariable Integer trainerId) {
        log.debug("method was invoked");
        return ResponseEntity.ok(trainerService.getTrainerGroups(trainerId));
    }

    @GetMapping("/{trainerId}/courses")
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('TRAINER')")
    public ResponseEntity<List<CourseDTO>> getTrainerCourses(@PathVariable Integer trainerId) {
        log.debug("method was invoked");
        return ResponseEntity.ok(trainerService.getTrainerCourses(trainerId));
    }

    @PostMapping("/new-level")
    public ResponseEntity<MessageResponse> addQualificationLevel(@RequestBody TrainerNewQualificationLevelRequest trainerNewQualificationLevelRequest) {
        log.debug("method was invoked");
        trainerService.createNewQualificationLevel(trainerNewQualificationLevelRequest.getQualificationLevel());
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
