package edu.netcracker.backend.controller;

import edu.netcracker.backend.message.request.manager.ManagerSetUserRequest;
import edu.netcracker.backend.message.response.ManagerResponse;
import edu.netcracker.backend.message.response.MessageResponse;
import edu.netcracker.backend.message.response.UserResponse;
import edu.netcracker.backend.service.ManagerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@RestController
@Slf4j
@RequestMapping("/api/managers")
@CrossOrigin(origins = "http://localhost:4200")
public class ManagerController {

    private ManagerService managerService;

    @Autowired
    public ManagerController(ManagerService managerService) {
        this.managerService = managerService;
    }

    @PostMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<MessageResponse> setManager(@Valid @RequestBody ManagerSetUserRequest managerSetUserRequest) {
        log.debug("method was invoked");
        managerService.setManager(managerSetUserRequest);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/{managerId}")
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('MANAGER')")
    public ResponseEntity<List<UserResponse>> getAllUsers(@PathVariable("managerId") @NotNull Integer managerId) {
        log.debug("method was invoked");
        return new ResponseEntity<>(managerService.getAllUsers(managerId), HttpStatus.OK);
    }

    @GetMapping("/search/{userId}")
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('MANAGER') or hasAuthority('EMPLOYEE')")
    public ResponseEntity<ManagerResponse> getUserManager(@PathVariable("userId") @NotNull Integer userId) {
        log.debug("method was invoked");
        return new ResponseEntity<>(new ManagerResponse(managerService.getUserManagerId(userId)), HttpStatus.OK);
    }

    @GetMapping(path = "/search/lastname/{managerLastName}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<List<UserResponse>> getManagersByLastName(
            @PathVariable("managerLastName") @NotBlank @Size(max = 30) String lastName) {
        log.debug("method was invoked");
        return ResponseEntity.ok().body(managerService.getManagerByLastName(lastName));
    }

    @GetMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<List<UserResponse>> getAllManagers() {
        log.debug("method was invoked");
        return ResponseEntity.ok().body(managerService.getAllManagers());
    }
}
