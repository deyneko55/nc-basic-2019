package edu.netcracker.backend.controller;

import edu.netcracker.backend.message.request.auth.EmailRequest;
import edu.netcracker.backend.message.request.auth.SignInRequest;
import edu.netcracker.backend.message.response.JwtResponse;
import edu.netcracker.backend.service.AuthenticationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@Slf4j
@RequestMapping("/api/auth")
@CrossOrigin(origins = "http://localhost:4200")
public class AuthController {

    private AuthenticationService authenticationService;

    @Autowired
    public AuthController(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @PostMapping(path = "/sign-in")
    public ResponseEntity<JwtResponse> signIn(@Valid @RequestBody SignInRequest signInRequest) {
        log.debug("method was invoked");
        return ResponseEntity.ok(authenticationService.signIn(signInRequest));
    }

    @PostMapping(path = "/password-recovery")
    public EmailRequest passwordRecovery(@Valid @RequestBody EmailRequest emailRequest) {
        log.debug("method was invoked");
        authenticationService.passwordRecovery(emailRequest);
        return emailRequest;
    }
}
