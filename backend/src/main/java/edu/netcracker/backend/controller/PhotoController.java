package edu.netcracker.backend.controller;

import edu.netcracker.backend.message.response.MessageResponse;
import edu.netcracker.backend.model.Photo;
import edu.netcracker.backend.service.PhotoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

@RestController
@Slf4j
@RequestMapping("/api/photos")
@CrossOrigin(origins = "http://localhost:4200")
public class PhotoController {

    private PhotoService photoService;

    @Autowired
    public PhotoController(PhotoService photoService) {
        this.photoService = photoService;
    }

    @GetMapping(path = "/{userId}")
    @ResponseBody
    public ResponseEntity<Photo> getPhoto(@PathVariable("userId") @NotNull Integer userId) {
        log.debug("method was invoked");
        return new ResponseEntity<>(photoService.getPhoto(userId), HttpStatus.OK);
    }

    @DeleteMapping(path = "/{userId}")
    @ResponseBody
    public ResponseEntity<MessageResponse> deletePhoto(@PathVariable("userId") Integer userId) {
        log.debug("method was invoked");
        photoService.deletePhoto(userId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(value = "/{userId}/upload")
    @ResponseBody
    public ResponseEntity<MessageResponse> uploadPhoto(@RequestParam(value = "photo") MultipartFile photo, @PathVariable Integer userId) {
        log.debug("method was invoked");
        photoService.uploadPhoto(userId, photo);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
