package edu.netcracker.backend.message.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CourseDifficultyLevelResponse {

    private Integer courseDifficultyLevelId;
}
