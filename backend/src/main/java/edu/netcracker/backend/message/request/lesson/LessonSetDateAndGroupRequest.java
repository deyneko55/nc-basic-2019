package edu.netcracker.backend.message.request.lesson;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LessonSetDateAndGroupRequest {

    @NotNull
    private Integer lessonId;

    @NotNull
    private Integer groupId;

    @NotBlank
    private String lessonDate;
}
