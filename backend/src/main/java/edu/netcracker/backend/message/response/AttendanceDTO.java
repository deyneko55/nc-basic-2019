package edu.netcracker.backend.message.response;


import edu.netcracker.backend.model.Attendance;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Setter
@Getter
@NoArgsConstructor
public class AttendanceDTO {

    private static final String DATE_PATTERN = "yyyy-MM-dd HH:mm";

    private Integer groupId;
    private String lessonName;
    private String lessonDate;
    private Integer userId;
    private String userFullName;
    private String userPresenceStatus;

    public AttendanceDTO(Attendance attendance) {
        groupId = attendance.getGroupId();
        lessonName = attendance.getLessonName();
        lessonDate = convertToString(attendance.getLessonDate());
        userId = attendance.getUserId();
        userFullName = attendance.getUserFullName();
    }

    public static String convertToString(LocalDateTime date) {
        return date.format(DateTimeFormatter.ofPattern(DATE_PATTERN));
    }
}
