package edu.netcracker.backend.message.request.trainer;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * @author Dmytro Vovk
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TrainerSetQualRequest {

    @NotNull
    private Integer userId;

    @NotNull
    private Integer qualificationId;

}
