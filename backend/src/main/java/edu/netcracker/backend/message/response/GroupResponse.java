package edu.netcracker.backend.message.response;

import edu.netcracker.backend.model.Group;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author Dmytro Vovk
 */

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class GroupResponse {

    public GroupResponse(Group group) {
        this.id = group.getId();
        this.nameGroup = group.getName();
        this.courseId = group.getCourseId();
    }

    @NotNull
    private Integer id;

    @NotBlank
    private String nameGroup;

    @NotNull
    private Integer courseId;
}
