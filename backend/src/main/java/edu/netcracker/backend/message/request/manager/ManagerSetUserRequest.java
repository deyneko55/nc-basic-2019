package edu.netcracker.backend.message.request.manager;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ManagerSetUserRequest {

    @NotNull
    private Integer managerId;

    @NotNull
    private Integer userId;
}
