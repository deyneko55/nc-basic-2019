package edu.netcracker.backend.message.request.lesson;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LessonFormRequest {

    @NotBlank
    @Size(min = 5, max = 15)
    private String name;

    @NotNull
    private Integer courseId;
}
