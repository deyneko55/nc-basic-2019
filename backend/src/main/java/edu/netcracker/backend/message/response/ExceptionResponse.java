package edu.netcracker.backend.message.response;

import edu.netcracker.backend.exception.RequestException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ExceptionResponse {

    private int status;
    private String error;
    private String message;
    private long timestamp;

    public static ExceptionResponse createExceptionResponse(RequestException exception) {
        return new ExceptionResponse(exception.getHttpStatus().value(),
                exception.getHttpStatus().name(),
                exception.getMessage(),
                System.currentTimeMillis());
    }
}
