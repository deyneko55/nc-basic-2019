package edu.netcracker.backend.message.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class UserCompletedCoursesResponse {

    private Integer courseId;
}
