package edu.netcracker.backend.message.request.group;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * @author Dmytro Vovk
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GroupDeleteRequest {
    @NotNull
    private Integer groupId;
}
