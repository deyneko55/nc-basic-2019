package edu.netcracker.backend.message.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

@Getter
@Setter
@AllArgsConstructor
public class JwtResponse {

    private String accessToken;

    private String type;

    private Integer userId;

    private Collection<GrantedAuthority> userRoles;
}
