package edu.netcracker.backend.message.request.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * @author Dmytro Vovk
 */

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class UserSearchRequest {
    private String username;
    private List<Long> roleIds;
}
