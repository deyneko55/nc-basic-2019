package edu.netcracker.backend.message.request.news;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class NewsCreationRequest {

    private String header;

    private String description;

    private String content;
}
