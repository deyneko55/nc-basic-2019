package edu.netcracker.backend.message.response;

import edu.netcracker.backend.model.Feedback;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FeedbackResponse {

    @NotNull
    private Integer feedbackId;

    @NotBlank
    private String content;

    @NotNull
    private Integer teacherId;

    @NotNull
    private Integer userId;

    public FeedbackResponse(Feedback feedback) {
        this.feedbackId = feedback.getFeedbackId();
        this.content = feedback.getContent();
        this.teacherId = feedback.getTrainerId();
        this.userId = feedback.getUserId();
    }
}
