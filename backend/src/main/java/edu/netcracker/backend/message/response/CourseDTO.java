package edu.netcracker.backend.message.response;

import edu.netcracker.backend.model.Course;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Getter
@Setter
@NoArgsConstructor
public class CourseDTO {

    private static final String datePattern = "yyyy-MM-dd";

    private Integer courseId;

    private String courseName;

    private String courseType;

    private String startDate;

    private String endDate;

    private String difficultyLevel;

    public CourseDTO(Course course) {
        this.courseId = course.getId();
        this.courseName = course.getCourseName();
        this.courseType = course.getCourseType();
        this.startDate = convertToString(course.getStartDate());
        this.endDate = convertToString(course.getEndDate());
        this.difficultyLevel = course.getDifficultyLevel();
    }

    public static LocalDate convertToLocalDate(String date) {
        return LocalDate.parse(date, DateTimeFormatter.ofPattern(datePattern));
    }

    public static String convertToString(LocalDate date) {
        return date.format(DateTimeFormatter.ofPattern(datePattern));
    }
}
