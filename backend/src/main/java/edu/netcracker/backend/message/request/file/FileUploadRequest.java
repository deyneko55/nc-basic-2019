package edu.netcracker.backend.message.request.file;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FileUploadRequest {

    @NotEmpty
    private MultipartFile[] files;

    private Integer lessonId;
}
