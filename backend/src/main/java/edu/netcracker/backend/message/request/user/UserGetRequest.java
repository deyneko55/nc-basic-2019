package edu.netcracker.backend.message.request.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * @author Dmytro Vovk
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserGetRequest {

    @NotBlank
    @Size(min = 3, max = 24)
    private String username;
}
