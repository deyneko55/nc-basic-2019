package edu.netcracker.backend.message.request.chat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author Dmytro Vovk
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ChatSendMessageRequest {

    @NotNull
    private Integer userId;

    @NotNull
    private Integer chatId;

    @NotBlank
    private String content;

    @NotBlank
    private String username;
}
