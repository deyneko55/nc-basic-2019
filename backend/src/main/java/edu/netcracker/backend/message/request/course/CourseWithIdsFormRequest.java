package edu.netcracker.backend.message.request.course;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CourseWithIdsFormRequest {

    @NotBlank
    private String courseName;

    @NotBlank
    private String startDate;

    @NotBlank
    private String endDate;

    @NotNull
    private Integer courseTypeId;

    @NotNull
    private Integer difficultyLevelId;
}
