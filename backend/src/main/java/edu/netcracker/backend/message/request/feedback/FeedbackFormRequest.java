package edu.netcracker.backend.message.request.feedback;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FeedbackFormRequest {

    @NotBlank
    private String content;

    @NotNull
    private Integer trainerId;

    @NotNull
    private Integer userId;
}
