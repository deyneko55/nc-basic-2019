package edu.netcracker.backend.message.request.group;

import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Getter
@NoArgsConstructor
public class GroupSetTrainerRequest {

    @NotNull
    private Integer trainerId;

    @NotNull
    private Integer groupId;
}
