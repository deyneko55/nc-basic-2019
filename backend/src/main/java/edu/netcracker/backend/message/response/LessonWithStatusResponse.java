package edu.netcracker.backend.message.response;

import edu.netcracker.backend.model.LessonWithStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LessonWithStatusResponse {

    private static final String datePattern = "yyyy-MM-dd HH:mm";

    private Integer id;
    private Integer courseId;
    private String lessonName;
    private String lessonDate;
    private String lessonStatus;

    public LessonWithStatusResponse(LessonWithStatus lesson) {
        this.id = lesson.getId();
        this.courseId = lesson.getCourseId();
        this.lessonName = lesson.getLessonName();
        this.lessonDate = convertToString(lesson.getLessonDate());
        this.lessonStatus = lesson.getLessonStatus();
    }

    public static String convertToString(LocalDateTime dateTime) {
        return dateTime.format(DateTimeFormatter.ofPattern(datePattern));
    }
}
