package edu.netcracker.backend.message.request.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserSetToGroupRequest {

    @NotNull
    private Integer userId;

    @NotNull
    private Integer groupId;
}
