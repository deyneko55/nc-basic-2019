package edu.netcracker.backend.message.request.auth;

import lombok.Getter;

import javax.validation.constraints.NotBlank;

@Getter
public class SignUpByLinkRequest extends SignUpRequest {

    @NotBlank
    private String registrationToken;
}
