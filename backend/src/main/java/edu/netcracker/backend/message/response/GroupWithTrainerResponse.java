package edu.netcracker.backend.message.response;

import edu.netcracker.backend.model.GroupWithTrainer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class GroupWithTrainerResponse extends GroupResponse {

    @NotNull
    private Integer trainerId;

    @NotBlank
    private String trainerName;

    public GroupWithTrainerResponse(GroupWithTrainer group) {
        super(group.getId(), group.getName(), group.getCourseId());
        this.trainerId = group.getTrainerId();
        this.trainerName = group.getFirstName() + " " + group.getLastName();
    }
}
