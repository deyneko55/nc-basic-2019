package edu.netcracker.backend.message.request.absencereason;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AbsenceReasonFormRequest {

    @NotBlank
    @Size(max = 30)
    private String absenceReasonName;
}
