package edu.netcracker.backend.message.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Dmytro Vovk
 */

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class CourseTypeStatsResponse {
    private String difficultyLevel;
    private Integer entitiesCount;
}
