package edu.netcracker.backend.message.request.chat;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author Dmytro Vovk
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ChatPrivateFormRequest {
    @NotNull
    private Integer senderId;

    @NotNull
    private Integer recipientId;

    @NotBlank
    private String chatName;
}
