package edu.netcracker.backend.message.request.news;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class NewsSetLiked {

    private Integer newsId;
    private Integer userId;
    private Boolean isLiked;
}
