package edu.netcracker.backend.message.request.desiredtime;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DesiredTimeRequest {

    @NotNull
    private Integer userId;

    @NotBlank
    private String userSchedule;
}
