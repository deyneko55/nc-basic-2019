package edu.netcracker.backend.message.response;


import edu.netcracker.backend.model.AbsenceReason;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AbsenceReasonResponse {

    @NotNull
    private Integer absenceReasonId;

    @NotBlank
    private String absenceReasonName;

    public AbsenceReasonResponse(AbsenceReason absenceReason) {
        this.absenceReasonId = absenceReason.getAbsenceReasonId();
        this.absenceReasonName = absenceReason.getAbsenceReasonName();
    }
}
