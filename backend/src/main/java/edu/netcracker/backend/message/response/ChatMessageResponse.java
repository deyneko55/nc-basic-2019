package edu.netcracker.backend.message.response;

import edu.netcracker.backend.model.Message;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.format.DateTimeFormatter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ChatMessageResponse {

    @NotNull
    private Integer messageId;

    @NotBlank
    private String messageSender;

    @NotBlank
    private String createDate;

    @NotNull
    private Integer userId;

    @NotNull
    private Integer chatId;

    @NotBlank
    private String content;

    @NotNull
    private Boolean status;

    public ChatMessageResponse(Message message) {
        this.messageId = message.getMessageId();
        this.messageSender = message.getMessageSender();
        this.createDate = message.getCreateDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH-mm"));
        this.userId = message.getUserId();
        this.chatId = message.getChatId();
        this.content = message.getContent();
        this.status = message.getStatus();
    }
}
