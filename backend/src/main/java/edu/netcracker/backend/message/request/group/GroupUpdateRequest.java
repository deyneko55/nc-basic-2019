package edu.netcracker.backend.message.request.group;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Dmytro Vovk
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GroupUpdateRequest {

    @NotNull
    private Integer groupId;

    @NotBlank
    @Size(min = 3, max = 15)
    private String name;

    @NotNull
    private Integer courseId;
}
