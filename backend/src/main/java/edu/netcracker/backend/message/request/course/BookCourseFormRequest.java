package edu.netcracker.backend.message.request.course;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BookCourseFormRequest {

    private Integer userId;

    private Integer courseId;
}
