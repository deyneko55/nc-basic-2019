package edu.netcracker.backend.message.request.group;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

/**
 * @author Dmytro Vovk
 */

@Getter
@Setter
public class GroupSearchRequest {
    @NotBlank
    private String groupName;
}
