package edu.netcracker.backend.message.request.course;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Dmytro Vovk
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CourseJoinRequest {
    private Integer userId;
    private Integer courseId;
}
