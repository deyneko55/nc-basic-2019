package edu.netcracker.backend.message.response;

import edu.netcracker.backend.model.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Dmytro Vovk
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserResponse {
    private Integer userId;
    private String firstName;
    private String lastName;
    private String username;
    private String email;
    private Integer roleId;

    public UserResponse(User user) {
        this.userId = user.getId();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.username = user.getLogin();
        this.email = user.getEmail();
        this.roleId = user.getRoleId();
    }
}
