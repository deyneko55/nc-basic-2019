package edu.netcracker.backend.message.request.lesson;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LessonUserStatus {

    @NotNull
    private Integer userId;

    @NotNull
    private Integer lessonId;

    @NotNull
    private Integer presenceStatusId;

    private Integer absenceReasonId;

    private String explanation;
}
