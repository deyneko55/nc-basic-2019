package edu.netcracker.backend.message.request.news;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * @author Dmytro Vovk
 */

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class NewsDeleteRequest {
    @NotNull
    private Integer newsPostId;
}
