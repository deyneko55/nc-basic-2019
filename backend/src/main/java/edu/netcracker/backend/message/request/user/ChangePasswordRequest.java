package edu.netcracker.backend.message.request.user;

import edu.netcracker.backend.message.validation.FieldMatch;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@FieldMatch(baseField = "newPassword", matchField = "matchNewPassword", message = "The password fields must match")
public class ChangePasswordRequest {

    @NotBlank
    @Size(min = 6, max = 100)
    private String oldPassword;

    @NotBlank
    @Size(min = 6, max = 30)
    private String newPassword;

    @NotBlank
    @Size(min = 6, max = 30)
    private String matchNewPassword;
}
