package edu.netcracker.backend.message.response;

import edu.netcracker.backend.model.Lesson;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LessonCreationResponse {

    private Integer id;
    private String lessonName;
    private Integer courseId;


    public LessonCreationResponse(Lesson lesson) {
        this.id = lesson.getId();
        this.courseId = lesson.getCourseId();
        this.lessonName = lesson.getLessonName();
    }
}
