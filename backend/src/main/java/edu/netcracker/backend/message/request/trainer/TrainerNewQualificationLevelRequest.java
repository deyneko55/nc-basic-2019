package edu.netcracker.backend.message.request.trainer;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TrainerNewQualificationLevelRequest {

    private String qualificationLevel;
}
