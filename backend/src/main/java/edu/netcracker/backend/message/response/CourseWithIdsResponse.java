package edu.netcracker.backend.message.response;

import edu.netcracker.backend.model.CourseWithIds;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CourseWithIdsResponse {

    @NotNull
    private Integer id;

    @NotNull
    private LocalDate startDate;

    @NotNull
    private LocalDate endDate;

    @NotBlank
    private String courseName;

    @NotNull
    private Integer courseTypeId;

    @NotNull
    private Integer difficultyLevelId;

    public CourseWithIdsResponse(CourseWithIds course) {
        this.id = course.getId();
        this.startDate = course.getStartDate();
        this.endDate = course.getEndDate();
        this.courseName = course.getCourseName();
        this.courseTypeId = course.getCourseTypeId();
        this.difficultyLevelId = course.getDifficultyLevelId();
    }
}
