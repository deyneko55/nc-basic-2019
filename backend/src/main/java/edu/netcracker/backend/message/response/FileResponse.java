package edu.netcracker.backend.message.response;

import edu.netcracker.backend.model.File;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FileResponse {

    private Integer fileId;
    private String fileName;

    public FileResponse(File file) {
        this.fileId = file.getFileId();
        this.fileName = file.getFileName();
    }
}
