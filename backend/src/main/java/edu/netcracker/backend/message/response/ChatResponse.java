package edu.netcracker.backend.message.response;

import edu.netcracker.backend.model.Chat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ChatResponse {

    @NotNull
    private Integer chatId;

    @NotBlank
    private String chatName;

    public ChatResponse(Chat chat) {
        this.chatId = chat.getChatId();
        this.chatName = chat.getChatName();
    }
}
