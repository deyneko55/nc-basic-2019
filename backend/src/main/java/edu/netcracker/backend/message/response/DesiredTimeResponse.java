package edu.netcracker.backend.message.response;

import edu.netcracker.backend.model.DesiredTime;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DesiredTimeResponse {

    @NotNull
    private Integer id;

    @NotNull
    private Integer userId;

    @NotNull
    private LocalDateTime userSchedule;

    public DesiredTimeResponse(DesiredTime desiredTime) {
        this.id = desiredTime.getDesiredTimeId();
        this.userId = desiredTime.getUserId();
        this.userSchedule = desiredTime.getUserSchedule();
    }


}
