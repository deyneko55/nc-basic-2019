package edu.netcracker.backend.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@EqualsAndHashCode
public class Attendance {

    @EqualsAndHashCode.Include
    private Integer groupId;
    private String lessonName;
    private LocalDateTime lessonDate;
    private Integer userId;
    private String userFullName;
    private String userPresenceStatus;
    private String absenceReason;
}
