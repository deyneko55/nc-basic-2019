package edu.netcracker.backend.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TrainerInfo {

    private Integer userId;
    private String login;
    private String email;
    private String firstName;
    private String lastName;
    private Integer roleId;
    private byte[] avatar;
    private String qualificationName;
}
