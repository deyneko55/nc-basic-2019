package edu.netcracker.backend.model;

import lombok.*;

/**
 * @author Dmytro Vovk
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class Chat {
    private Integer chatId;
    private String chatName;

}
