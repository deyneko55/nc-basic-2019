package edu.netcracker.backend.model;

import edu.netcracker.backend.message.request.chat.ChatSendMessageRequest;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Message {

    private Integer messageId;
    private String messageSender;
    private LocalDateTime createDate;
    private Integer userId;
    private Integer chatId;
    private String content;
    private Boolean status;

    public Message(Integer userId, Integer chatId, String content) {
        this.userId = userId;
        this.chatId = chatId;
        this.content = content;
    }

    public Message(ChatSendMessageRequest request) {
        this.userId = request.getUserId();
        this.chatId = request.getChatId();
        this.content = request.getContent();
        this.status = false;
        this.messageSender = request.getUsername();
    }
}
