package edu.netcracker.backend.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GroupWithTrainer extends Group {

    private Integer trainerId;

    private String firstName;

    private String lastName;

    public GroupWithTrainer(Integer id, String name, Integer courseId) {
        super(id, name, courseId);
    }
}
