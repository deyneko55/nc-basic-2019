package edu.netcracker.backend.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserStatistic {

    private Integer userId;
    private String firstName;
    private String lastName;
    private String username;
    private Integer visitedLessons;
    private Integer missedLessons;

    public String toString() {
        return "userId: " + userId + ", firstName:" + firstName + ", lastName: " + lastName + ", username: "
        + username + ", visitedLessons: " + visitedLessons + ", missedLessons: " + missedLessons;
    }
}
