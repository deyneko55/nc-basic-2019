package edu.netcracker.backend.model;

import lombok.*;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
@AllArgsConstructor
public class CourseWithIds {

    @EqualsAndHashCode.Include
    private Integer id;
    private LocalDate startDate;
    private LocalDate endDate;
    private String courseName;
    private Integer courseTypeId;
    private Integer difficultyLevelId;

    public CourseWithIds(LocalDate startDate, LocalDate endDate, String courseName, Integer courseTypeId, Integer difficultyLevelId) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.courseName = courseName;
        this.courseTypeId = courseTypeId;
        this.difficultyLevelId = difficultyLevelId;
    }
}
