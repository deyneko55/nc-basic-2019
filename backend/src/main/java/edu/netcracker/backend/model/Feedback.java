package edu.netcracker.backend.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Feedback {

    private Integer feedbackId;

    private String content;

    private Integer trainerId;

    private Integer userId;

    public Feedback(String content, Integer trainerId, Integer userId) {
        this.content = content;
        this.trainerId = trainerId;
        this.userId = userId;
    }
}
