package edu.netcracker.backend.model;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class LessonWithStatus {

    @EqualsAndHashCode.Include
    private Integer id;
    private Integer courseId;
    private Integer groupId;
    private String lessonName;
    private LocalDateTime lessonDate;
    private String lessonStatus;

}
