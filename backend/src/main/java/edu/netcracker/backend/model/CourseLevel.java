package edu.netcracker.backend.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CourseLevel {

    private Integer courseLevelId;
    private String courseLevel;
}
