package edu.netcracker.backend.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Dmytro Vovk
 */

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Notification {
    private Integer notificationId;
    private Integer recipientId;
    private String content;
    private Boolean isRead;

    public Notification(Integer recipientId, String content, Boolean isRead) {
        this.recipientId = recipientId;
        this.content = content;
        this.isRead = isRead;
    }
}
