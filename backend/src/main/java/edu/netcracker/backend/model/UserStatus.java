package edu.netcracker.backend.model;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class UserStatus {

    @EqualsAndHashCode.Include
    private Integer userId;
    private Integer lessonId;
    private Integer presenceStatusId;
    private String presenceStatus;
    private Integer absenceReasonId;
    private String explanation;
}