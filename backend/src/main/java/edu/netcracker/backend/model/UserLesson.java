package edu.netcracker.backend.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserLesson {

    private Integer lessonId;
    private String lessonName;
    private LocalDateTime lessonDate;
    private String lessonStatus;
    private String userStatus;
}

