package edu.netcracker.backend.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FeedbackAboutUser {

    private String content;

    private String groupName;

    private String courseName;

    private String fullName;
}
