package edu.netcracker.backend.model;

import edu.netcracker.backend.message.request.group.GroupFormRequest;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Group {


    private Integer id;
    private String name;
    private Integer courseId;

    public Group(String name, Integer courseId) {
        this.name = name;
        this.courseId = courseId;
    }

    public Group(GroupFormRequest groupFormRequest) {
        this.name = groupFormRequest.getNameGroup();
        this.courseId = groupFormRequest.getCourseId();
    }
}
