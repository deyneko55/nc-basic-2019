package edu.netcracker.backend.model;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * @author Dmytro Vovk
 */


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class NewsPost {

    private Integer newsPostId;

    @NotBlank
    @Size(min = 1, max = 30)
    private String header;

    @NotBlank
    @Size(min = 1, max = 100)
    private String description;

    private String content;

    private byte[] photo;

    private Integer numberOfLikes;

    private Integer numberOfViews;

    private Boolean likedByCurrentUser;
}
