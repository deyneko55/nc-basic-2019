package edu.netcracker.backend.model;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class Lesson {

    @EqualsAndHashCode.Include
    private Integer id;
    private Integer courseId;
    private Integer groupId;
    private String lessonName;
    private LocalDateTime lessonDate;


    public Lesson(Integer id, Integer groupId, String lessonName, LocalDateTime lessonDate) {
        this.id = id;
        this.groupId = groupId;
        this.lessonName = lessonName;
        this.lessonDate = lessonDate;
    }

    public Lesson(Integer courseId, String lessonName) {
        this.courseId = courseId;
        this.lessonName = lessonName;
    }

    @Override
    public String toString() {
        return "lessonId:" + id + ", courseId:" + courseId + ", groupId=" + groupId + ", lessonName='" + lessonName
                + ", lessonDate:" + lessonDate;
    }
}
