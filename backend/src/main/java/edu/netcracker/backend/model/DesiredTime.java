package edu.netcracker.backend.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DesiredTime {

    private Integer desiredTimeId;
    private Integer userId;
    private LocalDateTime userSchedule;

    public DesiredTime(Integer userId, LocalDateTime userSchedule) {
        this.userId = userId;
        this.userSchedule = userSchedule;
    }
}
