package edu.netcracker.backend.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class Course {

    @EqualsAndHashCode.Include
    private Integer id;
    private LocalDate startDate;
    private LocalDate endDate;
    private String courseName;
    private String courseType;
    private String difficultyLevel;

    public Course(Integer id, LocalDate startDate, LocalDate endDate, String courseName, String courseType, String difficultyLevel) {
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.courseName = courseName;
        this.courseType = courseType;
        this.difficultyLevel = difficultyLevel;
    }
}
