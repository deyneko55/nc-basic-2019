package edu.netcracker.backend.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class GroupStatistic {

    private Integer groupId;
    private String groupName;
    private String difficultyLevel;
    private Integer trainerId;
    private double attendancePercentage;
}
