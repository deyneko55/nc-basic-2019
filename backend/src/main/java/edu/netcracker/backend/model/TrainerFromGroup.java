package edu.netcracker.backend.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TrainerFromGroup {

    @NotNull
    private Integer trainerId;

    @NotNull
    private Integer groupId;

}
