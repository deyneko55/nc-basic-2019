package edu.netcracker.backend.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * @author Dmytro Vovk
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TrainerQualification {
    @NotBlank
    private Integer userId;

    @NotBlank
    @Size(min = 1, max = 5)
    private Integer qualificationId;
}
