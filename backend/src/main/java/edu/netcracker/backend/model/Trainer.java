package edu.netcracker.backend.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author Dmytro Vovk
 */


@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Trainer {
    @NotNull
    private Integer userId;

    @NotBlank
    private String qualificationName;
}
