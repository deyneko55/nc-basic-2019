package edu.netcracker.backend.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AbsenceReason {

    private Integer absenceReasonId;

    private String absenceReasonName;

    public AbsenceReason(String absenceReasonName) {
        this.absenceReasonName = absenceReasonName;
    }
}
