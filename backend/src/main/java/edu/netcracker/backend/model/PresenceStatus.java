package edu.netcracker.backend.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PresenceStatus {

    private Integer presenceStatusId;
    private String presenceStatusName;
}
