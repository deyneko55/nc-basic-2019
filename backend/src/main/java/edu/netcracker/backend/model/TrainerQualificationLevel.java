package edu.netcracker.backend.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author Dmytro Vovk
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TrainerQualificationLevel {
    @NotNull
    private Integer qualificationId;

    @NotBlank
    private String qualificationName;
}
