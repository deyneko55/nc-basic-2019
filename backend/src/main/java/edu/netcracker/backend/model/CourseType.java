package edu.netcracker.backend.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CourseType {

    private Integer courseTypeId;
    private String courseType;
}
