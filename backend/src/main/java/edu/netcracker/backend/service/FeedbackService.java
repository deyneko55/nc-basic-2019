package edu.netcracker.backend.service;

import edu.netcracker.backend.message.request.feedback.FeedbackFormRequest;
import edu.netcracker.backend.message.response.FeedbackResponse;
import edu.netcracker.backend.model.FeedbackAboutUser;

import java.util.List;

public interface FeedbackService {

    FeedbackResponse createFeedback(FeedbackFormRequest feedbackFormRequest);

    List<FeedbackAboutUser> getFeedbackAboutUser(Integer userId);
}
