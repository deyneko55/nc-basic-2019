package edu.netcracker.backend.service.impl;

import edu.netcracker.backend.dao.DesiredTimeDAO;
import edu.netcracker.backend.message.request.desiredtime.DesiredTimeRequest;
import edu.netcracker.backend.message.request.desiredtime.DesiredTimeUpdateRequest;
import edu.netcracker.backend.message.response.DesiredTimeResponse;
import edu.netcracker.backend.model.DesiredTime;
import edu.netcracker.backend.service.DesiredTimeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class DesiredTimeServiceImpl implements DesiredTimeService {

    @Autowired
    private DesiredTimeDAO desiredTimeDAO;

    @Override
    public DesiredTimeResponse createDesiredTime(DesiredTimeRequest desiredTimeRequest) {
        log.debug("creating desired time for user");
        String date = desiredTimeRequest.getUserSchedule().replace("T", " ");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        DesiredTime desiredTime = new DesiredTime();
        desiredTime.setUserId(desiredTimeRequest.getUserId());
        desiredTime.setUserSchedule(LocalDateTime.parse(date, formatter));
        return new DesiredTimeResponse(desiredTimeDAO.create(desiredTime));
    }

    @Override
    public void updateDesiredTime(DesiredTimeUpdateRequest desiredTimeUpdateRequest) {
        log.debug("method was invoked");
        String date = desiredTimeUpdateRequest.getUserSchedule().replace("T", " ");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        desiredTimeDAO.findById(desiredTimeUpdateRequest.getId()).ifPresent(time -> {
            desiredTimeDAO.update(new DesiredTime(desiredTimeUpdateRequest.getId(),
                    desiredTimeUpdateRequest.getUserId(), LocalDateTime.parse(date, formatter)));
        });
    }

    @Override
    public void deleteDesiredTime(Integer desiredTimeId) {
        log.debug("method was invoked");
        desiredTimeDAO.delete(desiredTimeId);
    }

    @Override
    public List<DesiredTimeResponse> getAll() {
        log.debug("method was invoked");
        List<DesiredTime> timeList = desiredTimeDAO.getAll();
        List<DesiredTimeResponse> desiredTimeResponseList = new ArrayList<>();
        timeList.forEach((time) -> desiredTimeResponseList.add(new DesiredTimeResponse(time)));
        return desiredTimeResponseList;
    }

    @Override
    public List<DesiredTimeResponse> getUsersDesiredTime(Integer userId) {
        log.debug("Method was invoked");
        List<DesiredTime> userDesiredTime = desiredTimeDAO.findUserDesiredTime(userId);
        List<DesiredTimeResponse> desiredTimeResponseList = new ArrayList<>();
        userDesiredTime.forEach((time) -> desiredTimeResponseList.add(new DesiredTimeResponse(time)));
        return desiredTimeResponseList;
    }

    @Override
    public List<DesiredTimeResponse> getAllDesiredTimeForCourse(Integer courseId) {
        log.debug("Method was invoked");
        List<DesiredTime> desiredTimes = desiredTimeDAO.getAllDesiredTimeForUser(courseId);
        List<DesiredTimeResponse> responses = new ArrayList<>();
        desiredTimes.forEach(desiredTime -> responses.add(new DesiredTimeResponse(desiredTime)));
        return responses;
    }
}
