package edu.netcracker.backend.service.impl;

import edu.netcracker.backend.dao.CourseDAO;
import edu.netcracker.backend.exception.RequestException;
import edu.netcracker.backend.message.request.course.CourseRequest;
import edu.netcracker.backend.message.request.course.CourseWithIdsFormRequest;
import edu.netcracker.backend.message.response.*;
import edu.netcracker.backend.model.*;
import edu.netcracker.backend.service.CourseService;
import edu.netcracker.backend.service.NotificationService;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.util.Lists;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class CourseServiceImpl implements CourseService {

    private final CourseDAO courseDAO;
    private final ModelMapper courseMapper;
    private final NotificationService notificationService;

    @Autowired
    public CourseServiceImpl(CourseDAO courseDAO, ModelMapper courseMapper, NotificationService notificationService) {
        this.courseDAO = courseDAO;
        this.courseMapper = courseMapper;
        this.notificationService = notificationService;
    }

    @Override
    public CourseDTO createCourse(CourseRequest course) {
        log.debug("Method was invoked");
        return convertToDTO(courseDAO.create(convertFromDTO(course)));
    }

    @Override
    public List<CourseDTO> getAllCourses() {
        log.debug("Method was invoked");
        List<Course> courses = courseDAO.getAll();

        if (courses.isEmpty()) {
            log.warn("Courses not found");
            throw new RequestException("No courses yet", HttpStatus.NOT_FOUND);
        }
        return courses.stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());
    }

    @Override
    public List<CourseDTO> getActiveCourses(LocalDate currentDate) {
        log.debug("Getting active courses");
        List<Course> courses = courseDAO.getAllCoursesInProgress(currentDate);

        if (courses.isEmpty()) {
            log.warn("Courses haven't been found with the current date");
            throw new RequestException("No courses with the current date", HttpStatus.NOT_FOUND);
        }
        return courses.stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());
    }

    @Override
    public void editCourse(CourseRequest course) {
        log.debug("Method was invoked");

        if (ifCourseExists(course.getCourseId())) {
            log.warn("No such course with id {}", course.getCourseId());
            throw new RequestException("Course does not exist", HttpStatus.NOT_FOUND);
        }
        courseDAO.update(convertFromDTO(course));
    }

    @Override
    public void deleteCourse(Integer courseId) {
        log.debug("Method was invoked");

        if (ifCourseExists(courseId)) {
            log.warn("No such course with userId {}", courseId);
            throw new RequestException("Course does not exist", HttpStatus.NOT_FOUND);
        }
        courseDAO.delete(courseId);
    }

    @Override
    public List<CourseDTO> findCourseByName(String name) {
        log.debug("Method was invoked");
        List<Course> courses = courseDAO.findCourseByName(name);

        if (courses.isEmpty()) {
            log.warn("Courses not found");
            throw new RequestException("No courses yet", HttpStatus.NOT_FOUND);
        }
        return courses.stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());
    }

    @Override
    public List<CourseDTO> getCoursesByUserId(Integer userId) {
        log.debug("Method was invoked");
        List<Course> courses = courseDAO.getCoursesByUserId(userId);

        if (courses.isEmpty()) {
            log.warn("Courses not found");
            throw new RequestException("No courses", HttpStatus.NOT_FOUND);
        }

        return courses.stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());
    }

    @Override
    public CourseDTO findById(Integer courseId) {
        log.debug("Method was invoked");
        return convertToDTO(courseDAO.findById(courseId).orElseThrow(() ->
                new RequestException("Course not found", HttpStatus.NOT_FOUND)));
    }

    private boolean ifCourseExists(Integer courseId) {
        log.debug("Checking if course exists");
        return !courseDAO.findById(courseId).isPresent();
    }

    @Override
    public List<CourseType> getAllCourseTypes() {
        log.debug("Method was invoked");
        List<CourseType> courseTypes = courseDAO.getAllCourseTypes();

        if (courseTypes.isEmpty()) {
            log.warn("Courses not found");
            throw new RequestException("No courses", HttpStatus.NOT_FOUND);
        }
        return courseTypes;
    }

    @Override
    public List<CourseLevel> getAllLevelsOfCourse() {
        log.debug("Method was invoked");
        return courseDAO.getAllLevelsOfCourse();
    }

    @Override
    public List<GroupResponse> getAllGroupsFromCourse(Integer courseId) {
        log.debug("Getting all groups from course");
        List<Group> allGroupsFromCourse = courseDAO.getAllGroupsFromCourse(courseId);
        List<GroupResponse> responses = new ArrayList<>();
        allGroupsFromCourse.forEach(group -> responses.add(new GroupResponse(group)));
        return responses;
    }

    @Override
    public List<CourseTypeStatsResponse> getUsersByCourseType(Integer courseTypeId) {
        log.debug("Getting users by course type");
        return courseDAO.getUsersByCourseType(courseTypeId);
    }

    @Override
    public List<CourseTypeStatsResponse> getGroupsByCourseType(Integer courseTypeId) {
        log.debug("Method was invoked");
        return courseDAO.getGroupsByCourseType(courseTypeId);
    }

    @Override
    public List<UserResponse> getAllUsersFromCourse(Integer courseId) {
        log.debug("Getting all users from course");
        List<User> allUsersFromCourse = courseDAO.getAllUsersFromCourse(courseId);
        List<UserResponse> responses = Lists.newArrayList();
        allUsersFromCourse.forEach(user -> responses.add(new UserResponse(user)));
        return responses;
    }

    @Override
    public void bookCourse(Integer userId, Integer courseId) {
        log.debug("Booking course");
        courseDAO.bookCourse(userId, courseId);
        notificationService.sendNotificationToUser(userId, "You were taken to the course");
    }

    @Override
    public List<UserResponse> getAllUsersFromDesiredCourse(Integer courseId) {
        log.debug("Method was invoked");
        List<User> allUsersFromDesiredCourse = courseDAO.getAllUsersFromDesiredCourse(courseId);
        List<UserResponse> responses = Lists.newArrayList();
        allUsersFromDesiredCourse.forEach(user -> responses.add(new UserResponse(user)));
        return responses;
    }

    @Override
    public List<CourseLevel> getLevelsOfCourseType(Integer courseTypeId) {
        log.debug("Method was invoked");

        if (!ifCourseTypeExists(courseTypeId)) {
            log.error("No such course type with id {}", courseTypeId);
            throw new RequestException("Course type does not exist", HttpStatus.NOT_FOUND);
        }
        return courseDAO.getLevelsOfCourseType(courseTypeId);
    }

    private boolean ifCourseTypeExists(Integer courseTypeId) {
        log.debug("Checking if course type exists");
        return courseDAO.findByCourseTypeId(courseTypeId).isPresent();
    }

    @Override
    public CourseDTO addNewCourseByIds(CourseWithIdsFormRequest course) {
        log.debug("Method was invoked");
        LocalDate startDate = Timestamp.valueOf(course.getStartDate()
                .replace("T", " ")
                .replace("Z", ""))
                .toLocalDateTime()
                .toLocalDate();
        LocalDate endDate = Timestamp.valueOf(course.getEndDate()
                .replace("T", " ")
                .replace("Z", ""))
                .toLocalDateTime()
                .toLocalDate();

        CourseWithIds courseWithIds = new CourseWithIds(startDate, endDate,
                course.getCourseName(), course.getCourseTypeId(), course.getDifficultyLevelId());
        return convertToDTO(courseDAO.addNewCourseByIds(courseWithIds));
    }

    @Override
    public CourseDifficultyLevelResponse createDifficultyLevel(String nameOfLevel) {
        CourseDifficultyLevelResponse courseDifficultyLevelResponse = new CourseDifficultyLevelResponse();
        int courseDifficultyType = courseDAO.createDifficultyLevel(nameOfLevel);
        courseDifficultyLevelResponse.setCourseDifficultyLevelId(courseDifficultyType);
        return courseDifficultyLevelResponse;
    }

    @Override
    public CourseTypeResponse createCourseType(String typeName) {
        CourseTypeResponse courseTypeResponse = new CourseTypeResponse();
        int courseTypeId = courseDAO.createCourseType(typeName);
        courseTypeResponse.setCourseTypeId(courseTypeId);
        return courseTypeResponse;
    }

    @Override
    public List<LessonCreationResponse> getAllLessonsFromCourse(Integer courseId) {
        log.debug("Getting all lessons of course");
        return courseDAO.getAllLessonsFromCourse(courseId);
    }

    @Override
    public List<GroupWithTrainerResponse> getGroupsWithTrainersFromCourse(Integer courseId) {
        List<GroupWithTrainerResponse> responses = new ArrayList<>();
        List<GroupWithTrainer> allGroupsWithTrainersFromCourse = courseDAO.getAllGroupsWithTrainersFromCourse(courseId);
        allGroupsWithTrainersFromCourse.forEach(groupWithTrainer -> responses.add(
                new GroupWithTrainerResponse(groupWithTrainer)));
        return responses;
    }

    private CourseDTO convertToDTO(Course course) {
        log.debug("Converting course to dto");
        CourseDTO courseDTO = courseMapper.map(course, CourseDTO.class);
        courseDTO.setStartDate(CourseDTO.convertToString(course.getStartDate()));
        courseDTO.setEndDate(CourseDTO.convertToString(course.getEndDate()));
        return courseDTO;
    }

    private Course convertFromDTO(CourseRequest courseRequest) {
        log.debug("Converting dto to course");
        Course course = courseMapper.map(courseRequest, Course.class);
        course.setStartDate(CourseDTO.convertToLocalDate(courseRequest.getStartDate()));
        course.setEndDate(CourseDTO.convertToLocalDate(courseRequest.getEndDate()));
        return course;
    }
}
