package edu.netcracker.backend.service.impl;

import edu.netcracker.backend.dao.UserDAO;
import edu.netcracker.backend.exception.RequestException;
import edu.netcracker.backend.message.request.user.ChangePasswordRequest;
import edu.netcracker.backend.message.request.auth.EmailRequest;
import edu.netcracker.backend.message.request.auth.SignInRequest;
import edu.netcracker.backend.message.response.JwtResponse;
import edu.netcracker.backend.message.response.MessageResponse;
import edu.netcracker.backend.model.User;
import edu.netcracker.backend.security.*;
import edu.netcracker.backend.security.user.UserPrincipal;
import edu.netcracker.backend.service.AuthenticationService;
import edu.netcracker.backend.service.EmailService;
import edu.netcracker.backend.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class AuthenticationServiceImpl implements AuthenticationService {

    private UserService userService;
    private EmailService emailService;
    private AuthenticationManager authenticationManager;
    private JwtTokenProvider jwtProvider;
    private UserDAO userDAO;

    @Autowired
    public AuthenticationServiceImpl(UserService userService, EmailService emailService, UserDAO userDAO,
                                     AuthenticationManager authenticationManager,
                                     JwtTokenProvider jwtProvider) {
        this.userService = userService;
        this.emailService = emailService;
        this.userDAO = userDAO;
        this.authenticationManager = authenticationManager;
        this.jwtProvider = jwtProvider;
    }

    @Override
    public JwtResponse signIn(SignInRequest signInRequest) {
        log.debug("Method was invoked");
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                signInRequest.getUsername(), signInRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        UserPrincipal userPrincipal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        log.debug("User {} is signed in the app", userPrincipal.getUsername());

        String accessToken = jwtProvider.generateAccessToken(userPrincipal.getUsername(), userPrincipal.getRole());

        return new JwtResponse(accessToken,
                "Bearer",
                userPrincipal.getId(),
                userPrincipal.getAuthorities());
    }

    @Override
    public MessageResponse changePassword(ChangePasswordRequest changePasswordRequest) {
        log.debug("Method was invoked");
        UserDetails userPrincipal = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        userDAO.findByUsername(userPrincipal.getUsername()).ifPresent(foundUser -> {
            userService.changePasswordForUser(foundUser, changePasswordRequest);
        });
        log.debug("Password for user: {} was changed", userPrincipal.getUsername());
        return new MessageResponse(HttpStatus.OK, "Password was changed");
    }

    @Override
    public void passwordRecovery(EmailRequest emailRequest) {
        log.debug("Method was invoked");
        User user = userService.findByEmail(emailRequest.getEmail());

        if (user == null) {
            log.error("User with email {} not found", emailRequest.getEmail());
            throw new RequestException("User not found", HttpStatus.NOT_FOUND);
        }

        String newPassword = userService.generatePasswordForUser(user);
        emailService.sendPasswordRecoveryMessage(emailRequest.getEmail(), user.getLogin(), newPassword);

        log.debug("Email with appropriate password was sent to email address {}", emailRequest.getEmail());
    }
}
