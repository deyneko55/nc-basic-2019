package edu.netcracker.backend.service.impl;

import edu.netcracker.backend.dao.ManagerDAO;
import edu.netcracker.backend.exception.RequestException;
import edu.netcracker.backend.message.request.manager.ManagerSetUserRequest;
import edu.netcracker.backend.message.response.UserResponse;
import edu.netcracker.backend.model.User;
import edu.netcracker.backend.model.UserStatistic;
import edu.netcracker.backend.service.ManagerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class ManagerServiceImpl implements ManagerService {

    @Autowired
    private ManagerDAO managerDAO;

    @Override
    public void setManager(ManagerSetUserRequest managerSetUserRequest) {
        log.debug("Method was invoked");
        Integer managerId = managerSetUserRequest.getManagerId();
        Integer userId = managerSetUserRequest.getUserId();

        if (!managerId.equals(userId)) {
            managerDAO.setManager(managerSetUserRequest.getManagerId(), managerSetUserRequest.getUserId());
        } else {
            log.error("Manager cannot be manager for himself");
            throw new RequestException("Manager cannot be manager for himself", HttpStatus.CONFLICT);
        }
    }

    @Override
    public List<UserResponse> getAllUsers(Integer managerId) {
        log.debug("Method was invoked");
        List<User> allUsers = managerDAO.getAllUsers(managerId);
        List<UserResponse> responses = new ArrayList<>();
        allUsers.forEach(user -> responses.add(new UserResponse(user)));
        return responses;
    }

    @Override
    public List<UserStatistic> getUserStatistic(Integer managerId) {
        log.debug("Method was invoked");
        return managerDAO.getUserStatistic(managerId);
    }

    @Override
    public Integer getUserManagerId(Integer userId) {
        log.debug("Method was invoked");
        return managerDAO.getUserManager(userId);
    }

    @Override
    public List<UserResponse> getManagerByLastName(String lastName) {
        log.debug("Method was invoked");
        List<User> managerByLastName = managerDAO.getManagerByLastName(lastName);
        List<UserResponse> responses = new ArrayList<>();

        if (managerByLastName.isEmpty()) {
            log.error("There are no managers with such surname");
            throw new RequestException("There ara no managers with such surname", HttpStatus.NOT_FOUND);
        }
        managerByLastName.forEach((manager) -> responses.add(new UserResponse(manager)));
        return responses;
    }

    @Override
    public List<UserResponse> getAllManagers() {
        log.debug("Method was invoked");
        List<User> allManagers = managerDAO.getAll();
        List<UserResponse> responses = new ArrayList<>();

        if (allManagers.isEmpty()) {
            log.error("There are no managers");
            throw new RequestException("There are no managers ", HttpStatus.NOT_FOUND);
        }
        allManagers.forEach((manager) -> responses.add(new UserResponse(manager)));
        return responses;
    }
}
