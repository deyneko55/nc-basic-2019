package edu.netcracker.backend.service.impl;

import edu.netcracker.backend.dao.MailTokenDAO;
import edu.netcracker.backend.model.Mail;
import edu.netcracker.backend.model.User;
import edu.netcracker.backend.service.EmailService;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@PropertySource("classpath:mail.properties")
@Slf4j
public class EmailServiceImpl implements EmailService {

    private JavaMailSender emailSender;

    @Autowired
    private MailTokenDAO mailTokenDAO;

    @Qualifier("getFreeMarkerConfiguration")
    @Autowired
    private Configuration freemarkerConfig;

    @Autowired
    public EmailServiceImpl(JavaMailSender emailSender) {
        this.emailSender = emailSender;
    }

    @Value("${mail.registration.subject}")
    private String registrationSubject;

    @Value("${mail.registration.text}")
    private String registrationText;

    @Value("${mail.registration.endPointUri}")
    private String registrationEndPointUri;

    @Value("${mail.source.url}")
    private String sourceUrl;

    @Value("${mail.password-recovery.subject}")
    private String passwordRecoverySubject;

    @Value("${mail.password-recovery.text}")
    private String passwordRecoveryText;

    @Value("${mail.lesson-cancellation}")
    private String lessonCancellationText;

    @Override
    public void sendRegistrationMessage(String to, String token) {
        log.debug("sending registration message");

        String registrationLink = String.format("%s%s?token=%s", sourceUrl, registrationEndPointUri, token);

        sendMessage(createMail(to, getSubject(registrationSubject, sourceUrl),
                getRegistrationText(registrationText, sourceUrl, registrationEndPointUri, token), registrationLink));

        mailTokenDAO.create(token);
    }

    @Override
    public void sendPasswordRecoveryMessage(String to, String username, String password) {
        log.debug("sending password recovery message");

        sendMessage(createMail(to, getSubject(passwordRecoverySubject, username),
                getPasswordRecoveryText(passwordRecoveryText, username, password), "https://LinkOnTrainingPortalSite.com"));
    }

    @Override
    public void sendLessonCancellationMessage(User trainer, String to, String lessonName, LocalDateTime dateTime) {
        log.debug("sending lesson cancellation message");

        sendMessage(createMail(to, "Lesson Cancellation", "Lesson \"" + lessonName + "\" "
                + lessonCancellationText + dateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))
                + "\nBest regards, " + trainer.getFirstName() + " " + trainer.getLastName(), ""));
    }

    @Override
    public void sendAbsentReasonMessage(List<String> to, String reason) {
        log.debug("sending absent reason message");

        for (String email : to) {
            sendMessage(createMail(email, "Student's Absence Reason", reason, ""));
        }
    }

    @Override
    public void sendUserMissedLessonMessage(String to) {
        log.debug("sending message with asking why user missed the lesson");
        sendMessage(createMail(to, "Missed Lesson",
                "You have missed a lesson. Please specify your absence reason.", ""));
    }

    @Override
    public void sendTrainerNewGroupMessage(String to, String groupName) {
        log.debug("method was invoked");
        sendMessage(createMail(to, "New group creation",
                "You are now trainer of " + groupName + " group. Have a nice work!", ""));
    }

    @Override
    public void sendManagerNewFeedbackMessage(String to, String text, User user) {
        log.debug("method was invoked");
        if (to != null) {
            sendMessage(createMail(to, "New Feedback on user: " + user.getFirstName() + " " + user.getLastName()
                    + " (username " + user.getLogin() + ").", text, ""));
        }
    }

    @Override
    public void sendManagerAbsenceWithoutReasonMessage(String to, User user) {
        log.debug("method was invoked");
        if (to != null) {
            sendMessage(createMail(to, "User's absence", "Your employee " + user.getFirstName() + " " + user.getLastName()
                    + " (username " + user.getLogin() + ") has missed the lesson without an important reason", ""));
        }
    }

    private void sendMessage(Mail mail) {
        log.debug("method was invoked");
        try {
            MimeMessage message = emailSender.createMimeMessage();
            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                    StandardCharsets.UTF_8.name());
            mimeMessageHelper.addAttachment("attachementFilename.png", new ClassPathResource("attachedFile.png"));

            freemarkerConfig.setClassForTemplateLoading(this.getClass(), "/templates");
            Template template = null;
            String html = null;
            try {
                template = freemarkerConfig.getTemplate("email-template.ftl");
                html = FreeMarkerTemplateUtils.processTemplateIntoString(template, mail.getModel());
            } catch (IOException e) {
                log.error("Email template not found");
            } catch (TemplateException e) {
                log.error("Can't process template into string");
            }

            mimeMessageHelper.setTo(mail.getTo());
            mimeMessageHelper.setText(html, true);
            mimeMessageHelper.setSubject(mail.getSubject());
            mimeMessageHelper.setFrom(mail.getFrom());

            emailSender.send(message);
            log.debug("Email was sent to email address {}", mail.getTo());
        } catch (MessagingException e) {
            log.error(String.format("Email was not sent. %s", e));
        }
    }

    private String getSubject(String subject, String line) {
        return String.format("%s : %s", subject, line);
    }

    private String getPasswordRecoveryText(String text, String username, String password) {
        return String.format("%s : %s. Your password: %s", text, username, password);
    }

    private String getRegistrationText(String text, String url, String endPointUri, String token) {
        return String.format("%s : %s?token=%s", text, url.concat(endPointUri), token);
    }

    private Mail createMail(String to, String subject, String recipientText, String link) {
        Mail mail = new Mail();
        mail.setFrom("ncbasictrainingcenter@gmail.com");
        mail.setTo(to);
        mail.setSubject(subject);
        Map<String, Object> model = new HashMap<>();
        model.put("name", "Recipient");
        model.put("text", recipientText);
        model.put("location", "Kyiv");
        model.put("signature", link);
        mail.setModel(model);
        return mail;
    }
}
