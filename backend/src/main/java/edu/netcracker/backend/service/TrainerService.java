package edu.netcracker.backend.service;

import edu.netcracker.backend.message.request.trainer.TrainerSetQualRequest;
import edu.netcracker.backend.message.response.CourseDTO;
import edu.netcracker.backend.message.response.GroupResponse;
import edu.netcracker.backend.message.response.UserResponse;
import edu.netcracker.backend.model.*;

import java.util.List;

public interface TrainerService {

    List<UserResponse> getAllTrainerUsers(Integer trainerId);

    List<UserResponse> getAllTrainersFromCourse(Integer courseId);

    List<UserResponse> getAllTrainers();

    List<UserResponse> getTrainerByLastName(String lastName);

    List<TrainerQualificationLevel> getAllQualificationLevels();

    TrainerQualification setTrainerQualification(TrainerSetQualRequest trainerSetQualRequest);

    TrainerQualification updateQualification(TrainerSetQualRequest trainerSetQualRequest);

    List<Trainer> getTrainersWithLevels();

    List<GroupResponse> getTrainerGroups(Integer trainerId);

    List<CourseDTO> getTrainerCourses(Integer trainerId);

    void createNewQualificationLevel(String newQualificationLevel);

    List<TrainerInfo> getUserTrainer(Integer userId);

    TrainerInfo getTrainerById(Integer trainerId);
}
