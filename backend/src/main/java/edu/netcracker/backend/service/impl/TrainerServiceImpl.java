package edu.netcracker.backend.service.impl;

import edu.netcracker.backend.dao.TrainerDAO;
import edu.netcracker.backend.exception.RequestException;
import edu.netcracker.backend.message.request.trainer.TrainerSetQualRequest;
import edu.netcracker.backend.message.response.CourseDTO;
import edu.netcracker.backend.message.response.GroupResponse;
import edu.netcracker.backend.message.response.UserResponse;
import edu.netcracker.backend.model.*;
import edu.netcracker.backend.service.TrainerService;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.util.Lists;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class TrainerServiceImpl implements TrainerService {

    private final TrainerDAO trainerDAO;
    private final ModelMapper courseMapper;


    @Autowired
    public TrainerServiceImpl(TrainerDAO trainerDAO, ModelMapper courseMapper) {
        this.trainerDAO = trainerDAO;
        this.courseMapper = courseMapper;
    }

    @Override
    public List<UserResponse> getAllTrainerUsers(Integer trainerId) {
        log.debug("Getting all trainer's students");
        List<User> students = trainerDAO.getAllTrainerStudents(trainerId);
        List<UserResponse> responses = new ArrayList<>();
        students.forEach(student -> responses.add(new UserResponse(student)));
        return responses;
    }

    @Override
    public List<UserResponse> getAllTrainersFromCourse(Integer courseId) {
        log.debug("Getting all trainers of course");
        List<User> teachers = trainerDAO.getAllTrainerFromCourse(courseId);
        List<UserResponse> responses = new ArrayList<>();
        teachers.forEach(teacher -> responses.add(new UserResponse(teacher)));
        return responses;
    }

    @Override
    public List<UserResponse> getAllTrainers() {
        log.debug("Getting all trainers");
        List<User> trainers = trainerDAO.getAll();
        List<UserResponse> responses = new ArrayList<>();
        trainers.forEach(trainer -> responses.add(new UserResponse(trainer)));
        return responses;
    }

    @Override
    public List<UserResponse> getTrainerByLastName(String lastName) {
        log.debug("Getting trainer info by his surname");
        List<User> trainerByLastName = trainerDAO.getTrainerByLastName(lastName);

        if (trainerByLastName.isEmpty()) {
            log.error("Trainer not found");
            throw new RequestException("Trainer not found ", HttpStatus.NOT_FOUND);
        } else {
            List<UserResponse> responses = Lists.newArrayList();
            trainerByLastName.forEach(trainer -> responses.add(new UserResponse(trainer)));
            return responses;
        }
    }

    public List<TrainerQualificationLevel> getAllQualificationLevels() {
        log.debug("Method was invoked");
        return trainerDAO.getAllQualificationLevels();
    }

    @Override
    public TrainerQualification setTrainerQualification(TrainerSetQualRequest trainerSetQualRequest) {
        log.debug("Method was invoked");
        if (trainerDAO.setTrainerQualification(trainerSetQualRequest) == 1) {
            return new TrainerQualification(trainerSetQualRequest.getUserId(), trainerSetQualRequest.getQualificationId());
        } else {
            log.error("Trainer not found");
            throw new RequestException("Trainer not found", HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public TrainerQualification updateQualification(TrainerSetQualRequest trainerSetQualRequest) {
        log.debug("Method was invoked");
        if (trainerDAO.updateQualification(trainerSetQualRequest) == 1) {
            return new TrainerQualification(trainerSetQualRequest.getUserId(), trainerSetQualRequest.getQualificationId());
        } else {
            log.error("Trainer not found");
            throw new RequestException("Trainer not found", HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public List<Trainer> getTrainersWithLevels() {
        log.debug("Method was invoked");
        return trainerDAO.getTrainersWithLevels();
    }

    @Override
    public List<GroupResponse> getTrainerGroups(Integer trainerId) {
        log.debug("Method was invoked");
        List<Group> trainerGroups = trainerDAO.getTrainerGroups(trainerId);

        if (trainerGroups.isEmpty()) {
            log.error("No group assigned for trainer id {}", trainerId);
            throw new RequestException("Groups not found", HttpStatus.NOT_FOUND);
        }
       return trainerGroups.stream()
                .map(GroupResponse::new).collect(Collectors.toList());
    }

    @Override
    public List<CourseDTO> getTrainerCourses(Integer trainerId) {
        log.debug("Method was invoked");
        List<Course> trainerCourses = trainerDAO.getTrainerCourses(trainerId);
        return trainerCourses.stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());
    }

    @Override
    public void createNewQualificationLevel(String newQualificationLevel) {
        log.debug("Method was invoked");
        trainerDAO.createQualificationLevel(newQualificationLevel);
    }

    @Override
    public List<TrainerInfo> getUserTrainer(Integer userId) {
        return trainerDAO.getUserTrainers(userId);
    }

    @Override
    public TrainerInfo getTrainerById(Integer trainerId) {
        return trainerDAO.getAllInfoAboutTrainer(trainerId)
                .orElseThrow(() -> new RequestException("Trainer not found", HttpStatus.NOT_FOUND));
    }

    private CourseDTO convertToDTO(Course course) {
        log.debug("Converting course to course dto");
        CourseDTO courseDTO = courseMapper.map(course, CourseDTO.class);
        courseDTO.setStartDate(CourseDTO.convertToString(course.getStartDate()));
        courseDTO.setEndDate(CourseDTO.convertToString(course.getEndDate()));
        return courseDTO;
    }
}
