package edu.netcracker.backend.service.impl;

import edu.netcracker.backend.dao.AbsenceReasonDAO;
import edu.netcracker.backend.message.response.AbsenceReasonResponse;
import edu.netcracker.backend.model.AbsenceReason;
import edu.netcracker.backend.service.AbsenceReasonService;
import org.apache.commons.compress.utils.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AbsenceReasonServiceImpl implements AbsenceReasonService {

    private AbsenceReasonDAO absenceReasonDAO;

    @Autowired
    public AbsenceReasonServiceImpl(AbsenceReasonDAO absenceReasonDAO) {
        this.absenceReasonDAO = absenceReasonDAO;
    }

    @Override
    public List<AbsenceReasonResponse> getAll() {
        List<AbsenceReason> all = absenceReasonDAO.getAll();
        List<AbsenceReasonResponse> reasonResponses = Lists.newArrayList();

        all.forEach(absenceReason -> reasonResponses.add(new AbsenceReasonResponse(absenceReason)));
        return reasonResponses;
    }
}
