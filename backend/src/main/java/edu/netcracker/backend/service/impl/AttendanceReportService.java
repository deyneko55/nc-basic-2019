package edu.netcracker.backend.service.impl;

import edu.netcracker.backend.dao.CourseDAO;
import edu.netcracker.backend.dao.GroupDAO;
import edu.netcracker.backend.dao.LessonDAO;
import edu.netcracker.backend.exception.RequestException;
import edu.netcracker.backend.model.*;
import edu.netcracker.backend.utils.ExcelBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.view.document.AbstractXlsxView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Slf4j
public class AttendanceReportService extends AbstractXlsxView {

    private static final int TITLE_ROW_COUNT = 1;
    private static final int TITLE_ROW_NUMBER = 0;
    private static final int FIRST_CELL = 0;

    private CourseDAO courseDAO;
    private LessonDAO lessonDAO;
    private GroupDAO groupDAO;

    @Autowired
    public AttendanceReportService(CourseDAO courseDAO, LessonDAO lessonDAO, GroupDAO groupDAO) {
        this.courseDAO = courseDAO;
        this.lessonDAO = lessonDAO;
        this.groupDAO = groupDAO;
    }

    @Override
    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook,
                                      HttpServletRequest request, HttpServletResponse response) {
        log.debug("Building excel document");

        if (model.containsKey("courseId")) {
            log.debug("Building excel document for course");
            Integer courseId = (Integer) model.get("courseId");
            List<Group> groups = courseDAO.getAllGroupsFromCourse(courseId);

            createBody(workbook, groups);
            setFilenameForCourse(response, courseId);
        } else {
            log.debug("Building excel document for group");
            Integer groupId = (Integer) model.get("groupId");

            Group group = groupDAO.findById(groupId).orElseThrow(() ->
                    new RequestException("Group not found", HttpStatus.NOT_FOUND));

            createBody(workbook, Collections.singletonList(group));
            setFilenameForGroup(response, group);
        }
    }

    private void createBody(Workbook workbook, List<Group> groups) {
        for (Group group : groups) {
            XSSFSheet sheet = ExcelBuilder.createSheet(workbook, group.getName());

            List<User> users = groupDAO.getAllUsersFromGroup(group.getName());
            int rowsCount = users.size() + TITLE_ROW_COUNT;

            XSSFRow[] rows = ExcelBuilder.createRows(sheet, rowsCount);

            ExcelBuilder.processTitleRow(rows[TITLE_ROW_NUMBER], FIRST_CELL, "Student List");

            createUsersCells(users, rows);
            createLessonsAndUsersStatusCells(group.getId(), rows);

            ExcelBuilder.setFont(workbook, rows[TITLE_ROW_NUMBER], "Arial",
                    HSSFColor.HSSFColorPredefined.GREEN, FillPatternType.SOLID_FOREGROUND, true,
                    HSSFColor.HSSFColorPredefined.WHITE);
        }
    }

    private void setFilenameForCourse(HttpServletResponse response, Integer courseId) {
        log.debug("Creating filename for course");
        Course course = courseDAO.findById(courseId)
                .orElseThrow(() -> new RequestException("Course not found", HttpStatus.NOT_FOUND));

        String filename = String.format("%s-%s-%s.xlsx", course.getCourseName(),
                course.getCourseType(), course.getDifficultyLevel());

        setResponseParameters(response, filename);
    }

    private void setFilenameForGroup(HttpServletResponse response, Group group) {
        log.debug("Creating filename for group");
        String filename = String.format("%s.xlsx", group.getName());
        setResponseParameters(response, filename);
    }

    private void setResponseParameters(HttpServletResponse response, String filename) {
        response.setContentType("application/ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=" + filename);
    }

    private void createUsersCells(List<User> users, XSSFRow[] rows) {
        int userRowIndex = 1;
        for (User user : users) {
            String fullName = String.format("%s %s", user.getFirstName(), user.getLastName());
            ExcelBuilder.createCell(rows[userRowIndex], FIRST_CELL).setCellValue(fullName);
            userRowIndex++;
        }
    }

    private void createLessonsAndUsersStatusCells(Integer groupId, XSSFRow[] rows) {
        List<LessonWithStatus> lessons = lessonDAO.getAllGroupLessons(groupId);
        List<Attendance> attendances = groupDAO.getGroupAttendance(groupId);

        int colIndex = 1;
        for (LessonWithStatus lesson : lessons) {
            String lessonInfo = String.format("%s ( %s )", lesson.getLessonName(),
                    lesson.getLessonDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")));

            ExcelBuilder.processTitleRow(rows[TITLE_ROW_NUMBER], colIndex, lessonInfo);

            List<Attendance> attendanceByLesson = attendances.stream()
                    .filter(attendance -> attendance.getLessonName().equals(lesson.getLessonName()))
                    .collect(Collectors.toList());

            int rowIndex = 1;
            for (Attendance attendance : attendanceByLesson) {
                if (attendance.getAbsenceReason() == null) {
                    ExcelBuilder.createCell(rows[rowIndex], colIndex).setCellValue(attendance.getUserPresenceStatus());
                } else {
                    ExcelBuilder.createCell(rows[rowIndex], colIndex).setCellValue("Absence reason: "
                            + attendance.getAbsenceReason());
                }
                rowIndex++;
            }
            colIndex++;
        }
    }
}
