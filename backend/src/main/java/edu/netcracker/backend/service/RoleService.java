package edu.netcracker.backend.service;

import edu.netcracker.backend.model.Role;

import java.util.List;

public interface RoleService {

    String getRoleName(Integer roleId);

    List<Role> getAll();
}
