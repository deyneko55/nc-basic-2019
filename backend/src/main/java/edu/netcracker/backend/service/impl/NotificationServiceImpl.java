package edu.netcracker.backend.service.impl;

import edu.netcracker.backend.dao.NotificationDAO;
import edu.netcracker.backend.message.request.course.CourseJoinRequest;
import edu.netcracker.backend.message.response.CourseDTO;
import edu.netcracker.backend.model.Notification;
import edu.netcracker.backend.model.User;
import edu.netcracker.backend.service.CourseService;
import edu.netcracker.backend.service.ManagerService;
import edu.netcracker.backend.service.NotificationService;
import edu.netcracker.backend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Dmytro Vovk
 */

@Service
public class NotificationServiceImpl implements NotificationService {

    private final SimpMessagingTemplate messagingTemplate;
    private final NotificationDAO notificationDAO;
    private CourseService courseService;
    private UserService userService;
    private ManagerService managerService;

    @Autowired
    public NotificationServiceImpl(SimpMessagingTemplate messagingTemplate, NotificationDAO notificationDAO,
                                   @Lazy CourseService courseService, @Lazy UserService userService,
                                   @Lazy ManagerService managerService) {
        this.messagingTemplate = messagingTemplate;
        this.notificationDAO = notificationDAO;
        this.courseService = courseService;
        this.userService = userService;
        this.managerService = managerService;
    }

    @Override
    public void sendNotificationToUser(Integer userId, String messageBody) {
        Notification notification = save(userId, messageBody);
        if (notification.getNotificationId() != null) {
            sendNotification("/notifications/" + notification.getRecipientId(), notification);
        }
    }

    @Override
    public Notification save(Integer userId, String content) {
        return notificationDAO.create(new Notification(userId, content, false));
    }

    @Override
    public List<Notification> getUserLastNotifications(Integer userId) {
        return notificationDAO.getUserLastNotifications(userId);
    }

    @Override
    public List<Notification> getAllUserNotifications(Integer userId) {
        return notificationDAO.getAllUserNotifications(userId);
    }

    private void sendNotification(String destination, Notification notification) {
        messagingTemplate.convertAndSend(destination, notification);
    }

    @Override
    public List<Notification> getUnreadNotifications(Integer userId) {
        return notificationDAO.getUnreadNotifications(userId);
    }

    @Override
    public void markAllAsRead(Integer userId) {
        notificationDAO.markAllAsRead(userId);
    }

    @Override
    public void sendCourseJoinRequest(CourseJoinRequest courseJoinRequest) {
        User user = userService.findById(courseJoinRequest.getUserId());
        CourseDTO course = courseService.findById(courseJoinRequest.getCourseId());
        String message = "User " + user.getFirstName() + " " + user.getLastName() + " (" + user.getLogin() + ") "
                + "asked to be taken on course " + "'" + course.getCourseName()  + "'.";
        Integer managerId = managerService.getUserManagerId(courseJoinRequest.getUserId());

        sendNotificationToUser(75, message); // ADMIN
        if (managerId != null) {
            sendNotificationToUser(managerId, message);
        }
    }
}
