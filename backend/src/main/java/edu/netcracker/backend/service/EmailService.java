package edu.netcracker.backend.service;

import edu.netcracker.backend.model.User;

import java.time.LocalDateTime;
import java.util.List;

public interface EmailService {

    void sendRegistrationMessage(String to, String token);

    void sendPasswordRecoveryMessage(String to, String username, String password);

    void sendLessonCancellationMessage(User trainer, String to, String lessonName, LocalDateTime dateTime);

    void sendAbsentReasonMessage(List<String> to, String reason);

    void sendUserMissedLessonMessage(String to);

    void sendTrainerNewGroupMessage(String to, String groupName);

    void sendManagerNewFeedbackMessage(String to, String text, User user);

    void sendManagerAbsenceWithoutReasonMessage(String to, User user);
}
