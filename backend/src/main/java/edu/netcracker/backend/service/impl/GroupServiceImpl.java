package edu.netcracker.backend.service.impl;

import edu.netcracker.backend.dao.GroupDAO;
import edu.netcracker.backend.dao.TrainerDAO;
import edu.netcracker.backend.dao.UserDAO;
import edu.netcracker.backend.exception.RequestException;
import edu.netcracker.backend.message.request.group.GroupSetTrainerRequest;
import edu.netcracker.backend.message.request.group.GroupDeleteRequest;
import edu.netcracker.backend.message.request.group.GroupFormRequest;
import edu.netcracker.backend.message.request.group.GroupUpdateRequest;
import edu.netcracker.backend.message.response.AttendanceDTO;
import edu.netcracker.backend.message.response.GroupResponse;
import edu.netcracker.backend.message.response.GroupWithTrainerResponse;
import edu.netcracker.backend.message.response.UserResponse;
import edu.netcracker.backend.model.*;
import edu.netcracker.backend.service.EmailService;
import edu.netcracker.backend.service.GroupService;
import edu.netcracker.backend.service.NotificationService;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Dmytro Vovk
 */

@Service
@Slf4j
public class GroupServiceImpl implements GroupService {

    private GroupDAO groupDAO;
    private EmailService emailService;
    private UserDAO userDAO;
    private final NotificationService notificationService;
    private final TrainerDAO trainerDAO;

    @Autowired
    public GroupServiceImpl(GroupDAO groupDAO, EmailService emailService, UserDAO userDAO,
                            NotificationService notificationService, TrainerDAO trainerDAO) {
        this.groupDAO = groupDAO;
        this.emailService = emailService;
        this.userDAO = userDAO;
        this.notificationService = notificationService;
        this.trainerDAO = trainerDAO;
    }


    @Override
    public GroupResponse createGroup(GroupFormRequest groupFormRequest) {
        log.debug("method was invoked");
        if (groupDAO.findGroupByClarifiedName(groupFormRequest.getNameGroup()).isPresent()) {
            log.error("Group is already exists");
            throw new RequestException("Group with courseName " + groupFormRequest.getNameGroup() + " already exists",
                    HttpStatus.CONFLICT);
        }
        return new GroupResponse(groupDAO.create(new Group(groupFormRequest)));
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public void updateGroup(GroupUpdateRequest groupUpdateRequest) {
        log.debug("method was invoked");
        groupDAO.findById(groupUpdateRequest.getGroupId()).ifPresent(group -> {
            groupDAO.changeGroupName(groupUpdateRequest.getGroupId(), groupUpdateRequest.getName());
            groupDAO.setCourseForGroup(groupUpdateRequest.getCourseId(), groupUpdateRequest.getGroupId());
        });
    }

    @Override
    public void deleteGroup(GroupDeleteRequest groupDeleteRequest) {
        log.debug("method was invoked");
        groupDAO.delete(groupDeleteRequest.getGroupId());
    }

    @Override
    public List<UserResponse> getGroupMembers(Integer groupId) {
        log.debug("Getting group members");
        List<UserResponse> userResponses = Lists.newArrayList();
        groupDAO.getAllUsersFromGroup(getById(groupId).getNameGroup()).forEach(user -> {
            userResponses.add(new UserResponse(user));
        });
        return userResponses;
    }

    @Override
    public List<GroupResponse> getGroupsByName(String name) {
        log.debug("Getting groups by name");
        List<GroupResponse> groupResponses = Lists.newArrayList();
        groupDAO.findGroupByName(name).forEach(group -> {
            groupResponses.add(new GroupResponse(group));
        });
        return groupResponses;
    }

    @Override
    public List<GroupResponse> getGroupsByUserId(Integer userId) {
        log.debug("method was invoked");
        List<GroupResponse> responses = new ArrayList<>();
        groupDAO.getGroupsByUserId(userId).forEach(group -> responses.add(new GroupResponse(group)));
        return responses;
    }

    @Override
    public GroupWithTrainerResponse getById(Integer id) {
        log.debug("method was invoked");
        return new GroupWithTrainerResponse(groupDAO.findById(id).orElseThrow(() ->
                new RequestException("Group is not found", HttpStatus.NOT_FOUND)));
    }

    @Override
    public List<GroupResponse> getAllGroups() {
        log.debug("method was invoked");
        List<GroupResponse> groups = Lists.newArrayList();
        groupDAO.getAll().forEach(group -> {
            groups.add(new GroupResponse(group));
        });
        return groups;
    }

    @Override
    public List<GroupStatistic> getGroupStatistics() {
        log.debug("method was invoked");
        return groupDAO.getGroupStatistic();
    }

    @Override
    public boolean existsGroup(Integer groupId) {
        log.debug("Checking if group exists");
        return groupDAO.findById(groupId).isPresent();
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public void assignTrainerToTheGroup(GroupSetTrainerRequest trainerForGroup) {
        log.debug("Method was invoked");
        if (!existsGroup(trainerForGroup.getGroupId())) {
            log.error("No such group with id {}", trainerForGroup.getGroupId());
            throw new RequestException("Group does not exist", HttpStatus.NOT_FOUND);
        }

        Optional<User> trainer = groupDAO.findTrainerByGroupId(trainerForGroup.getGroupId());

        if (!trainer.isPresent()) {
            log.debug("assigning trainer to the group");
            groupDAO.assignTrainerToTheGroup(trainerForGroup);
        } else {
            log.debug("setting new trainer for group");
            trainerDAO.changeTrainerForGroup(trainerForGroup.getGroupId(), trainer.get().getId(),
                    trainerForGroup.getTrainerId());
        }

        GroupWithTrainer group = groupDAO.findById(trainerForGroup.getGroupId()).orElseThrow(() ->
                new RequestException("Group for trainer not found", HttpStatus.NOT_FOUND));

        emailService.sendTrainerNewGroupMessage(userDAO.getEmailByUserId(
                trainerForGroup.getTrainerId()), group.getName());
        notificationService.sendNotificationToUser(trainerForGroup.getTrainerId(),
                "You are now trainer of " + group.getName() + " group. Have a nice work!");
        log.debug("Message with new group has been sent to trainer ");
    }

    @Override
    public void addNewUserToGroup(Integer userId, Integer groupId) {
        log.debug("method was invoked");
        groupDAO.addNewUserToGroup(userId, groupId);
        notificationService.sendNotificationToUser(userId, "You were assigned to a group");
    }

    @Override
    public List<AttendanceDTO> getGroupAttendance(Integer groupId) {
        log.debug("Getting group attendance");
        List<Attendance> groupAttendance = groupDAO.getGroupAttendance(groupId);

        if (groupAttendance.isEmpty()) {
            log.error("Group attendance not found");
            throw new RequestException("Group attendance not found", HttpStatus.NOT_FOUND);
        }
        return groupAttendance.stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());
    }

    @Override
    public List<UserResponse> getGroupMates(Integer userId) {
        List<UserResponse> groupMates = Lists.newArrayList();
        groupDAO.getGroupsByUserId(userId).forEach(group -> {
            groupMates.addAll(getGroupMembers(group.getId()));
        });
        return groupMates;
    }

    private AttendanceDTO convertToDTO(Attendance attendance) {
        log.debug("Converting attendance to dto");
        AttendanceDTO attendanceDTO = new AttendanceDTO(attendance);

        if (attendance.getAbsenceReason() == null) {
            attendanceDTO.setUserPresenceStatus(attendance.getUserPresenceStatus());
        } else {
            attendanceDTO.setUserPresenceStatus(attendance.getAbsenceReason());
        }
        return attendanceDTO;
    }
}
