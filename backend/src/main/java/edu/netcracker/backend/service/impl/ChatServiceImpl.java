package edu.netcracker.backend.service.impl;

import edu.netcracker.backend.dao.ChatDAO;
import edu.netcracker.backend.dao.MessageDAO;
import edu.netcracker.backend.message.request.chat.ChatGroupFormRequest;
import edu.netcracker.backend.message.request.chat.ChatPrivateFormRequest;
import edu.netcracker.backend.message.request.chat.ChatSendMessageRequest;
import edu.netcracker.backend.message.response.ChatMessageResponse;
import edu.netcracker.backend.message.response.ChatResponse;
import edu.netcracker.backend.model.Chat;
import edu.netcracker.backend.model.Message;
import edu.netcracker.backend.service.ChatService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ChatServiceImpl implements ChatService {

    private final MessageDAO messageDAO;
    private final SimpMessagingTemplate messagingTemplate;
    private ChatDAO chatDAO;

    @Autowired
    public ChatServiceImpl(MessageDAO messageDAO, SimpMessagingTemplate messagingTemplate, ChatDAO chatDAO) {
        this.messagingTemplate = messagingTemplate;
        this.messageDAO = messageDAO;
        this.chatDAO = chatDAO;
    }

    @Override
    public void getLastMessagesFromChat(Integer chatId, Integer userId) {
        List<Message> lastMessagesFromChat = messageDAO.getLastMessagesFromChat(chatId);
        List<ChatMessageResponse> responses = new ArrayList<>();
        lastMessagesFromChat.forEach(message -> {
            responses.add(new ChatMessageResponse(message));
        });
        responses.forEach(chatMessageResponse -> {
            messagingTemplate.convertAndSend("/chats/" + chatId + "/" + userId, chatMessageResponse);
        });
    }

    @Override
    public ChatResponse createChatForGroup(ChatGroupFormRequest chatGroupFormRequest) {
        return new ChatResponse(chatDAO.createChatForGroup(chatGroupFormRequest.getGroupId()));
    }

    @Override
    public void processMessage(ChatSendMessageRequest messageRequest) {
        Message message = messageDAO.create(new Message(messageRequest));
        if (message.getMessageId() != null) {
            sendMessage(message);
            // sendNotification(message);
        }
    }

    private void sendMessage(@NotNull final Message message) {
        messagingTemplate.convertAndSend("/chats/" + message.getChatId(),
                new ChatMessageResponse(message));
    }

    @Override
    public ChatResponse createPrivateChat(ChatPrivateFormRequest chatPrivateFormRequest) {
        Chat chat = chatDAO.create(new Chat(null, chatPrivateFormRequest.getChatName()));
        chatDAO.addUserToPrivateChat(chatPrivateFormRequest.getSenderId(), chat.getChatId());
        chatDAO.addUserToPrivateChat(chatPrivateFormRequest.getRecipientId(), chat.getChatId());
        return new ChatResponse(chat);
    }

    private void sendNotification(@NotNull final Message message) {
        chatDAO.getChatUsersIds(message.getChatId()).forEach(id -> {
            if (!id.equals(message.getUserId())) {
                messagingTemplate.convertAndSend("/notifications/" + id,
                        new ChatMessageResponse(message));
            }
        });
    }

    @Override
    public Chat getByGroupId(Integer groupId) {
        return chatDAO.findByGroupId(groupId);
    }
}
