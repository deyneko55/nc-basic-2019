package edu.netcracker.backend.service;

import edu.netcracker.backend.message.request.desiredtime.DesiredTimeRequest;
import edu.netcracker.backend.message.request.desiredtime.DesiredTimeUpdateRequest;
import edu.netcracker.backend.message.response.DesiredTimeResponse;

import java.util.List;

public interface DesiredTimeService {

    DesiredTimeResponse createDesiredTime(DesiredTimeRequest desiredTimeRequest);

    void updateDesiredTime(DesiredTimeUpdateRequest desiredTimeUpdateRequest);

    void deleteDesiredTime(Integer desiredTimeId);

    List<DesiredTimeResponse> getAll();

    List<DesiredTimeResponse> getUsersDesiredTime(Integer userId);

    List<DesiredTimeResponse> getAllDesiredTimeForCourse(Integer courseId);
}
