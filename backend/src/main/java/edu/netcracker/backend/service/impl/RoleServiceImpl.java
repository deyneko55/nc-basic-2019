package edu.netcracker.backend.service.impl;

import edu.netcracker.backend.dao.RoleDAO;
import edu.netcracker.backend.model.Role;
import edu.netcracker.backend.service.RoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleDAO roleDAO;

    @Override
    public String getRoleName(Integer roleId) {
        log.debug("Getting role name");
        return roleDAO.getRoleName(roleId);
    }

    @Override
    public List<Role> getAll() {
        log.debug("Getting all roles");
        return roleDAO.getAll();
    }
}
