package edu.netcracker.backend.service.impl;

import edu.netcracker.backend.dao.FeedbackDAO;
import edu.netcracker.backend.dao.UserDAO;
import edu.netcracker.backend.message.request.feedback.FeedbackFormRequest;
import edu.netcracker.backend.message.response.FeedbackResponse;
import edu.netcracker.backend.model.Feedback;
import edu.netcracker.backend.model.FeedbackAboutUser;
import edu.netcracker.backend.model.User;
import edu.netcracker.backend.service.EmailService;
import edu.netcracker.backend.service.FeedbackService;
import edu.netcracker.backend.service.NotificationService;
import edu.netcracker.backend.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Slf4j
public class FeedbackServiceImpl implements FeedbackService {

    private FeedbackDAO feedbackDAO;
    private EmailService emailService;
    private UserDAO userDAO;
    private UserService userService;
    private NotificationService notificationService;

    @Autowired
    public FeedbackServiceImpl(FeedbackDAO feedbackDAO, EmailService emailService, UserDAO userDAO, UserService userService,
                               NotificationService notificationService) {
        this.feedbackDAO = feedbackDAO;
        this.emailService = emailService;
        this.userDAO = userDAO;
        this.userService = userService;
        this.notificationService = notificationService;
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public FeedbackResponse createFeedback(FeedbackFormRequest feedbackFormRequest) {
        log.debug("Creating feedback");
        Feedback feedback = feedbackDAO.create(new Feedback(feedbackFormRequest.getContent(), feedbackFormRequest.getTrainerId(),
                feedbackFormRequest.getUserId()));
        Integer managerId = userDAO.getUsersManagerIdByUserId(feedbackFormRequest.getUserId());
        User user = userService.findById(feedbackFormRequest.getUserId());
        notificationService.sendNotificationToUser(managerId, "New Feedback on user: " + user.getFirstName()
                + " " + user.getLastName() + " (username " + user.getLogin() + ").");
        log.debug("Message with new feedback has been sent to manager");
        return new FeedbackResponse(feedback);
    }

    @Override
    public List<FeedbackAboutUser> getFeedbackAboutUser(Integer userId) {
        log.info("method was invoked");
        return feedbackDAO.getUserFeedback(userId);
    }
}
