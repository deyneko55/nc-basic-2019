package edu.netcracker.backend.service;

import edu.netcracker.backend.message.request.course.CourseJoinRequest;
import edu.netcracker.backend.model.Notification;

import java.util.List;

/**
 * @author Dmytro Vovk
 */

public interface NotificationService {
    void sendNotificationToUser(Integer userId, String body);

    Notification save(Integer userId, String content);

    List<Notification> getUserLastNotifications(Integer userId);

    List<Notification> getAllUserNotifications(Integer userId);

    List<Notification> getUnreadNotifications(Integer userId);

    void markAllAsRead(Integer userId);

    void sendCourseJoinRequest(CourseJoinRequest courseJoinRequest);
}
