package edu.netcracker.backend.service.impl;

import edu.netcracker.backend.dao.FileDAO;
import edu.netcracker.backend.dao.GroupDAO;
import edu.netcracker.backend.dao.LessonDAO;
import edu.netcracker.backend.dao.UserDAO;
import edu.netcracker.backend.exception.RequestException;
import edu.netcracker.backend.message.request.lesson.*;
import edu.netcracker.backend.message.response.FileResponse;
import edu.netcracker.backend.message.response.LessonCreationResponse;
import edu.netcracker.backend.message.response.LessonResponse;
import edu.netcracker.backend.message.response.LessonWithStatusResponse;
import edu.netcracker.backend.model.*;
import edu.netcracker.backend.service.EmailService;
import edu.netcracker.backend.service.LessonService;
import edu.netcracker.backend.service.NotificationService;
import edu.netcracker.backend.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class LessonServiceImpl implements LessonService {

    private UserDAO userDAO;
    private LessonDAO lessonDAO;
    private GroupDAO groupDAO;
    private FileDAO fileDAO;
    private EmailService emailService;
    private UserService userService;
    private NotificationService notificationService;

    @Autowired
    public LessonServiceImpl(LessonDAO lessonDAO, FileDAO fileDAO, EmailService emailService,
                             UserDAO userDAO,
                             GroupDAO groupDAO,
                             UserService userService,
                             NotificationService notificationService) {
        this.lessonDAO = lessonDAO;
        this.fileDAO = fileDAO;
        this.emailService = emailService;
        this.userDAO = userDAO;
        this.userService = userService;
        this.groupDAO = groupDAO;
        this.notificationService = notificationService;
    }

    @Override
    public LessonCreationResponse createLesson(LessonFormRequest lessonFormRequest) {
        log.debug("Creating lesson");
        if (lessonDAO.findByName(lessonFormRequest.getName()).isPresent()) {
            log.error("Lesson with name: {} is already exists", lessonFormRequest.getName());
            throw new RequestException("Lesson with lessonName " + lessonFormRequest.getName() + " already exists", HttpStatus.CONFLICT);
        }
        Lesson lesson = new Lesson(lessonFormRequest.getCourseId(), lessonFormRequest.getName());
        return new LessonCreationResponse(lessonDAO.create(lesson));
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public void updateLesson(LessonUpdateRequest lessonUpdateRequest) {
        log.debug("Updating lesson");
        String date = lessonUpdateRequest.getLessonDate().replace("T", " ");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

        lessonDAO.findById(lessonUpdateRequest.getId()).ifPresent(lesson -> {
            lessonDAO.update(new Lesson(lessonUpdateRequest.getId(), lessonUpdateRequest.getGroupId(),
                    lessonUpdateRequest.getLessonName(), LocalDateTime.parse(date, formatter)));
        });
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public void setDateForLessonOfGroup(LessonSetDateAndGroupRequest lessonOfGroup) {
        log.debug("Setting date for lesson of group");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        String date = Timestamp.valueOf(lessonOfGroup.getLessonDate()
                .replace("T", " ")
                .replace("Z", ""))
                .toLocalDateTime()
                .format(formatter);

        List<User> users = lessonDAO.findEmployeesByGroupAndLessonId(lessonOfGroup.getLessonId(), lessonOfGroup.getGroupId());
        lessonDAO.findById(lessonOfGroup.getLessonId()).ifPresent(lesson -> {
            log.debug("LessonService.setDateForLessonOfGroup(LessonSetDateAndGroupRequest lessonOfGroup) was invoked.");
            lessonDAO.setDateForLessonOfGroup(lessonOfGroup.getGroupId(), lessonOfGroup.getLessonId(),
                    LocalDateTime.parse(date, formatter));
            users.forEach(user -> {
                notificationService.sendNotificationToUser(user.getId(),
                        "Lesson '" + lesson.getLessonName() + "', IS RESCHEDULED FOR "
                                + LocalDateTime.parse(date, formatter));
            });
        });
    }

    @Override
    public void updateCourseForLesson(LessonUpdateCourseRequest lessonUpdateCourseRequest) {
        log.debug("Updating course for lesson");
        lessonDAO.findById(lessonUpdateCourseRequest.getLessonId()).ifPresent(lesson -> {
            lessonDAO.addLessonToCourse(lessonUpdateCourseRequest.getLessonId(),
                    lessonUpdateCourseRequest.getCourseId());
        });
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public void setUserStatus(LessonUserStatus lessonUserStatus) {
        log.debug("Setting user status at lesson");

        lessonDAO.setUserStatus(lessonUserStatus.getUserId(), lessonUserStatus.getLessonId(),
                lessonUserStatus.getPresenceStatusId(), lessonUserStatus.getAbsenceReasonId(), lessonUserStatus.getExplanation());
        UserStatus uS = lessonDAO.getUserStatus(lessonUserStatus.getUserId(), lessonUserStatus.getLessonId());

        int absenceStatusWithoutReason = 3;
        if (uS.getPresenceStatusId() == absenceStatusWithoutReason) {
            emailService.sendUserMissedLessonMessage(userDAO.getEmailByUserId(lessonUserStatus.getUserId()));
            log.debug("Message with question why the user missed the lesson was sent");

            emailService.sendManagerAbsenceWithoutReasonMessage(userDAO.getEmailByUserId(userDAO.getUsersManagerIdByUserId(lessonUserStatus.getUserId())),
                    userService.findById(lessonUserStatus.getUserId()));
            lessonDAO.findById(lessonUserStatus.getLessonId()).ifPresent(lesson -> {
                notificationService.sendNotificationToUser(lessonUserStatus.getUserId(),
                        "You missed lesson named as " + lesson.getLessonName() + " and scheduled for "
                                + lesson.getLessonDate() + ". Please, describe absence reason.");
            });
        }
    }

    @Override
    public void deleteUserStatus(Integer userId, Integer lessonId) {
        log.debug("Deletion user status");
        lessonDAO.deleteUserStatus(userId, lessonId);
    }

    @Override
    public UserStatus getUserStatus(Integer userId, Integer lessonId) {
        log.debug("Getting user status");
        return lessonDAO.getUserStatus(userId, lessonId);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<LessonWithStatusResponse> getAllLessons(Integer groupId) {
        log.debug("Getting all lessons with schedule and students");

        List<LessonWithStatusResponse> lessonResponses = new ArrayList<>();
        lessonDAO.getAllGroupLessons(groupId).forEach((lessonWithStatus) ->
                lessonResponses.add(new LessonWithStatusResponse(lessonWithStatus)));
        return lessonResponses;
    }

    @Override
    public List<FileResponse> getLessonFiles(Integer lessonId) {
        log.debug("Method was invoked");
        List<File> lessonFiles = fileDAO.getFilesByLessonId(lessonId);

        if (lessonFiles.isEmpty()) {
            log.error("No lesson files with id {}", lessonId);
            throw new RequestException("Lesson files not found", HttpStatus.NOT_FOUND);
        }
        return lessonFiles.stream().map(FileResponse::new).collect(Collectors.toList());
    }

    @Override
    public LessonResponse getGroupLesson(Integer lessonId, Integer groupId) {
        log.debug("Method was invoked");
        Optional<Lesson> lessonById = lessonDAO.getGroupLesson(lessonId, groupId);
        return lessonById.map(LessonResponse::new).orElse(null);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public void cancelTheLesson(Integer lessonId, Integer groupId) {
        log.debug("Cancellation the lesson");

        lessonDAO.getGroupLesson(lessonId, groupId).ifPresent(lesson -> {
            List<User> users = lessonDAO.findEmployeesByGroupAndLessonId(lessonId, groupId);
            lessonDAO.updateLessonStatusToCanceled(groupId, lessonId);

            groupDAO.findTrainerByGroupId(groupId)
                    .ifPresent(trainer ->
                            users.forEach(user -> {
                                emailService.sendLessonCancellationMessage(trainer, user.getEmail(),
                                        lesson.getLessonName(), lesson.getLessonDate());
                                notificationService.sendNotificationToUser(user.getId(),
                                        "Lesson '" + lesson.getLessonName() + "', scheduled for "
                                                + lesson.getLessonDate() + " IS CANCELED");
                            }));
        });
    }

    @Override
    public List<PresenceStatus> getAllPresenceStatuses() {
        log.debug("Method was invoked");
        return lessonDAO.getAllPresenceStatuses();
    }
}
