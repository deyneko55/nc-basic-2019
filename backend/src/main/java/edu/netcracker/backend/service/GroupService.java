package edu.netcracker.backend.service;

import edu.netcracker.backend.message.request.group.GroupSetTrainerRequest;
import edu.netcracker.backend.message.request.group.GroupDeleteRequest;
import edu.netcracker.backend.message.request.group.GroupFormRequest;
import edu.netcracker.backend.message.request.group.GroupUpdateRequest;
import edu.netcracker.backend.message.response.AttendanceDTO;
import edu.netcracker.backend.message.response.GroupResponse;
import edu.netcracker.backend.message.response.GroupWithTrainerResponse;
import edu.netcracker.backend.message.response.UserResponse;
import edu.netcracker.backend.model.GroupStatistic;

import java.util.List;

/**
 * @author Dmytro Vovk
 */

public interface GroupService {

    GroupResponse createGroup(GroupFormRequest groupFormRequest);

    void updateGroup(GroupUpdateRequest groupUpdateRequest);

    void deleteGroup(GroupDeleteRequest groupDeleteRequest);

    boolean existsGroup(Integer groupId);

    GroupWithTrainerResponse getById(Integer id);

    void assignTrainerToTheGroup(GroupSetTrainerRequest trainerForGroup);

    void addNewUserToGroup(Integer userId, Integer groupId);

    List<GroupResponse> getAllGroups();

    List<GroupStatistic> getGroupStatistics();

    List<AttendanceDTO> getGroupAttendance(Integer groupId);

    List<UserResponse> getGroupMembers(Integer groupId);

    List<GroupResponse> getGroupsByName(String name);

    List<GroupResponse> getGroupsByUserId(Integer userId);

    List<UserResponse> getGroupMates(Integer userId);
}
