package edu.netcracker.backend.service;

import edu.netcracker.backend.model.File;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface FileService {

    void uploadFile(Integer lessonId, MultipartFile file);

    void deleteFile(Integer id);

    File getFile(Integer id);

    List<File> getAllFiles();
}
