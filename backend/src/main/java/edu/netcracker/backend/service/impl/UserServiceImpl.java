package edu.netcracker.backend.service.impl;

import edu.netcracker.backend.dao.MailTokenDAO;
import edu.netcracker.backend.dao.RoleDAO;
import edu.netcracker.backend.dao.UserDAO;
import edu.netcracker.backend.exception.RequestException;
import edu.netcracker.backend.message.request.absencereason.AbsenceReasonFormRequest;
import edu.netcracker.backend.message.request.auth.SignUpByLinkRequest;
import edu.netcracker.backend.message.request.auth.SignUpRequest;
import edu.netcracker.backend.message.request.user.ChangePasswordRequest;
import edu.netcracker.backend.message.request.user.UserChangeStatusRequest;
import edu.netcracker.backend.message.request.user.UserEditRequest;
import edu.netcracker.backend.message.response.*;
import edu.netcracker.backend.model.*;
import edu.netcracker.backend.security.*;
import edu.netcracker.backend.service.UserService;
import edu.netcracker.backend.utils.PasswordGeneratorUtils;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class UserServiceImpl implements UserService {

    private PasswordEncoder passwordEncoder;
    private UserDAO userDAO;
    private RoleDAO roleDAO;
    private MailTokenDAO mailTokenDAO;
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    public UserServiceImpl(PasswordEncoder passwordEncoder, UserDAO userDAO, JwtTokenProvider jwtTokenProvider,
                           RoleDAO roleDAO, MailTokenDAO mailTokenDAO) {
        this.passwordEncoder = passwordEncoder;
        this.userDAO = userDAO;
        this.roleDAO = roleDAO;
        this.mailTokenDAO = mailTokenDAO;
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @Override
    public UserResponse createUser(SignUpRequest signUpRequest) {
        log.debug("Method was invoked");

        if (existsByUsername(signUpRequest.getUsername())) {
            log.error("User with username {} already exists", signUpRequest.getUsername());
            throw new RequestException("Username already exists", HttpStatus.CONFLICT);
        }

        if (existsByEmail(signUpRequest.getEmail())) {
            log.error("User with email {} already exists", signUpRequest.getEmail());
            throw new RequestException("Email already exists", HttpStatus.CONFLICT);
        }
        User user = new User(signUpRequest.getUsername(), passwordEncoder.encode(signUpRequest.getPassword()),
                signUpRequest.getEmail(), signUpRequest.getFirstName(), signUpRequest.getLastName(), signUpRequest.getRoleId());
        user.setRole(roleDAO.getRoleName(signUpRequest.getRoleId()));

        return new UserResponse(userDAO.create(user));
    }

    private boolean checkIfRegistrationLinkIsValid(String registrationToken) {
        log.debug("Checking if registration link is valid");
        try {
            if (mailTokenDAO.ifLinkExists(registrationToken)) {
                if (!jwtTokenProvider.getExpiration(registrationToken).before(new Date())) {
                    return true;
                } else {
                    deleteRegistrationToken(registrationToken);
                }
            }
        } catch (RequestException e) {
            log.error("Link is invalid - {}", e.getMessage());
            return false;
        }
        return false;
    }

    @Override
    public UserResponse signUpByRegistrationLink(SignUpByLinkRequest signUpRequest) {
        log.debug("Signing up by registration link");
        if (!checkIfRegistrationLinkIsValid(signUpRequest.getRegistrationToken())) {
            log.error("Registration token is not valid");
            throw new RequestException("Token is not valid", HttpStatus.NOT_FOUND);
        }
        return createUser(signUpRequest);
    }

    @Override
    public void deleteRegistrationToken(String mailToken) {
        log.debug("Deletion photo");
        mailTokenDAO.deleteByToken(mailToken);
    }

    @Override
    public void save(User user) {
        log.debug("Method was invoked");
        userDAO.create(user);
    }

    @Override
    public boolean existsByEmail(String email) {
        log.debug("Checking if email exists");
        return userDAO.findByEmail(email).isPresent();
    }

    @Override
    public boolean existsByUsername(String username) {
        log.debug("Checking if username exists");
        return userDAO.findByUsername(username).isPresent();
    }

    @Override
    public void changePasswordForUser(User user, ChangePasswordRequest changePasswordRequest) {
        log.debug("Changing password for user");

        if (oldPasswordNotMatched(user.getPassword(), changePasswordRequest.getOldPassword())) {
            log.error("Invalid password {}", changePasswordRequest.getOldPassword());
            throw new RequestException("invalid password", HttpStatus.BAD_REQUEST);
        }
        user.setPassword(passwordEncoder.encode(changePasswordRequest.getNewPassword()));
        userDAO.updateUserPassword(user);
        log.debug("Password for user has been changed");
    }

    @Override
    public String generatePasswordForUser(User user) {
        log.debug("Generating new password for user");
        String newPassword = PasswordGeneratorUtils.generatePassword();
        user.setPassword(passwordEncoder.encode(newPassword));
        userDAO.updateUserPassword(user);

        log.debug("Password for user has been generated");
        return newPassword;
    }

    @Override
    public User findByEmail(String email) {
        log.debug("Method was invoked");
        return userDAO.findByEmail(email).orElse(null);
    }

    @Override
    public void updateUser(UserEditRequest userEditRequest) {
        log.debug("Method was invoked");
        User user = new User(userEditRequest.getUserId(), userEditRequest.getUsername(), userEditRequest.getFirstName(),
                userEditRequest.getLastName(), userEditRequest.getEmail());
        if (findById(userEditRequest.getUserId()) != null) {
            userDAO.update(user);
        }
    }

    @Override
    public void changeUserStatus(UserChangeStatusRequest userChangeStatusRequest) {

    }

    @Override
    public User findById(Integer id) {
        return userDAO.findById(id).orElseThrow(() ->
                new RequestException("User is not found", HttpStatus.NOT_FOUND));
    }

    @Override
    public List<UserResponse> getByUsernameOrRole(String username, List<Long> roles) {
        List<UserResponse> users = Lists.newArrayList();
        userDAO.getUsersByLoginOrRole(username, roles).forEach(user -> {
            users.add(new UserResponse(user));
        });
        return users;
    }

    @Override
    public UserResponse getUserResponse(Integer userId) {
        return new UserResponse(findById(userId));
    }

    @Override
    public List<Group> getUserGroups(Integer userId) {
        return null;
    }

    @Override
    public List<UserResponse> getAllUsers() {
        List<UserResponse> allUsers = Lists.newArrayList();
        userDAO.getAll().forEach(user ->
                allUsers.add(new UserResponse(user)));
        return allUsers;
    }

    @Override
    public void specifyAbsentReason(Integer lessonId, Integer userId, String reason) {
        userDAO.specifyAbsentReason(lessonId, userId, reason);
    }

    @Override
    public AbsenceReasonResponse addNewAbsenceReason(AbsenceReasonFormRequest absenceReasonFormRequest) {
        AbsenceReason absenceReason = new AbsenceReason(absenceReasonFormRequest.getAbsenceReasonName());
        return new AbsenceReasonResponse(userDAO.addAbsenceReason(absenceReason));
    }

    @Override
    public List<String> getUsersSuperiorsEmails(Integer userId) {
        return userDAO.getUsersSuperiorsEmails(userId);
    }

    private boolean oldPasswordNotMatched(String userPassword, String oldPassword) {
        return !passwordEncoder.matches(oldPassword, userPassword);
    }

    @Override
    public List<UserGroupsAndCoursesResponse> getUsersGroupsAndCourses(Integer userId) {
        return userDAO.getUsersGroupsAndCourses(userId);
    }

    @Override
    public List<UserCompletedCoursesResponse> getUsersCompletedCourses(Integer userId) {
        return userDAO.getCompletedCourses(userId);
    }

    @Override
    public List<Chat> getUserChats(Integer userId) {
        return userDAO.getUserChats(userId);
    }

    @Override
    public void deleteUserFromDB(Integer userId) {
        userDAO.delete(userId);
    }

    @Override
    public List<UserLesson> getUsersLessons(Integer userId, Integer courseId) {
        return userDAO.getUsersLessons(userId, courseId);
    }

    @Override
    public List<AttendanceDTO> getUserAttendance(Integer userId, Integer groupId) {
        log.debug("Getting user attendance");
        List<Attendance> userAttendance = userDAO.getUserAttendance(userId, groupId);

        if (userAttendance.isEmpty()) {
            log.error("User attendance not found");
            throw new RequestException("User attendance not found", HttpStatus.NOT_FOUND);
        }
        return userAttendance.stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());
    }

    private AttendanceDTO convertToDTO(Attendance attendance) {
        log.debug("Converting attendance to dto");
        AttendanceDTO attendanceDTO = new AttendanceDTO(attendance);

        if (attendance.getAbsenceReason() == null) {
            attendanceDTO.setUserPresenceStatus(attendance.getUserPresenceStatus());
        } else {
            attendanceDTO.setUserPresenceStatus(attendance.getAbsenceReason());
        }
        return attendanceDTO;
    }
}
