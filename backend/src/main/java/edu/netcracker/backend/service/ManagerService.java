package edu.netcracker.backend.service;

import edu.netcracker.backend.message.request.manager.ManagerSetUserRequest;
import edu.netcracker.backend.message.response.UserResponse;
import edu.netcracker.backend.model.UserStatistic;

import java.util.List;

public interface ManagerService {

    void setManager(ManagerSetUserRequest managerSetUserRequest);

    List<UserResponse> getAllUsers(Integer managerId);

    List<UserStatistic> getUserStatistic(Integer managerId);

    Integer getUserManagerId(Integer userId);

    List<UserResponse> getManagerByLastName(String lastName);

    List<UserResponse> getAllManagers();
}
