package edu.netcracker.backend.service.impl;

import edu.netcracker.backend.dao.FileDAO;
import edu.netcracker.backend.exception.RequestException;
import edu.netcracker.backend.model.File;
import edu.netcracker.backend.service.FileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
@Slf4j
public class FileServiceImpl implements FileService {

    @Autowired
    private FileDAO fileDAO;

    @Override
    public void uploadFile(Integer lessonId, MultipartFile file) {
        log.debug("uploading file");
        fileDAO.uploadFile(file, lessonId);
    }

    @Override
    public void deleteFile(Integer id) {
        log.debug("deleting file");
        fileDAO.deleteFile(id);
    }

    @Override
    public File getFile(Integer id) {
        log.debug("getting file");

        return fileDAO.getFile(id).orElseThrow(() ->
                new RequestException("File does not exist", HttpStatus.NOT_FOUND));
    }

    @Override
    public List<File> getAllFiles() {
        log.debug("getting all files");
        return fileDAO.getAllFiles();
    }
}
