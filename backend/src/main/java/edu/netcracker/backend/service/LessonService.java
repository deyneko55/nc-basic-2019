package edu.netcracker.backend.service;

import edu.netcracker.backend.message.request.lesson.*;
import edu.netcracker.backend.message.response.FileResponse;
import edu.netcracker.backend.message.response.LessonCreationResponse;
import edu.netcracker.backend.message.response.LessonResponse;
import edu.netcracker.backend.message.response.LessonWithStatusResponse;
import edu.netcracker.backend.model.PresenceStatus;
import edu.netcracker.backend.model.UserStatus;

import java.util.List;

public interface LessonService {

    LessonCreationResponse createLesson(LessonFormRequest lessonFormRequest);

    void updateLesson(LessonUpdateRequest lessonUpdateRequest);

    void updateCourseForLesson(LessonUpdateCourseRequest lessonUpdateCourseRequest);

    void setDateForLessonOfGroup(LessonSetDateAndGroupRequest lessonOfGroup);

    void setUserStatus(LessonUserStatus lessonUserStatus);

    void deleteUserStatus(Integer userId, Integer lessonId);

    void cancelTheLesson(Integer lessonId, Integer groupId);

    UserStatus getUserStatus(Integer userId, Integer lessonId);

    List<LessonWithStatusResponse> getAllLessons(Integer groupId);

    List<FileResponse> getLessonFiles(Integer lessonId);

    LessonResponse getGroupLesson(Integer lessonId, Integer groupId);

    List<PresenceStatus> getAllPresenceStatuses();
}
