package edu.netcracker.backend.service.impl;

import edu.netcracker.backend.dao.PhotoDAO;
import edu.netcracker.backend.model.Photo;
import edu.netcracker.backend.service.PhotoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
@Slf4j
public class PhotoServiceImpl implements PhotoService {

    @Autowired
    private PhotoDAO photoDAO;

    @Override
    public void uploadPhoto(Integer userId, MultipartFile photo) {
        log.debug("Uploading photo");
        photoDAO.uploadPhoto(photo, userId);
    }

    @Override
    public void deletePhoto(Integer userId) {
        log.debug("Deleting photo");
        photoDAO.deletePhoto(userId);
    }

    @Override
    public Photo getPhoto(Integer userId) {
        log.debug("Getting photo");
        return photoDAO.getPhoto(userId);
    }
}
