package edu.netcracker.backend.service;

import edu.netcracker.backend.message.response.AbsenceReasonResponse;

import java.util.List;

public interface AbsenceReasonService {

    List<AbsenceReasonResponse> getAll();
}
