package edu.netcracker.backend.service;

import edu.netcracker.backend.message.request.chat.ChatGroupFormRequest;
import edu.netcracker.backend.message.request.chat.ChatPrivateFormRequest;
import edu.netcracker.backend.message.request.chat.ChatSendMessageRequest;
import edu.netcracker.backend.message.response.ChatResponse;
import edu.netcracker.backend.model.Chat;

public interface ChatService {

    void getLastMessagesFromChat(Integer chatId, Integer userId);

    ChatResponse createChatForGroup(ChatGroupFormRequest chatGroupFormRequest);

    void processMessage(ChatSendMessageRequest messageRequest);

    ChatResponse createPrivateChat(ChatPrivateFormRequest chatPrivateFormRequest);

    Chat getByGroupId(Integer groupId);
}
