package edu.netcracker.backend.service;

import edu.netcracker.backend.message.request.user.ChangePasswordRequest;
import edu.netcracker.backend.message.request.auth.EmailRequest;
import edu.netcracker.backend.message.request.auth.SignInRequest;
import edu.netcracker.backend.message.response.JwtResponse;
import edu.netcracker.backend.message.response.MessageResponse;

public interface AuthenticationService {

    JwtResponse signIn(SignInRequest signInRequest);

    MessageResponse changePassword(ChangePasswordRequest changePasswordRequest);

    void passwordRecovery(EmailRequest emailRequest);

}
