package edu.netcracker.backend.service;

import edu.netcracker.backend.message.request.course.CourseRequest;
import edu.netcracker.backend.message.request.course.CourseWithIdsFormRequest;
import edu.netcracker.backend.message.response.*;
import edu.netcracker.backend.model.CourseLevel;
import edu.netcracker.backend.model.CourseType;

import java.time.LocalDate;
import java.util.List;

public interface CourseService {

    CourseDTO createCourse(CourseRequest course);

    void editCourse(CourseRequest course);

    void deleteCourse(Integer courseId);

    CourseDTO findById(Integer courseId);

    List<CourseDTO> findCourseByName(String name);

    List<CourseDTO> getAllCourses();

    List<CourseDTO> getCoursesByUserId(Integer userId);

    List<CourseDTO> getActiveCourses(LocalDate currentDate);

    List<CourseType> getAllCourseTypes();

    List<CourseLevel> getLevelsOfCourseType(Integer courseTypeId);

    List<CourseLevel> getAllLevelsOfCourse();

    List<GroupResponse> getAllGroupsFromCourse(Integer courseId);

    List<CourseTypeStatsResponse> getUsersByCourseType(Integer courseTypeId);

    List<CourseTypeStatsResponse> getGroupsByCourseType(Integer courseTypeId);

    List<UserResponse> getAllUsersFromCourse(Integer courseId);

    void bookCourse(Integer userId, Integer courseId);

    List<UserResponse> getAllUsersFromDesiredCourse(Integer courseId);

    List<LessonCreationResponse> getAllLessonsFromCourse(Integer courseId);

    List<GroupWithTrainerResponse> getGroupsWithTrainersFromCourse(Integer courseId);

    CourseDTO addNewCourseByIds(CourseWithIdsFormRequest course);

    CourseDifficultyLevelResponse createDifficultyLevel(String nameOfLevel);

    CourseTypeResponse createCourseType(String typeName);

}
