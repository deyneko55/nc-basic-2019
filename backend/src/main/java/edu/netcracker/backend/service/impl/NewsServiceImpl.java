package edu.netcracker.backend.service.impl;

import edu.netcracker.backend.dao.NewsDAO;
import edu.netcracker.backend.exception.RequestException;
import edu.netcracker.backend.model.NewsPost;
import edu.netcracker.backend.model.NewsPostId;
import edu.netcracker.backend.model.Slider;
import edu.netcracker.backend.service.NewsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author Dmytro Vovk
 */

@Service
@Slf4j
public class NewsServiceImpl implements NewsService {

    private final NewsDAO newsDAO;

    @Autowired
    public NewsServiceImpl(NewsDAO newsDAO) {
        this.newsDAO = newsDAO;
    }

    @Override
    public NewsPostId createNewsPost(String header, String description, String content) {
        log.debug("Method was invoked");
        return newsDAO.create(header, description, content);
    }

    @Override
    public void uploadNewPhoto(Integer newsId, MultipartFile photo) {
        log.debug("Method was invoked");
        newsDAO.uploadNewsPhoto(newsId, photo);
    }

    @Override
    public List<NewsPost> getAll(Integer userId) {
        log.debug("Method was invoked");
        return newsDAO.getAll(userId);
    }

    @Override
    public NewsPost update(NewsPost newsPost) {
        log.debug("Method was invoked");
        if (newsDAO.update(newsPost) == 1) {
            return newsPost;
        } else {
            throw new RequestException("Post isn't found", HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public NewsPost getById(Integer id, Integer userId) {
        log.debug("Method was invoked");
        return newsDAO.findById(id, userId).orElseThrow(() ->
                new RequestException("Post isn't found", HttpStatus.NOT_FOUND));
    }

    @Override
    public Integer delete(Integer id) {
        log.debug("Method was invoked");

        if (newsDAO.delete(id) == 1) {
            return id;
        } else {
            throw new RequestException("Post isn't found", HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public void setNewsViewed(Integer newId, Integer userId) {
        log.debug("Method was invoked");
        newsDAO.setNewsViewed(newId, userId);
    }

    @Override
    public void setNewsLiked(Integer newsId, Integer userId, Boolean isLiked) {
        log.debug("Method was invoked");
        newsDAO.setNewsLiked(newsId, userId, isLiked);
    }

    @Override
    public List<Slider> getNewsFromSlider() {
        log.debug("Method was invoked");
        return newsDAO.getNewsForSlider();
    }

    @Override
    public void setNewsForSlider(Integer sliderId, Integer newsId) {
        log.debug("Method was invoked");
        newsDAO.setNewsForSlider(sliderId, newsId);
    }
}