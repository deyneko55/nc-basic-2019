package edu.netcracker.backend.service;

import edu.netcracker.backend.model.NewsPost;
import edu.netcracker.backend.model.NewsPostId;
import edu.netcracker.backend.model.Slider;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author Dmytro Vovk
 */

public interface NewsService {

    NewsPostId createNewsPost(String header, String description, String content);

    void uploadNewPhoto(Integer newsId, MultipartFile photo);

    List<NewsPost> getAll(Integer userId);

    NewsPost update(NewsPost newsPost);

    NewsPost getById(Integer id, Integer userId);

    Integer delete(Integer id);

    void setNewsViewed(Integer newsId, Integer userId);

    void setNewsLiked(Integer newsId, Integer userId, Boolean isLiked);

    List<Slider> getNewsFromSlider();

    void setNewsForSlider(Integer sliderId, Integer newsId);
}
