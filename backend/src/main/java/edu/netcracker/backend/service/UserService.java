package edu.netcracker.backend.service;

import edu.netcracker.backend.message.request.absencereason.AbsenceReasonFormRequest;
import edu.netcracker.backend.message.request.auth.SignUpByLinkRequest;
import edu.netcracker.backend.message.request.auth.SignUpRequest;
import edu.netcracker.backend.message.request.user.ChangePasswordRequest;
import edu.netcracker.backend.message.request.user.UserChangeStatusRequest;
import edu.netcracker.backend.message.request.user.UserEditRequest;
import edu.netcracker.backend.message.response.*;
import edu.netcracker.backend.model.*;

import java.util.List;

public interface UserService {

    UserResponse createUser(SignUpRequest signUpRequest);

    UserResponse signUpByRegistrationLink(SignUpByLinkRequest signUpRequest);

    void save(User user);

    void changePasswordForUser(User user, ChangePasswordRequest changePasswordRequest);

    void updateUser(UserEditRequest userEditRequest);

    void changeUserStatus(UserChangeStatusRequest userChangeStatusRequest);

    void deleteRegistrationToken(String mailToken);

    boolean existsByEmail(String email);

    boolean existsByUsername(String username);

    String generatePasswordForUser(User user);

    User findByEmail(String email);

    User findById(Integer id);

    UserResponse getUserResponse(Integer userId);

    List<AttendanceDTO> getUserAttendance(Integer userId, Integer groupId);

    List<Group> getUserGroups(Integer userId);

    List<UserResponse> getByUsernameOrRole(String username, List<Long> roles);

    List<UserResponse> getAllUsers();

    void specifyAbsentReason(Integer lessonId, Integer userId, String reason);

    List<String> getUsersSuperiorsEmails(Integer userId);

    List<UserGroupsAndCoursesResponse> getUsersGroupsAndCourses(Integer userId);

    List<UserCompletedCoursesResponse> getUsersCompletedCourses(Integer userId);

    AbsenceReasonResponse addNewAbsenceReason(AbsenceReasonFormRequest absenceReasonFormRequest);

    List<Chat> getUserChats(Integer userId);

    void deleteUserFromDB(Integer userId);

    List<UserLesson> getUsersLessons(Integer userId, Integer courseId);
}
