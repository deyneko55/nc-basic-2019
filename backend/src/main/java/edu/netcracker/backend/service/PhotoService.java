package edu.netcracker.backend.service;

import edu.netcracker.backend.model.Photo;
import org.springframework.web.multipart.MultipartFile;

public interface PhotoService {

    void uploadPhoto(Integer userId, MultipartFile photo);

    void deletePhoto(Integer userId);

    Photo getPhoto(Integer userId);
}
