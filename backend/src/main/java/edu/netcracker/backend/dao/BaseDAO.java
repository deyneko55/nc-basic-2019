package edu.netcracker.backend.dao;

import java.util.List;
import java.util.Optional;

public interface BaseDAO<E> {

    E create(E entity);

    void update(E entity);

    void delete(Integer id);

    Optional<E> findById(Integer id);

     List<E> getAll();
}