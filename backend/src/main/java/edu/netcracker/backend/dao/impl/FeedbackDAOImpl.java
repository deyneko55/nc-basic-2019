package edu.netcracker.backend.dao.impl;

import edu.netcracker.backend.dao.FeedbackDAO;
import edu.netcracker.backend.model.Feedback;
import edu.netcracker.backend.model.FeedbackAboutUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Repository
@PropertySource("classpath:sql/feedbackdao.properties")
public class FeedbackDAOImpl implements FeedbackDAO {

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Value("${INSERT_FEEDBACK}")
    private String INSERT_FEEDBACK;

    @Value("${GET_USER_FEEDBACK}")
    private String GET_USER_FEEDBACK;

    private static final String CONTENT = "content";
    private static final String USER_ID = "userId";
    private static final String TEACHER_ID = "teacherId";

    @Override
    public Feedback create(Feedback feedback) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue(CONTENT, feedback.getContent())
                .addValue(USER_ID, feedback.getUserId())
                .addValue(TEACHER_ID, feedback.getTrainerId());
        namedParameterJdbcTemplate.update(INSERT_FEEDBACK, parameters, keyHolder, new String[]{"id"});
        feedback.setFeedbackId(Objects.requireNonNull(keyHolder.getKey()).intValue());
        return feedback;
    }

    @Override
    public List<FeedbackAboutUser> getUserFeedback(Integer userId) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue(USER_ID, userId);

        return namedParameterJdbcTemplate.query(GET_USER_FEEDBACK, parameterSource,
                new BeanPropertyRowMapper<>(FeedbackAboutUser.class));
    }

    @Override
    public void update(Feedback entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(Integer id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Optional<Feedback> findById(Integer id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<Feedback> getAll() {
        throw new UnsupportedOperationException();
    }

    @Autowired
    public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }
}
