package edu.netcracker.backend.dao.mapper;

import edu.netcracker.backend.model.Trainer;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Dmytro Vovk
 */

public class TrainerMapper implements RowMapper<Trainer> {
    @Override
    public Trainer mapRow(ResultSet resultSet, int i) throws SQLException {
        Trainer trainer = new Trainer();
        trainer.setUserId(resultSet.getInt(1));
        trainer.setQualificationName(resultSet.getString(2));
        return trainer;
    }

}
