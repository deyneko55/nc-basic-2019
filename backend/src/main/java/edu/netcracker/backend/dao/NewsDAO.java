package edu.netcracker.backend.dao;

import edu.netcracker.backend.model.NewsPost;
import edu.netcracker.backend.model.NewsPostId;
import edu.netcracker.backend.model.Slider;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;

/**
 * @author Dmytro Vovk
 */

public interface NewsDAO {

    NewsPostId create(String header, String description, String content);

    void uploadNewsPhoto(Integer newsId, MultipartFile photo);

    Integer update(NewsPost entity);

    Integer delete(Integer id);

    Optional<NewsPost> findById(Integer key, Integer userId);

    List<NewsPost> getAll(Integer userId);

    void setNewsViewed(Integer newsId, Integer userId);

    void setNewsLiked(Integer newsId, Integer userId, Boolean isLiked);

    List<Slider> getNewsForSlider();

    void setNewsForSlider(Integer sliderId, Integer newsId);
}
