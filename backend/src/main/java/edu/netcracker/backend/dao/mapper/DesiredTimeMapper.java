package edu.netcracker.backend.dao.mapper;

import edu.netcracker.backend.model.DesiredTime;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

public class DesiredTimeMapper implements RowMapper<DesiredTime> {

    @Override
    public DesiredTime mapRow(ResultSet resultSet, int i) throws SQLException {
        DesiredTime desiredTime = new DesiredTime();
        desiredTime.setDesiredTimeId(resultSet.getInt(1));
        desiredTime.setUserId(resultSet.getInt(2));
        desiredTime.setUserSchedule(resultSet.getObject(3, LocalDateTime.class));
        return desiredTime;
    }
}
