package edu.netcracker.backend.dao.mapper;

import edu.netcracker.backend.model.Message;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Dmytro Vovk
 */

public class MessageMapper implements RowMapper<Message> {
    @Override
    public Message mapRow(ResultSet resultSet, int i) throws SQLException {
        Message message = new Message();
        message.setMessageId(resultSet.getInt(1));
        message.setMessageSender(resultSet.getString(2));
        message.setCreateDate(resultSet.getTimestamp(3).toLocalDateTime());
        message.setUserId(resultSet.getInt(4));
        message.setChatId(resultSet.getInt(5));
        message.setContent(resultSet.getString(6));
        message.setStatus(resultSet.getBoolean(7));
        return message;
    }
}
