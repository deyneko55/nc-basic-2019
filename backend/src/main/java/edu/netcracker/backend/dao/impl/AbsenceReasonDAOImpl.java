package edu.netcracker.backend.dao.impl;

import edu.netcracker.backend.dao.AbsenceReasonDAO;
import edu.netcracker.backend.model.AbsenceReason;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;
import java.util.Optional;

@Repository
@PropertySource("classpath:sql/absencereasondao.properties")
public class AbsenceReasonDAOImpl implements AbsenceReasonDAO {

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Value("${GET_ALL}")
    private String GET_ALL;

    @Override
    public AbsenceReason create(AbsenceReason entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void update(AbsenceReason entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(Integer id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Optional<AbsenceReason> findById(Integer id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<AbsenceReason> getAll() {
        return namedParameterJdbcTemplate.query(GET_ALL, new BeanPropertyRowMapper<>(AbsenceReason.class));
    }
}
