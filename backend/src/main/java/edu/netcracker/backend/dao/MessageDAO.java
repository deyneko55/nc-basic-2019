package edu.netcracker.backend.dao;

import edu.netcracker.backend.model.Message;

import java.util.List;
import java.util.Optional;

public interface MessageDAO extends BaseDAO<Message> {

    @Override
    Message create(Message entity);

    @Override
    void update(Message entity);

    @Override
    void delete(Integer id);

    @Override
    Optional<Message> findById(Integer id);

    @Override
    List<Message> getAll();

    List<Message> getLastMessagesFromChat(Integer chatId);
}
