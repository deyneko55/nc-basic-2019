package edu.netcracker.backend.dao;

import edu.netcracker.backend.model.Photo;
import org.springframework.web.multipart.MultipartFile;


public interface PhotoDAO {

    void uploadPhoto(MultipartFile photo, Integer id);

    void deletePhoto(Integer id);

    Photo getPhoto(Integer id);
}
