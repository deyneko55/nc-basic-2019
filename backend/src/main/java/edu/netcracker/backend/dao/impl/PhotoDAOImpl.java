package edu.netcracker.backend.dao.impl;

import edu.netcracker.backend.dao.PhotoDAO;
import edu.netcracker.backend.model.Photo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import javax.sql.DataSource;
import java.io.IOException;

@Repository
@PropertySource("classpath:sql/photodao.properties")
public class PhotoDAOImpl implements PhotoDAO {

    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Value("${UPLOAD_PHOTO}")
    private String UPLOAD_PHOTO;

    @Value("${GET_PHOTO}")
    private String GET_PHOTO;

    @Value("${DELETE_PHOTO}")
    private String DELETE_PHOTO;

    @Override
    public void uploadPhoto(MultipartFile photo, Integer id) {
        try {
            jdbcTemplate.update(UPLOAD_PHOTO, photo.getBytes(), id);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deletePhoto(Integer id) {
        jdbcTemplate.update(DELETE_PHOTO, id);
    }

    @Override
    public Photo getPhoto(Integer id) {
        Photo photo = jdbcTemplate.queryForObject(GET_PHOTO, new Object[]{id},
                new BeanPropertyRowMapper<>(Photo.class));
        return photo;
    }

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }
}
