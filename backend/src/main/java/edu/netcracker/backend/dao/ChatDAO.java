package edu.netcracker.backend.dao;

import edu.netcracker.backend.model.Chat;

import java.util.List;
import java.util.Optional;

public interface ChatDAO extends BaseDAO<Chat> {

    @Override
    Chat create(Chat chat);

    @Override
    void update(Chat chat);

    @Override
    void delete(Integer chatId);

    @Override
    Optional<Chat> findById(Integer chatId);

    @Override
    List<Chat> getAll();

    Chat createChatForGroup(Integer groupId);

    void addUserToPrivateChat(Integer userId, Integer chatId);

    List<Integer> getChatUsersIds(Integer chatId);

    Chat findByGroupId(Integer groupId);
}
