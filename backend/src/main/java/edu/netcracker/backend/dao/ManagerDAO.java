package edu.netcracker.backend.dao;

import edu.netcracker.backend.model.User;
import edu.netcracker.backend.model.UserStatistic;

import java.util.List;
import java.util.Optional;

public interface ManagerDAO extends BaseDAO<User> {
    @Override
    User create(User entity);

    @Override
    void update(User entity);

    @Override
    void delete(Integer id);

    @Override
    Optional<User> findById(Integer id);

    @Override
    List<User> getAll();

    void setManager(Integer managerId, Integer userId);

    List<User> getAllUsers(Integer managerId);

    List<UserStatistic> getUserStatistic(Integer managerId);

    Integer getUserManager(Integer userId);

    List<User> getManagerByLastName(String lastName);
}
