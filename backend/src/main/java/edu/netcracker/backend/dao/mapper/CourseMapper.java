package edu.netcracker.backend.dao.mapper;

import edu.netcracker.backend.model.Course;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

@Component
public class CourseMapper implements RowMapper<Course> {
    @Override
    public Course mapRow(ResultSet resultSet, int i) throws SQLException {
        Course course = new Course();
        course.setId(resultSet.getInt(1));
        course.setCourseName(resultSet.getString(2));
        course.setCourseType(resultSet.getString(3));
        course.setStartDate(resultSet.getObject(4, LocalDate.class));
        course.setEndDate(resultSet.getObject(5, LocalDate.class));
        course.setDifficultyLevel(resultSet.getString(6));
        return course;
    }
}
