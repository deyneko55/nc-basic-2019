package edu.netcracker.backend.dao.mapper;

import edu.netcracker.backend.model.NewsPost;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Dmytro Vovk
 */

public class NewsMapper implements RowMapper<NewsPost> {
    @Override
    public NewsPost mapRow(ResultSet resultSet, int i) throws SQLException {
        NewsPost newsPost = new NewsPost();
        newsPost.setNewsPostId(resultSet.getInt(1));
        newsPost.setHeader(resultSet.getString(2));
        newsPost.setDescription(resultSet.getString(3));
        newsPost.setContent(resultSet.getString(4));
        newsPost.setPhoto(resultSet.getBytes(5));
        return newsPost;
    }
}
