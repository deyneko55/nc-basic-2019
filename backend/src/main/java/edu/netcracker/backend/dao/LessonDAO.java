package edu.netcracker.backend.dao;

import edu.netcracker.backend.model.Lesson;
import edu.netcracker.backend.model.PresenceStatus;
import edu.netcracker.backend.model.LessonWithStatus;
import edu.netcracker.backend.model.User;
import edu.netcracker.backend.model.UserStatus;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface LessonDAO extends BaseDAO<Lesson> {

    @Override
    Lesson create(Lesson lesson);

    @Override
    void update(Lesson lesson);

    @Override
    void delete(Integer lessonId);

    @Override
    Optional<Lesson> findById(Integer lessonId);

    @Override
    List<Lesson> getAll();

    void addLessonToCourse(Integer lessonId, Integer courseId);

    void setUserStatus(Integer userId, Integer lessonId, Integer presenceStatusId, Integer absenceReasonId, String explanation);

    void setDateForLessonOfGroup(Integer groupId, Integer lessonId, LocalDateTime lessonDate);

    void deleteUserStatus(Integer userId, Integer lessonId);

    Optional<Lesson> getGroupLesson(Integer lessonId, Integer groupId);

    Optional<Lesson> findByName(String lessonName);

    List<LessonWithStatus> getAllGroupLessons(Integer groupId);

    List<User> findEmployeesByGroupAndLessonId(Integer lessonId, Integer groupId);

    UserStatus getUserStatus(Integer userId, Integer lessonId);

    Integer getCountOfPresentEmployees(Integer lessonId);

    UserStatus getUserStatusFromLesson(Integer lessonId, Integer userId);

    List<PresenceStatus> getAllPresenceStatuses();

    void updateLessonStatusToCanceled(Integer groupId, Integer lessonId);
}
