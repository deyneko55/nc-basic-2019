package edu.netcracker.backend.dao.impl;

import edu.netcracker.backend.dao.GroupDAO;
import edu.netcracker.backend.dao.mapper.AttendanceMapper;
import edu.netcracker.backend.dao.mapper.UserMapper;
import edu.netcracker.backend.message.request.group.GroupSetTrainerRequest;
import edu.netcracker.backend.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@Repository
@PropertySource("classpath:sql/groupdao.properties")
public class GroupDAOImpl implements GroupDAO {

    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Value("${INSERT_NEW_USER_INTO_GROUP}")
    private String INSERT_NEW_USER_INTO_GROUP;

    @Value("${INSERT_NEW_GROUP}")
    private String INSERT_NEW_GROUP;

    @Value("${PRINT_ALL_GROUPS}")
    private String PRINT_ALL_GROUPS;

    @Value("${FIND_GROUP_BY_NAME}")
    private String FIND_GROUP_BY_NAME;

    @Value("${PRINT_ALL_USERS_FROM_GROUP}")
    private String PRINT_ALL_USERS_FROM_GROUP;

    @Value("${DELETE_GROUP}")
    private String DELETE_GROUP;

    @Value("${SET_COURSE_FOR_GROUP}")
    private String SET_COURSE_FOR_GROUP;

    @Value("${PRINT_SCHEDULE}")
    private String PRINT_SCHEDULE;

    @Value("${PRINT_START_DATE}")
    private String PRINT_START_DATE;

    @Value("${FIND_GROUP_BY_ID}")
    private String FIND_GROUP_BY_ID;

    @Value("${FIND_GROUP_BY_CLARIFY_NAME}")
    private String FIND_GROUP_BY_CLARIFY_NAME;

    @Value("${CHANGE_GROUP_NAME}")
    private String CHANGE_GROUP_NAME;

    @Value("${GET_GROUP_STATISTIC_DATA}")
    private String GET_GROUP_STATISTIC_DATA;

    @Value("${GET_GROUP_TRAINERS}")
    private String GET_GROUP_TRAINERS;

    @Value("${COUNT_PRESENT_USERS}")
    private String COUNT_PRESENT_USERS;

    @Value("${COUNT_LESSONS}")
    private String COUNT_LESSONS;

    @Value("${GET_USERS_STATUS_ON_LESSONS}")
    private String GET_USERS_STATUS_ON_LESSONS;

    @Value("${DELETE_USER_FROM_BOOKED_COURSE}")
    private String DELETE_USER_FROM_BOOKED_COURSE;

    @Value("${GET_TRAINER_BY_GROUP_ID}")
    private String GET_TRAINER_BY_GROUP_ID;

    @Value("${GET_GROUPS_BY_USER_ID}")
    private String GET_GROUPS_BY_USER_ID;

    @Value("${GET_TRAINER_ID_FROM_GROUP}")
    private String GET_TRAINER_ID_FROM_GROUP;

    private final String GROUP_NAME = "group_name";
    private final String GROUP_ID = "group_id";
    private final String USER_ID = "user_id";
    private final String COURSE_ID = "courseId";
    private final String NEW_GROUP_NAME = "new_group_name";

    @Override
    public Group create(Group group) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(GROUP_NAME, group.getName())
                .addValue(COURSE_ID, group.getCourseId());
        namedParameterJdbcTemplate.update(INSERT_NEW_GROUP, params, keyHolder, new String[]{"id"});

        group.setId(Objects.requireNonNull(keyHolder.getKey()).intValue());
        if (group.getCourseId() != null) {
            setCourseForGroup(group.getCourseId(), group.getId());
        }
        group.setId(keyHolder.getKey().intValue());
        return group;
    }

    @Override
    public void delete(Integer groupId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(GROUP_ID, groupId);
        namedParameterJdbcTemplate.update(DELETE_GROUP, params);
    }

    @Override
    @Transactional
    public Optional<GroupWithTrainer> findById(Integer groupId) {
        Optional<Group> group = find(FIND_GROUP_BY_ID, groupId);

        if (!group.isPresent()) {
            return Optional.empty();
        }

        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue(GROUP_ID, group.get().getId());

        GroupWithTrainer groupWithTrainer;

        try {
            groupWithTrainer = namedParameterJdbcTemplate.queryForObject(GET_TRAINER_ID_FROM_GROUP, parameterSource,
                    new BeanPropertyRowMapper<>(GroupWithTrainer.class));

            groupWithTrainer.setId(group.get().getId());
            groupWithTrainer.setName(group.get().getName());
            groupWithTrainer.setCourseId(group.get().getCourseId());
        } catch (EmptyResultDataAccessException e) {
            groupWithTrainer = new GroupWithTrainer(group.get().getId(), group.get().getName(), group.get().getCourseId());

            return Optional.of(groupWithTrainer);
        }

        return Optional.of(groupWithTrainer);
    }

    private Optional<Group> find(String query, Object arg) {
        Group group;
        try {
            group = jdbcTemplate.queryForObject(query, new Object[]{arg},
                    new BeanPropertyRowMapper<>(Group.class));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
        if (group == null) {
            return Optional.empty();
        } else {
            return Optional.of(group);
        }
    }

    @Override
    public Optional<User> findTrainerByGroupId(Integer groupId) {
        User user;
        MapSqlParameterSource param = new MapSqlParameterSource();
        param.addValue(GROUP_ID, groupId);
        try {
            user = namedParameterJdbcTemplate.queryForObject(GET_TRAINER_BY_GROUP_ID, param, (resultSet, i) -> {
                User trainer = new User();
                trainer.setId(resultSet.getInt(1));
                trainer.setLogin(resultSet.getString(2));
                trainer.setEmail(resultSet.getString(3));
                trainer.setFirstName(resultSet.getString(4));
                trainer.setLastName(resultSet.getString(5));
                trainer.setRoleId(resultSet.getInt(6));
                return trainer;
            });
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
        if (user != null) {
            return Optional.of(user);
        } else {
            return Optional.empty();
        }
    }

    @Override
    public void addNewUserToGroup(Integer userId, Integer groupId) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue(GROUP_ID, groupId)
                .addValue(USER_ID, userId);
        namedParameterJdbcTemplate.update(INSERT_NEW_USER_INTO_GROUP, parameterSource);
        namedParameterJdbcTemplate.update(DELETE_USER_FROM_BOOKED_COURSE, parameterSource);
    }

    @Override
    public List<Group> findGroupByName(String groupName) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(GROUP_NAME, groupName + "%");
        return namedParameterJdbcTemplate.query(FIND_GROUP_BY_NAME, params, new BeanPropertyRowMapper<>(Group.class));
    }

    @Override
    public List<Group> getGroupsByUserId(Integer userId) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue(USER_ID, userId);
        return namedParameterJdbcTemplate.query(GET_GROUPS_BY_USER_ID, parameterSource,
                new BeanPropertyRowMapper<>(Group.class));
    }

    @Override
    public Optional<Group> findGroupByClarifiedName(String groupName) {
        return find(FIND_GROUP_BY_CLARIFY_NAME, groupName);
    }

    @Override
    public List<Group> getAll() {
        return namedParameterJdbcTemplate.query(PRINT_ALL_GROUPS, new BeanPropertyRowMapper<>(Group.class));
    }

    @Override
    public void setCourseForGroup(Integer courseId, Integer groupId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(COURSE_ID, courseId)
                .addValue(GROUP_ID, groupId);
        namedParameterJdbcTemplate.update(SET_COURSE_FOR_GROUP, params);
    }

    @Override
    public void changeGroupName(Integer id, String newName) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(NEW_GROUP_NAME, newName)
                .addValue(GROUP_ID, id);
        namedParameterJdbcTemplate.update(CHANGE_GROUP_NAME, params);
    }

    @Override
    public void update(Group group) {
    }

    @Override
    public List<String> getSchedule(Integer id) {
        List<String> dates = new ArrayList<>();

        Course course = jdbcTemplate.queryForObject(PRINT_SCHEDULE, new Object[]{id},
                new BeanPropertyRowMapper<>(Course.class));

        if (course != null) {
            dates.add(course.getStartDate().toString());
            dates.add(course.getEndDate().toString());
            return dates;
        } else {
            return null;
        }

    }

    private Map<Integer, Integer> statsMap(String query, String columnLabel) {
        return jdbcTemplate.query(query, (ResultSetExtractor<Map>) rs -> map(rs, columnLabel));
    }

    private HashMap<Integer, Integer> map(ResultSet rs, String columnLabel) throws SQLException {
        HashMap<Integer, Integer> mapRet = new HashMap<>();
        while (rs.next()) {
            mapRet.put(rs.getInt("groupId"),
                    rs.getInt(columnLabel));
        }
        return mapRet;
    }

    @Override
    public List<GroupStatistic> getGroupStatistic() {
        List<GroupStatistic> groupStatistics = namedParameterJdbcTemplate.query(GET_GROUP_STATISTIC_DATA, new BeanPropertyRowMapper<>(GroupStatistic.class));
        Map<Integer, Integer> trainers = statsMap(GET_GROUP_TRAINERS, "userId");
        Map<Integer, Integer> presentUsers = statsMap(COUNT_PRESENT_USERS, "pres");
        Map<Integer, Integer> allLessons = statsMap(COUNT_LESSONS, "all");
        Map<Integer, Double> resStats = new HashMap<>();
        for (Map.Entry<Integer, Integer> pU : presentUsers.entrySet()) {
            for (Map.Entry<Integer, Integer> aL : allLessons.entrySet()) {
                if (pU.getKey().equals(aL.getKey())) {
                    resStats.put(pU.getKey(), Double.valueOf(pU.getValue())
                            / Double.valueOf(aL.getValue()));
                }
            }
        }
        for (GroupStatistic groupStatistic : groupStatistics) {
            for (Map.Entry<Integer, Integer> tr : trainers.entrySet()) {
                if (groupStatistic.getGroupId().equals(tr.getKey())) {
                    groupStatistic.setTrainerId(tr.getValue());
                }
            }
            for (Map.Entry<Integer, Double> rS : resStats.entrySet()) {
                if (groupStatistic.getGroupId().equals(rS.getKey())) {
                    groupStatistic.setAttendancePercentage(rS.getValue());
                }
            }

        }
        return groupStatistics;
    }

    @Override
    public List<User> getAllUsersFromGroup(String groupName) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(GROUP_NAME, groupName);
        return namedParameterJdbcTemplate.query(PRINT_ALL_USERS_FROM_GROUP, params, new UserMapper());
    }

    @Override
    public void assignTrainerToTheGroup(GroupSetTrainerRequest trainerForGroup) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(USER_ID, trainerForGroup.getTrainerId())
                .addValue(GROUP_ID, trainerForGroup.getGroupId());

        namedParameterJdbcTemplate.update(INSERT_NEW_USER_INTO_GROUP, params, keyHolder, new String[]{GROUP_ID});
    }

    @Override
    public List<Attendance> getGroupAttendance(Integer groupId) {
        MapSqlParameterSource param = new MapSqlParameterSource();
        param.addValue(GROUP_ID, groupId);

        return namedParameterJdbcTemplate.query(GET_USERS_STATUS_ON_LESSONS, param, new AttendanceMapper());
    }

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }
}
