package edu.netcracker.backend.dao.mapper;

import edu.netcracker.backend.model.Attendance;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;


public class AttendanceMapper implements RowMapper<Attendance> {

    @Override
    public Attendance mapRow(ResultSet resultSet, int i) throws SQLException {
        Attendance attendance = new Attendance();
        attendance.setGroupId(resultSet.getInt(1));
        attendance.setLessonName(resultSet.getString(2));
        attendance.setLessonDate(resultSet.getObject(3, LocalDateTime.class));
        attendance.setUserId(resultSet.getInt(4));
        attendance.setUserFullName(resultSet.getString(5));
        attendance.setUserPresenceStatus(resultSet.getString(6));
        attendance.setAbsenceReason(resultSet.getString(7));
        return attendance;
    }
}
