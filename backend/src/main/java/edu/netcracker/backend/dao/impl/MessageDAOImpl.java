package edu.netcracker.backend.dao.impl;

import edu.netcracker.backend.dao.MessageDAO;
import edu.netcracker.backend.dao.mapper.MessageMapper;
import edu.netcracker.backend.model.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Repository
@PropertySource("classpath:sql/chatdao.properties")
public class MessageDAOImpl implements MessageDAO {

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Value("${GET_LAST_500_MESSAGES}")
    private String GET_LAST_500_MESSAGES;

    @Value("${SAVE_NEW_MESSAGE}")
    private String SAVE_NEW_MESSAGE;


    private static final String CHAT_ID = "chat_id";
    private static final String USER_ID = "user_id";
    private static final String SEND_DATE = "send_date";
    private static final String CONTENT = "content";
    private static final String STATUS = "status";

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public Message create(Message entity) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        KeyHolder keyHolder = new GeneratedKeyHolder();
        params.addValue(CHAT_ID, entity.getChatId())
                .addValue(USER_ID, entity.getUserId())
                .addValue(CONTENT, entity.getContent())
                .addValue(STATUS, entity.getStatus());
        namedParameterJdbcTemplate.update(SAVE_NEW_MESSAGE, params, keyHolder, new String[]{"id"});
        entity.setMessageId(Objects.requireNonNull(keyHolder.getKey()).intValue());
        entity.setCreateDate(LocalDateTime.now());
        return entity;
    }

    @Override
    public void update(Message entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(Integer id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Optional<Message> findById(Integer id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<Message> getAll() {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<Message> getLastMessagesFromChat(Integer chatId) {
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue(CHAT_ID, chatId);
        return namedParameterJdbcTemplate.query(GET_LAST_500_MESSAGES, parameters, new MessageMapper());
    }
}
