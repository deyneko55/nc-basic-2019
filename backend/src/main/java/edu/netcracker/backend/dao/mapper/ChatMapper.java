package edu.netcracker.backend.dao.mapper;

import edu.netcracker.backend.model.Chat;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Dmytro Vovk
 */

public class ChatMapper implements RowMapper<Chat> {
    @Override
    public Chat mapRow(ResultSet resultSet, int i) throws SQLException {
        Chat chat = new Chat();
        chat.setChatId(resultSet.getInt(1));
        chat.setChatName(resultSet.getString(2));
        return chat;
    }
}
