package edu.netcracker.backend.dao.mapper;

import edu.netcracker.backend.model.LessonWithStatus;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

public class LessonWithStatusMapper implements RowMapper<LessonWithStatus> {

    @Override
    public LessonWithStatus mapRow(ResultSet resultSet, int i) throws SQLException {
        LessonWithStatus lesson = new LessonWithStatus();
        lesson.setId(resultSet.getInt(1));
        lesson.setLessonName(resultSet.getString(2));
        lesson.setCourseId(resultSet.getInt(3));
        lesson.setLessonDate(resultSet.getObject(4, LocalDateTime.class));
        lesson.setLessonStatus(resultSet.getString(5));
        return lesson;
    }
}
