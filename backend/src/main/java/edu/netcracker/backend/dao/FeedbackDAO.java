package edu.netcracker.backend.dao;

import edu.netcracker.backend.model.Feedback;
import edu.netcracker.backend.model.FeedbackAboutUser;

import java.util.List;
import java.util.Optional;

public interface FeedbackDAO extends BaseDAO<Feedback> {
    @Override
    Feedback create(Feedback feedback);

    @Override
    void update(Feedback entity);

    @Override
    void delete(Integer id);

    @Override
    Optional<Feedback> findById(Integer id);

    @Override
    List<Feedback> getAll();

    List<FeedbackAboutUser> getUserFeedback(Integer userId);
}
