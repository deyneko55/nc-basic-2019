package edu.netcracker.backend.dao.mapper;

import edu.netcracker.backend.model.Lesson;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

public class LessonMapper implements RowMapper<Lesson> {

    @Override
    public Lesson mapRow(ResultSet resultSet, int i) throws SQLException {
        Lesson lesson = new Lesson();
        lesson.setId(resultSet.getInt(1));
        lesson.setLessonName(resultSet.getString(2));
        lesson.setCourseId(resultSet.getInt(3));
        lesson.setLessonDate(resultSet.getObject(4, LocalDateTime.class));
        return lesson;
    }
}
