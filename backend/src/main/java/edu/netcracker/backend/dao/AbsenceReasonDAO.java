package edu.netcracker.backend.dao;

import edu.netcracker.backend.model.AbsenceReason;

import java.util.List;
import java.util.Optional;

public interface AbsenceReasonDAO extends BaseDAO<AbsenceReason> {


    @Override
    AbsenceReason create(AbsenceReason entity);

    @Override
    void update(AbsenceReason entity);

    @Override
    void delete(Integer id);

    @Override
    Optional<AbsenceReason> findById(Integer id);

    @Override
    List<AbsenceReason> getAll();
}
