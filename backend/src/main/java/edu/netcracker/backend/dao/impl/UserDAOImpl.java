package edu.netcracker.backend.dao.impl;

import edu.netcracker.backend.dao.UserDAO;
import edu.netcracker.backend.dao.mapper.AttendanceMapper;
import edu.netcracker.backend.dao.mapper.ChatMapper;
import edu.netcracker.backend.dao.mapper.UserLessonMapper;
import edu.netcracker.backend.model.*;
import edu.netcracker.backend.message.response.UserCompletedCoursesResponse;
import edu.netcracker.backend.message.response.UserGroupsAndCoursesResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Repository
@PropertySource("classpath:sql/userdao.properties")
public class UserDAOImpl implements UserDAO {

    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Value("${FIND_BY_USERNAME}")
    private String FIND_BY_USERNAME;

    @Value("${FIND_BY_EMAIL}")
    private String FIND_BY_EMAIL;

    @Value("${FIND_ALL_USER_ROLES}")
    private String FIND_ALL_USER_ROLES;

    @Value("${INSERT_INTO_USER}")
    private String INSERT_INTO_USER;

    @Value("${INSERT_INTO_USER_ROLE}")
    private String INSERT_INTO_USER_ROLE;

    @Value("${FIND_USER_BY_ID}")
    private String FIND_USER_BY_ID;

    @Value("${FIND_MANAGER_ID_BY_USER_ID}")
    private String FIND_MANAGER_ID_BY_USER_ID;

    @Value("${UPDATE_USER_INFO}")
    private String UPDATE_USER_INFO;

    @Value("${GET_ALL_USERS_BY_ROLE_IDS}")
    private String GET_ALL_USERS_BY_ROLE_IDS;

    @Value("${FIND_USERS_BY_USERNAME}")
    private String FIND_USERS_BY_USERNAME;

    @Value("${FIND_USERS_BY_USERNAME_AND_ROLE}")
    private String FIND_USERS_BY_USERNAME_AND_ROLE;

    @Value("${GET_ALL_USERS}")
    private String GET_ALL_USERS;

    @Value("${UPDATE_USER_PASSWORD}")
    private String UPDATE_USER_PASSWORD;

    @Value("${GET_USER_GROUPS}")
    private String GET_USER_GROUPS;

    @Value("${UPDATE_ABSENT_REASON}")
    private String UPDATE_ABSENT_REASON;

    @Value("${GET_USERS_SUPERIORS_EMAILS}")
    private String GET_USERS_SUPERIORS_EMAILS;

    @Value("${INSERT_NEW_ABSENCE_REASON}")
    private String INSERT_NEW_ABSENCE_REASON;

    @Value("${GET_ALL_ABSENCE_REASONS}")
    private String GET_ALL_ABSENCE_REASONS;

    @Value("${GET_USERS_GROUPS_AND_COURSES}")
    private String GET_USERS_GROUPS_AND_COURSES;

    @Value("${GET_USERS_PASSED_COURSES}")
    private String GET_USERS_PASSED_COURSES;

    @Value("${GET_USER_CHATS}")
    private String GET_USER_CHATS;

    @Value("${DELETE_USER_FROM_DB}")
    private String DELETE_USER_FROM_DB;

    @Value("${GET_USERS_LESSONS_AND_STATUSES}")
    private String GET_USERS_LESSONS_AND_STATUSES;

    @Value("${GET_USER_STATUSES_IN_LESSONS_OF_GROUP}")
    private String GET_USER_STATUSES_IN_LESSONS_OF_GROUP;

    private static final String USER_ID = "user_id";
    private static final String COURSE_ID = "course_id";
    private static final String GROUP_ID = "group_id";
    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";
    private static final String FIRST_NAME = "first_name";
    private static final String LAST_NAME = "last_name";
    private static final String EMAIL = "email";
    private static final String REASON = "reason";

    @Override
    public User create(User user) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(LOGIN, user.getLogin())
                .addValue(PASSWORD, user.getPassword())
                .addValue(FIRST_NAME, user.getFirstName())
                .addValue(LAST_NAME, user.getLastName())
                .addValue(EMAIL, user.getEmail());
        namedParameterJdbcTemplate.update(INSERT_INTO_USER, params, keyHolder, new String[]{"id"});

        user.setId(Objects.requireNonNull(keyHolder.getKey()).intValue());
        insertIntoUserRole(user.getId(), user.getRoleId());
        return user;
    }

    @Override
    public Optional<User> findByEmail(String email) {
        try {
            User user = jdbcTemplate.queryForObject(FIND_BY_EMAIL, new Object[]{email},
                    new BeanPropertyRowMapper<>(User.class));
            if (user != null) {
                return addRoles(user);
            } else {
                return Optional.empty();
            }
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public Optional<User> findByUsername(String username) {
        try {
            User user = jdbcTemplate.queryForObject(FIND_BY_USERNAME, new Object[]{username},
                    new BeanPropertyRowMapper<>(User.class));
            if (user == null) {
                return Optional.empty();
            } else {
                return addRoles(user);
            }
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public void update(User user) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(USER_ID, user.getId())
                .addValue(LOGIN, user.getLogin())
                .addValue(FIRST_NAME, user.getFirstName())
                .addValue(LAST_NAME, user.getLastName())
                .addValue(EMAIL, user.getEmail());
        namedParameterJdbcTemplate.update(UPDATE_USER_INFO, params);
    }

    @Override
    public void delete(Integer userId) {
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue(USER_ID, userId);
        namedParameterJdbcTemplate.update(DELETE_USER_FROM_DB, parameters);
    }

    @Override
    public Optional<User> findById(Integer userId) {
        User user = null;
        try {
            user = jdbcTemplate.queryForObject(FIND_USER_BY_ID, new Object[]{userId},
                    new BeanPropertyRowMapper<>(User.class));
        } catch (EmptyResultDataAccessException e) {
            e.printStackTrace();
        }

        if (user == null) {
            return Optional.empty();
        } else {
            return Optional.of(user);
        }
    }

    @Override
    public List<User> getAll() {
        return namedParameterJdbcTemplate.query(GET_ALL_USERS, new BeanPropertyRowMapper<>(User.class));
    }

    @Override
    public void updateUserPassword(User user) {
        jdbcTemplate.update(UPDATE_USER_PASSWORD, user.getPassword(), user.getEmail());
    }

    @Override
    public List<User> getUsersByLoginOrRole(String login, List<Long> roleIds) {
        List<User> users;
        MapSqlParameterSource params = new MapSqlParameterSource();

        if (login == null) {
            params.addValue("ids", roleIds);
            users = namedParameterJdbcTemplate.query(GET_ALL_USERS_BY_ROLE_IDS, params, new BeanPropertyRowMapper<>(User.class));
            return users;
        } else if (roleIds == null) {
            params.addValue(LOGIN, login + "%");
            users = namedParameterJdbcTemplate.query(FIND_USERS_BY_USERNAME, params, new BeanPropertyRowMapper<>(User.class));
            return users;
        } else {
            params.addValue(LOGIN, login + "%")
                    .addValue("ids", roleIds);
            users = namedParameterJdbcTemplate.query(FIND_USERS_BY_USERNAME_AND_ROLE, params, new BeanPropertyRowMapper<>(User.class));
        }

        return users;
    }

    @Override
    public List<Group> getUserGroups(Integer userId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(USER_ID, userId);
        return null;
    }

    @Override
    public void specifyAbsentReason(Integer lessonId, Integer userId, String reason) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("userId", userId)
                .addValue("lessonId", lessonId)
                .addValue("reason", reason);
        namedParameterJdbcTemplate.update(UPDATE_ABSENT_REASON, params);
    }

    @Override
    public List<String> getUsersSuperiorsEmails(Integer userId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("userId", userId);
        List<String> emails = namedParameterJdbcTemplate.queryForList(GET_USERS_SUPERIORS_EMAILS, params, String.class);
        return emails;
    }

    private void insertIntoUserRole(Integer userId, Integer roleId) {
        jdbcTemplate.update(INSERT_INTO_USER_ROLE, userId, roleId);
    }

    private Optional<User> addRoles(User user) {
        String role = jdbcTemplate.queryForObject(FIND_ALL_USER_ROLES, String.class, user.getId());
        user.setRole(role);
        return Optional.of(user);
    }

    @Override
    public List<String> getAllAbsenceReasons() {
        return null;
    }

    @Override
    public AbsenceReason addAbsenceReason(AbsenceReason absenceReason) {
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        KeyHolder keyHolder = new GeneratedKeyHolder();
        parameters.addValue(REASON, absenceReason.getAbsenceReasonName());
        namedParameterJdbcTemplate.update(INSERT_NEW_ABSENCE_REASON, parameters, keyHolder, new String[]{"id"});
        absenceReason.setAbsenceReasonId(Objects.requireNonNull(keyHolder.getKey()).intValue());
        return absenceReason;
    }

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public List<UserGroupsAndCoursesResponse> getUsersGroupsAndCourses(Integer userId) {
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("userId", userId);
        return namedParameterJdbcTemplate.query(GET_USERS_GROUPS_AND_COURSES, sqlParameterSource, new BeanPropertyRowMapper<>(UserGroupsAndCoursesResponse.class));
    }

    @Override
    public List<UserCompletedCoursesResponse> getCompletedCourses(Integer userId) {
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("userId", userId);
        return namedParameterJdbcTemplate.query(GET_USERS_PASSED_COURSES, sqlParameterSource, new BeanPropertyRowMapper<>(UserCompletedCoursesResponse.class));
    }

    @Override
    public String getEmailByUserId(Integer userId) {
        User user = new User();
        try {
            user = jdbcTemplate.queryForObject(FIND_USER_BY_ID, new Object[]{userId},
                    new BeanPropertyRowMapper<>(User.class));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return user.getEmail();
    }

    @Override
    public Integer getUsersManagerIdByUserId(Integer userId) {
        return jdbcTemplate.queryForObject(FIND_MANAGER_ID_BY_USER_ID, new Object[]{userId}, Integer.class);
    }

    @Override
    public List<Chat> getUserChats(Integer userId) {
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue(USER_ID, userId);
        return namedParameterJdbcTemplate.query(GET_USER_CHATS, sqlParameterSource, new ChatMapper());
    }

    @Override
    public List<Attendance> getUserAttendance(Integer userId, Integer groupId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(GROUP_ID, groupId)
                .addValue(USER_ID, userId);

        return namedParameterJdbcTemplate.query(GET_USER_STATUSES_IN_LESSONS_OF_GROUP,
                params, new AttendanceMapper());
    }

    @Override
    public List<UserLesson> getUsersLessons(Integer userId, Integer courseId) {
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue(USER_ID, userId);
        sqlParameterSource.addValue(COURSE_ID, courseId);
        return namedParameterJdbcTemplate.query(GET_USERS_LESSONS_AND_STATUSES, sqlParameterSource, new UserLessonMapper());
    }
}
