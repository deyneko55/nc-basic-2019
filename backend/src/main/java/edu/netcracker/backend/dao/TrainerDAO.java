package edu.netcracker.backend.dao;

import edu.netcracker.backend.message.request.trainer.TrainerSetQualRequest;
import edu.netcracker.backend.model.*;

import java.util.List;
import java.util.Optional;

public interface TrainerDAO extends BaseDAO<User> {
    @Override
    User create(User user);

    @Override
    void update(User user);

    @Override
    void delete(Integer trainerId);

    @Override
    Optional<User> findById(Integer trainerId);

    @Override
    List<User> getAll();

    List<Group> getTrainerGroups(Integer trainerId);

    List<Course> getTrainerCourses(Integer trainerId);

    List<User> getAllTrainerStudents(Integer trainerId);

    List<User> getAllTrainerFromCourse(Integer courseId);

    List<User> getTrainerByLastName(String lastName);

    List<TrainerQualificationLevel> getAllQualificationLevels();

    Integer setTrainerQualification(TrainerSetQualRequest trainerSetQualRequest);

    Integer updateQualification(TrainerSetQualRequest trainerSetQualRequest);

    List<Trainer> getTrainersWithLevels();

    void createQualificationLevel(String newQualificationLevel);

    void changeTrainerForGroup(Integer groupId, Integer oldTrainerId, Integer newTrainerId);

    List<TrainerInfo> getUserTrainers(Integer userId);

    Optional<TrainerInfo> getAllInfoAboutTrainer(Integer trainerId);
}
