package edu.netcracker.backend.dao.mapper;

import edu.netcracker.backend.message.response.CourseTypeStatsResponse;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Dmytro Vovk
 */

public class CourseTypeStatsMapper implements RowMapper<CourseTypeStatsResponse> {
    @Override
    public CourseTypeStatsResponse mapRow(ResultSet resultSet, int i) throws SQLException {
        CourseTypeStatsResponse response = new CourseTypeStatsResponse();
        response.setDifficultyLevel(resultSet.getString(1));
        response.setEntitiesCount(resultSet.getInt(2));
        return response;
    }
}
