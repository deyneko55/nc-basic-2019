package edu.netcracker.backend.dao.mapper;

import edu.netcracker.backend.model.Notification;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Dmytro Vovk
 */

public class NotificationMapper implements RowMapper<Notification> {
    @Override
    public Notification mapRow(ResultSet resultSet, int i) throws SQLException {
        Notification notification = new Notification();
        notification.setNotificationId(resultSet.getInt(1));
        notification.setRecipientId(resultSet.getInt(2));
        notification.setContent(resultSet.getString(3));
        notification.setIsRead(resultSet.getBoolean(4));
        return notification;
    }
}