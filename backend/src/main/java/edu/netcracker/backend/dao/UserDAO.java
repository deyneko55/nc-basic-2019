package edu.netcracker.backend.dao;

import edu.netcracker.backend.model.*;
import edu.netcracker.backend.message.response.UserCompletedCoursesResponse;
import edu.netcracker.backend.message.response.UserGroupsAndCoursesResponse;

import java.util.List;
import java.util.Optional;

public interface UserDAO extends BaseDAO<User> {

    User create(User user);

    void update(User user);

    void delete(Integer userId);

    Optional<User> findById(Integer userId);

    Optional<User> findByEmail(String email);

    Optional<User> findByUsername(String username);

    List<User> getAll();

    void updateUserPassword(User user);

    List<User> getUsersByLoginOrRole(String login, List<Long> roleIds);

    List<Group> getUserGroups(Integer userId);

    void specifyAbsentReason(Integer lessonId, Integer userId, String reason);

    List<String> getUsersSuperiorsEmails(Integer userId);

    List<String> getAllAbsenceReasons();

    AbsenceReason addAbsenceReason(AbsenceReason absenceReason);

    List<UserGroupsAndCoursesResponse> getUsersGroupsAndCourses(Integer userId);

    List<UserCompletedCoursesResponse> getCompletedCourses(Integer userId);

    String getEmailByUserId(Integer userId);

    Integer getUsersManagerIdByUserId(Integer userId);

    List<Chat> getUserChats(Integer userId);

    List<UserLesson> getUsersLessons(Integer userId, Integer courseId);

    List<Attendance> getUserAttendance(Integer userId, Integer groupId);
}
