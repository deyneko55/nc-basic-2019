package edu.netcracker.backend.dao.impl;

import edu.netcracker.backend.dao.NewsDAO;
import edu.netcracker.backend.dao.mapper.NewsMapper;
import edu.netcracker.backend.dao.mapper.SliderMapper;
import edu.netcracker.backend.exception.RequestException;
import edu.netcracker.backend.model.NewsPost;
import edu.netcracker.backend.model.NewsPostId;
import edu.netcracker.backend.model.Slider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import javax.sql.DataSource;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @author Dmytro Vovk
 */

@Repository
@PropertySource("classpath:sql/newsdao.properties")
public class NewsDAOImpl implements NewsDAO {

    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    private final String POST_ID = "id";
    private final String HEADER = "header";
    private final String DESCRIPTION = "description";
    private final String CONTENT = "content";
    private final String PICTURE = "picture";
    private final String USER_ID = "userId";
    private final String NEWS_ID = "newsId";
    private final String SLIDER_ID = "sliderId";

    @Value("${CREATE_POST}")
    private String CREATE_POST;

    @Value("${ADD_PHOTO_FOR_POST}")
    private String ADD_PHOTO_FOR_POST;

    @Value("${UPDATE_POST}")
    private String UPDATE_POST;

    @Value("${GET_BY_ID}")
    private String GET_BY_ID;

    @Value("${GET_ALL_NEWS}")
    private String GET_ALL_NEWS;

    @Value("${DELETE_POST}")
    private String DELETE_POST;

    @Value("${GET_USER_VIEW_NEWS}")
    private String GET_USER_VIEW_NEWS;

    @Value("${SET_NEWS_VIEWED}")
    private String SET_NEWS_VIEWED;

    @Value("${GET_NEWS_IS_LIKED}")
    private String GET_NEWS_IS_LIKED;

    @Value("${SET_NEWS_LIKED}")
    private String SET_NEWS_LIKED;

    @Value("${DELETE_NEWS_LIKE}")
    private String DELETE_NEWS_LIKE;

    @Value("${GET_NEWS_FOR_SLIDER}")
    private String GET_NEWS_FOR_SLIDER;

    @Value("${UPDATE_NEWS_FOR_SLIDER}")
    private String UPDATE_NEWS_FOR_SLIDER;

    @Value("${GET_COUNT_OF_LIKED_NEWS}")
    private String GET_COUNT_OF_LIKED_NEWS;

    @Value("${GET_COUNT_OF_VIEWED_NEWS}")
    private String GET_COUNT_OF_VIEWED_NEWS;

    @Value("${GET_USER_ID_OF_LIKED_NEWS}")
    private String GET_USER_ID_OF_LIKED_NEWS;

    @Override
    public NewsPostId create(String header, String description, String content) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(HEADER, header)
                .addValue(DESCRIPTION, description)
                .addValue(CONTENT, content);

        namedParameterJdbcTemplate.update(CREATE_POST, params, keyHolder, new String[]{"id"});

        NewsPostId newsPostId = new NewsPostId();
        newsPostId.setNewsPostId(Objects.requireNonNull(keyHolder.getKey()).intValue());
        return newsPostId;
    }

    @Override
    public void uploadNewsPhoto(Integer newsId, MultipartFile picture) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        try {
            params.addValue(POST_ID, newsId)
                    .addValue(PICTURE, picture.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }

        namedParameterJdbcTemplate.update(ADD_PHOTO_FOR_POST, params);
    }

    @Override
    public Integer update(NewsPost entity) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(POST_ID, entity.getNewsPostId())
                .addValue(HEADER, entity.getHeader())
                .addValue(DESCRIPTION, entity.getDescription())
                .addValue(CONTENT, entity.getContent())
                .addValue(PICTURE, entity.getPhoto());

        return namedParameterJdbcTemplate.update(UPDATE_POST, params);
    }

    @Override
    public Integer delete(Integer id) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(POST_ID, id);
        return namedParameterJdbcTemplate.update(DELETE_POST, params);
    }

    @Override
    public Optional<NewsPost> findById(Integer key, Integer userId) {
        NewsPost newsPost = null;
        try {
            newsPost = jdbcTemplate.queryForObject(GET_BY_ID, new Object[]{key}, new NewsMapper());
            Objects.requireNonNull(newsPost).setNumberOfLikes(getCountOfLikedNews(key));
            newsPost.setNumberOfViews(getCountOfViewedNews(key));

            if (getUserIdOfLikedNews(userId, newsPost.getNewsPostId()) == null) {
                newsPost.setLikedByCurrentUser(false);
            } else {
                newsPost.setLikedByCurrentUser(true);
            }
        } catch (EmptyResultDataAccessException e) {
            e.printStackTrace();
        }

        if (newsPost == null) {
            throw new RequestException("Post isn't found", HttpStatus.NOT_FOUND);
        } else {
            return Optional.of(newsPost);
        }
    }

    @Override
    public List<NewsPost> getAll(Integer userId) {
        List<NewsPost> newsPosts = jdbcTemplate.query(GET_ALL_NEWS, new NewsMapper());

        for (NewsPost newsPost : newsPosts) {
            Integer numberOfLikes = getCountOfLikedNews(newsPost.getNewsPostId());
            Integer numberOfViews = getCountOfViewedNews(newsPost.getNewsPostId());

            newsPost.setNumberOfLikes(numberOfLikes);
            newsPost.setNumberOfViews(numberOfViews);

            if (getUserIdOfLikedNews(userId, newsPost.getNewsPostId()) == null) {
                newsPost.setLikedByCurrentUser(false);
            } else {
                newsPost.setLikedByCurrentUser(true);
            }
        }
        return newsPosts;
    }

    @Override
    public void setNewsViewed(Integer newsId, Integer userId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(USER_ID, userId);
        params.addValue(NEWS_ID, newsId);
        if (!namedParameterJdbcTemplate
                .queryForObject(GET_USER_VIEW_NEWS, params, Boolean.class)) {
            namedParameterJdbcTemplate.update(SET_NEWS_VIEWED, params);
        }
    }

    @Override
    public void setNewsLiked(Integer newsId, Integer userId, Boolean isLiked) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(USER_ID, userId);
        params.addValue(NEWS_ID, newsId);
        if (!isLiked && namedParameterJdbcTemplate
                .queryForObject(GET_NEWS_IS_LIKED, params, Boolean.class)) {
            namedParameterJdbcTemplate.update(DELETE_NEWS_LIKE, params);
        }
        if (isLiked && !namedParameterJdbcTemplate
                .queryForObject(GET_NEWS_IS_LIKED, params, Boolean.class)) {
            namedParameterJdbcTemplate.update(SET_NEWS_LIKED, params);
        }
    }

    @Override
    public List<Slider> getNewsForSlider() {
        return namedParameterJdbcTemplate.query(GET_NEWS_FOR_SLIDER, new SliderMapper());
    }

    @Override
    public void setNewsForSlider(Integer sliderId, Integer newsId) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue(SLIDER_ID, sliderId)
                .addValue(NEWS_ID, newsId);
        namedParameterJdbcTemplate.update(UPDATE_NEWS_FOR_SLIDER, parameterSource);
    }

    private Integer getCountOfLikedNews(Integer newsId) {
        return jdbcTemplate.queryForObject(GET_COUNT_OF_LIKED_NEWS, new Object[]{newsId}, Integer.class);
    }

    private Integer getCountOfViewedNews(Integer newsId) {
        return jdbcTemplate.queryForObject(GET_COUNT_OF_VIEWED_NEWS, new Object[]{newsId}, Integer.class);
    }

    private Integer getUserIdOfLikedNews(Integer userId, Integer newsId) {
        Integer currentUserId;
        try {
            currentUserId = jdbcTemplate.queryForObject(GET_USER_ID_OF_LIKED_NEWS,
                    new Object[]{userId, newsId}, Integer.class);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
        if (currentUserId == null) {
            return null;
        } else {
            return currentUserId;
        }
    }
}
