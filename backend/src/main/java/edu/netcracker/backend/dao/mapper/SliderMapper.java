package edu.netcracker.backend.dao.mapper;

import edu.netcracker.backend.model.Slider;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SliderMapper implements RowMapper<Slider> {

    @Override
    public Slider mapRow(ResultSet resultSet, int i) throws SQLException {
        Slider slider = new Slider();
        slider.setNewsId(resultSet.getInt("news_id"));
        slider.setSliderId(resultSet.getInt("id"));
        return slider;
    }
}
