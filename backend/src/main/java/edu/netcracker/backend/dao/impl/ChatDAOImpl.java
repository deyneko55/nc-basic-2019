package edu.netcracker.backend.dao.impl;

import edu.netcracker.backend.dao.ChatDAO;
import edu.netcracker.backend.dao.GroupDAO;
import edu.netcracker.backend.dao.mapper.ChatMapper;
import edu.netcracker.backend.model.Chat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Repository
@PropertySource("classpath:sql/chatdao.properties")
public class ChatDAOImpl implements ChatDAO {

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private JdbcTemplate jdbcTemplate;
    private GroupDAO groupDAO;

    private final String GROUP_ID = "groupId";
    private final String CHAT_ID = "chatId";
    private final String USER_ID = "userId";
    private final String CHAT_NAME = "chatName";

    @Value("${INSERT_GROUP_TO_THEIR_CHAT}")
    private String INSERT_GROUP_TO_THEIR_CHAT;

    @Value("${INSERT_STUDENTS_TO_CHAT}")
    private String INSERT_STUDENTS_TO_CHAT;

    @Value("${GET_CHAT_NAME}")
    private String GET_CHAT_NAME;

    @Value("${CREATE_CHAT}")
    private String CREATE_CHAT;

    @Value("${ADD_USER_TO_CHAT}")
    private String ADD_USER_TO_CHAT;

    @Value("${GET_ALL_USERS_FROM_CHAT}")
    private String GET_ALL_USERS_FROM_CHAT;

    @Value("${GET_CHAT_BY_GROUP_ID}")
    private String GET_CHAT_BY_GROUP_ID;


    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);

    }

    @Override
    public Chat create(Chat chat) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue(CHAT_NAME, chat.getChatName());
        namedParameterJdbcTemplate.update(CREATE_CHAT, parameters, keyHolder, new String[]{"id"});
        chat.setChatId(Objects.requireNonNull(keyHolder.getKey()).intValue());
        return chat;
    }

    @Override
    public void update(Chat chat) {

    }

    @Override
    public void delete(Integer chatId) {

    }

    @Override
    public Optional<Chat> findById(Integer chatId) {
        return null;
    }

    public Chat findByGroupId(Integer groupId) {
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue(GROUP_ID, groupId);
        return namedParameterJdbcTemplate.queryForObject(GET_CHAT_BY_GROUP_ID, parameters,
                new ChatMapper());
    }

    @Override
    public List<Chat> getAll() {
        return null;
    }

    @Override
    public Chat createChatForGroup(Integer groupId) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue(GROUP_ID, groupId);
        namedParameterJdbcTemplate.update(INSERT_GROUP_TO_THEIR_CHAT, parameters, keyHolder, new String[]{"id"});

        Integer chatId = Objects.requireNonNull(keyHolder.getKey()).intValue();
        parameters.addValue(CHAT_ID, chatId);

        Chat chat = new Chat();
        chat.setChatId(chatId);
        chat.setChatName(getChatName(parameters));

        namedParameterJdbcTemplate.update(INSERT_STUDENTS_TO_CHAT, parameters);

        return chat;
    }

    private String getChatName(MapSqlParameterSource params) {
        return namedParameterJdbcTemplate.queryForObject(GET_CHAT_NAME, params, String.class);
    }

    @Override
    public void addUserToPrivateChat(Integer userId, Integer chatId) {
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue(CHAT_ID, chatId);
        parameters.addValue(USER_ID, userId);
        namedParameterJdbcTemplate.update(ADD_USER_TO_CHAT, parameters);
    }

    @Override
    public List<Integer> getChatUsersIds(Integer chatId) {
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue(CHAT_ID, chatId);
        return namedParameterJdbcTemplate.query(GET_ALL_USERS_FROM_CHAT, parameters,
                new BeanPropertyRowMapper<>(Integer.class));
    }
}
