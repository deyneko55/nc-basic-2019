package edu.netcracker.backend.dao.impl;

import edu.netcracker.backend.dao.NotificationDAO;
import edu.netcracker.backend.dao.mapper.NotificationMapper;
import edu.netcracker.backend.model.Notification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @author Dmytro Vovk
 */

@Repository
@PropertySource("classpath:sql/notificationdao.properties")
public class NotificationDAOImpl implements NotificationDAO {

    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Value("${CREATE_NOTIFICATION}")
    private String CREATE_NOTIFICATION;

    @Value("${GET_LAST_NOTIFICATIONS}")
    private String GET_LAST_NOTIFICATIONS;

    @Value("${GET_ALL_NOTIFICATIONS}")
    private String GET_ALL_NOTIFICATIONS;

    @Value("${GET_ALL_USER_NOTIFICATIONS}")
    private String GET_ALL_USER_NOTIFICATIONS;

    @Value("${MARK_AS_READ}")
    private String MARK_AS_READ;

    @Value("${GET_UNREAD_NOTIFICATIONS}")
    private String GET_UNREAD_NOTIFICATIONS;

    private final String USER_ID = "user_id";
    private final String CONTENT = "content";
    private final String STATUS = "status";

    @Override
    public Notification create(Notification entity) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(USER_ID, entity.getRecipientId())
                .addValue(CONTENT, entity.getContent())
                .addValue(STATUS, entity.getIsRead());
        namedParameterJdbcTemplate.update(CREATE_NOTIFICATION, params, keyHolder, new String[]{"id"});
        entity.setNotificationId(Objects.requireNonNull(keyHolder.getKey()).intValue());
        return entity;
    }

    @Override
    public void update(Notification entity) {

    }

    @Override
    public void delete(Integer id) {

    }

    @Override
    public Optional<Notification> findById(Integer id) {
        return Optional.empty();
    }

    @Override
    public List<Notification> getAll() {
        return null;
    }

    @Override
    public List<Notification> getUserLastNotifications(Integer userId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(USER_ID, userId);
        return namedParameterJdbcTemplate.query(GET_LAST_NOTIFICATIONS, params, new NotificationMapper());
    }

    @Override
    public List<Notification> getAllUserNotifications(Integer userId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(USER_ID, userId);
        return namedParameterJdbcTemplate.query(GET_ALL_USER_NOTIFICATIONS, params, new NotificationMapper());
    }

    @Override
    public void markAllAsRead(Integer userId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(USER_ID, userId);
        namedParameterJdbcTemplate.update(MARK_AS_READ, params);
    }

    @Override
    public List<Notification> getUnreadNotifications(Integer userId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(USER_ID, userId);
        return namedParameterJdbcTemplate.query(GET_UNREAD_NOTIFICATIONS, params, new NotificationMapper());
    }
}
