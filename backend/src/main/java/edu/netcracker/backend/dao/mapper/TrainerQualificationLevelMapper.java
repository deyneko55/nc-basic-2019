package edu.netcracker.backend.dao.mapper;

import edu.netcracker.backend.model.TrainerQualificationLevel;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Dmytro Vovk
 */

public class TrainerQualificationLevelMapper implements RowMapper<TrainerQualificationLevel> {
    @Override
    public TrainerQualificationLevel mapRow(ResultSet resultSet, int i) throws SQLException {
        TrainerQualificationLevel trainerQualificationLevel = new TrainerQualificationLevel();
        trainerQualificationLevel.setQualificationId(resultSet.getInt(1));
        trainerQualificationLevel.setQualificationName(resultSet.getString(2));
        return trainerQualificationLevel;
    }
}
