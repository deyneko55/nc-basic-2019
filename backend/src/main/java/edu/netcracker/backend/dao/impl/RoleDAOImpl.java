package edu.netcracker.backend.dao.impl;

import edu.netcracker.backend.dao.RoleDAO;
import edu.netcracker.backend.dao.mapper.RoleMapper;
import edu.netcracker.backend.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;
import java.util.Optional;

@Repository
@PropertySource("classpath:sql/roledao.properties")
public class RoleDAOImpl implements RoleDAO {

    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Value("${FIND_ROLE_BY_ROLE_NAME}")
    private String FIND_ROLE_BY_ROLE_NAME;

    @Value("${FIND_IN_ROLES}")
    private String FIND_IN_ROLES;

    @Value("${FIND_ROLE_BY_ID}")
    private String FIND_ROLE_BY_ID;

    @Value("${GET_ALL_ROLES}")
    private String GET_ALL_ROLES;

    @Override
    public Optional<Role> findByName(String roleName) {
        try {
            Role role = jdbcTemplate.queryForObject(FIND_ROLE_BY_ROLE_NAME, new Object[]{roleName},
                    new BeanPropertyRowMapper<>(Role.class));

            return Optional.ofNullable(role);
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public Integer findIn(List<Long> args) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("list_id", args);
        return namedParameterJdbcTemplate.queryForObject(FIND_IN_ROLES, params, Integer.class);
    }

    @Override
    public String getRoleName(Integer roleId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("roleId", roleId);
        return namedParameterJdbcTemplate.queryForObject(FIND_ROLE_BY_ID, params, String.class);
    }

    @Override
    public List<Role> getAll() {
        return namedParameterJdbcTemplate.query(GET_ALL_ROLES, new RoleMapper());
    }

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }
}
