package edu.netcracker.backend.dao.mapper;

import edu.netcracker.backend.message.response.LessonCreationResponse;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class LessonCreationMapper implements RowMapper<LessonCreationResponse> {

    @Override
    public LessonCreationResponse mapRow(ResultSet resultSet, int i) throws SQLException {
        LessonCreationResponse lessonCreationResponse = new LessonCreationResponse();
        lessonCreationResponse.setId(resultSet.getInt(1));
        lessonCreationResponse.setLessonName(resultSet.getString(2));
        lessonCreationResponse.setCourseId(resultSet.getInt(3));
        return lessonCreationResponse;
    }
}
