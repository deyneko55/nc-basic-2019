package edu.netcracker.backend.dao.impl;

import edu.netcracker.backend.dao.LessonDAO;
import edu.netcracker.backend.dao.mapper.LessonMapper;
import edu.netcracker.backend.dao.mapper.LessonWithStatusMapper;
import edu.netcracker.backend.dao.mapper.UserStatusMapper;
import edu.netcracker.backend.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.*;

@Repository
@PropertySource("classpath:sql/lessondao.properties")
public class LessonDAOImpl implements LessonDAO {

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private final String LESSON_NAME = "lessonName";
    private final String COURSE_ID = "courseId";
    private final String LESSON_ID = "lessonId";
    private final String GROUP_ID = "groupId";
    private final String SCHEDULE_ID = "scheduleId";
    private final String LESSON_DATE = "lessonDate";
    private final String USER_ID = "userId";
    private final String PRESENCE_STATUS_ID = "presenceStatusId";
    private final String ABSENCE_REASON_ID = "absenceReasonId";
    private final String EXPLANATION = "explanation";

    @Value("${INSERT_LESSON_BY_NAME_AND_COURSE}")
    private String INSERT_LESSON_BY_NAME_AND_COURSE;

    @Value("${INSERT_INTO_SCHEDULE}")
    private String INSERT_INTO_SCHEDULE;

    @Value("${UPDATE_COURSE_FOR_LESSON}")
    private String UPDATE_COURSE_FOR_LESSON;

    @Value("${UPDATE_LESSON_NAME}")
    private String UPDATE_LESSON_NAME;

    @Value("${UPDATE_SCHEDULE_FOR_LESSON}")
    private String UPDATE_SCHEDULE_FOR_LESSON;

    @Value("${GET_ALL_LESSONS_OF_GROUP}")
    private String GET_ALL_LESSONS_OF_GROUP;

    @Value("${GET_LESSON_BY_ID}")
    private String GET_LESSON_BY_ID;

    @Value("${GET_GROUP_LESSON}")
    private String GET_GROUP_LESSON;

    @Value("${DELETE_LESSON}")
    private String DELETE_LESSON;

    @Value("${GET_LESSON_BY_NAME}")
    private String GET_LESSON_BY_NAME;

    @Value("${INSERT_INTO_USER_LESSON}")
    private String INSERT_INTO_USER_LESSON;

    @Value("${UPDATE_USER_LESSON}")
    private String UPDATE_USER_LESSON;

    @Value("${DELETE_USER_LESSON}")
    private String DELETE_USER_LESSON;

    @Value("${GET_USER_LESSON}")
    private String GET_USER_LESSON;

    @Value("${GET_USERS_BY_LESSON_ID}")
    private String GET_USERS_BY_LESSON_ID;

    @Value("${GET_COUNT_OF_PRESENT_PEOPLE}")
    private String GET_COUNT_OF_PRESENT_PEOPLE;

    @Value("${GET_USER_LESSON_EXISTS}")
    private String GET_USER_LESSON_EXISTS;

    @Value("${GET_USER_STATUS_FROM_LESSON}")
    private String GET_USER_STATUS_FROM_LESSON;

    @Value("${INSERT_INTO_LESSON_SCHEDULE}")
    private String INSERT_INTO_LESSON_SCHEDULE;

    @Value("${DELETE_FROM_USER_LESSON}")
    private String DELETE_FROM_USER_LESSON;

    @Value("${UPDATE_LESSON_STATUS}")
    private String UPDATE_LESSON_STATUS_TO_PASSED;

    @Value("${GET_ALL_PRESENCE_STATUSES}")
    private String GET_ALL_PRESENCE_STATUSES;

    @Value("${UPDATE_LESSON_STATUS_TO_CANCELED}")
    private String UPDATE_LESSON_STATUS_TO_CANCELED;

    @Override
    public Lesson create(Lesson lesson) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue(LESSON_NAME, lesson.getLessonName());
        parameterSource.addValue(COURSE_ID, lesson.getCourseId());
        namedParameterJdbcTemplate.update(INSERT_LESSON_BY_NAME_AND_COURSE, parameterSource, keyHolder, new String[]{"id"});
        lesson.setId(Objects.requireNonNull(keyHolder.getKey()).intValue());
        return lesson;
    }

    @Override
    public Optional<Lesson> findById(Integer lessonId) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue(LESSON_ID, lessonId);
        Lesson lesson;
        try {
            lesson = namedParameterJdbcTemplate.queryForObject(GET_LESSON_BY_ID, parameterSource, (rs, i) -> {
                Lesson lessonResult = new Lesson();
                lessonResult.setId(rs.getInt(1));
                lessonResult.setLessonName(rs.getString(2));
                lessonResult.setCourseId(rs.getInt(3));
                return lessonResult;
            });
        } catch (EmptyResultDataAccessException ex) {
            return Optional.empty();
        }
        if (lesson == null) {
            return Optional.empty();
        } else {
            return Optional.of(lesson);
        }
    }

    @Override
    public void addLessonToCourse(Integer lessonId, Integer courseId) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue(COURSE_ID, courseId);
        parameterSource.addValue(LESSON_ID, lessonId);
        namedParameterJdbcTemplate.update(UPDATE_COURSE_FOR_LESSON, parameterSource);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<LessonWithStatus> getAllGroupLessons(Integer groupId) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue(GROUP_ID, groupId);
        List<LessonWithStatus> lessons = namedParameterJdbcTemplate.query(
                GET_ALL_LESSONS_OF_GROUP, parameterSource, new LessonWithStatusMapper());

        lessons.forEach(lessonWithStatus -> {
            if (lessonWithStatus.getLessonDate().isBefore(LocalDateTime.now())
                    && !lessonWithStatus.getLessonStatus().equals("CANCELED")) {
                parameterSource.addValue(LESSON_ID, lessonWithStatus.getId());
                namedParameterJdbcTemplate.update(UPDATE_LESSON_STATUS_TO_PASSED, parameterSource);
            }
        });

        return namedParameterJdbcTemplate.query(GET_ALL_LESSONS_OF_GROUP, parameterSource,
                new LessonWithStatusMapper());
    }

    @Override
    public Optional<Lesson> getGroupLesson(Integer lessonId, Integer groupId) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue(LESSON_ID, lessonId);
        parameterSource.addValue(GROUP_ID, groupId);
        Lesson lesson;
        try {
            lesson = namedParameterJdbcTemplate.queryForObject(GET_GROUP_LESSON, parameterSource, new LessonMapper());
        } catch (EmptyResultDataAccessException ex) {
            return Optional.empty();
        }
        if (lesson == null) {
            return Optional.empty();
        } else {
            return Optional.of(lesson);
        }
    }

    @Transactional
    @Override
    public void update(Lesson lesson) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        KeyHolder keyHolder = new GeneratedKeyHolder();

        parameterSource.addValue(LESSON_ID, lesson.getId());
        parameterSource.addValue(LESSON_NAME, lesson.getLessonName());
        parameterSource.addValue(LESSON_DATE, Timestamp.valueOf(lesson.getLessonDate()));
        parameterSource.addValue(GROUP_ID, lesson.getGroupId());

        namedParameterJdbcTemplate.update(UPDATE_LESSON_NAME, parameterSource);
        namedParameterJdbcTemplate.update(INSERT_INTO_SCHEDULE, parameterSource, keyHolder, new String[]{"id"});
        parameterSource.addValue(SCHEDULE_ID, Objects.requireNonNull(keyHolder.getKey()).intValue());
        namedParameterJdbcTemplate.update(INSERT_INTO_LESSON_SCHEDULE, parameterSource);
        namedParameterJdbcTemplate.update(UPDATE_SCHEDULE_FOR_LESSON, parameterSource);
    }

    @Transactional
    @Override
    public void setDateForLessonOfGroup(Integer groupId, Integer lessonId, LocalDateTime lessonDate) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue(LESSON_ID, lessonId)
                .addValue(LESSON_DATE, Timestamp.valueOf(lessonDate))
                .addValue(GROUP_ID, groupId);

        namedParameterJdbcTemplate.update(DELETE_FROM_USER_LESSON, parameterSource);
        namedParameterJdbcTemplate.update(INSERT_INTO_SCHEDULE, parameterSource, keyHolder, new String[]{"id"});
        parameterSource.addValue(SCHEDULE_ID, Objects.requireNonNull(keyHolder.getKey()).intValue());
        namedParameterJdbcTemplate.update(INSERT_INTO_LESSON_SCHEDULE, parameterSource);
    }

    @Override
    public void delete(Integer lessonId) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue(LESSON_ID, lessonId);
        namedParameterJdbcTemplate.update(DELETE_LESSON, parameterSource);
    }

    @Override
    public Optional<Lesson> findByName(String lessonName) {
        Lesson lesson;
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue(LESSON_NAME, lessonName);
        try {
            lesson = namedParameterJdbcTemplate.queryForObject(GET_LESSON_BY_NAME, parameterSource, new LessonMapper());
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
        if (lesson != null) {
            return Optional.of(lesson);
        } else {
            return Optional.empty();
        }
    }

    @Override
    public void setUserStatus(Integer userId, Integer lessonId, Integer presenceStatusId, Integer absenceReasonId, String explanation) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue(USER_ID, userId);
        parameterSource.addValue(LESSON_ID, lessonId);
        parameterSource.addValue(PRESENCE_STATUS_ID, presenceStatusId);
        parameterSource.addValue(ABSENCE_REASON_ID, absenceReasonId);
        parameterSource.addValue(EXPLANATION, explanation);
        if (namedParameterJdbcTemplate
                .queryForObject(GET_USER_LESSON_EXISTS, parameterSource, Boolean.class)) {
            namedParameterJdbcTemplate.update(UPDATE_USER_LESSON, parameterSource);
        } else {
            namedParameterJdbcTemplate.update(INSERT_INTO_USER_LESSON, parameterSource);
        }
    }

    @Override
    public void deleteUserStatus(Integer userId, Integer lessonId) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue(USER_ID, userId);
        parameterSource.addValue(LESSON_ID, lessonId);
        namedParameterJdbcTemplate.update(DELETE_USER_LESSON, parameterSource);
    }

    @Override
    public UserStatus getUserStatus(Integer userId, Integer lessonId) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue(USER_ID, userId);
        parameterSource.addValue(LESSON_ID, lessonId);
        UserStatus userStatus = namedParameterJdbcTemplate.queryForObject(GET_USER_LESSON, parameterSource, new UserStatusMapper());
        return userStatus;
    }

    @Override
    public List<User> findEmployeesByGroupAndLessonId(Integer lessonId, Integer groupId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(LESSON_ID, lessonId);
        params.addValue(GROUP_ID, groupId);
        return namedParameterJdbcTemplate.query(GET_USERS_BY_LESSON_ID, params, (resultSet, i) -> {
            User user = new User();
            user.setId(resultSet.getInt(1));
            user.setLogin(resultSet.getString(2));
            user.setEmail(resultSet.getString(3));
            user.setFirstName(resultSet.getString(4));
            user.setLastName(resultSet.getString(5));
            return user;
        });
    }

    @Override
    public Integer getCountOfPresentEmployees(Integer lessonId) {
        MapSqlParameterSource param = new MapSqlParameterSource();
        param.addValue(LESSON_ID, lessonId);
        Integer countOfEmployees = namedParameterJdbcTemplate.queryForObject(GET_COUNT_OF_PRESENT_PEOPLE,
                param, Integer.class);
        if (countOfEmployees == null) {
            return 0;
        }
        return countOfEmployees;
    }


    @Override
    public UserStatus getUserStatusFromLesson(Integer lessonId, Integer userId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(LESSON_ID, lessonId);
        params.addValue(USER_ID, userId);
        return namedParameterJdbcTemplate.queryForObject(GET_USER_STATUS_FROM_LESSON, params, (resultSet, i) -> {
            UserStatus userStatusMapper = new UserStatus();
            userStatusMapper.setLessonId(resultSet.getInt(1));
            userStatusMapper.setUserId(resultSet.getInt(2));
            userStatusMapper.setPresenceStatusId(resultSet.getInt(3));
            userStatusMapper.setPresenceStatus(resultSet.getString(4));
            return userStatusMapper;
        });
    }

    @Override
    public List<Lesson> getAll() {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<PresenceStatus> getAllPresenceStatuses() {
        return namedParameterJdbcTemplate.query(GET_ALL_PRESENCE_STATUSES,
                new BeanPropertyRowMapper<>(PresenceStatus.class));
    }

    @Override
    public void updateLessonStatusToCanceled(Integer groupId, Integer lessonId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(GROUP_ID, groupId)
                .addValue(LESSON_ID, lessonId);
        namedParameterJdbcTemplate.update(UPDATE_LESSON_STATUS_TO_CANCELED, params);
    }

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }
}
