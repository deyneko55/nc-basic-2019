package edu.netcracker.backend.dao;

import edu.netcracker.backend.model.Role;

import java.util.List;
import java.util.Optional;

public interface RoleDAO {

    Optional<Role> findByName(String roleName);

    //    List<Role> findIn(List<Long> args);
    Integer findIn(List<Long> args);

    String getRoleName(Integer roleId);

    List<Role> getAll();
}
