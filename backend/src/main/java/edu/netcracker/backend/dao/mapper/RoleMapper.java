package edu.netcracker.backend.dao.mapper;

import edu.netcracker.backend.model.Role;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RoleMapper implements RowMapper<Role> {

    @Override
    public Role mapRow(ResultSet resultSet, int i) throws SQLException {
        Role role = new Role();
        role.setId(resultSet.getInt(1));
        role.setName(resultSet.getString(2));
        return role;
    }
}
