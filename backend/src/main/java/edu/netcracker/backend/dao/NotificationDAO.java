package edu.netcracker.backend.dao;

import edu.netcracker.backend.model.Notification;

import java.util.List;
import java.util.Optional;

/**
 * @author Dmytro Vovk
 */

public interface NotificationDAO extends BaseDAO<Notification> {
    Notification create(Notification entity);

    void update(Notification entity);

    void delete(Integer id);

    Optional<Notification> findById(Integer id);

    List<Notification> getAll();

    List<Notification> getUserLastNotifications(Integer userId);

    List<Notification> getAllUserNotifications(Integer userId);

    void markAllAsRead(Integer userId);

    List<Notification> getUnreadNotifications(Integer userId);
}
