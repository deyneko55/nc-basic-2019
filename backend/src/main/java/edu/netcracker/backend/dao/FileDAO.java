package edu.netcracker.backend.dao;

import edu.netcracker.backend.model.File;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;

public interface FileDAO {

    void uploadFile(MultipartFile file, Integer lessonId);

    void deleteFile(Integer id);

    Optional<File> getFile(Integer id);

    List<File> getFilesByLessonId(Integer id);

    List<File> getAllFiles();
}
