package edu.netcracker.backend.dao.impl;

import edu.netcracker.backend.dao.CourseDAO;
import edu.netcracker.backend.dao.mapper.*;
import edu.netcracker.backend.message.response.CourseTypeStatsResponse;
import edu.netcracker.backend.message.response.LessonCreationResponse;
import edu.netcracker.backend.model.*;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.time.LocalDate;
import java.util.*;

@Repository
@Slf4j
@PropertySource("classpath:sql/coursedao.properties")
public class CourseDAOImpl implements CourseDAO {

    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Value("${INSERT_NEW_COURSE_WITH_ALL_INFO}")
    private String INSERT_NEW_COURSE_WITH_ALL_INFO;

    @Value("${PRINT_ALL_COURSES}")
    private String PRINT_ALL_COURSES;

    @Value("${PRINT_COURSES_IN_PROGRESS}")
    private String PRINT_COURSES_IN_PROGRESS;

    @Value("${UPDATE_ALL_INFO_ABOUT_COURSE}")
    private String UPDATE_ALL_INFO_ABOUT_COURSE;

    @Value("${DELETE_COURSE}")
    private String DELETE_COURSE;

    @Value("${GET_COURSE_BY_ID}")
    private String GET_COURSE_BY_ID;

    @Value("${GET_ALL_GROUPS_FROM_COURSE}")
    private String GET_ALL_GROUPS_FROM_COURSE;

    @Value("${GET_ALL_USERS_FROM_COURSE}")
    private String GET_ALL_USERS_FROM_COURSE;

    @Value("${INSERT_NEW_COURSE_TYPE}")
    private String INSERT_NEW_COURSE_TYPE;

    @Value("${INSERT_NEW_COURSE_TYPE2}")
    private String INSERT_NEW_COURSE_TYPE2;

    @Value("${INSERT_NEW_DIFFICULTY_LEVEL}")
    private String INSERT_NEW_DIFFICULTY_LEVEL;

    @Value("${GET_COURSES_BY_NAME}")
    private String GET_COURSES_BY_NAME;

    @Value("${SELECT_ID_FROM_COURSE}")
    private String SELECT_ID_FROM_COURSE;

    @Value("${PRINT_COURSE_TYPES}")
    private String PRINT_COURSE_TYPES;

    @Value("${PRINT_COURSE_LEVELS}")
    private String PRINT_COURSE_LEVELS;

    @Value("${COUNT_USERS_BY_COURSE_TYPE}")
    private String COUNT_USERS_BY_COURSE_TYPE;

    @Value("${COUNT_GROUPS_BY_COURSE_TYPE}")
    private String COUNT_GROUPS_BY_COURSE_TYPE;

    @Value("${INSERT_DESIRED_COURSE}")
    private String INSERT_DESIRED_COURSE;

    @Value("${INSERT_NEW_COURSE_BY_IDS}")
    private String INSERT_NEW_COURSE_BY_IDS;

    @Value("${GET_LEVELS_BY_COURSE_TYPE}")
    private String GET_LEVELS_BY_COURSE_TYPE;

    @Value("${GET_COURSE_TYPE_BY_ID}")
    private String GET_COURSE_TYPE_BY_ID;

    @Value("${GET_ALL_USERS_FROM_DESIRED_COURSE}")
    private String GET_ALL_USERS_FROM_DESIRED_COURSE;

    @Value("${INSERT_NEW_COURSE_TYPE_BY_ID}")
    private String INSERT_NEW_COURSE_TYPE_BY_ID;

    @Value("${UPDATE_COURSE_TYPE}")
    private String UPDATE_COURSE_TYPE;

    @Value("${INSERT_NEW_DESIRED_TIME_FOR_COURSE}")
    private String INSERT_NEW_DESIRED_TIME_FOR_COURSE;

    @Value("${GET_ALL_LESSONS_FROM_COURSE}")
    private String GET_ALL_LESSONS_FROM_COURSE;

    @Value("${GET_COURSES_FROM_USER}")
    private String GET_ALL_COURSES_FROM_USER;

    @Value("${GET_TRAINER_ID_FROM_COURSE_ID}")
    private String GET_TRAINER_ID_FROM_COURSE_ID;

    private static final String COURSE_TYPE_ID = "course_type_id";
    private static final String COURSE_ID = "courseId";
    private static final String USER_ID = "userId";
    private static final String COURSE_NAME = "courseName";
    private static final String START_DATE = "startDate";
    private static final String END_DATE = "endDate";
    private static final String DIFFICULTY_LEVEL_ID = "difficulty_level_id";


    @Transactional
    @Override
    public Course create(Course course) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        log.debug("Method was invoked");
        Integer courseTypeId = createCourseType(course.getCourseType(), course.getDifficultyLevel());

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("name", course.getCourseName())
                .addValue("start_date", course.getStartDate())
                .addValue("end_date", course.getEndDate())
                .addValue("course_type_id", courseTypeId);
        namedParameterJdbcTemplate.update(INSERT_NEW_COURSE_WITH_ALL_INFO, params, keyHolder, new String[]{"id"});
        course.setId(Objects.requireNonNull(keyHolder.getKey()).intValue());
        return course;
    }

    @Override
    public void delete(Integer courseId) {
        log.debug("Method was invoked");
        jdbcTemplate.update(DELETE_COURSE, courseId);
    }

    @Override
    public void update(Course course) {
        log.debug("Method was invoked");

        jdbcTemplate.update(UPDATE_ALL_INFO_ABOUT_COURSE, course.getCourseName(), course.getStartDate(),
                course.getEndDate(), course.getCourseType(), course.getId());
    }

    @Override
    public Optional<Course> findById(Integer courseId) {
        log.debug("Method was invoked");
        try {
            Course course = jdbcTemplate.queryForObject(GET_COURSE_BY_ID, new Object[]{courseId},
                    new CourseMapper());
            if (course == null) {
                return Optional.empty();
            } else {
                return Optional.of(course);
            }
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    public Integer createDifficultyLevel(String nameOfLevel) {
        log.debug("Method was invoked");
        KeyHolder keyHolder = new GeneratedKeyHolder();
        MapSqlParameterSource params = new MapSqlParameterSource();

        params.addValue("name", nameOfLevel);
        namedParameterJdbcTemplate.update(INSERT_NEW_DIFFICULTY_LEVEL, params, keyHolder, new String[]{"id"});
        return Objects.requireNonNull(keyHolder.getKey()).intValue();
    }


    private Integer createCourseType(String typeName, String nameOfLevel) {
        log.debug("Method was invoked");
        Integer levelId = createDifficultyLevel(nameOfLevel);
        KeyHolder keyHolder = new GeneratedKeyHolder();

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("name", typeName)
                .addValue("difficulty_level_id", levelId);
        namedParameterJdbcTemplate.update(INSERT_NEW_COURSE_TYPE, params, keyHolder, new String[]{"id"});
        return Objects.requireNonNull(keyHolder.getKey()).intValue();
    }

    @Override
    public Integer createCourseType(String typeName) {
        log.debug("Method was invoked");
        KeyHolder keyHolder = new GeneratedKeyHolder();
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("name", typeName);
        namedParameterJdbcTemplate.update(INSERT_NEW_COURSE_TYPE2, params, keyHolder, new String[]{"id"});
        return Objects.requireNonNull(keyHolder.getKey()).intValue();
    }

    @Override
    public List<Course> getAll() {
        log.debug("Method was invoked");
        return namedParameterJdbcTemplate.query(PRINT_ALL_COURSES, new CourseMapper());
    }

    @Override
    public List<Course> getAllCoursesInProgress(LocalDate currentDate) {
        log.debug("Method was invoked");
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("current_date", currentDate);
        return namedParameterJdbcTemplate.query(PRINT_COURSES_IN_PROGRESS, params, new CourseMapper());
    }

    @Override
    public List<Course> findCourseByName(String name) {
        log.debug("Method was invoked");
        String nameTemplate = "%" + name + "%";
        return new ArrayList<>(jdbcTemplate.query(GET_COURSES_BY_NAME, new Object[]{nameTemplate}, new CourseMapper()));
    }

    @Override
    public List<Group> getAllGroupsFromCourse(Integer courseId) {
        log.debug("Method was invoked");
        return getGroupWithTrainers(courseId);
    }

    @Override
    public List<User> getAllUsersFromCourse(Integer courseId) {
        log.debug("Method was invoked");
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue(COURSE_ID, courseId);
        List<User> users = Lists.newArrayList();
        try {
            users = namedParameterJdbcTemplate.query(GET_ALL_USERS_FROM_COURSE, parameters, new UserMapper());
        } catch (EmptyResultDataAccessException e) {
            e.printStackTrace();
        }
        return users;
    }

    @Override
    public List<CourseType> getAllCourseTypes() {
        log.debug("Method was invoked");
        return jdbcTemplate.query(PRINT_COURSE_TYPES, (resultSet, i) -> {
            CourseType courseType = new CourseType();
            courseType.setCourseTypeId(resultSet.getInt(2));
            courseType.setCourseType(resultSet.getString(1));
            return courseType;
        });
    }

    @Override
    public List<CourseLevel> getAllLevelsOfCourse() {
        log.debug("Method was invoked");
        return jdbcTemplate.query(PRINT_COURSE_LEVELS, new BeanPropertyRowMapper<>(CourseLevel.class));
    }

    @Override
    public List<CourseTypeStatsResponse> getUsersByCourseType(Integer courseTypeId) {
        return getEntitiesByCourseType(COUNT_USERS_BY_COURSE_TYPE, courseTypeId);
    }

    @Override
    public List<CourseTypeStatsResponse> getGroupsByCourseType(Integer courseTypeId) {
        return getEntitiesByCourseType(COUNT_GROUPS_BY_COURSE_TYPE, courseTypeId);
    }

    private List<CourseTypeStatsResponse> getEntitiesByCourseType(String query, Integer courseTypeId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(COURSE_TYPE_ID, courseTypeId);
        return namedParameterJdbcTemplate.query(query, params,
                new CourseTypeStatsMapper());
    }

    @Override
    public void bookCourse(Integer userId, Integer courseId) {
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue(COURSE_ID, courseId)
                .addValue(USER_ID, userId);
        namedParameterJdbcTemplate.update(INSERT_DESIRED_COURSE, parameters);
        namedParameterJdbcTemplate.update(INSERT_NEW_DESIRED_TIME_FOR_COURSE, parameters);
    }

    @Override
    public List<User> getAllUsersFromDesiredCourse(Integer courseId) {
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue(COURSE_ID, courseId);
        return namedParameterJdbcTemplate.query(GET_ALL_USERS_FROM_DESIRED_COURSE, parameters, new UserMapper());
    }

    @Override
    public List<CourseLevel> getLevelsOfCourseType(Integer courseTypeId) {
        log.debug("Method was invoked");
        return jdbcTemplate.query(GET_LEVELS_BY_COURSE_TYPE,
                new BeanPropertyRowMapper<>(CourseLevel.class), courseTypeId);
    }

    @Override
    public Optional<CourseType> findByCourseTypeId(Integer courseTypeId) {
        log.debug("Method was invoked");
        try {
            CourseType courseType = jdbcTemplate.queryForObject(GET_COURSE_TYPE_BY_ID,
                    new BeanPropertyRowMapper<>(CourseType.class), courseTypeId);
            if (courseType == null) {
                return Optional.empty();
            } else {
                return Optional.of(courseType);
            }
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public Course addNewCourseByIds(CourseWithIds course) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue(COURSE_NAME, course.getCourseName())
                .addValue(START_DATE, course.getStartDate())
                .addValue(END_DATE, course.getEndDate())
                .addValue(COURSE_TYPE_ID, course.getCourseTypeId())
                .addValue(DIFFICULTY_LEVEL_ID, course.getDifficultyLevelId());

        namedParameterJdbcTemplate.update(INSERT_NEW_COURSE_BY_IDS, parameters, keyHolder, new String[]{"id"});
        Integer courseId = Objects.requireNonNull(keyHolder.getKey()).intValue();

        parameters.addValue(COURSE_ID, courseId);

        keyHolder = new GeneratedKeyHolder();
        namedParameterJdbcTemplate.update(INSERT_NEW_COURSE_TYPE_BY_ID, parameters, keyHolder, new String[]{"id"});
        Integer courseTypeId = Objects.requireNonNull(keyHolder.getKey()).intValue();

        parameters.addValue("newCourseType", courseTypeId);
        namedParameterJdbcTemplate.update(UPDATE_COURSE_TYPE, parameters);

        return findById(courseId).get();
    }

    @Override
    public List<LessonCreationResponse> getAllLessonsFromCourse(Integer courseId) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue(COURSE_ID, courseId);
        return namedParameterJdbcTemplate.query(GET_ALL_LESSONS_FROM_COURSE, parameterSource,
                new LessonCreationMapper());
    }

    @Override
    public List<Course> getCoursesByUserId(Integer userId) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue(USER_ID, userId);
        return namedParameterJdbcTemplate.query(GET_ALL_COURSES_FROM_USER, parameterSource, new CourseMapper());
    }

    @Override
    public List<GroupWithTrainer> getAllGroupsWithTrainersFromCourse(Integer courseId) {
        List<Group> groups = getGroupWithTrainers(courseId);

        List<Integer> ids = new ArrayList<>();
        groups.forEach(group -> ids.add(group.getId()));

        List<GroupWithTrainer> trainers = new ArrayList<>();

        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("ids", ids)
                .addValue(COURSE_ID, courseId);
        try {
            trainers = namedParameterJdbcTemplate.query(GET_TRAINER_ID_FROM_COURSE_ID, parameterSource,
                    new GroupWithTrainerMapper());
        } catch (EmptyResultDataAccessException e) {
            for (Group group : groups) {
                trainers.add(new GroupWithTrainer(group.getId(), group.getName(), group.getCourseId()));
            }
        } catch (Exception e) {
            return Lists.emptyList();
        }

        List<GroupWithTrainer> extraGroups = new ArrayList<>();
        List<Group> bookedGroups = new ArrayList<>();

        for (Group group : groups) {
            trainers.forEach(groupWithTrainer -> {
                if (groupWithTrainer.getId().equals(group.getId())) {
                    groupWithTrainer.setName(group.getName());
                    groupWithTrainer.setCourseId(group.getCourseId());
                    bookedGroups.add(group);
                }
            });
        }
        groups.removeAll(bookedGroups);

        groups.forEach(group ->
                extraGroups.add(new GroupWithTrainer(group.getId(), group.getName(), group.getCourseId()))
        );

        trainers.addAll(extraGroups);
        return trainers;

    }

    private List<Group> getGroupWithTrainers(Integer courseId) {
        List<Group> groups = new ArrayList<>();
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue(COURSE_ID, courseId);
        try {
            groups = namedParameterJdbcTemplate.query(GET_ALL_GROUPS_FROM_COURSE, parameterSource,
                    new BeanPropertyRowMapper<>(Group.class));
        } catch (EmptyResultDataAccessException e) {
            e.printStackTrace();
        }
        return groups;
    }

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

}
