package edu.netcracker.backend.dao;

import edu.netcracker.backend.message.request.group.GroupSetTrainerRequest;
import edu.netcracker.backend.model.*;

import java.util.List;
import java.util.Optional;

public interface GroupDAO  {

    Group create(Group group);

    void update(Group group);

    void delete(Integer groupId);

    void addNewUserToGroup(Integer userId, Integer groupId);

    Optional<GroupWithTrainer> findById(Integer groupId);

    List<Group> getAll();

    List<Group> getGroupsByUserId(Integer userId);

    List<Group> findGroupByName(String groupName);

    Optional<Group> findGroupByClarifiedName(String groupName);

    Optional<User> findTrainerByGroupId(Integer groupId);

    List<User> getAllUsersFromGroup(String groupName);

    void setCourseForGroup(Integer courseId, Integer groupId);

    void changeGroupName(Integer id, String newName);

    void assignTrainerToTheGroup(GroupSetTrainerRequest trainerForGroup);

    List<String> getSchedule(Integer id);

    List<GroupStatistic> getGroupStatistic();

    List<Attendance> getGroupAttendance(Integer groupId);
}
