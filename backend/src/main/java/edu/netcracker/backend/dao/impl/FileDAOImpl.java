package edu.netcracker.backend.dao.impl;

import edu.netcracker.backend.dao.FileDAO;
import edu.netcracker.backend.model.File;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import javax.sql.DataSource;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Repository
@Slf4j
@PropertySource("classpath:sql/attachmentdao.properties")
public class FileDAOImpl implements FileDAO {

    private JdbcTemplate jdbcTemplate;

    @Value("${UPLOAD_FILE}")
    private String UPLOAD_FILE;

    @Value("${GET_FILE}")
    private String GET_FILE;

    @Value("${GET_ALL_FILES}")
    private String GET_ALL_FILES;

    @Value("${DELETE_FILE}")
    private String DELETE_FILE;

    @Value("${GET_LESSON_FILES}")
    private String GET_LESSON_FILES;

    @Override
    public void uploadFile(MultipartFile file, Integer lessonId) {
        try {
            jdbcTemplate.update(UPLOAD_FILE, file.getOriginalFilename(), file.getBytes(), lessonId);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteFile(Integer id) {
        jdbcTemplate.update(DELETE_FILE, id);
    }

    @Override
    public Optional<File> getFile(Integer id) {
        log.debug("FileDAOImpl.getFile(Integer id) was invoked");
        try {
            File file = jdbcTemplate.queryForObject(GET_FILE, new Object[]{id},
                    new BeanPropertyRowMapper<>(File.class));
            if (file == null) {
                return Optional.empty();
            } else {
                return Optional.of(file);
            }
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public List<File> getFilesByLessonId(Integer id) {
        log.debug("Method was invoked");

        return jdbcTemplate.query(GET_LESSON_FILES, new Object[]{id}, (resultSet, i) -> {
            File file = new File();
            file.setFileId(resultSet.getInt(1));
            file.setFileName(resultSet.getString(2));
            return file;
        });
    }

    @Override
    public List<File> getAllFiles() {
        return jdbcTemplate.query(GET_ALL_FILES, new BeanPropertyRowMapper<>(File.class));
    }

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
}
