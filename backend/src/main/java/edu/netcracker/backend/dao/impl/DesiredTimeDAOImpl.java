package edu.netcracker.backend.dao.impl;

import edu.netcracker.backend.dao.DesiredTimeDAO;
import edu.netcracker.backend.dao.mapper.DesiredTimeMapper;
import edu.netcracker.backend.model.DesiredTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Repository
@PropertySource("classpath:sql/desiredtimedao.properties")
public class DesiredTimeDAOImpl implements DesiredTimeDAO {

    @Value("${INSERT_NEW_DESIRED_TIME}")
    private String INSERT_NEW_DESIRED_TIME;

    @Value("${GET_DESIRED_TIME}")
    private String GET_DESIRED_TIME;

    @Value("${GET_ALL_TIMES}")
    private String GET_ALL_TIMES;

    @Value("${GET_ACCEPTABLE_TIME_FOR_COURSE}")
    private String GET_ACCEPTABLE_TIME_FOR_COURSE;

    @Value("${FIND_BY_ID}")
    private String FIND_BY_ID;

    @Value("${UPDATE_TIME}")
    private String UPDATE_TIME;

    @Value("${DELETE_TIME}")
    private String DELETE_TIME;

    private static final String DESIRED_TIME = "desiredTime";
    private static final String DESIRED_TIME_ID = "desiredTimeId";
    private static final String USER_ID = "userId";
    private static final String COURSE_ID = "courseId";


    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public DesiredTime create(DesiredTime desiredTime) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue(USER_ID, desiredTime.getUserId())
                .addValue(DESIRED_TIME, Timestamp.valueOf(desiredTime.getUserSchedule()));
        namedParameterJdbcTemplate.update(INSERT_NEW_DESIRED_TIME, parameters, keyHolder, new String[]{"id"});
        desiredTime.setDesiredTimeId(Objects.requireNonNull(keyHolder.getKey()).intValue());
        return desiredTime;
    }

    @Override
    public void update(DesiredTime desiredTime) {
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue(DESIRED_TIME_ID, desiredTime.getDesiredTimeId())
                .addValue(DESIRED_TIME, Timestamp.valueOf(desiredTime.getUserSchedule()));
        namedParameterJdbcTemplate.update(UPDATE_TIME, parameters);
    }

    @Override
    public void delete(Integer desiredTimeId) {
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue(DESIRED_TIME_ID, desiredTimeId);
        namedParameterJdbcTemplate.update(DELETE_TIME, parameters);
    }

    @Override
    public Optional<DesiredTime> findById(Integer desiredTimeId) {
        return find(desiredTimeId, DESIRED_TIME_ID);
    }

    @Override
    public List<DesiredTime> getAll() {
        return namedParameterJdbcTemplate.query(GET_ALL_TIMES, new DesiredTimeMapper());
    }

    @Override
    public List<DesiredTime> findUserDesiredTime(Integer userId) {
        MapSqlParameterSource param = new MapSqlParameterSource();
        param.addValue(USER_ID, userId);
        return namedParameterJdbcTemplate.query(GET_DESIRED_TIME, param, new DesiredTimeMapper());
    }

    private Optional<DesiredTime> find(Integer userId, String sqlRequest) {
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue(sqlRequest, userId);
        DesiredTime desiredTime;
        try {
            desiredTime = namedParameterJdbcTemplate.queryForObject(FIND_BY_ID, parameters,
                    new DesiredTimeMapper());
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
        if (desiredTime != null) {
            return Optional.of(desiredTime);
        } else {
            return Optional.empty();
        }
    }

    @Override
    public List<DesiredTime> getAllDesiredTimeForUser(Integer courseId) {
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue(COURSE_ID, courseId);
        return namedParameterJdbcTemplate.query(GET_ACCEPTABLE_TIME_FOR_COURSE, parameters, new DesiredTimeMapper());
    }
}
