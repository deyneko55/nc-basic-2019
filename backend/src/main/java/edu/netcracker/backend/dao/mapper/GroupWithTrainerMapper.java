package edu.netcracker.backend.dao.mapper;

import edu.netcracker.backend.model.GroupWithTrainer;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class GroupWithTrainerMapper implements RowMapper<GroupWithTrainer> {

    @Override
    public GroupWithTrainer mapRow(ResultSet resultSet, int i) throws SQLException {
        GroupWithTrainer group = new GroupWithTrainer();
        group.setTrainerId(resultSet.getInt(1));
        group.setId(resultSet.getInt(2));
        group.setFirstName(resultSet.getString(3));
        group.setLastName(resultSet.getString(4));

        return group;
    }
}
