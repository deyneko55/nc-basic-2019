package edu.netcracker.backend.dao.mapper;

import edu.netcracker.backend.model.UserLesson;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserLessonMapper implements RowMapper<UserLesson> {
    @Override
    public UserLesson mapRow(ResultSet resultSet, int i) throws SQLException {
        UserLesson userLesson = new UserLesson();
        userLesson.setLessonId(resultSet.getInt(1));
        userLesson.setLessonName(resultSet.getString(2));
        userLesson.setLessonDate(resultSet.getTimestamp(3).toLocalDateTime());
        userLesson.setLessonStatus(resultSet.getString(4));
        userLesson.setUserStatus(resultSet.getString(5));
        return userLesson;
    }
}
