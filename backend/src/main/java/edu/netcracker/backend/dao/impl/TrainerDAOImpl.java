package edu.netcracker.backend.dao.impl;

import edu.netcracker.backend.dao.TrainerDAO;
import edu.netcracker.backend.dao.mapper.*;
import edu.netcracker.backend.message.request.trainer.TrainerSetQualRequest;
import edu.netcracker.backend.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Repository
@PropertySource("classpath:sql/trainerdao.properties")
public class TrainerDAOImpl implements TrainerDAO {

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private JdbcTemplate jdbcTemplate;

    @Value("${GET_TEACHER_STUDENTS}")
    private String GET_TEACHER_STUDENTS;

    @Value("${GET_TRAINER_LEVELS}")
    private String GET_TRAINER_LEVELS;

    @Value("${GET_TRAINERS_WITH_QUALIFICATION_NAMES}")
    private String GET_TRAINERS_WITH_QUALIFICATION_NAMES;

    @Value("${SET_TRAINER_QUALIFICATION_AND_INFO}")
    private String SET_TRAINER_QUALIFICATION_AND_INFO;

    @Value("${GET_ALL_TRAINERS_FROM_COURSE}")
    private String GET_ALL_TRAINERS_FROM_COURSE;

    @Value("${GET_ALL_TRAINERS}")
    private String GET_ALL_TRAINERS;

    @Value("${GET_TRAINER_BY_LAST_NAME}")
    private String GET_TRAINER_BY_LAST_NAME;

    @Value("${GET_TRAINER_GROUPS}")
    private String GET_TRAINER_GROUPS;

    @Value("${GET_TRAINER_COURSES}")
    private String GET_TRAINER_COURSES;

    @Value("${CREATE_TRAINER_QUALIFICATION_LEVEL}")
    private String CREATE_NEW_QUALIFICATION_LEVEL;

    @Value("${GET_USER_TRAINERS}")
    private String GET_USER_TRAINERS;

    @Value("${GET_TRAINER_BY_ID}")
    private String GET_TRAINER_BY_ID;

    @Value("${UPDATE_TRAINER_FOR_GROUP}")
    private String UPDATE_TRAINER_FOR_GROUP;

    @Value("${UPDATE_TRAINER_LEVEL}")
    private String UPDATE_TRAINER_LEVEL;

    private static final String TRAINER_ID = "trainerId";
    private static final String COURSE_ID = "courseId";
    private static final String LAST_NAME = "lastName";
    private final String USER_ID = "user_id";
    private final String INFORMATION = "information";
    private final String QUALIFICATION_ID = "qualification_id";

    @Autowired
    public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate, JdbcTemplate jdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public User create(User user) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void update(User user) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(Integer trainerId) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Optional<User> findById(Integer trainerId) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<User> getAll() {
        return namedParameterJdbcTemplate.query(GET_ALL_TRAINERS, new UserMapper());
    }

    @Override
    public List<User> getAllTrainerStudents(Integer trainerId) {
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue(TRAINER_ID, trainerId);
        return namedParameterJdbcTemplate.query(GET_TEACHER_STUDENTS, parameters,
                new BeanPropertyRowMapper<>(User.class));
    }

    @Override
    public List<User> getAllTrainerFromCourse(Integer courseId) {
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue(COURSE_ID, courseId);
        return namedParameterJdbcTemplate.query(GET_ALL_TRAINERS_FROM_COURSE, parameters,
                new BeanPropertyRowMapper<>(User.class));
    }

    @Override
    public List<User> getTrainerByLastName(String lastName) {
        String value = "%" + lastName + "%";
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue(LAST_NAME, value);
        return namedParameterJdbcTemplate.query(GET_TRAINER_BY_LAST_NAME, parameters, new UserMapper());
    }

    @Override
    public List<TrainerQualificationLevel> getAllQualificationLevels() {
        return jdbcTemplate.query(GET_TRAINER_LEVELS, new TrainerQualificationLevelMapper());
    }

    @Override
    public Integer setTrainerQualification(TrainerSetQualRequest trainerSetQualRequest) {
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue(USER_ID, trainerSetQualRequest.getUserId())
                .addValue(QUALIFICATION_ID, trainerSetQualRequest.getQualificationId());
        return namedParameterJdbcTemplate.update(SET_TRAINER_QUALIFICATION_AND_INFO, parameters);
    }

    @Override
    public Integer updateQualification(TrainerSetQualRequest trainerSetQualRequest) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue(USER_ID, trainerSetQualRequest.getUserId())
                .addValue(QUALIFICATION_ID, trainerSetQualRequest.getQualificationId());
        return namedParameterJdbcTemplate.update(UPDATE_TRAINER_LEVEL, parameterSource);
    }

    @Override
    public List<Trainer> getTrainersWithLevels() {
        return jdbcTemplate.query(GET_TRAINERS_WITH_QUALIFICATION_NAMES, new TrainerMapper());
    }

    @Override
    public void createQualificationLevel(String newQualificationLevel) {
        jdbcTemplate.update(CREATE_NEW_QUALIFICATION_LEVEL, newQualificationLevel);
    }

    @Override
    public List<Group> getTrainerGroups(Integer trainerId) {
        MapSqlParameterSource param = new MapSqlParameterSource();
        param.addValue(USER_ID, trainerId);
        return namedParameterJdbcTemplate.query(GET_TRAINER_GROUPS, param, new BeanPropertyRowMapper<>(Group.class));
    }

    @Override
    public List<Course> getTrainerCourses(Integer trainerId) {
        MapSqlParameterSource param = new MapSqlParameterSource();
        param.addValue(USER_ID, trainerId);
        return namedParameterJdbcTemplate.query(GET_TRAINER_COURSES, param, new CourseMapper());
    }

    @Override
    public List<TrainerInfo> getUserTrainers(Integer userId) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue(USER_ID, userId);
        return namedParameterJdbcTemplate.query(GET_USER_TRAINERS, parameterSource,
                new BeanPropertyRowMapper<>(TrainerInfo.class));
    }

    @Override
    public Optional<TrainerInfo> getAllInfoAboutTrainer(Integer trainerId) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue(TRAINER_ID, trainerId);
        TrainerInfo trainerInfo;
        try {
            trainerInfo = namedParameterJdbcTemplate.queryForObject(GET_TRAINER_BY_ID, parameterSource,
                    new BeanPropertyRowMapper<>(TrainerInfo.class));
            return Optional.of(Objects.requireNonNull(trainerInfo));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public void changeTrainerForGroup(Integer groupId, Integer oldTrainerId, Integer newTrainerId) {
        jdbcTemplate.update(UPDATE_TRAINER_FOR_GROUP, newTrainerId, oldTrainerId, groupId);
    }
}
