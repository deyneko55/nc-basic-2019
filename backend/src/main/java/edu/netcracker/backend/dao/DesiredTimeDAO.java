package edu.netcracker.backend.dao;

import edu.netcracker.backend.model.DesiredTime;

import java.util.List;
import java.util.Optional;

public interface DesiredTimeDAO extends BaseDAO<DesiredTime> {
    @Override
    DesiredTime create(DesiredTime entity);

    @Override
    void update(DesiredTime entity);

    @Override
    void delete(Integer id);

    @Override
    Optional<DesiredTime> findById(Integer desiredTimeId);

    @Override
    List<DesiredTime> getAll();

    List<DesiredTime> findUserDesiredTime(Integer userId);

    List<DesiredTime> getAllDesiredTimeForUser(Integer courseId);
}
