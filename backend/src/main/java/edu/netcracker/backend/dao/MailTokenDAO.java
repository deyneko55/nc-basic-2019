package edu.netcracker.backend.dao;

import java.util.List;
import java.util.Optional;

public interface MailTokenDAO extends BaseDAO<String> {

    @Override
    String create(String token);

    @Override
    void update(String token);

    @Override
    void delete(Integer id);

    @Override
    Optional<String> findById(Integer id);

    @Override
    List<String> getAll();

    Optional<String> findByToken(String mailToken);

    boolean ifLinkExists(String token);

    void deleteByToken(String token);
}
