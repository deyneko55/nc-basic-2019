package edu.netcracker.backend.dao.mapper;

import edu.netcracker.backend.model.UserStatus;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserStatusMapper implements RowMapper<UserStatus> {

    @Override
    public UserStatus mapRow(ResultSet resultSet, int i) throws SQLException {
        UserStatus userStatus = new UserStatus();
        userStatus.setUserId(resultSet.getInt(1));
        userStatus.setLessonId(resultSet.getInt(2));
        userStatus.setPresenceStatusId(resultSet.getInt(3));
        userStatus.setAbsenceReasonId(resultSet.getInt(4));
        userStatus.setExplanation(resultSet.getString(5));
        return userStatus;
    }
}