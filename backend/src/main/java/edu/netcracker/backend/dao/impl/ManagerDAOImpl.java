package edu.netcracker.backend.dao.impl;

import edu.netcracker.backend.dao.ManagerDAO;
import edu.netcracker.backend.dao.mapper.UserMapper;
import edu.netcracker.backend.model.User;
import edu.netcracker.backend.model.UserStatistic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;
import java.util.Optional;

@Repository
@PropertySource("classpath:sql/userdao.properties")
public class ManagerDAOImpl implements ManagerDAO {

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Value("${SET_MANAGER_TO_USER}")
    private String SET_MANAGER_TO_USER;

    @Value("${GET_MANAGER_USERS}")
    private String GET_MANAGER_USERS;

    @Value("${GET_MANAGER_USER_STATISTIC}")
    private String GET_MANAGER_USER_STATISTIC;

    @Value("${GET_MANAGER_ID}")
    private String GET_MANAGER_ID;

    @Value("${GET_MANAGER_BY_LAST_NAME}")
    private String GET_MANAGER_BY_LAST_NAME;

    @Value("${GET_ALL_MANAGERS}")
    private String GET_ALL_MANAGERS;

    @Override
    public User create(User entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void update(User entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(Integer id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Optional<User> findById(Integer id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<User> getAll() {
        return namedParameterJdbcTemplate.query(GET_ALL_MANAGERS, new UserMapper());
    }

    private static final String MANAGER_ID = "managerId";
    private static final String USER_ID = "userId";
    private static final String LAST_NAME = "lastName";

    @Override
    public void setManager(Integer managerId, Integer userId) {
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue(MANAGER_ID, managerId)
                .addValue(USER_ID, userId);
        namedParameterJdbcTemplate.update(SET_MANAGER_TO_USER, parameters);
    }

    @Override
    public List<User> getAllUsers(Integer managerId) {
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue(MANAGER_ID, managerId);
        return namedParameterJdbcTemplate.query(GET_MANAGER_USERS, parameters, new BeanPropertyRowMapper<>(User.class));
    }

    @Override
    public List<UserStatistic> getUserStatistic(Integer managerId) {
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue(MANAGER_ID, managerId);
        List<UserStatistic> userStatistics = namedParameterJdbcTemplate
                .query(GET_MANAGER_USER_STATISTIC, parameters, new BeanPropertyRowMapper<>(UserStatistic.class));
        int presentStatus = 1;
        for (int i = 0; i < userStatistics.size() - 1; i++) {
            int userId = userStatistics.get(i).getUserId();
            int visitedLessons = 0;
            int missedLessons = 0;
            for (int j = i + 1; j < userStatistics.size(); j++) {
                if (userStatistics.get(j).getUserId().equals(userId)) {
                    if (userStatistics.get(j).getVisitedLessons().equals(presentStatus)) {
                        visitedLessons++;
                    } else {
                        missedLessons++;
                    }
                    userStatistics.remove(j);
                    j--;
                }
            }
            if (userStatistics.get(i).getVisitedLessons() != presentStatus) {
                missedLessons++;
            }
            if (userStatistics.get(i).getVisitedLessons() == presentStatus) {
                visitedLessons++;
            }
            userStatistics.get(i).setMissedLessons(missedLessons);
            userStatistics.get(i).setVisitedLessons(visitedLessons);

        }
        return userStatistics;
    }

    @Override
    public Integer getUserManager(Integer userId) {
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue(USER_ID, userId);
        return namedParameterJdbcTemplate.queryForObject(GET_MANAGER_ID, sqlParameterSource, Integer.class);
    }

    @Override
    public List<User> getManagerByLastName(String lastName) {
        String value = "%" + lastName + "%";
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue(LAST_NAME, value);
        return namedParameterJdbcTemplate.query(GET_ALL_MANAGERS, parameters, new UserMapper());
    }
}
