package edu.netcracker.backend.dao.impl;

import edu.netcracker.backend.dao.MailTokenDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Repository
@PropertySource("classpath:sql/mailtokendao.properties")
public class MailTokenDAOImpl implements MailTokenDAO {

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private JdbcTemplate jdbcTemplate;

    @Value("${INSERT_REGISTRATION_TOKEN}")
    private String INSERT_REGISTRATION_TOKEN;

    @Value("${FIND_TOKEN}")
    private String FIND_TOKEN;

    @Value("${DELETE_TOKEN}")
    private String DELETE_TOKEN;

    @Override
    public String create(String token) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        MapSqlParameterSource param = new MapSqlParameterSource();
        param.addValue("token", token);
        namedParameterJdbcTemplate.update(INSERT_REGISTRATION_TOKEN, param, keyHolder, new String[]{"id"});
        return String.valueOf(Objects.requireNonNull(keyHolder.getKey()).intValue());
    }

    @Override
    public boolean ifLinkExists(String token) {
        return findByToken(token).isPresent();
    }

    @Override
    public void update(String token) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(Integer id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Optional<String> findById(Integer id) {
        throw new UnsupportedOperationException();
    }

    public Optional<String> findByToken(String mailToken) {
        try {
            String registrationToken = jdbcTemplate.queryForObject(FIND_TOKEN, new Object[]{mailToken}, String.class);
            if (registrationToken != null) {
                return Optional.of(registrationToken);
            } else {
                return Optional.empty();
            }
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public List<String> getAll() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void deleteByToken(String token) {
        jdbcTemplate.update(DELETE_TOKEN, token);
    }


    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }
}
