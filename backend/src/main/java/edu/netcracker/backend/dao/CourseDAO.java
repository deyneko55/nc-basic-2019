package edu.netcracker.backend.dao;

import edu.netcracker.backend.message.response.CourseTypeStatsResponse;
import edu.netcracker.backend.message.response.LessonCreationResponse;
import edu.netcracker.backend.model.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface CourseDAO {

    Course create(Course course);

    void delete(Integer courseId);

    void update(Course course);

    Optional<Course> findById(Integer courseId);

    Optional<CourseType> findByCourseTypeId(Integer courseTypeId);

    List<Course> getAll();

    List<Course> getAllCoursesInProgress(LocalDate currentDate);

    List<Course> findCourseByName(String name);

    List<Course> getCoursesByUserId(Integer userId);

    List<CourseType> getAllCourseTypes();

    List<CourseLevel> getLevelsOfCourseType(Integer courseTypeId);

    List<CourseLevel> getAllLevelsOfCourse();

    List<Group> getAllGroupsFromCourse(Integer courseId);

    List<User> getAllUsersFromCourse(Integer courseId);

    List<CourseTypeStatsResponse> getUsersByCourseType(Integer courseTypeId);

    List<CourseTypeStatsResponse> getGroupsByCourseType(Integer courseTypeId);

    void bookCourse(Integer userId, Integer courseId);

    List<User> getAllUsersFromDesiredCourse(Integer courseId);

    Course addNewCourseByIds(CourseWithIds course);

    List<LessonCreationResponse> getAllLessonsFromCourse(Integer courseId);

    Integer createDifficultyLevel(String nameOfLevel);

    Integer createCourseType(String typeName);

    List<GroupWithTrainer> getAllGroupsWithTrainersFromCourse(Integer courseId);
}
