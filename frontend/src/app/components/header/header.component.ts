import {Component, OnInit, ViewChild} from '@angular/core';
import {UserService} from "../../shared/user/user.service";
import {ModalDirective} from "ngx-bootstrap";
import {Router} from "@angular/router";
import {RoleService} from "../../shared/user/role.service";
import {ToastrService} from "ngx-toastr";
import {Login} from "../../shared/user/login.model";
import {Reset} from "../../shared/user/reset.model";
import {HttpErrorResponse} from "@angular/common/http";
import {Email} from "../../shared/user/email.model";
import {Slider} from "../../shared/news/slider.model";
import {NewsService} from "../../shared/news/news.service";
import {News} from "../../shared/news/news.model";
import {DomSanitizer} from "@angular/platform-browser";
import {WebSocketService} from "../../shared/chat/web-socket.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";

//import './bootstrap.js';

@Component({
  selector: 'header',
  templateUrl: './header.component.html',
})
export class Header implements OnInit{


  constructor(private newsService: NewsService, private userService : UserService, private emailService: UserService,
              private router: Router, private roleService: RoleService, private toastr: ToastrService,
              private websocketService: WebSocketService, private sanitizer: DomSanitizer){}

  ngOnInit(){
   this.getNewsFoSlider();
    this.formGroup = new FormGroup({
      Email: new FormControl('', [
        Validators.required,
        Validators.pattern(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)
      ]),
      Password: new FormControl('', [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(20)
      ])
    });
  }

  onReset() {
    this.formGroup.reset();
  }

  onValid() {
    console.log(this.formGroup);
  }

  formGroup: FormGroup;

  slider:Slider[] = [];

  getNewsFoSlider() {
    this.newsService.getNewsForSlider().subscribe((data: Slider[]) => {
      console.log(data);
        this.slider = data;
        this.getNews();
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Cannot get slider!");
      });
  }
  isCollapsed = true;


  sliderNews : News[] = [];
  getNews(){
    for (let slide of this.slider){
      this.newsService.getNews(slide.newsId.toString()).subscribe((data: News) => {
          let News = data;
          let byteArrayImage: any;
          let imageData: any;
          let sanitizeImageData: any;
          byteArrayImage = data["photo"];
          if (byteArrayImage != null) {
            imageData = 'data:image/png;base64,' + byteArrayImage;
            sanitizeImageData = this.sanitizer.bypassSecurityTrustUrl(imageData);
            News.picture = sanitizeImageData;
          }
          this.sliderNews.push(News);
        },
        (error: HttpErrorResponse) => {
          this.toastr.error("Cannot get slide!");
        });
    }
  }

  @ViewChild('courseModal') public courseModal: ModalDirective;

  login : Login = new Login();
  reset : Reset = new Reset();



  OnSubmit(userName: string, passWord: string) {
    this.userService.userAunthentication(userName, passWord).subscribe((data: any) => {
        localStorage.setItem('userToken', data.accessToken);
        localStorage.setItem('userRoles', data.userRoles[0].authority);
        localStorage.setItem('userId', data.userId);
        localStorage.setItem('constant_userId', data.userId);
        this.websocketService.connectToNotifications();
        this.router.navigate(['/home']);
      },
      (error: HttpErrorResponse) => {
        if (error.status === 401) {
          this.toastr.error("wrong login or password!");
        }
      })
  }

  email : Email = new Email();
  passwordRecovery(email : Email){
   this.userService.passwordRecovery(email).subscribe((data: any) => {
       this.toastr.success("new password sent to the mail!");
     },
     (error: HttpErrorResponse) => {
       if (error.status === 400) {
         this.toastr.error("wrong email!");
       }
     })
  }



}


