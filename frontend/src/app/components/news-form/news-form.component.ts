import {Component, ViewChild} from '@angular/core';
import {CourseFilter} from "../../shared/course/course-filter";
import {BsDatepickerConfig, ModalDirective, PageChangedEvent} from "ngx-bootstrap";
//import {NewsService} from "../../shared/course/course.service";
import {Router} from "@angular/router";
import {RoleService} from "../../shared/user/role.service";
import {ToastrService} from "ngx-toastr";
import {NgForm} from "@angular/forms";
import {Course} from "../../shared/course/course.model";
import {HttpErrorResponse} from "@angular/common/http";
import {News} from "../../shared/news/news.model";
import {NewsService} from "../../shared/news/news.service";
import {DomSanitizer} from "@angular/platform-browser";

@Component({
    selector: 'news-form',
    templateUrl: './news-form.component.html',
    //styleUrls: []
})
export class NewsForm {
  newsList: News[] = [];
  sliceViaPaginationNewsList: News[];

  ngOnInit(): void {
    this.getAllNews();
  }

  constructor(private service: NewsService, private router: Router, private toastr: ToastrService, private sanitizer: DomSanitizer) {
  }

  pageChanged(event: PageChangedEvent): void {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.sliceViaPaginationNewsList = this.newsList.slice(startItem, endItem);
  }

  goToNews(newId : number){
    localStorage.setItem('newId', newId.toString());
    this.router.navigate(['/news/' + newId]);
  }

  getAllNews() {
    this.service.getAllNews().subscribe((data: News[]) => {
        this.newsList = data;
        for (let i = 0; i < data.length; i++) {
          let byteArrayImage: any;
          let imageData: any;
          let sanitizeImageData: any;
          byteArrayImage = data[i]["photo"];
          if (byteArrayImage != null) {
            imageData = 'data:image/png;base64,' + byteArrayImage;
            sanitizeImageData = this.sanitizer.bypassSecurityTrustUrl(imageData);
            this.newsList[i].picture = sanitizeImageData;
          }
        }
        this.newsListSearch = this.newsList;
        this.sliceViaPaginationNewsList = this.newsList.slice(0, 10);
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Cannot get an information!");
      });
  }


  searchString: string;
  newsListSearch: News[] = [];

  search() {
    console.log(this.searchString);
    let searchNews: News[] = [];
    for (let news of this.newsList) {
      if ((news.header.substr(0, this.searchString.length) === this.searchString)) {
        searchNews.push(news);
      }
    }
    this.newsListSearch = searchNews;
    console.log(this.newsListSearch);
    if (this.searchString === '') {
      this.sliceViaPaginationNewsList = this.newsList.slice(0, 10);
      this.newsListSearch = this.newsList;
    } else {
      this.sliceViaPaginationNewsList = this.newsListSearch.slice(0, 10);
    }
    console.log(this.sliceViaPaginationNewsList);
  }



  public  id : string;
  @ViewChild('newsModal') public newsModal: ModalDirective;

}

