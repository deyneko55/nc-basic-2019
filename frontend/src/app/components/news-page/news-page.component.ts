import {Component, OnInit} from '@angular/core';
import {NewsService} from "../../shared/news/news.service";
import {ToastrService} from "ngx-toastr";
import {DomSanitizer} from "@angular/platform-browser";
import {News} from "../../shared/news/news.model";
import {HttpErrorResponse} from "@angular/common/http";

@Component({
    selector: 'news-page',
    templateUrl: './news-page.component.html',
    //styleUrls: []
})
export class NewsPageComponent implements OnInit{

  constructor(private service: NewsService, private toastr: ToastrService, private sanitizer: DomSanitizer) {
  }

  ngOnInit(): void {
    this.getInfo();
    this.setView();
  }


  news :News = new News();

  getInfo(){
    this.service.getNews(localStorage.getItem('newId')).subscribe((data: News) => {
        this.news = data;
        let byteArrayImage: any;
        let imageData: any;
        let sanitizeImageData: any;
        byteArrayImage = data["photo"];
        if (byteArrayImage != null) {
          imageData = 'data:image/png;base64,' + byteArrayImage;
          sanitizeImageData = this.sanitizer.bypassSecurityTrustUrl(imageData);
          this.news.picture = sanitizeImageData;
        }
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Cannot get an information!");
      });
  }

  setView(){
    this.service.setView( +localStorage.getItem('constant_userId') , +localStorage.getItem('newId')).subscribe((data: any) => {
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Cannot get an information!");
      });
  }
}

