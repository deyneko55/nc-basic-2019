import {Component, OnInit, ViewChild} from '@angular/core';
import {ModalDirective} from "ngx-bootstrap";
import {RoleService} from "../../shared/user/role.service";
import {ToastrService} from "ngx-toastr";
import {ActivatedRoute, Router} from "@angular/router";
import {FormControl, NgForm} from "@angular/forms";
import {HttpErrorResponse} from "@angular/common/http";
import {GroupFilter} from "../../shared/group/group-filter.model";
import {GroupService} from "../../shared/group/group.service";
import {CourseService} from "../../shared/course/course.service";
import {Group} from "../../shared/group/group.model";
import {Course} from "../../shared/course/course.model";
import {ProfileService} from "../../shared/profile/profile.service";
import {Profile} from "../../shared/profile/profile.model";
import {TrainerService} from "../../shared/trainer/trainer.service";
import {UserService} from "../../shared/user/user.service";
import {UserFilter} from "../../shared/user/user-filter.model";
import {Select} from "../user/admin-setting-form/select.model";
import {AddEmploee} from "../../shared/course/add-emploee.motel";
import {LessonService} from "../../shared/lessons/lesson.service";
import {DesiredTimeService} from "../../shared/desired-time/desired-time.service";
import {DesiredTime} from "../../shared/desired-time/desired-time.model";
import {DayWeek} from "../../shared/desired-time/day-week.model";
import {conditionallyCreateMapObjectLiteral} from "@angular/compiler/src/render3/view/util";
import {Lesson} from "../../shared/lessons/lesson.model";
import {el} from "@angular/platform-browser/testing/src/browser_util";
import { DomSanitizer } from '@angular/platform-browser';
import {FileResponse} from "../../shared/lessons/file-response.model";
import {FileModel} from "../../shared/lessons/file.model";

@Component({
  selector: 'app-course-page',
  templateUrl: './course-page.component.html',
})
export class CoursePageComponent implements OnInit {
  @ViewChild('trainerModal') public trainerModal: ModalDirective;
  @ViewChild('employeeModal') public employeeModal: ModalDirective;
  @ViewChild('materialsModal') public materialsModal: ModalDirective;
  @ViewChild('lessensModal') public lessensModal: ModalDirective;

  distributedPanel = true;
  unallocatedPanel = true;
  materialsPanel = true;
  lessonsPanel = true;

  closeAll() {
    this.distributedPanel = true;
    this.unallocatedPanel = true;
    this.materialsPanel = true;
    this.lessonsPanel = true;
  }

  course: Course = new Course();
  groups: Group[] = [];
  users: Profile[] = [];
  allEmployees: Profile[] = [];
  public id: string;

  constructor(private trainerService: TrainerService, private userService: UserService, private service: CourseService,
              private groupService: GroupService, private  lessonServise : LessonService, private router: Router, private roleService: RoleService,
              private route: ActivatedRoute, private toastr: ToastrService, private userServise: UserService,
              private desiredTimeService : DesiredTimeService ,private sanitizer: DomSanitizer) {
    route.params.subscribe(params => this.id = params['id']);
  }

  ngOnInit() {
    this.getInfoAboutCourse();
    if(this.roleService.roleMatch(['ADMIN'])){
      this.getTrainers();
      this.getUnallocatedUsers(this.id);
      this.getGroups();
      this.getUsersByCourse();
    }
    this.getEmloyees();
    this.getUsersDesiredTime();
    this.getCourseLessons();

    const data = 'some text';
    const blob = new Blob([data], { type: 'application/octet-stream' });

    this.fileUrl = this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(blob));
  }

  fileUrl;

  userFilter: UserFilter = {
    username: null,
    roleIds: [4],
  };

  employeesSelect: Select[] = [];
  employeeSelect: Select;

  getEmloyees() {
    this.userServise.getUsersByFilters(this.userFilter).subscribe((data: Profile[]) => {
        this.allEmployees = data;
        for (let _i = 0; _i < this.allEmployees.length; _i++) {
          this.employeeSelect = {
            text: this.allEmployees[_i].firstName + " " + this.allEmployees[_i].lastName,
            id: this.allEmployees[_i].userId
          };
          this.employeesSelect.push(this.employeeSelect);
        }
      },
      (error: HttpErrorResponse) => {
        if (error.status === 500) {
          this.toastr.error("Not possible to get information!");
        }
      });
  }

  addEmployee: AddEmploee = new AddEmploee();
  addEmploy(addEmployee: AddEmploee) {
    this.service.addEmployee(addEmployee).subscribe((data: any) => {
      this.toastr.success("employee add to course");
    },
      (error: HttpErrorResponse) => {
        this.toastr.error("Cannot add emploee to course!");
      });
    this.getUnallocatedUsers(this.id);
  }

  doSelect(value: any) {
    this.addEmployee = {
      userId: value,
      courseId: +this.id,
    };
  }

  onSubmit(lessonForm : NgForm){
    console.log(lessonForm);
    this.lessensModal.hide();
    this.addLessons(lessonForm);
  }

  addLessons(form: NgForm){
    this.lessonServise.addLesson(form.value).subscribe((data : any) =>{
        this.toastr.success( "Created new lesson!");
        this.lessonServise.getCourseLessons(this.id);
      },
      (error: HttpErrorResponse) => {
        if (error.status === 400) {
          this.toastr.error(error.error.Message);
        } else {
          this.toastr.error("Cannot inserted a lesson!");
        }
      })
  }


  getInfoAboutCourse() {
    this.service.refreshInfo(+this.id).subscribe((data: any) => {
        this.course = data as Course;
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Cannot get an information!");
      });
  }

  getInfoAboutTrainer(){

  }

  public groupId: string;
  public trainerId: string;
  public materialId: string;
  conditionAddForm: boolean = true;

  goToGroup(groupId: string) {
    if (groupId.length == 0) {
      this.toastr.warning("Please select group!");
      return;
    }
    this.router.navigate(['/groups/' + groupId]);
  }

  getGroups() {
    this.service.getGroupsOfCourse(this.id).subscribe((data: Group[]) => {
        this.groups = data;
        for(let group of this.groups){
          if(group.trainerName === "null null"){
            group.trainerName = '';
          }
        }
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to get groups!");
      });
  }

  getUsersByCourse() {
    this.service.getUsersByCourse(this.id).subscribe((data: any) => {
        this.users = data as Profile[];
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to get users!");
      });
  }

  goToProfile(profileId: number) {
    localStorage.setItem('userId', profileId.toString());
    this.router.navigate(['/users/' + profileId]);
  }

  getTrainers() {
    this.trainerService.getAllTrainers().subscribe((data: any) => {
        this.trainerService.trainers = data as Profile[];
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to get trainers!");
      });
  }

  insertTrainer(form: NgForm) {
    this.trainerService.insertTrainerToGroup(form.value).subscribe((data: any) => {
        this.getGroups();
        this.toastr.success(data.message);
        this.trainerModal.hide();
      },
      (error: HttpErrorResponse) => {
        this.toastr.error(error.message);
        this.trainerModal.hide();
      });
  }

  unallocatedUsers :  Profile[] = [];
  getUnallocatedUsers(courseId : string){
    this.service.getUnallocatedUsers(courseId).subscribe((data: Profile[]) => {
        this.unallocatedUsers =data;
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to add trainer!");
      });
  }


  lesonsSelect : Select[] = [];
  lessons : Lesson[] = [];
  getCourseLessons(){
    this.lessonServise.getCourseLessons(this.id).subscribe((data: Lesson[]) => {
        this.lessons =data;
        for(let lesson of this.lessons){
          let sel : Select = {
            text : lesson.lessonName,
            id : lesson.id,
          };
          this.lesonsSelect.push(sel);
        }
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to get lessons!");
      });
  }

  selectLesson(lessonId : any){
    this.lessonId = lessonId;
    this.getFilesByLesson();
  }



  times = [
    {name: "8:00" , count : 0},
    {name: "10:00", count : 0},
    {name: "12:00", count : 0},
    {name: "14:00", count : 0},
    {name: "16:00", count : 0},
    {name: "18:00", count : 0},
    {name: "20:00", count : 0},
  ];

  days = [
    {name : "1", times : this.times},
    {name : "2", times : this.times},
    {name : "3", times : this.times},
    {name : "4", times : this.times},
    {name : "5", times : this.times},
    {name : "6", times : this.times},
    {name : "7", times : this.times},
  ];

  userDesiredTimes : DesiredTime[] =[];
  getUsersDesiredTime(){
    this.desiredTimeService.getAllDesiredTimeForCourse(this.id).subscribe((data: DesiredTime[]) => {
        this.userDesiredTimes =data;
          for(let _i = 1; _i < 8; _i++){

            let times = [
              {name: "8:00" , count : 0},
              {name: "10:00", count : 0},
              {name: "12:00", count : 0},
              {name: "14:00", count : 0},
              {name: "16:00", count : 0},
              {name: "18:00", count : 0},
              {name: "20:00", count : 0},
            ];
            for(let time of times){
              for (let user of this.userDesiredTimes){
                if (('2019-04-0' + this.days[_i-1].name + 'T' + time.name + ':00') ===  user.userSchedule) {
                  time.count++;
                }
              }
            }
            this.days[_i-1].times = times;
          }

        },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible get information!");
      });
  }

  fileToUpload: File = null;
  fileExt: string[] = [ '.txt', '.xls' , '.docx'];
  modalImageUrl: string = null;
  handleFileInput(file: FileList, File) {
    this.fileToUpload = file.item(0);
    var ext = this.fileToUpload.name.match(/\..+$/)[0];
    console.log(this.fileToUpload.name);
    if (this.fileExt.join().search(ext[ext.length - 1]) == -1) {
      this.toastr.warning('Your file must have a format: .txt .xls .txt');
      File.value = null;
      return;
    }
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.modalImageUrl = event.target.result;
    };
    reader.readAsDataURL(this.fileToUpload);
  }

  lessonId : number;
  OnFileSubmit(File) {
    this.materialsModal.hide();
    this.lessonServise.uploadFile(this.fileToUpload , this.lessonId).subscribe((data: any) => {
      this.getFilesByLesson();
        this.toastr.success("file upload!");
        File.value = null;
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Cannot add file!");

      });
  }

  public ngxControl = new FormControl();

  private _ngxDefaultTimeout;
  private _ngxDefaultInterval;
  private _ngxDefault;

  ngOnDestroy(): void {
    clearTimeout(this._ngxDefaultTimeout);
    clearInterval(this._ngxDefaultInterval);
  }

  public doNgxDefault1(): any {
    return this._ngxDefault;
  }

  public doNgxDefault2(): any {
    return this._ngxDefault;
  }

  public doNgxDefault3(): any {
    return this._ngxDefault;
  }

  public inputTyped = (source: string, text: string) => console.log('SingleDemoComponent.inputTyped', source, text);

  public doFocus = () => console.log('SingleDemoComponent.doFocus');

  public doBlur = () => console.log('SingleDemoComponent.doBlur');

  public doOpen = () => console.log('SingleDemoComponent.doOpen');

  public doClose = () => console.log('SingleDemoComponent.doClose');

  public doRemove = (value: any) => console.log('SingleDemoComponent.doRemove', value);



  userTime: string = '';
  daysUser: DayWeek[] = [];
  userDesiredTimesList: DesiredTime[] = [];
  getTime(userId : string) {
    this.desiredTimeService.getUserDesiredTime(userId).subscribe((data: DesiredTime[]) => {
        this.userDesiredTimesList = data ;
        let days : DayWeek[] = [];
        for (let _i = 1; _i < 8; _i++) {
          let day = {
            times: [{name: "08:00", isSelect: this.isDesiredTime("08:00", [_i].toString(), this.userDesiredTimesList )},
              {name: "10:00", isSelect: this.isDesiredTime("10:00", [_i].toString() , this.userDesiredTimesList)},
              {name: "12:00", isSelect: this.isDesiredTime("12:00", [_i].toString(), this.userDesiredTimesList)},
              {name: "14:00", isSelect: this.isDesiredTime("14:00", [_i].toString(), this.userDesiredTimesList)},
              {name: "16:00", isSelect: this.isDesiredTime("16:00", [_i].toString(), this.userDesiredTimesList)},
              {name: "18:00", isSelect: this.isDesiredTime("18:00", [_i].toString(), this.userDesiredTimesList)},
              {name: "20:00", isSelect: this.isDesiredTime("20:00", [_i].toString(), this.userDesiredTimesList)},],
          };
          days.push(day);
        }
        this.daysUser = days;
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to get information!");
      });
  }

  files : FileResponse[] =[];
  filesSelect : Select[] =[];
  getFilesByLesson(){
    this.lessonServise.getLessonsFile(this.lessonId).subscribe((data: FileResponse[]) => {
        this.files = data;
        let selec : Select[] =[];
        for(let file of this.files){
          let sel : Select = {
            text : file.fileName,
            id : file.fileId,
          };
          selec.push(sel);
        }
        this.filesSelect = selec;
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Cannot get file!");

      });
  }

  isDesiredTime(time: string, dayWeek: string , userDesiredTimesList: DesiredTime[]) {
    for (let _i = 0; _i < userDesiredTimesList.length; _i++) {
      if (('2019-04-0' + dayWeek + 'T' + time + ':00') ===  userDesiredTimesList[_i].userSchedule) {
        return true;
      }
    }
    return false;
  }

  fileId : number;
  fileName : string ;
  selectFile(fileId : any){
    this.fileId = fileId;
    this.fileName = this.filesSelect.find(x=>x.id ===fileId).text;
  }





}
