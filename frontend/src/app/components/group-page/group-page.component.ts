import {Component, OnInit, ViewChild} from '@angular/core';
import {ModalDirective} from "ngx-bootstrap";
import {AccordionPanelComponent} from 'ngx-bootstrap/accordion';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {RoleService} from "../../shared/user/role.service";
import {ActivatedRoute, Router} from "@angular/router";
import {ToastrService} from "ngx-toastr";
import {Group} from "../../shared/group/group.model";
import {GroupService} from "../../shared/group/group.service";
import {User} from "../../shared/user/user.model";
import {Profile} from "../../shared/profile/profile.model";
import {FormControl, NgForm} from "@angular/forms";
import {LessonService} from "../../shared/lessons/lesson.service";
import {Lesson} from "../../shared/lessons/lesson.model";
import {SetLessonsDateFoGroupModel} from "../../shared/lessons/setLessons-date-fo-group.model";
import {ChatMessage} from "../../shared/chat/message.model";
import {WebSocketService} from "../../shared/chat/web-socket.service";
import {ChatModel} from "../../shared/chat/chat.model";
import {SetUserToGrop} from "../../shared/user/set-user-to-group.nodel";
import {CourseService} from "../../shared/course/course.service";
import {Select} from "../user/admin-setting-form/select.model";
import {Course} from "../../shared/course/course.model";
import {TrainerService} from "../../shared/trainer/trainer.service";
import {TrainerInfo} from "../../shared/trainer/trainer-info.model";
import {UserService} from "../../shared/user/user.service";
import {ProfileService} from "../../shared/profile/profile.service";
import {DomSanitizer} from "@angular/platform-browser";



@Component({
  selector: 'app-group-page',
  templateUrl: './group-page.component.html',
})
export class GroupPageComponent {
  id: string;
  isCollapsed = false;
  group: Group = new Group();
  profiles: Profile[] = [];
  lessons: Lesson[] = [];
  messages: ChatMessage[] = [];
  currentUser: Profile = new Profile();
  messageText = '';

  constructor(private sanitizer: DomSanitizer, private  lessonServise: LessonService, private service: GroupService, private router: Router,
              private roleService: RoleService, private route: ActivatedRoute, private toastr: ToastrService,
              private socketService: WebSocketService,private courseService : CourseService ,
              private  trainerSerrvice : TrainerService , private profileService : ProfileService) {
    route.params.subscribe(params => this.id = params['id']);
  }

  lessonsDeletedId: number = null;

  usersPanel = false;
  lessonsPanel = true;
  chatPanel = true;
  schedulePanel = true;

  closeAll() {
    this.usersPanel = true;
    this.lessonsPanel = true;
    this.chatPanel = true;
    this.schedulePanel = true;
  }

  ngOnInit() {
    this.getInfo();
  }

  @ViewChild('p1') p1: AccordionPanelComponent;
  @ViewChild('p2') p2: AccordionPanelComponent;
  isP1Open: boolean = true;
  isP2Open: boolean = false;

  open(panelNum) {
    const panel = this[`p${panelNum}`];
    panel.isOpen = !panel.isOpen;
  }

  getMembersInfo() {
    this.service.getMembersInfo(this.id).subscribe((data: any) => {
        this.profiles = data as Profile[];
      },
      (error: HttpErrorResponse) => {
        if (error.status === 400) {
          this.toastr.error(error.error.Message);
        } else {
          this.toastr.error("Cannot get users!");
        }
      });
  }

  goToProfile(profileId: number) {
    localStorage.setItem('userId', profileId.toString());
    this.router.navigate(['/users/' + profileId]);
  }

  goToCourse(courseId: number) {
    this.router.navigate(['/courses/' + courseId]);
  }

  getInfo() {
    this.service.refreshInfo(this.id).subscribe((data: any) => {
        this.group = data as Group;
        this.getMembersInfo();
        this.setDate.groupId = this.group.id;
        this.getLessons();
        this.getCourseLessons();
        if(this.roleService.roleMatch(['ADMIN'])){
          this.getUnallocatedUsers(this.group.courseId.toString());
        }
        this.getInfoAboutCourse();
        this.getTrainerInfo();
      },
      (error: HttpErrorResponse) => {
        if (error.status === 400) {
          this.toastr.error(error.error.Message);
        } else {
          this.toastr.error("Cannot get information!!");
        }
      });
  }

  course : Course = new Course();
  getInfoAboutCourse() {
    this.courseService.refreshInfo(+this.group.courseId).subscribe((data: any) => {
        this.course = data as Course;
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Cannot get an information!");
      });
  }

  trainerInfo : TrainerInfo ={
    userId : null,
    login : null,
    firstName : '',
    lastName : "",
    email : null,
    roleId : null,
    avatar : null,
    qualificationName : '',
  };

  getTrainerInfo(){
    this.trainerSerrvice.getTrainerInfo(this.group.trainerId.toString()).subscribe((data: TrainerInfo) => {
        this.trainerInfo = data;
          let byteArrayImage: any;
          let imageData: any;
          let sanitizeImageData: any;
          byteArrayImage = data["avatar"];
          if (byteArrayImage != null) {
            imageData = 'data:image/png;base64,' + byteArrayImage;
            sanitizeImageData = this.sanitizer.bypassSecurityTrustUrl(imageData);
            this.trainerInfo.avatar = sanitizeImageData;
          }
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to get info about trainer!");
      });
  }

  unallocatedUsers : Profile[] =[];
  unallocatedUsersSelect : Select[] = [];
  getUnallocatedUsers(courseId : string){
    this.courseService.getUnallocatedUsers(courseId).subscribe((data: Profile[]) => {
        this.unallocatedUsers =[];
        this.unallocatedUsersSelect =[];
        this.unallocatedUsers =data;
        for (let user of this.unallocatedUsers){
          let userSelect : Select = {
            text : user.username + " "+ user.email,
          id : user.userId,
          };
          this.unallocatedUsersSelect.push(userSelect);
        }
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to add trainer!");
      });
  }

  Courselessons : Lesson[] = [];
  getCourseLessons(){
    this.lessonServise.getCourseLessons(this.group.courseId.toString()).subscribe((data: Lesson[]) => {
        this.Courselessons =data;
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to get lessons!");
      });
  }

  getLessons(){
    this.lessonServise.getGroupLessons(+this.id).subscribe((data: any) => {
      this.lessons = data as Lesson[];
      for (let i = 0 ; i <  this.lessons.length ; i++) {
        for(let _i = 0; _i< this.lessons.length-1 ; _i++){
          if(+this.lessons[_i].lessonDate.substr(5 , 2)>+this.lessons[_i+1].lessonDate.substr(5,2)){
            let les : Lesson = this.lessons[_i];
            this.lessons[_i] = this.lessons[_i+1];
            this.lessons[_i+1] = les;
          }
          if((+this.lessons[_i].lessonDate.substr(5 , 2)===+this.lessons[_i+1].lessonDate.substr(5,2))
            &&+this.lessons[_i].lessonDate.substr(8 , 2)>+this.lessons[_i+1].lessonDate.substr(8,2)){
            let les : Lesson = this.lessons[_i];
            this.lessons[_i] = this.lessons[_i+1];
            this.lessons[_i+1] = les;
          }
        }
      }
    }, (error: HttpErrorResponse) => {
      if (error.status === 400) {
        this.toastr.error(error.error.Message);
      } else {
        this.toastr.error("Cannot get information!!");
      }
    });
  }

  onSubmit(lessonForm: NgForm) {
    this.lessensModal.hide();
      this.setDateFoLesson();
  }

  userId : number;
  selectUser(userId : any){
    this.userId = userId;
  }
  addUserToGroup( userId : any){
    let setUser : SetUserToGrop = {
      userId : userId,
      groupId : +this.id,
    };
    this.service.setUserToGroup(setUser).subscribe((data : any) =>{
        this.getMembersInfo();
        this.getUnallocatedUsers(this.group.courseId.toString());
        this.toastr.success( "user added to group!");
      },
      (error: HttpErrorResponse) => {
        if (error.status === 400) {
          this.toastr.error(error.error.Message);
        } else {
          this.toastr.error("failed to add user to group!");
        }
      });
    this.userId = null;
    this.employeeModal.hide();
  }

  lessonsDate : Date  = new Date();
  hours : string;
  minutes : number;
  setDate : SetLessonsDateFoGroupModel = {
    lessonId : null,
    groupId : +this.id,
    lessonDate : ''
  };

  setDateFoLesson(){
    if (+this.hours > 23) {
      this.hours = '23';
      this.lessonsDate.setHours(+this.hours +3 , 0 , 0 ,0);
    } else {
      this.lessonsDate.setHours(+this.hours +3 , 0 , 0 ,0);
    }
    if (this.minutes > 59) {
      this.minutes = 59;
      this.lessonsDate.setMinutes(this.minutes , 0 ,0);
    } else {
      this.lessonsDate.setMinutes(this.minutes , 0 ,0);
    }
    console.log(this.lessonsDate.toJSON());
    this.setDate.lessonDate = this.lessonsDate.toJSON();
    this.lessonServise.setDateForLessonOfGroup(this.setDate).subscribe((data : any) =>{
        this.toastr.success("schedule changed!");
      this.getLessons();
      },
      (error: HttpErrorResponse) => {
        if (error.status === 400) {
          this.toastr.error(error.error.Message);
        } else {
          this.toastr.error("You have not changed the schedule!");
        }
      });
    this.lessonsDate= null;
    this.hours= null;
    this.minutes= null;
  }

  connectToGroupChat() {
    this.getCurrentUserInfo();
    const _this = this;
    const groupId = +_this.id;
    this.service.getChatByGroupId(groupId).subscribe((data: ChatModel) => {
      console.log('!!!!!!!!' + data);
      _this.socketService.disconnect();
      _this.socketService.connect(data.chatId);
      if (_this.messages.length >= 1) {
        _this.messages = _this.messages.splice(0, _this.messages.length);
        _this.socketService.messages = _this.socketService.messages.splice(0, _this.socketService.messages.length);
      }
      _this.messages = _this.socketService.messages;
    });
  }

  public sendMessage() {
    const _this = this;
    _this.socketService.sendMessage(_this.messageText, _this.currentUser.userId, _this.currentUser.username);
    _this.messageText = '';
  }

  getCurrentUserInfo() {
    const _this = this;
    _this.socketService.getUserInfo().subscribe((data) => {
        _this.currentUser = data as Profile;
      },
      (error: HttpErrorResponse) => {
        if (error.status === 500) {
          _this.toastr.error('Not possible to get information!');
        }
      });
  }

  getLessonDate(lessonName: string){
    if(this.lessons.find(x => x.lessonName === lessonName)!= undefined){
    let date = this.lessons.find(x => x.lessonName === lessonName).lessonDate;
    return date;
    }
    else  return "-";
  }

  declineDeleteLesson() {
    this.lessonsDeletedId = null;
    this.deleteConfirmModal.hide();
  }

  confirmDeleteLesson(lessonId: number){
    this.lessonServise.canselLesson(lessonId , +this.id).subscribe((data) => {
        this.getLessons();
        this.toastr.success('lesson canceled!');
      },
      (error: HttpErrorResponse) => {
        if (error.status === 500) {
          this.toastr.error('Not possible to get information!');
        }
      });
    this.deleteConfirmModal.hide();
  }


  @ViewChild('lessensModal') public lessensModal: ModalDirective;
  @ViewChild('employeeModal') public employeeModal: ModalDirective;
  @ViewChild('deleteConfirmModal') public deleteConfirmModal: ModalDirective;

  public ngxControl = new FormControl();

  private _ngxDefaultTimeout;
  private _ngxDefaultInterval;
  private _ngxDefault;

  ngOnDestroy(): void {
    clearTimeout(this._ngxDefaultTimeout);
    clearInterval(this._ngxDefaultInterval);
  }

  public doNgxDefault1(): any {
    return this._ngxDefault;
  }

  public doNgxDefault2(): any {
    return this._ngxDefault;
  }

  public doNgxDefault3(): any {
    return this._ngxDefault;
  }

  public inputTyped = (source: string, text: string) => console.log('SingleDemoComponent.inputTyped', source, text);

  public doFocus = () => console.log('SingleDemoComponent.doFocus');

  public doBlur = () => console.log('SingleDemoComponent.doBlur');

  public doOpen = () => console.log('SingleDemoComponent.doOpen');

  public doClose = () => console.log('SingleDemoComponent.doClose');
  doSelectOptions($event){

  }
}
