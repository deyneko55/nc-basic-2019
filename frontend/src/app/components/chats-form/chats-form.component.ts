import {Component, ViewChild} from '@angular/core';
import {FormControl} from '@angular/forms';
import {INgxSelectOption} from 'ngx-select-ex';
import {WebSocketService} from '../../shared/chat/web-socket.service';
import {ChatModel} from '../../shared/chat/chat.model';
import {Profile} from '../../shared/profile/profile.model';
import {HttpErrorResponse} from '@angular/common/http';
import {ToastrService} from 'ngx-toastr';
import {ChatMessage} from '../../shared/chat/message.model';
import {Select} from "../user/admin-setting-form/select.model";
import {UserService} from "../../shared/user/user.service";


@Component({
  selector: 'chats-form',
  templateUrl: './chats-form.component.html'
})
export class ChatsForm {
  constructor(private service: WebSocketService, private toastr: ToastrService, private userService: UserService) {
    this._ngxDefaultTimeout = setTimeout(() => {
      this._ngxDefaultInterval = setInterval(() => {
        const idx = Math.floor(Math.random() * (this.usersInGroup.length - 1));
        this._ngxDefault = this.usersInGroup[idx];
      }, 2000);
    }, 2000);
  }

  @ViewChild('write_msg') formValue;
  chats: ChatModel[] = [];
  click = true;
  currentUser: Profile = new Profile();
  messageText = '';
  messages: ChatMessage[] = [];
  selectedUserId: number;
  selectedUserName: string;

  public usersInGroup = [
    'User 1',
    'User 2',
    'User 3',
    'User 4',
    'User 5',
    'User 6',
    'User 7',
    'User 8'];

  usersSelect: Select[] = [];
  userSelect: Select;

  getUsers() {
    this.userService.getGroupMates(+localStorage.getItem('constant_userId')).subscribe((data: any) => {
        this.userService.usersProfileList = data as Profile[];
        for (let _i = 0; _i < this.userService.usersProfileList.length; _i++) {
          this.userSelect = {
            text: this.userService.usersProfileList[_i].username,
            id: this.userService.usersProfileList[_i].userId
          };
          if (this.userService.usersProfileList[_i].userId != +localStorage.getItem('constant_userId')) {
            let found: boolean = false;
            for (let j in this.chats) {
              if (!this.chats[j].chatName.includes(this.userSelect.text) && (found == false)) {
                //console.log(!this.chats[j].chatName.includes(this.userSelect.text) + ' PLUS ' + (found == false));
                this.usersSelect.push(this.userSelect);
                found = true;
              }
            }
          }
        }
      },
      (error: HttpErrorResponse) => {
        this.toastr.error('Not possible to get users!');
      });
  }


  public ngxControl = new FormControl();

  private _ngxDefaultTimeout;
  private _ngxDefaultInterval;
  private _ngxDefault;

  ngOnInit() {
    this.getCurrentUserInfo();
    this.getUserChats();
    this.getUsers();
  }

  public doNgxDefault(): any {
    return this._ngxDefault;
  }

  public connectToChat(chatId: number) {
    this.getCurrentUserInfo();
    const _this = this;
    _this.service.disconnect();
    //_this.service.connectToNotifications();
    _this.service.connect(chatId);
    if (_this.messages.length >= 1) {
      _this.messages = _this.messages.splice(0, _this.messages.length);
      _this.service.messages = _this.service.messages.splice(0, _this.service.messages.length);
    }
    _this.messages = _this.service.messages;
  }

  getCurrentUserInfo() {
    const _this = this;
    _this.service.getUserInfo().subscribe((data) => {
        _this.currentUser = data as Profile;
      },
      (error: HttpErrorResponse) => {
        if (error.status === 500) {
          _this.toastr.error('Not possible to get information!');
        }
      });
  }

  public sendMessage() {
    const _this = this;
    _this.service.sendMessage(_this.messageText, _this.currentUser.userId, _this.currentUser.username);
    _this.messageText = '';
  }

  public startPrivateChat() {
    const _this = this;
    // get first&last name by id
    for (let i in this.usersSelect) {
      if (this.usersSelect[i].id === this.selectedUserId) {
        this.selectedUserName = this.usersSelect[i].text;
        this.usersSelect.splice(+i, 1);
      }
    }
    _this.service.createPrivateChat(this.currentUser.userId, this.selectedUserId,
      _this.currentUser.username + ' ' + _this.selectedUserName).subscribe((data: ChatModel) => {
      data.chatName = _this.selectedUserName;
      _this.chats.unshift(data);
    });
  }

  public inputTyped = (source: string, text: string) => console.log('SingleDemoComponent.inputTyped', source, text);

  public doFocus = () => console.log('SingleDemoComponent.doFocus');

  public doBlur = () => console.log('SingleDemoComponent.doBlur');

  public doOpen = () => console.log('SingleDemoComponent.doOpen');

  public doClose = () => console.log('SingleDemoComponent.doClose');

  public doSelect = (value: number) => {
    this.selectedUserId = value;
  };

  public doRemove = (value: any) => console.log('SingleDemoComponent.doRemove', value);

  public doSelectOptions = (options: INgxSelectOption[]) => console.log('SingleDemoComponent.doSelectOptions', options);


  private async getUserChats() {
    console.log(this.currentUser);
    if (!this.currentUser.username) {
      console.log(this.currentUser.username);
      await this.getCurrentUserInfo();
    }
    this.service.getUserChats().subscribe((data: ChatModel[]) => {
        this.chats = data;
        for (let i in this.chats) {
          if (this.chats[i].chatName.includes(this.currentUser.username)) {
            this.chats[i].chatName = this.chats[i].chatName.replace(this.currentUser.username, '');
          }
        }
      },
      (error: HttpErrorResponse) => {
        if (error.status === 500) {
          this.toastr.error('Not possible to get information!');
        }
      });
  }
}

