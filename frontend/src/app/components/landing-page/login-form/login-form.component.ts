import {Component} from '@angular/core';
import {UserService} from "../../../shared/user/user.service";
import {Router} from "@angular/router";
import {HttpErrorResponse} from "@angular/common/http";
import {RoleService} from "../../../shared/user/role.service";
import {Login} from "../../../shared/user/login.model";
import {ToastrService} from "ngx-toastr";
import {WebSocketService} from "../../../shared/chat/web-socket.service";

@Component({
  selector: 'login-form',
  templateUrl: './login-form.component.html',
})
export class LoginForm {
  constructor(private userService: UserService, private router: Router, private roleService: RoleService, private toastr: ToastrService) {
  }

  login: Login = new Login();

  OnSubmit(userName: string, passWord: string) {
    this.userService.userAunthentication(userName, passWord).subscribe((data: any) => {
        localStorage.setItem('userToken', data.accessToken);
        localStorage.setItem('userRoles', data.userRoles[0].authority);
        localStorage.setItem('userId', data.userId);
        localStorage.setItem('constant_userId', data.userId);
        this.router.navigate(['/users/' + data.userId]);
      },
      (error: HttpErrorResponse) => {
        if (error.status === 400) {
          this.toastr.error("wrong login or password!");
        }
      })
  }
}

