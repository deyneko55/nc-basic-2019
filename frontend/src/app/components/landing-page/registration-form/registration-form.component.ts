import {Component, OnInit, ViewChild} from '@angular/core';
import {User} from "../../../shared/user/user.model";
import {UserService} from "../../../shared/user/user.service";
import {NgForm} from "@angular/forms";
import {HttpErrorResponse} from "@angular/common/http";
import {ActivatedRoute} from "@angular/router";
import {ToastrService} from "ngx-toastr";
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
  selector: 'registration-form',
  templateUrl: './registration-form.component.html',
})
export class RegistrationForm implements OnInit {
  user: User = new User();

  @ViewChild('registerModal') public registerModal: ModalDirective;
  constructor(private userService: UserService, private route: ActivatedRoute, private toastr: ToastrService) {
    this.route.queryParams.subscribe(params => {
      userService.token = params['token'];
    })
  }

  ngOnInit() {
    console.log("я регерстрируюсь!");
    this.resetForm();
  }


  resetForm(form?: NgForm) {
    if (form != null)
      form.reset();
    this.user = {
      username: '',
      firstName: '',
      lastName: '',
      password: '',
      email: '',
      roleId: 4
    }
  }

  OnSubmit(form: NgForm) {
    this.userService.registerUserByToken(form.value , this.userService.token).subscribe((data: User) => {
        this.toastr.success(`User ${data.username} was create!`);
        this.resetForm(form);
      },
      (error: HttpErrorResponse) => {
        this.toastr.error(error.message);
      });
  }
}

