import { Component } from '@angular/core';
import {UserService} from "../../../../shared/user/user.service";

@Component({
    selector: 'authentication-container',
    templateUrl: './authentication-container.component.html',
    styleUrls: ['./authentication-container.component.css']
})
export class InfoAuthenticationContainer {

  constructor(private userService : UserService){
  }
} 

