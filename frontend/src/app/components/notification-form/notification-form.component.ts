import {Component} from '@angular/core';
import {NotificationService} from "../../shared/notification/notification.service";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {ToastrService} from "ngx-toastr";
import {ChatModel} from "../../shared/chat/chat.model";
import {NotificationModel} from "../../shared/notification/notification.model";

@Component({
  selector: 'notification-form',
  templateUrl: './notification-form.component.html',
  //styleUrls: ['../css/all.css', ]
})
export class NotificationForm {

  constructor (private notificationService: NotificationService, private toastr: ToastrService) {}

  ngOnInit() {
    this.getAllUserNotifications();
  }

  notif: NotificationModel [] = [];

  getUnreadNotifications(){

  }

  getLastNotifications(){

  }

  getAllUserNotifications(){
    this.notificationService.getAllUserNotifications().subscribe((data: NotificationModel[]) => {
        this.notif = data;
      },
      (error: HttpErrorResponse) => {
        if (error.status === 500) {
          this.toastr.error('Not possible to get information!');
        }
      });
  }

  markAllAsRead(){

  }
}

