import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {RoleService} from "../../../shared/user/role.service";
import {ToastrService} from "ngx-toastr";
import {ProfileService} from "../../../shared/profile/profile.service";
import {Profile} from "../../../shared/profile/profile.model";
import {HttpErrorResponse} from "@angular/common/http";
import {ModalDirective} from 'ngx-bootstrap';
import {NgForm} from "@angular/forms";
import {DomSanitizer} from '@angular/platform-browser';
import {DesiredTimeService} from "../../../shared/desired-time/desired-time.service";
import {TrainerService} from "../../../shared/trainer/trainer.service";
import {FeedbackAboutUser} from "../../../shared/trainer/feedback-about-user.model";


@Component({
  selector: 'user-profile-form',
  templateUrl: './user-profile-form.component.html',
  //styleUrls: ['../css/all.css', ]
})
export class UserProfileForm implements OnInit {
  @ViewChild('profileModal') public mainInfoModal: ModalDirective;
  @ViewChild('imageProfileModal') public imageProfileModal: ModalDirective;
  profile: Profile = new Profile();
  fileToUpload: File = null;
  imageData: any;
  imageUrl: string = "assets/img/icon_profile.png";
  modalImageUrl: string = null;
  sanitizeImageData: any = null;
  byteArrayImage: any;

  constructor(private sanitizer: DomSanitizer, private service: ProfileService, private roleService: RoleService,
              private toastr: ToastrService , private trainerService : TrainerService) {
  }


  setImageProfile() {
    if (this.sanitizeImageData == null) {
      return this.imageUrl;
    }
    return this.sanitizeImageData;
  }

  ngOnInit() {
    this.getInfo();
    this.getPhotoProfile();
    if (!this.roleService.roleMatch(['EMPLOYEE'])) {
      this.getFeedback();
    }
  }

  updateProfile(profile: Profile) {
    this.service.formData = Object.assign({}, profile);
  }

  getPhotoProfile() {
    this.service.getPhoto().subscribe((data) => {
          this.byteArrayImage = data["avatar"];
      if (this.byteArrayImage != null) {
        this.imageData = 'data:image/png;base64,' + this.byteArrayImage;
        this.sanitizeImageData = this.sanitizer.bypassSecurityTrustUrl(this.imageData);
      }
    });
  }


  getInfo() {
    this.service.refreshInfo().subscribe((data) => {
        this.profile = data as Profile
      },
      (error: HttpErrorResponse) => {
        if (error.status === 500) {
          this.toastr.error("Not possible to get information!");
        }
      });
  }

  OnSubmit(form: NgForm) {
    this.mainInfoModal.hide();
    this.service.updateProfile(form.value).subscribe(() => {
        this.getInfo();
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Cannot edit a profile!");
      });
  }

  handlerCloseModal(Image: any) {
    Image = null;
    this.modalImageUrl = null;
  }

  OnImageProfileSubmit(Image) {
    this.imageProfileModal.hide();
    this.service.postFile(this.fileToUpload).subscribe((data: any) => {
        Image.value = null;
        this.getPhotoProfile();
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Cannot change an image!");

      });
  }

  fileExt: string[] = ['.jpeg', '.png', '.jpg'];

  handleFileInput(file: FileList, Image) {
    this.fileToUpload = file.item(0);
    var ext = this.fileToUpload.name.match(/\..+$/)[0];
    if (this.fileExt.join().search(ext[ext.length - 1]) == -1) {
      this.toastr.warning('Your file must have a format: .png .jpeg .jpg');
      Image.value = null;
      return;
    }
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.modalImageUrl = event.target.result;
    };
    reader.readAsDataURL(this.fileToUpload);
  }


  public messages = [
    {author: 'Traner 1', info: 'Lol', course: 'Course 1', group: 'Group 1', data: '13 Juny'},
    {author: 'Traner 2', info: 'Lol_lol', course: 'Course 1', group: 'Group 2', data: '13 Juny'},
    {author: 'Traner 3', info: 'Kek', course: 'Course 2', group: 'Group 1', data: '13 Juny'},
    {author: 'Traner 4', info: 'Super', course: 'Course 2', group: 'Group 2', data: '13 Juny'},

  ];

  isShow(){
    if(localStorage.getItem('constant_userId')===localStorage.getItem('userId')){
      return true;
    }
    return false;
  }

  feedbackAboutUser : FeedbackAboutUser[]=[];
  getFeedback(){
    this.trainerService.getUserFeedback(localStorage.getItem('userId')).subscribe((data: FeedbackAboutUser[]) => {
        this.feedbackAboutUser=data;
        console.log(this.feedbackAboutUser);
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Cannot get feedback!");
      });
  }

  isShowFeedback(){
    if(this.roleService.roleMatch(['ADMIN'])){
      if(localStorage.getItem('constant_userId')===localStorage.getItem('userId')){
        return false;
      }
      else {
        return true;
      }
    }
    if(this.roleService.roleMatch(['TRAINER'])){
      if(localStorage.getItem('constant_userId')===localStorage.getItem('userId')){
        return false;
      }
      else {
        return true;
      }
    }
    if(this.roleService.roleMatch(['MANAGER'])){
      if(localStorage.getItem('constant_userId')===localStorage.getItem('userId')){
        return false;
      }
      else {
        return true;
      }
    }
    if(this.roleService.roleMatch(['EMPLOYEE'])){
        return false;
      }
  }

}

