import { Component } from '@angular/core';
import {TrainerService} from "../../../../shared/trainer/trainer.service";
import {UserService} from "../../../../shared/user/user.service";
import {CourseService} from "../../../../shared/course/course.service";
import {GroupService} from "../../../../shared/group/group.service";
import {ActivatedRoute, Router} from "@angular/router";
import {RoleService} from "../../../../shared/user/role.service";
import {ToastrService} from "ngx-toastr";


@Component({
    selector: 'wrapper-container',
    templateUrl: './wrapper-container.component.html',
    //styleUrls: ['../../css/all.css']
})
export class WrapperContainer {

  constructor( private roleService: RoleService) {}
}

