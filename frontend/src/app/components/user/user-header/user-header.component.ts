import { Component } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {RoleService} from "../../../shared/user/role.service";
import {UserService} from "../../../shared/user/user.service";
import {ToastrService} from "ngx-toastr";
import {WebSocketService} from "../../../shared/chat/web-socket.service";
import {NotificationModel} from "../../../shared/notification/notification.model";
import {NotificationService} from "../../../shared/notification/notification.service";
import {HttpErrorResponse} from "@angular/common/http";


@Component({
    selector: 'user-header',
    templateUrl: './user-header.component.html',
    //styleUrls: ['../css/all.css', ]
})
export class UserHeader {
  public id : string;
  notifications = true;

  constructor(private router: Router,private route: ActivatedRoute,private service : RoleService,
              private roleService : RoleService, private toastr : ToastrService, private websocketService: WebSocketService,
              private notificationService: NotificationService) {
    route.params.subscribe(params=>this.id=params['id']);
    if(this.id != localStorage.getItem('userId'))
      localStorage.setItem('userId',this.id);
    this.getLastNotifications();
  }

  notif: NotificationModel [] = [];

  getLastNotifications(){
    this.notificationService.getUserLastNotifications().subscribe((data: NotificationModel[]) => {
        this.notif = data;
      });
  }

  ngOnInit(){
    this.roleService.getRoles();
  }
  goToProfile(){
    this.router.navigate(['/users/' + localStorage.getItem('constant_userId')]);
  }
  logOut(){
    localStorage.removeItem('userToken');
    localStorage.removeItem('userId');
    localStorage.removeItem('userRoles');
    //localStorage.removeItem('constant_userId');
    this.websocketService.disconnectFromNotifications();
    this.router.navigate(['']);
  }
}

