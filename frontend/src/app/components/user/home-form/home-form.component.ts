import {Component, OnInit, ViewChild} from '@angular/core';
import {ModalDirective, PageChangedEvent} from "ngx-bootstrap";
import {NewsService} from "../../../shared/news/news.service";
import {News} from "../../../shared/news/news.model";
import {Course} from "../../../shared/course/course.model";
import {HttpErrorResponse} from "@angular/common/http";
import {ToastrService} from "ngx-toastr";
import {DomSanitizer} from "@angular/platform-browser";
import {Router} from "@angular/router";
import {NgForm} from "@angular/forms";
import {AddNew} from "../../../shared/news/add-new.model";
import {Profile} from "../../../shared/profile/profile.model";
import {ProfileService} from "../../../shared/profile/profile.service";
import {RoleService} from "../../../shared/user/role.service";
import {AddEmploee} from "../../../shared/course/add-emploee.motel";


@Component({
  selector: 'home-form',
  templateUrl: './home-form.component.html',
})
export class HomeForm implements OnInit {
  public id: string;
  newsList: News[] = [];
  newsListSearch: News[] = [];
  sliceViaPaginationNewsList: News[];
  @ViewChild('homeModal') public homeModal: ModalDirective;

  constructor(private service: NewsService, private toastr: ToastrService, private sanitizer: DomSanitizer,
              private router: Router, private roleService: RoleService) {
  }

  pageChanged(event: PageChangedEvent): void {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.sliceViaPaginationNewsList = this.newsList.slice(startItem, endItem);
  }

  resetForm(form?: NgForm) {
    if (form != null)
      form.resetForm();
    this.addNew = {
      description: '',
      header: '',
      content: ''
    }
  }

  ngOnInit(): void {
    this.getAllNews();
  }

  getAllNews() {
    this.service.getAllNews().subscribe((data: News[]) => {
        this.newsList = data;
        for (let i = 0; i < data.length; i++) {
          let byteArrayImage: any;
          let imageData: any;
          let sanitizeImageData: any;
          byteArrayImage = data[i]["photo"];
          if (byteArrayImage != null) {
            imageData = 'data:image/png;base64,' + byteArrayImage;
            sanitizeImageData = this.sanitizer.bypassSecurityTrustUrl(imageData);
            this.newsList[i].picture = sanitizeImageData;
          }
        }
        this.newsListSearch = this.newsList;
        this.sliceViaPaginationNewsList = this.newsList.slice(0, 9);
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Cannot get an information!");
      });
  }

  goToNews(newId: number) {
    localStorage.setItem('newId', newId.toString());
    this.router.navigate(['/news/' + newId]);
  }

  addNew: AddNew = new AddNew();

  addNews() {
    this.homeModal.hide();
    this.service.createNews(this.addNew).subscribe((data: any) => {
        this.toastr.success("news inserted!");
        this.newsId = data.newsPostId;
        this.OnImageProfileSubmit(Image);
        this.service.getAllNews();
      },
      (error: HttpErrorResponse) => {
        if (error.status === 400) {
          this.toastr.error("Cannot inserted a news!");
        }
      });
  }

  newsId: number;

  OnImageProfileSubmit(Image) {
    this.service.postPhotoNews(this.fileToUpload, this.newsId).subscribe((data: any) => {
        Image.value = null;
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Cannot add an image!");

      });
  }

  fileExt: string[] = ['.jpeg', '.png', '.jpg'];
  fileToUpload: File = null;
  modalImageUrl: string = null;

  handleFileInput(file: FileList, Image) {
    this.fileToUpload = file.item(0);
    var ext = this.fileToUpload.name.match(/\..+$/)[0];
    if (this.fileExt.join().search(ext[ext.length - 1]) == -1) {
      this.toastr.warning('Your file must have a format: .png .jpeg .jpg');
      Image.value = null;
      return;
    }
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.modalImageUrl = event.target.result;
    };
    reader.readAsDataURL(this.fileToUpload);
  }

  setLike(newsId: number, isLiked: boolean) {
    isLiked = !isLiked;
    this.service.setLike(+localStorage.getItem('constant_userId'), newsId, isLiked).subscribe((data: any) => {
        this.getAllNews();
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Cannot set like!");

      });
  }

  searchString: string;

  search() {
    console.log(this.searchString);
    let searchNews: News[] = [];
    for (let news of this.newsList) {
      if ((news.header.substr(0, this.searchString.length) === this.searchString)) {
        searchNews.push(news);
      }
    }
    this.newsListSearch = searchNews;
    console.log(this.newsListSearch);
    if (this.searchString === '') {
      this.newsListSearch = this.newsList;
      this.sliceViaPaginationNewsList = this.newsList.slice(0, 9);
    } else {
      this.sliceViaPaginationNewsList = this.newsListSearch.slice(0, 9);
    }
  }
}

