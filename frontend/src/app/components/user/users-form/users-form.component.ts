import {Component, ViewChild} from '@angular/core';
import {FormControl, NgForm} from "@angular/forms";
import {ModalDirective, PageChangedEvent} from "ngx-bootstrap";
import {RoleService} from "../../../shared/user/role.service";
import {ToastrService} from "ngx-toastr";
import {HttpErrorResponse} from "@angular/common/http";
import {UserService} from "../../../shared/user/user.service";
import {User} from "../../../shared/user/user.model";
import {Router} from "@angular/router";
import {UserFilter} from "../../../shared/user/user-filter.model";
import {Profile} from "../../../shared/profile/profile.model";
import {ProfileService} from "../../../shared/profile/profile.service";
import {TrainerService} from "../../../shared/trainer/trainer.service";
import {ManagerService} from "../../../shared/manager/manager.service";
import {Select} from "../admin-setting-form/select.model";
import {INgxSelectOption} from "ngx-select-ex";
import {CuorseType} from "../../../shared/course/cuorse-type.model";
import {SetManager} from "../../../shared/manager/set-manager.model";
import {ManagerResponse} from "../../../shared/manager/manager-response.model";
import {Feedback} from "../../../shared/trainer/feedback.model";
import {Level} from "../../../shared/trainer/level.model";

@Component({
  selector: 'users-form',
  templateUrl: './users-form.component.html',
})
export class UsersForm {

  @ViewChild('registerModal') public registerModal: ModalDirective;
  @ViewChild('userEditModal') public userEditModal: ModalDirective;
  @ViewChild('deleteConfirmModal') public deleteConfirmModal: ModalDirective;
  @ViewChild('addManagerModal') public addManagerModal: ModalDirective;
  @ViewChild('editQualificationLevelModal') public editQualificationLevelModal: ModalDirective;
  user: User = new User();
  userFilter: UserFilter = new UserFilter();
  rolesList: Array<number> = [];
  sliceViaPaginationUsersList: Profile[];
  profile: Profile = new Profile();

  constructor(private service: UserService, private profileService: ProfileService, private roleService: RoleService,
              private toastr: ToastrService, private router: Router, private trainerService: TrainerService,
              private  managerServise: ManagerService) {
  }


  ngOnInit() {
    if(this.roleService.roleMatch(['ADMIN'])){
      this.getManagers();
      this.getQualificationLevels();
    }
    this.getInfo();
  }

  pageChanged(event: PageChangedEvent): void {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.sliceViaPaginationUsersList = this.service.usersProfileList.slice(startItem, endItem);
  }

  getInfo() {
    if (this.roleService.roleMatch(['ADMIN'])) {
      this.getUsers();
    }
    if (this.roleService.roleMatch(['TRAINER'])) {
      this.getTrainerUsers();
    }
    if (this.roleService.roleMatch(['MANAGER'])) {
      this.getManagerUsers();
    }
  }

  getUsers() {
    this.service.getUsers().subscribe((data: Profile[]) => {
        this.service.usersProfileList = data;
        this.sliceViaPaginationUsersList = this.service.usersProfileList.slice(0, 10);
      },
      (error: HttpErrorResponse) => {
        if (error.status === 500) {
          this.toastr.error("Not possible to get information!");
        }
      });
  }

  getTrainerUsers() {
    this.trainerService.getTrainerUsers(localStorage.getItem('constant_userId')).subscribe((data: Profile[]) => {
        this.service.usersProfileList = data;
        this.sliceViaPaginationUsersList = this.service.usersProfileList.slice(0, 10);
      },
      (error: HttpErrorResponse) => {
        if (error.status === 500) {
          this.toastr.error("Not possible to get information!");
        }
      });
  }

  getManagerUsers() {
    this.managerServise.getAllMenegersUsers(localStorage.getItem('constant_userId')).subscribe((data: Profile[]) => {
        this.service.usersProfileList = data;
        this.sliceViaPaginationUsersList = this.service.usersProfileList.slice(0, 10);
      },
      (error: HttpErrorResponse) => {
        if (error.status === 500) {
          this.toastr.error("Not possible to get information!");
        }
      });
  }


  employeeEmail: string = '';

  onEmployeeRegister(email: string) {
    this.service.createRegistrationLink(email).subscribe((data: any) => {
        this.toastr.success("registration link sent!")
      },
      (error: HttpErrorResponse) => {
        if (error.status === 500) {
          this.toastr.error("failed to send registration link!");
        }
      });
  }

  resetForm(form?: NgForm) {
    if (form != null)
      form.resetForm();
    this.user = {
      username: '',
      password: '',
      firstName: '',
      lastName: '',
      email: '',
      roleId: null
    }
  }

  goToProfile(profileId: number) {
    localStorage.setItem('userId', profileId.toString());
    this.router.navigate(['/users/' + profileId]);
  }

  onSubmit(form: NgForm) {
    this.registerModal.hide();
    this.registerUser(form);
  }

  onSubmit1(form: NgForm) {
    this.addManagerModal.hide();
  }

  getUsersByFilters(form: NgForm) {
    if (form.value.username === undefined) {
      form.value.username = null;
    }
    let checks = document.getElementsByClassName('roles-check');
    this.rolesList.length = 0;

    for (let i = 0; i < checks.length; i++) {
      const input = checks[i] as HTMLInputElement;
      if (input.checked) {
        this.rolesList[this.rolesList.length] = +input.value;
      }
    }
    if (this.rolesList.length !== 0) {
      this.userFilter = {
        username: form.value.username,
        roleIds: this.rolesList
      };
      this.service.getUsersByFilters(this.userFilter).subscribe((data: Profile[]) => {
          this.service.usersProfileList = data;
          this.sliceViaPaginationUsersList = this.service.usersProfileList.slice(0, 10);
        },
        (error: HttpErrorResponse) => {
          this.toastr.error("Not possible to get information!");
        });
    } else {
      this.service.usersProfileList.length = 0;
    }
  }

  registerUser(form: NgForm) {
    this.service.registerUser(form.value).subscribe((data: User) => {
        this.toastr.success(`User ${data.username} has created!`);
        this.resetForm(form);
        this.getUsers();
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Cannot inserted an user!");
      });
  }

  userDeletedId: number = null;
  userSetId: number = null;

  resetEditForm(form: NgForm) {
    if (form != null)
      form.resetForm();
    this.service.editUser = {
      userId: null,
      username: '',
      firstName: '',
      lastName: '',
      email: ''
    }
  }


  setManeger: SetManager = new SetManager();

  setManger() {
    this.managerServise.setManager(this.setManeger).subscribe((data: any) => {
        this.toastr.success(`User is tied to manager!`);
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to set managers!");
      });
  }

  getProfileUser(id: string) {
    this.profileService.getProfile(+id).subscribe((data: any) => {
        this.profile = data as Profile;
        if (data != null)
          this.populateEditUserForm();
        this.userEditModal.show();
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Cannot get a trainer!");
      });
  }

  onEditUser(form: NgForm) {
    this.service.updateUser(form.value).subscribe(() => {
        this.userEditModal.hide();
        this.getUsers();
        this.toastr.success(`User has edited!`);
        this.resetEditForm(form);
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Cannot updated an user!");
      });
  }

  populateEditUserForm() {
    this.service.editUser.userId = this.profile.userId;
    this.service.editUser.email = this.profile.email;
    this.service.editUser.firstName = this.profile.firstName;
    this.service.editUser.lastName = this.profile.lastName;
    this.service.editUser.username = this.profile.username;
  }


  declineDeleteUser() {
    this.userDeletedId = null;
    this.deleteConfirmModal.hide();
  }

  confirmDeleteUser(userId: number) {
    this.service.getDeleteUser(userId).subscribe(() => {
        this.deleteConfirmModal.hide();
        this.toastr.success(`User has deleted!`);
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Cannot delete an user!");
      });
  }

  managers: Profile[] = [];
  managersSelect: Select[] = [];
  managerSelect: Select;

  getManagers() {
    this.managerServise.getAllManagers().subscribe((data: any) => {
        this.managers = data as Profile[];
        for (let _i = 0; _i < this.managers.length; _i++) {
          this.managerSelect = {
            text: this.managers[_i].firstName + " " + this.managers[_i].email,
            id: this.managers[_i].userId
          };
          this.managersSelect.push(this.managerSelect);
        }
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to get managers!");
      });
  }

  userManager: ManagerResponse = new ManagerResponse();

  getManageByUserId(userid: number) {
    this.managerServise.getUserManager(userid.toString()).subscribe((data: ManagerResponse) => {
        this.userManager = data;
        localStorage.setItem('userId', this.userManager.managerId.toString());
        this.router.navigate(['/users/' + this.userManager.managerId.toString()]);
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to get managers!");
      });
  }

  feedback: Feedback = {
    trainerId: null,
    userId: null,
    content: '',
  };

  setFeedback() {
    this.feedback.trainerId = +localStorage.getItem('constant_userId');
    this.trainerService.setFeedback(this.feedback).subscribe((data: any) => {
        this.toastr.success("feedback left!");
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("failed to leave a feedback!");
      });
    this.feedback = {
      trainerId: null,
      userId: null,
      content: '',
    };
  }


  public ngxControl = new FormControl();

  private _ngxDefaultTimeout;
  private _ngxDefaultInterval;
  private _ngxDefault;

  ngOnDestroy(): void {
    clearTimeout(this._ngxDefaultTimeout);
    clearInterval(this._ngxDefaultInterval);
  }

  public doNgxDefault1(): any {
    return this._ngxDefault;
  }

  public doNgxDefault2(): any {
    return this._ngxDefault;
  }

  public doNgxDefault3(): any {
    return this._ngxDefault;
  }

  setUserId(userId: number) {
    this.setManeger.userId = userId;
  }

  doSelectManager(idManager: any) {
    this.setManeger.managerId = idManager;
  }

  qualificationLevels : Level[] =[];
  qualificationLevelsSelect : Select[] =[];
  getQualificationLevels(){
    this.trainerService.getQualificationLevels().subscribe((data: Level[]) => {
        this.qualificationLevels = data;
        for (let level of this.qualificationLevels) {
          let sel : Select = {
            text : level.qualificationName,
            id : level.qualificationId,
          };
          this.qualificationLevelsSelect.push(sel);
        }
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Cannot get a qualification levels!");
      });
  }

  selectLevel(levelId : any) {
    this.qualificationLevelId = levelId;
  }



  trainerId : number;
  qualificationLevelId : number;
  setQualificationLevel(){
    this.trainerService.setQualificationLevel(this.trainerId , this.qualificationLevelId).subscribe((data: any) => {
        this.toastr.success("level set!");
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("level don`t set!");
      });
    this.editQualificationLevelModal.hide()
  }


  public inputTyped = (source: string, text: string) => console.log('SingleDemoComponent.inputTyped', source, text);

  public doFocus = () => console.log('SingleDemoComponent.doFocus');

  public doBlur = () => console.log('SingleDemoComponent.doBlur');

  public doOpen = () => console.log('SingleDemoComponent.doOpen');

  public doClose = () => console.log('SingleDemoComponent.doClose');

  public doSelect = (value: any) => console.log('SingleDemoComponent.doSelect', value);

  public doRemove = (value: any) => console.log('SingleDemoComponent.doRemove', value);

  public doSelectOptions = (options: INgxSelectOption[]) => console.log('SingleDemoComponent.doSelectOptions', options);

}

