import {Component, OnInit, ViewChild} from '@angular/core';
import {ModalDirective} from "ngx-bootstrap";
import {CourseService} from "../../../shared/course/course.service";
import {RoleService} from "../../../shared/user/role.service";
import {ToastrService} from "ngx-toastr";
import {HttpErrorResponse} from "@angular/common/http";
import {Course} from "../../../shared/course/course.model";
import {INgxSelectOption} from "ngx-select-ex";
import {Profile} from "../../../shared/profile/profile.model";
import {Select} from "../admin-setting-form/select.model";
import {Group} from "../../../shared/group/group.model";
import {UserService} from "../../../shared/user/user.service";
import {GroupService} from "../../../shared/group/group.service";
import {TrainerService} from "../../../shared/trainer/trainer.service";
import {AttendanceService} from "../../../shared/attendance/attendance.servise";
import {LessonService} from "../../../shared/lessons/lesson.service";
import {Lesson} from "../../../shared/lessons/lesson.model";
import {AttendanceGroup} from "../../../shared/attendance/attendance-group.model";
import {PreentStatus} from "../../../shared/lessons/preent-status.model";
import {SetAttendance} from "../../../shared/attendance/set-attendance.model";
import {ManagerService} from "../../../shared/manager/manager.service";
import {ProfileService} from "../../../shared/profile/profile.service";
import {AbsenceReason} from "../../../shared/attendance/absence-reason.model";

@Component({
  selector: 'attendance-form',
  templateUrl: './attendance-form.component.html',
})
export class AttendanceForm implements OnInit {
  constructor(private userService: UserService, private toastr: ToastrService, private  groupService: GroupService,
              private  courseService: CourseService, private  lessonsServise: LessonService,
              private  attendanceService: AttendanceService, private roleService: RoleService,
              private trainerService: TrainerService, private managerServise: ManagerService, private profileService: ProfileService) {
  }

  @ViewChild('setStatusModal') public setStatusModal: ModalDirective;
  @ViewChild('setAbsenceReasonModal') public setAbsenceReasonModal: ModalDirective;

  ngOnInit(): void {
    this.getCourses();
    this.getProfile();
    this.getAllAbsenceReason();
    if (this.roleService.roleMatch(['ADMIN']) || this.roleService.roleMatch(['TRAINER'])) {
      this.getAllPresenceStatuses();
    }
    if (this.roleService.roleMatch(['MANAGER'])) {
      this.getManagerUsers();
    }
    if (this.roleService.roleMatch(['EMPLOYEE'])) {
      this.selectUser(localStorage.getItem('constant_userId'));
    }
  }


  absenceReason: AbsenceReason[] = [];
  absenceReasonSelect : Select[] = [];
  getAllAbsenceReason(){
    this.attendanceService.getAllAbsenceReason().subscribe((data: AbsenceReason[]) => {
        this.absenceReason = data;
        for (let res of this.absenceReason){
          let sel : Select = {
            text : res.absenceReasonName,
            id : res.absenceReasonId,
          };
          this.absenceReasonSelect.push(sel);
        }

      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Cannot get a absence reasons!");
      });
  }

  absenceReasonId : number;
  selectReason(value : any){
    this.absenceReasonId = value;
  }

  setReason(){
    this.setstatus.absenceReasonId = this.absenceReasonId;
    this.setAttendance();
  }

  emp: Profile = new Profile();

  getProfile() {
    this.profileService.getProfile(+localStorage.getItem('constant_userId')).subscribe((data: Profile) => {
        this.emp = data;
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to get profile!");
      });
  }

  courses: Course[] = [];
  coursesSelect: Select[] = [];

  courseId: string;
  courseName: string;

  downloadAtendenceReport() {
    this.attendanceService.downloadCourseReport(this.courseId).subscribe(data => this.downloadFile(data, this.courseName)),
      error => console.log("Error downloading the file."),
      () => console.log("Error");
  }

  groupName: string;

  downloadGroupReport() {
    this.attendanceService.downloadGroupReport(this.groupId.toString()).subscribe(data => this.downloadFile(data, this.groupName)),
      error => console.log("Error downloading the file."),
      () => console.log("Error");
  }

  downloadFile(data: any, name: string) {
    var url = window.URL.createObjectURL(new Blob([data]));
    var a = document.createElement('a');
    document.body.appendChild(a);
    a.setAttribute('style', 'display: none');
    a.href = url;
    a.download = name + ' repotr.xlsx';
    a.click();
    window.URL.revokeObjectURL(url);
    a.remove(); // remove the element
  }

  getCourses() {
    if (this.roleService.roleMatch(['ADMIN'])) {
      this.getAllCourses();
    }
    if (this.roleService.roleMatch(['TRAINER'])) {
      this.getTrainerCourses();
      this.getTrainerGroups();
    }
  }

  getAllCourses() {
    this.courseService.getCourses().subscribe((data: Course[]) => {
        this.courses = data;
        for (let _i = 0; _i < this.courses.length; _i++) {
          let courseSelect = {
            text: this.courses[_i].courseName,
            id: this.courses[_i].courseId,
          };
          this.coursesSelect.push(courseSelect);
        }
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to get information!");
      });
  }

  getTrainerCourses() {
    this.trainerService.getTrainersCourse(localStorage.getItem('constant_userId')).subscribe((data: Course[]) => {
        this.courses = data;
        for (let _i = 0; _i < this.courses.length; _i++) {
          let courseSelect = {
            text: this.courses[_i].courseName,
            id: this.courses[_i].courseId,
          };
          this.coursesSelect.push(courseSelect);
        }
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to get information!");
      });
  }

  trainerGroups: Group[] = [];

  getTrainerGroups() {
    this.trainerService.getTrainerGroups(localStorage.getItem('constant_userId')).subscribe((data: any) => {
        this.trainerGroups = data as Group[];
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to get groups!");
      });
  }


  groupsByCourse: Group[] = [];
  groupSelect: Select[] = [];
  groups: Group[] = [];

  groupId: number = null;
  selectCourseId: number;

  getG(value: any) {
    this.groupSelect = [];
    this.selectCourseId = value;
    if (this.roleService.roleMatch(['ADMIN']) || this.roleService.roleMatch(['TRAINER'])) {
      this.getGroupsByCourse(value);
    }
    if (this.roleService.roleMatch(['MANAGER']) || this.roleService.roleMatch(['EMPLOYEE'])) {
      this.getUserGroups(value);
    }
  }

  getGroupsByCourse(value: any) {
    console.log(value);
    this.courseService.getGroupsOfCourse(value).subscribe((data: Group[]) => {
        let groups: Group[] = data;
        let temporarySelect: Select[] = [];
        for (let _i = 0; _i < groups.length; _i++) {
          let groupSelect = {
            text: groups[_i].nameGroup,
            id: groups[_i].id,
          };
          if (this.roleService.roleMatch(['TRAINER'])) {
            for (let _k = 0; _k < this.trainerGroups.length; _k++) {
              if (this.trainerGroups[_k].id === groupSelect.id) {
                temporarySelect.push(groupSelect);
              }
            }
          } else {
            temporarySelect.push(groupSelect);
          }
        }
        this.groupSelect = temporarySelect;
        this.groupsByCourse = groups;
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to get information!");
      });
    this.courseId = value;
    this.courseName = this.coursesSelect.find(x => x.id == value).text;
  }

  groupLessons: Lesson[] = [];

  getGroupLessons(groupId: any) {
    this.lessonsServise.getGroupLessons(groupId).subscribe((data: Lesson[]) => {
        this.groupLessons = [];
        this.groupLessons = data;
        for (let i = 0; i < this.groupLessons.length; i++) {
          for (let _i = 0; _i < this.groupLessons.length - 1; _i++) {
            if (+this.groupLessons[_i].lessonDate.substr(5, 2) > +this.groupLessons[_i + 1].lessonDate.substr(5, 2)) {
              let les: Lesson = this.groupLessons[_i];
              this.groupLessons[_i] = this.groupLessons[_i + 1];
              this.groupLessons[_i + 1] = les;
            }
            if ((+this.groupLessons[_i].lessonDate.substr(5, 2) === +this.groupLessons[_i + 1].lessonDate.substr(5, 2))
              && +this.groupLessons[_i].lessonDate.substr(8, 2) > +this.groupLessons[_i + 1].lessonDate.substr(8, 2)) {
              let les: Lesson = this.groupLessons[_i];
              this.groupLessons[_i] = this.groupLessons[_i + 1];
              this.groupLessons[_i + 1] = les;
            }
          }
        }
        this.attendanceService.getGroupAttendance(groupId).subscribe((data: AttendanceGroup[]) => {
            this.attendanceGroup = data;
            if (this.roleService.roleMatch(['EMPLOYEE'])) {
              let users: Profile[] = [];
              users.push(this.emp);
              this.usersGroup = users;
              this.table();
            } else {
              this.groupService.getMembersInfo(groupId).subscribe((data: any) => {
                  if (this.roleService.roleMatch(['TRAINER']) || this.roleService.roleMatch(['ADMIN'])) {
                    this.usersGroup = data as Profile[];
                  }
                  this.table();
                },
                (error: HttpErrorResponse) => {
                  if (error.status === 400) {
                    this.toastr.error(error.error.Message);
                  } else {
                    this.toastr.error("Cannot get users!");
                  }
                });
            }
          },
          (error: HttpErrorResponse) => {
            this.toastr.error("Not possible to get information!");
          });

      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to get lessons!");
      });
    this.groupName = this.groupSelect.find(x => x.id == groupId).text;
  }

  attendanceGroup: AttendanceGroup[] = [];


  doSelectGroup(groupId: any) {
    this.getGroupLessons(groupId);
    this.groupId = groupId;
  }

  usersGroup: Profile[] = [];


  lessonsTable = [
    {
      lessonsName: "",
      lessonsStatus: '',
      lessonsId: null,
      userAttendance: [{userName: '', attendanceStatus: '', userId: null}]
    },
  ];


  table() {
    let lessonsTable = [];
    for (let _i = 0; _i < this.groupLessons.length; _i++) {
      let lessonAtendance = {
        lessonsName: this.groupLessons[_i].lessonName,
        lessonsStatus: this.groupLessons[_i].lessonStatus,
        lessonsId: this.groupLessons[_i].id,
        userAttendance: [] = [],
      };
      for (let _k = 0; _k < this.usersGroup.length; _k++) {
        let user = {
          userName: this.usersGroup[_k].firstName + ' ' + this.usersGroup[_k].lastName, attendanceStatus: '',
          userId: this.usersGroup[_k].userId,
        };
        for (let att of this.attendanceGroup) {
          if (att.lessonName === this.groupLessons[_i].lessonName && att.userFullName === user.userName) {
            user.attendanceStatus = att.userPresenceStatus;
          }
        }
        if (this.roleService.roleMatch(['MANAGER'])) {
          if (user.userId === this.userId) {
            lessonAtendance.userAttendance.push(user);
          }
        } else {
          lessonAtendance.userAttendance.push(user);
        }
      }
      lessonsTable.push(lessonAtendance);
    }
    this.lessonsTable = lessonsTable;
  }

  presentStatus: PreentStatus[] = [];
  presentStatusSelect: Select[] = [];

  getAllPresenceStatuses() {
    this.lessonsServise.getAllPresenceStatuses().subscribe((data: PreentStatus[]) => {
        this.presentStatus = data;
        for (let status of this.presentStatus) {
          let stat: Select = {
            text: status.presenceStatusName,
            id: status.presenceStatusId,
          };
          this.presentStatusSelect.push(stat);
        }
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Cannot get a courses types!");
      });
  }


  setstatus: SetAttendance = {
    userId: null,
    lessonId: null,
    presenceStatusId: null,
    absenceReasonId: null,
    explanation: null,
  };


  setAttendance() {
    this.attendanceService.setAttendance(this.setstatus).subscribe((data: any) => {
        this.doSelectGroup(this.groupId);
        this.toastr.success("status changed!");
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Cannot set status!");
      });
    this.setStatusModal.hide();
    this.setstatus.presenceStatusId = null;
  }

  setStatus(statusId: any) {
    this.setstatus.presenceStatusId = statusId;
  }

  showModal(lessonNumber: number) {
    if (this.groupLessons[lessonNumber].lessonStatus === 'PASSED') {
      this.setStatusModal.show();
    }
  }

  status: string = '';

  showModalEmployee(lessonNumber: number) {
    console.log(this.status);
    if (this.groupLessons[lessonNumber].lessonStatus === 'PASSED') {
      if (this.status === 'other') {
        this.status = '';
        this.setAbsenceReasonModal.show();
      }
    }
  }

  usersSelect: Select[] = [];


  getManagerUsers() {
    this.managerServise.getAllMenegersUsers(localStorage.getItem('constant_userId')).subscribe((data: Profile[]) => {
        this.usersGroup = data;
        console.log(this.usersGroup);
        for (let user of this.usersGroup) {
          let sel: Select = {
            text: user.firstName + ' ' + user.lastName,
            id: user.userId,
          };
          this.usersSelect.push(sel);
        }
      },
      (error: HttpErrorResponse) => {
        if (error.status === 500) {
          this.toastr.error("Not possible to get information!");
        }
      });
  }

  selectUser(userId: any) {
    this.coursesSelect = [];
    this.groupSelect = [];
    this.userId = userId;
    this.getUserCourses();
  }

  userId: number;

  getUserCourses() {
    this.userService.getCoursesByUserId(this.userId.toString()).subscribe((data: any) => {
        this.courses = data as Course[];
        this.coursesSelect = [];
        for (let course of this.courses) {
          let sel: Select = {
            text: course.courseName,
            id: course.courseId,
          };
          this.coursesSelect.push(sel);
        }
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to get information!");
      });
  }

  getUserGroups(value: any) {
    this.userService.getGroupsByUserId(this.userId.toString()).subscribe((data: any) => {
        this.groups = data as Group[];
        this.groupSelect = [];
        for (let group of this.groups) {
          let sel: Select = {
            text: group.nameGroup,
            id: group.id,
          };
          if (group.courseId === this.selectCourseId) {
            this.groupSelect.push(sel);
          }
        }
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to get groups!");
      });
  }

  setABsentReason(){

  }


  private _ngxDefault;

  public doNgxDefault1(): any {
    return this._ngxDefault;
  }

  public doNgxDefault2(): any {
    return this._ngxDefault;
  }

  public doNgxDefault3(): any {
    return this._ngxDefault;
  }

  public inputTyped = (source: string, text: string) => console.log('SingleDemoComponent.inputTyped', source, text);

  public doFocus = () => console.log('SingleDemoComponent.doFocus');

  public doBlur = () => console.log('SingleDemoComponent.doBlur');

  public doOpen = () => console.log('SingleDemoComponent.doOpen');

  public doClose = () => console.log('SingleDemoComponent.doClose');

  public doSelect = (value: any) => console.log('SingleDemoComponent.doSelect', value);

  public doRemove = (value: any) => console.log('SingleDemoComponent.doRemove', value);

  public doSelectOptions = (options: INgxSelectOption[]) => console.log('SingleDemoComponent.doSelectOptions', options);

}

