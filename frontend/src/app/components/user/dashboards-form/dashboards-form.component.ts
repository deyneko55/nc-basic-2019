import {Component, OnInit, ViewChild} from '@angular/core'
import { Axis, BubblePoint, BubbleSeries, LineSeries, Point, LegendOptions } from 'ngx-charts-extension';
import { ColorScheme } from 'ngx-charts-extension';
import {RoleService} from "../../../shared/user/role.service";

@Component({
  selector: 'dashboards-form',
  templateUrl: './dashboards-form.component.html',
})
export class DashboardsForm {

  constructor( private roleService: RoleService) {
    Object.assign(this.levelT)
  }


  conditionAddForm: boolean = true;
  public levelT = [
    {name: 'Level 1', value: 45},
    {name: 'Level 2', value: 35},
    {name: 'Level 3', value: 30},
    {name: 'Level 4', value: 20},
    {name: 'Level 5', value: 15},
    {name: 'Level 6', value: 6}
  ];

  public levelE = [
    {name: 'A 1', value: 123},
    {name: 'A 2', value: 90},
    {name: 'B 1', value: 90},
    {name: 'B 2', value: 57},
    {name: 'C 1', value: 43},
    {name: 'C 2', value: 40}
  ];

  public attendance = [
    {name: 'Course 1', series: [
        {name: 'Lesson 1', value: 30},
        {name: 'Lesson 2', value: 27},
        {name: 'Lesson 3', value: 30},
        {name: 'Lesson 4', value: 29},
        {name: 'Lesson 5', value: 19},
        {name: 'Lesson 6', value: 29},
        {name: 'Lesson 7', value: 26},
        {name: 'Lesson 8', value: 30},
      ]
    },
  ];

  viewPie: any[] = [500, 400];
  viewLine: any[] = [1300, 550];
  viewBar: any[] = [1300, 450];
  // options for the chart
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  positionLegend = 'below';
  showXAxisLabel = true;
  xAxisLabel = 'Number';
  showYAxisLabel = true;
  yAxisLabel = 'Value';
  timeline = true;

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  // line, area
  autoScale = true;

  //pie
  showLabels = true;
  explodeSlices = false;
  doughnut = false;


}

