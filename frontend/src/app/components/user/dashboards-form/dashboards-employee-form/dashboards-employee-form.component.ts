import {Component, OnInit, ViewChild} from '@angular/core'
import { Axis, BubblePoint, BubbleSeries, LineSeries, Point, LegendOptions } from 'ngx-charts-extension';
import { ColorScheme } from 'ngx-charts-extension';

@Component({
  selector: 'dashboards-employee-form',
  templateUrl: './dashboards-employee-form.component.html',
})
export class DashboardsEmployeeForm {
  conditionAddForm: boolean = true;
  public levelT = [
    {name: 'Present', value: 17},
    {name: 'Absent', value: 2},
    {name: 'Late', value: 3},
    {name: 'Other', value: 5}
  ];

  viewPie: any[] = [800, 330];
  // options for the chart
  gradient = false;
  showLegend = true;
  positionLegend = 'right';

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  //pie
  showLabels = true;
  explodeSlices = false;
  doughnut = false;

  constructor() {
    Object.assign(this.levelT)
  }

}

