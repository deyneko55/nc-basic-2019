import {Component, OnInit, ViewChild} from '@angular/core'
import {Axis, BubblePoint, BubbleSeries, LineSeries, Point, LegendOptions} from 'ngx-charts-extension';
import {ColorScheme} from 'ngx-charts-extension';
import {FormControl} from "@angular/forms";
import {INgxSelectOption} from "ngx-select-ex";
import {Profile} from "../../../../shared/profile/profile.model";
import {HttpErrorResponse} from "@angular/common/http";
import {UserService} from "../../../../shared/user/user.service";
import {ProfileService} from "../../../../shared/profile/profile.service";
import {RoleService} from "../../../../shared/user/role.service";
import {ToastrService} from "ngx-toastr";
import {Router} from "@angular/router";
import {TrainerService} from "../../../../shared/trainer/trainer.service";
import {ManagerService} from "../../../../shared/manager/manager.service";
import {TrainerInfo} from "../../../../shared/trainer/trainer-info.model";
import {Group} from "../../../../shared/group/group.model";
import {PieChart} from "../dashboards-admin-form/pie-chart.model";
import {TrainerLevel} from "../../../../shared/trainer/trainer-level.model";
import {Course} from "../../../../shared/course/course.model";
import {CourseService} from "../../../../shared/course/course.service";
import {GroupService} from "../../../../shared/group/group.service";
import {Select} from "../../admin-setting-form/select.model";
import {Lesson} from "../../../../shared/lessons/lesson.model";
import {AttendanceGroup} from "../../../../shared/attendance/attendance-group.model";
import {LessonService} from "../../../../shared/lessons/lesson.service";
import {AttendanceService} from "../../../../shared/attendance/attendance.servise";

@Component({
  selector: 'dashboards-trainer-form',
  templateUrl: './dashboards-trainer-form.component.html',
})
export class DashboardsTrainerForm implements OnInit {
  conditionAddForm: boolean = true;

  constructor(private toastr: ToastrService, private trainerService: TrainerService, private courseService: CourseService,
              private groupService: GroupService, private lessonsServise : LessonService, private attendanceService : AttendanceService) {
  }

  ngOnInit() {
    this.getTrainerUsers();
    this.getTrainerInfo();
    this.getTrainerGroups();
    this.getTrainerCourses();
    this.getTrainerGroup();
  }


  coursesSelect: Select[] = [];
  coursesList: Course[] = [];

  getTrainerCourses() {
    this.trainerService.getTrainersCourse(localStorage.getItem('constant_userId')).subscribe((data: any) => {
        this.coursesList = data as Course[];
        for (let _i = 0; _i < this.coursesList.length; _i++) {
          let courseSelect = {
            text: this.coursesList[_i].courseName,
            id: this.coursesList[_i].courseId,
          };
          this.coursesSelect.push(courseSelect);
        }
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to get information!");
      });
  }

  trainerGroups: Group[] = [];

  getTrainerGroup() {
    this.trainerService.getTrainerGroups(localStorage.getItem('constant_userId')).subscribe((data: any) => {
        this.trainerGroups = data as Group[];
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to get groups!");
      });
  }

  groupSelect: Select[] = [];
  groupsByCourse: Group[] = [];

  getGroupsByCourse(value: any) {
    this.courseService.getGroupsOfCourse(value).subscribe((data: Group[]) => {
        let groups: Group[] = data;
        let temporarySelect: Select[] = [];
        for (let _i = 0; _i < groups.length; _i++) {
          let groupSelect = {
            text: groups[_i].nameGroup,
            id: groups[_i].id,
          };
          for (let _k = 0; _k < this.trainerGroups.length; _k++) {
            if (this.trainerGroups[_k].id === groupSelect.id) {
              temporarySelect.push(groupSelect);
            }
          }
        }
        this.groupSelect = temporarySelect;
        this.groupsByCourse = groups;
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to get information!");
      });
  }


  users: Profile[] = [];

  getTrainerUsers() {
    this.trainerService.getTrainerUsers(localStorage.getItem('constant_userId')).subscribe((data: Profile[]) => {
        this.users = data;
      },
      (error: HttpErrorResponse) => {
        if (error.status === 500) {
          this.toastr.error("Not possible to get information!");
        }
      });
  }

  trainerInfo: TrainerInfo = {
    userId: null,
    login: null,
    firstName: '',
    lastName: "",
    email: null,
    roleId: null,
    avatar: null,
    qualificationName: '',
  };

  getTrainerInfo() {
    this.trainerService.getTrainerInfo(localStorage.getItem('constant_userId')).subscribe((data: TrainerInfo) => {
        this.trainerInfo = data;
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to get info about trainer!");
      });
  }


  studentlevelChart: PieChart[] = [];
  isShowlevelChart = false;
  groupsList: Group[] = [];

  getTrainerGroups() {
    this.trainerService.getTrainerGroups(localStorage.getItem('constant_userId')).subscribe((data: any) => {
        this.groupsList = data as Group[];
        for (let group of this.groupsList) {
          let Chart: PieChart = {
            name: null,
            value: null,
          };
          this.courseService.refreshInfo(+group.courseId).subscribe((data: Course) => {
            Chart.name = data.difficultyLevel;
            this.groupService.getMembersInfo(group.id.toString()).subscribe((data: Profile[]) => {
              Chart.value = data.length;
              this.studentlevelChart.push(Chart);
              this.isShowlevelChart = true;
            });
          });
        }
        console.log(this.studentlevelChart);
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to get groups!");
      });
  }

  groupLessons: Lesson[] = [];

  getGroupLessons(groupId: any) {
    console.log(groupId);
    this.lessonsServise.getGroupLessons(groupId).subscribe((data: Lesson[]) => {
        this.groupLessons = data;
        this.isGetLesson = true;
        this.lineChart();
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to get lessons!");
      });
    this.isGetLesson = false;
  }

  attendanceGroup: AttendanceGroup[] = [];

  getGroupAttendance(groupId: any) {
    this.attendanceService.getGroupAttendance(groupId).subscribe((data: AttendanceGroup[]) => {
        this.attendanceGroup = data;
        this.isGetGroupAttendance = true;
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to get information!");
      });
    this.isGetGroupAttendance = false;
  }



  isGetLesson = true;
  isGetGroupAttendance = true;

  lineChart() {
    let attendance = [
      {
        name: 'number of attendees', series: [],
      },
    ];
    for (let _i = 0; _i < this.groupLessons.length; _i++) {
      let series = {
        name: this.groupLessons [_i].lessonName + this.groupLessons [_i].lessonDate, value: 0,
      };
      for (let _k = 0; _k < this.attendanceGroup.length; _k++) {
        if (this.attendanceGroup[_k].lessonName === this.groupLessons [_i].lessonName && this.attendanceGroup[_k].userPresenceStatus === "present") {
          series.value++;
        }
      }
      attendance[0].series.push(series);
    }
    this.attendance = attendance;
  }

  public attendance = [
    {
      name: 'Course 1', series: [
        {name: 'Lesson 1', value: 30},
        {name: 'Lesson 2', value: 27},
        {name: 'Lesson 3', value: 30},
        {name: 'Lesson 4', value: 29},
        {name: 'Lesson 5', value: 19},
        {name: 'Lesson 6', value: 29},
        {name: 'Lesson 7', value: 26},
        {name: 'Lesson 8', value: 30},
      ]
    },
  ];

  viewLine: any[] = [];

  onResize(event) {
    this.viewLine = [event.target.innerWidth / 1.2, 600];
  }

  // options for the chart
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  positionLegend = 'below';
  showXAxisLabel = true;
  xAxisLabel = 'lessons';
  showYAxisLabel = true;
  yAxisLabel = 'number of students';
  timeline = true;

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  // line, area
  autoScale = true;


  public ngxControl = new FormControl();

  private _ngxDefaultTimeout;
  private _ngxDefaultInterval;
  private _ngxDefault;


  public doNgxDefault1(): any {
    return this._ngxDefault;
  }

  public inputTyped = (source: string, text: string) => console.log('SingleDemoComponent.inputTyped', source, text);

  public doFocus = () => console.log('SingleDemoComponent.doFocus');

  public doBlur = () => console.log('SingleDemoComponent.doBlur');

  public doOpen = () => console.log('SingleDemoComponent.doOpen');

  public doClose = () => console.log('SingleDemoComponent.doClose');

  public doSelect = (value: any) => console.log('SingleDemoComponent.doSelect', value);

  public doRemove = (value: any) => console.log('SingleDemoComponent.doRemove', value);

  public doSelectOptions = (options: INgxSelectOption[]) => console.log('SingleDemoComponent.doSelectOptions', options);

}

