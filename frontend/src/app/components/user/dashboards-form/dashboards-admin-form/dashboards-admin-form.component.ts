import {Component, OnInit} from '@angular/core'
import {FormControl, NgForm} from "@angular/forms";
import {INgxSelectOption} from "ngx-select-ex";
import {TrainerService} from "../../../../shared/trainer/trainer.service";
import {ManagerService} from "../../../../shared/manager/manager.service";
import {Profile} from "../../../../shared/profile/profile.model";
import {HttpErrorResponse} from "@angular/common/http";
import {ToastrService} from "ngx-toastr";
import {UserService} from "../../../../shared/user/user.service";
import {ProfileService} from "../../../../shared/profile/profile.service";
import {UserFilter} from "../../../../shared/user/user-filter.model";
import {Group} from "../../../../shared/group/group.model";
import {GroupService} from "../../../../shared/group/group.service";
import {CuorseType} from "../../../../shared/course/cuorse-type.model";
import {CourseService} from "../../../../shared/course/course.service";
import {Select} from "../../admin-setting-form/select.model";
import {CourseTypeStats} from "../../../../shared/course/course-type-stats.model";
import {PieChart} from "./pie-chart.model";
import {TrainerLevel} from "../../../../shared/trainer/trainer-level.model";
import {Level} from "../../../../shared/trainer/level.model";
import {Course} from "../../../../shared/course/course.model";
import {LessonService} from "../../../../shared/lessons/lesson.service";
import {Lesson} from "../../../../shared/lessons/lesson.model";
import {AttendanceService} from "../../../../shared/attendance/attendance.servise";
import {AttendanceGroup} from "../../../../shared/attendance/attendance-group.model";

@Component({
  selector: 'dashboards-admin-form',
  templateUrl: './dashboards-admin-form.component.html',
})
export class DashboardsAdminForm implements OnInit {

  constructor(private userService: UserService, private toastr: ToastrService, private  groupService: GroupService,
              private  courseService: CourseService, private  trainerService: TrainerService, private attendanceService: AttendanceService,
              private  lessonsServise: LessonService) {
    this._ngxDefaultTimeout = setTimeout(() => {
      this._ngxDefaultInterval = setInterval(() => {
        const idx = Math.floor(Math.random() * (this.courses.length - 1));
        this._ngxDefault = this.courses[idx];
        // console.log('new default value = ', this._ngxDefault);
      }, 2000);
    }, 2000);
    this.viewLine = [innerWidth / 1.3, 400];

  }

  userFilter: UserFilter;
  emploees: Profile[] = [];
  trainers: Profile[] = [];
  groups: Group[] = [];
  courseTypes: CuorseType[];

  ngOnInit() {
    this.getEmploees();
    this.getTrainers();
    this.getGroups();
    this.getCoursTypes();
    this.getTrainersLevels();
    this.getTrainersWithLevels();
    this.getAllCourses();
  }


  getEmploees() {
    this.userFilter = {
      username: "",
      roleIds: [4]
    };
    this.userService.getUsersByFilters(this.userFilter).subscribe((data: Profile[]) => {
        this.emploees = data
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to get information!");
      });
  }

  getTrainers() {
    this.userFilter = {
      username: "",
      roleIds: [3]
    };
    this.userService.getUsersByFilters(this.userFilter).subscribe((data: Profile[]) => {
        this.trainers = data
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to get information!");
      });
  }

  getGroups() {
    this.groupService.getGroups().subscribe((data: Group[]) => {
        this.groups = data
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to get information!");
      });
  }

  courseTypeSelect: Select = new Select();
  coursesTypesSelect: Select[] = [];

  getCoursTypes() {
    this.courseService.getCourseTypes().subscribe((data: CuorseType[]) => {
        this.courseTypes = data;
        for (let _i = 0; _i < this.courseTypes.length; _i++) {
          this.courseTypeSelect = {
            text: this.courseTypes[_i].courseType,
            id: this.courseTypes[_i].courseTypeId,
          };
          this.coursesTypesSelect.push(this.courseTypeSelect);
        }
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to get information!");
      });
  }


  courseTypeStatsUser: CourseTypeStats[] = [];
  courseTypeStatsUsersChart: PieChart[] = [{name : "pre-intermediate" , value : 10} , {name : "upper-intermediate" , value : 20}];
  isShowUsersChart = true;

  getUsersByCourseType(courseTypeId: number) {
    this.courseService.getUsersByCourseType(courseTypeId).subscribe((data: CourseTypeStats[]) => {
        this.courseTypeStatsUser = data;
        let temporaryChart: PieChart[] = [];
        for (let _i = 0; _i < this.courseTypeStatsUser.length; _i++) {
          let PieChart = {
            name: this.courseTypeStatsUser[_i].difficultyLevel,
            value: this.courseTypeStatsUser[_i].entitiesCount,
          };
          temporaryChart.push(PieChart);
        }
        this.courseTypeStatsUsersChart = temporaryChart;
        if (this.courseTypeStatsUsersChart.length > 0) {
          this.isShowUsersChart = true;
        }
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to get information!");
      });
    this.isShowUsersChart = false;
  }

  doSelectTypeFoUsers(value: any) {
    this.getUsersByCourseType(value);
  }

  courseTypeStatsGroups: CourseTypeStats[] = [];
  courseTypeStatsGroupsChart: PieChart[] = [{name : "pre-intermediate" , value : 2} , {name : "upper-intermediate" , value : 3}];
  isShowGroupsChart = true;

  getGroupsByCourseType(courseTypeId: number) {
    this.courseService.getGroupsByCourseType(courseTypeId).subscribe((data: CourseTypeStats[]) => {
        this.courseTypeStatsGroups = data;
        let temporaryChart: PieChart[] = [];
        for (let _i = 0; _i < this.courseTypeStatsGroups.length; _i++) {
          let PieChart = {
            name: this.courseTypeStatsGroups[_i].difficultyLevel,
            value: this.courseTypeStatsGroups[_i].entitiesCount,
          };
          temporaryChart.push(PieChart);
        }
        this.courseTypeStatsGroupsChart = temporaryChart;
        if (this.courseTypeStatsGroupsChart.length > 0) {
          this.isShowGroupsChart = true;
        }
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to get information!");
      });
    this.isShowGroupsChart = false;
  }


  doSelectTypeFoGroups(value: any) {
    this.getGroupsByCourseType(value);
  }


  trainerLevels: Level[] = [];

  getTrainersLevels() {
    this.trainerService.getTrainersLevels().subscribe((data: Level[]) => {
        this.trainerLevels = data;
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to get information!");
      });
  }


  trainersWithLevel: TrainerLevel[] = [];
  trainersLevelChart: PieChart[] = [];
  isShowTrainersChart = false;

  getTrainersWithLevels() {
    this.trainerService.getTrainersWithLevels().subscribe((data: TrainerLevel[]) => {
        this.trainersWithLevel = data;
        for (let _i = 0; _i < this.trainerLevels.length; _i++) {
          let PieChart = {
            name: this.trainerLevels[_i].qualificationName,
            value: 0,
          };
          for (let _k = 0; _k < this.trainersWithLevel.length; _k++) {
            if (this.trainerLevels[_i].qualificationName === this.trainersWithLevel[_k].qualificationName) {
              PieChart.value++;
            }
          }
          this.trainersLevelChart.push(PieChart);
        }
        this.isShowTrainersChart = true;
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to get information!");
      });
  }


  courses: Course[] = [];
  coursesSelect: Select[] = [];

  getAllCourses() {
    this.courseService.getCourses().subscribe((data: Course[]) => {
        this.courses = data;
        for (let _i = 0; _i < this.courses.length; _i++) {
          let courseSelect = {
            text: this.courses[_i].courseName,
            id: this.courses[_i].courseId,
          };
          this.coursesSelect.push(courseSelect);
        }
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to get information!");
      });
  }

  groupsByCourse: Group[] = [];
  groupSelect: Select[] = [];

  getGroupsByCourse(value: any) {
    this.courseService.getGroupsOfCourse(value).subscribe((data: Group[]) => {
      let groups: Group[] = data;
      let temporarySelect : Select[] = [];
        for (let _i = 0; _i < groups.length; _i++) {
          let groupSelect = {
            text: groups[_i].nameGroup,
            id: groups[_i].id,
          };
          temporarySelect.push(groupSelect);
        }
        this.groupSelect = temporarySelect;
        this.groupsByCourse = groups;
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to get information!");
      });
  }

  attendanceGroup: AttendanceGroup[] = [];

  getGroupAttendance(groupId: any) {
    this.attendanceService.getGroupAttendance(groupId).subscribe((data: AttendanceGroup[]) => {
        this.attendanceGroup = data;
        this.isGetGroupAttendance = true;
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to get information!");
      });
    this.isGetGroupAttendance = false;
  }


  groupLessons: Lesson[] = [];

  getGroupLessons(groupId: any) {
    console.log(groupId);
    this.lessonsServise.getGroupLessons(groupId).subscribe((data: Lesson[]) => {
        this.groupLessons = data;
        this.isGetLesson = true;
        this.lineChart();
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to get lessons!");
      });
    this.isGetLesson = false;
  }


  public attendance = [
    {
      name: 'Course 1', series: [
        {name: 'Lesson 1', value: 30},
        {name: 'Lesson 2', value: 27},
        {name: 'Lesson 3', value: 30},
        {name: 'Lesson 4', value: 29},
        {name: 'Lesson 5', value: 19},
        {name: 'Lesson 6', value: 29},
        {name: 'Lesson 7', value: 26},
        {name: 'Lesson 8', value: 30},
      ]
    },
  ];


  isGetLesson = true;
  isGetGroupAttendance = true;
  lineChart() {
    let attendance = [
      {
        name: 'number of attendees', series: [],
      },
    ];
    for (let _i = 0; _i < this.groupLessons.length; _i++) {
      let series = {
        name: this.groupLessons [_i].lessonName + this.groupLessons [_i].lessonDate, value: 0,
      };
      for (let _k = 0; _k < this.attendanceGroup.length; _k++) {
        if (this.attendanceGroup[_k].lessonName === this.groupLessons [_i].lessonName && this.attendanceGroup[_k].userPresenceStatus === "present") {
          series.value++;
        }
      }
      attendance[0].series.push(series);
    }
    this.attendance = attendance;
  }






  viewPie: any[] = [500, 400];
  viewLine: any[] = [];

  onResize(event) {
    this.viewLine = [event.target.innerWidth / 1.18, 600];
  }

  // options for the chart
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  positionLegend = 'below';
  showXAxisLabel = true;
  xAxisLabel = 'lessons';
  showYAxisLabel = true;
  yAxisLabel = 'quantity of attendees';
  timeline = true;

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  // line, area
  autoScale = true;

  //pie
  showLabels = true;
  explodeSlices = false;
  doughnut = false;

  public ngxControl = new FormControl();

  private _ngxDefaultTimeout;
  private _ngxDefaultInterval;
  private _ngxDefault;


  public doNgxDefault1(): any {
    return this._ngxDefault;
  }

  public doNgxDefault2(): any {
    return this._ngxDefault;
  }

  public doNgxDefault3(): any {
    return this._ngxDefault;
  }

  public inputTyped = (source: string, text: string) => console.log('SingleDemoComponent.inputTyped', source, text);

  public doFocus = () => console.log('SingleDemoComponent.doFocus');

  public doBlur = () => console.log('SingleDemoComponent.doBlur');

  public doOpen = () => console.log('SingleDemoComponent.doOpen');

  public doClose = () => console.log('SingleDemoComponent.doClose');

  public doSelect = (value: any) => console.log('SingleDemoComponent.doSelect', value);

  public doRemove = (value: any) => console.log('SingleDemoComponent.doRemove', value);

  public doSelectOptions = (options: INgxSelectOption[]) => console.log('SingleDemoComponent.doSelectOptions', options);


}

