import {Component, OnInit, ViewChild} from '@angular/core'
import { Axis, BubblePoint, BubbleSeries, LineSeries, Point, LegendOptions } from 'ngx-charts-extension';
import { ColorScheme } from 'ngx-charts-extension';
import {UserService} from "../../../../shared/user/user.service";
import {HttpErrorResponse} from "@angular/common/http";
import {UserStatistick} from "../../../../shared/user/user-statistick.model";
import {ToastrService} from "ngx-toastr";
import {Select} from "../../admin-setting-form/select.model";
import {FormControl} from "@angular/forms";
import {INgxSelectOption} from "ngx-select-ex";
import {PieChart} from "../dashboards-admin-form/pie-chart.model";

@Component({
  selector: 'dashboards-manager-form',
  templateUrl: './dashboards-manager-form.component.html',
})
export class DashboardsManagerForm implements OnInit{
  conditionAddForm: boolean = true;

  constructor(private userService : UserService, private toastr : ToastrService) {
  }

  ngOnInit(){
      this.getStatistick();
  }

  statistik:UserStatistick[] =[];
  statistikSelect : Select[] =[];
  getStatistick(){
    this.userService.getStatistik(localStorage.getItem('constant_userId')).subscribe((data: UserStatistick[]) => {
        this.statistik = data;
        for (let i = 0; i < this.statistik.length ; i++) {
          let sel : Select ={
            text : this.statistik[i].firstName + ' ' + this.statistik[i].lastName,
            id : i,
          };
          this.statistikSelect.push(sel);
        }
      },
      (error: HttpErrorResponse) => {
        if (error.status === 400) {
          this.toastr.error("cannot get statistick!");
        }
      })
  }

  courseTypeStatsGroupsChart: PieChart[] = [{name : "pre-intermediate" , value : 2} , {name : "upper-intermediate" , value : 3}];
  doSelect(value : any){
    this.courseTypeStatsGroupsChart = [{name : 'present' , value : this.statistik[value].visitedLessons}, {name : "absent" , value : this.statistik[value].missedLessons} ]
  }


  viewPie: any[] = [800, 330];
  // options for the chart
  gradient = false;
  showLegend = true;
  positionLegend = 'right';

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  //pie
  showLabels = true;
  explodeSlices = false;
  doughnut = false;

  ngOnDestroy(): void {
    clearTimeout(this._ngxDefaultTimeout);
    clearInterval(this._ngxDefaultInterval);
  }


  private _ngxDefaultTimeout;
  private _ngxDefaultInterval;
  private _ngxDefault;

  public doNgxDefault1(): any {
    return this._ngxDefault;
  }


  public inputTyped = (source: string, text: string) => console.log('SingleDemoComponent.inputTyped', source, text);

  public doFocus = () => console.log('SingleDemoComponent.doFocus');

  public doBlur = () => console.log('SingleDemoComponent.doBlur');

  public doOpen = () => console.log('SingleDemoComponent.doOpen');

  public doClose = () => console.log('SingleDemoComponent.doClose');

  public doRemove = (value: any) => console.log('SingleDemoComponent.doRemove', value);

  public doSelectOptions = (options: INgxSelectOption[]) => console.log('SingleDemoComponent.doSelectOptions', options);

}

