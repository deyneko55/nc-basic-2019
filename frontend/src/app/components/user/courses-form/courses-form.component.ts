import {Component, ViewChild} from '@angular/core';
import {BsDatepickerConfig, ModalDirective, PageChangedEvent} from "ngx-bootstrap";
import {CourseService} from "../../../shared/course/course.service";
import {RoleService} from "../../../shared/user/role.service";
import {ToastrService} from "ngx-toastr";
import {FormControl, NgForm} from "@angular/forms";
import {HttpErrorResponse} from "@angular/common/http";
import {Course} from "../../../shared/course/course.model";
import {Router} from "@angular/router";
import {CourseFilter} from "../../../shared/course/course-filter";
import {INgxSelectOption} from "ngx-select-ex";
import {Select} from "../admin-setting-form/select.model";
import {Profile} from "../../../shared/profile/profile.model";
import {CuorseType} from "../../../shared/course/cuorse-type.model";
import {CourseLevel} from "../../../shared/course/course-level.model";
import {CourseWithIdsFormRequest} from "../../../shared/course/course-with-ids-form-request.model";
import {TrainerService} from "../../../shared/trainer/trainer.service";
import {UserService} from "../../../shared/user/user.service";
import {NotificationService} from "../../../shared/notification/notification.service";

@Component({
  selector: 'courses-form',
  templateUrl: './courses-form.component.html',
})
export class CoursesForm {
  conditionAddForm: boolean = true;
  courseFilter: CourseFilter = new CourseFilter();
  datePickerConfig: Partial<BsDatepickerConfig>;
  sliceViaPaginationCourseList: Course[];
  allSliceViaPaginationCourseList : Course[];
  course: Course = new Course();
  @ViewChild('courseModal') public courseModal: ModalDirective;
  @ViewChild('courseEditModal') public courseEditModal: ModalDirective;
  myCourses = true;
  allCourses = false;

  closeAll() {
    this.myCourses = true;
    this.allCourses = true;
  }

  constructor(private service: CourseService, private router: Router, private roleService: RoleService,
              private toastr: ToastrService, private  trainerService : TrainerService ,private userService : UserService,
              private motService : NotificationService) {
    this.datePickerConfig = Object.assign({},
      {
        containerClass: 'theme-dark-blue',
        dateInputFormat: 'YYYY-MM-DD'
      });
  }

  ngOnInit() {
    this.getCourses();
    if(this.roleService.roleMatch(['ADMIN'])){
      this.getCourseTypes();
    }
  }

  getCourses(){
      this.getAllCourses();
    if(this.roleService.roleMatch(['TRAINER'])) {
      this.getTrainerCourses();
    }
    if(this.roleService.roleMatch(['EMPLOYEE'])||this.roleService.roleMatch(['MANAGER'])){
      this.getEmployeeCourses();
    }
  }

  getEmployeeCourses(){
    this.userService.getCoursesByUserId(localStorage.getItem('constant_userId')).subscribe((data: any) => {
      console.log(data);
        this.service.coursesList = data as Course[];
        this.sliceViaPaginationCourseList = this.service.coursesList.slice(0, 10);
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to get information!");
      });
  }

  pageChanged(event: PageChangedEvent): void {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.sliceViaPaginationCourseList = this.service.coursesList.slice(startItem, endItem);
  }

  resetForm(form?: NgForm) {
    if (form != null)
      form.resetForm();
    this.service.formData1 = {
      courseName: '',
      startDate: '',
      endDate: '',
      courseTypeId: null,
      difficultyLevelId: null,
    }
  }


  populateForm(course: CourseWithIdsFormRequest) {
    this.service.formData1 = Object.assign({}, course);
  }

  getAllCourses() {
    this.service.getCourses().subscribe((data: any) => {
        this.service.allCoursesList = data as Course[];
        this.allSliceViaPaginationCourseList = this.service.allCoursesList.slice(0, 10);
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to get information!");
      });
  }

  getTrainerCourses(){
    this.trainerService.getTrainersCourse(localStorage.getItem('constant_userId')).subscribe((data: any) => {
        this.service.coursesList = data as Course[];
        this.sliceViaPaginationCourseList = this.service.coursesList.slice(0, 10);
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to get information!");
      });
  }

  getCoursesByFilters(form: NgForm) {
    if (form.value.name !== undefined) {
      this.service.getCoursesByFilters(form.value).subscribe((data: any) => {
          this.service.coursesList = data as Course[];
          this.sliceViaPaginationCourseList = this.service.coursesList.slice(0, 10);
        },
        (error: HttpErrorResponse) => {
          this.toastr.error("Not possible to get information!");
        });
    } else {
      this.getCourses();
    }
  }

  goToCourse(courseId: number) {
    this.router.navigate(['/courses/' + courseId]);
  }

  onSubmit(form: NgForm) {
    this.courseModal.hide();
    this.insertNewCourse(form);
  }

  getDifficultyCourseByIdCourse(id: number): string {
    if (id != null) {
      return this.service.coursesList.find(x => x.courseId == id).difficultyLevel;
    }
  }

  getTypeCourseByIdCourse(id: number): string {
    if (id != null) {
      return this.service.coursesList.find(x => x.courseId == id).courseType;
    }
  }

  wantToCourse(userId : number){
    console.log(userId);
    this.motService.wantToCourse(userId , +localStorage.getItem('constant_userId') ).subscribe((data: any) => {
        this.toastr.success("notification send to admin");
      },
      (error: HttpErrorResponse) => {
        this.toastr.success("notification send to admin");
      });
  }

  courseEditId = null;
  courseEditName = '';
  courseEditStart = '';
  courseEditEnd = '';
  courseEditType = '';
  courseEditLevel = '';
  updateCourse(form: NgForm) {
    this.service.updateCourse(form.value).subscribe((data: any) => {
        this.courseEditModal.hide();
        this.toastr.success(`Course has edited!`);
        this.service.getCourses();
      },
      (error: HttpErrorResponse) => {
        if (error.status === 400) {
          this.toastr.error(error.error.Message);
        } else {
          this.toastr.error("Cannot updated a course!");
        }
      });
  }

  insertNewCourse(form: NgForm) {
    this.service.insertNewCourse(form.value).subscribe((data: any) => {
        this.toastr.success("Course inserted!");
        this.service.getCourses();
      },
      (error: HttpErrorResponse) => {
          this.toastr.error("Cannot inserted a course!");
      });
  }

  insertCourse(form: NgForm) {
    this.service.insertCourse(form.value).subscribe((data: any) => {
        this.toastr.success(data.Message);
        this.service.getCourses();
      },
      (error: HttpErrorResponse) => {
        if (error.status === 400) {
          this.toastr.error(error.error.Message);
        } else {
          this.toastr.error("Cannot inserted a course!");
        }
      });
  }

  courseTypes: CuorseType[] = [];
  courseTypesSelect : Select[] = [];
  getCourseTypes(){
    this.service.getCourseTypes().subscribe((data: CuorseType[]) => {
        this.courseTypes = data;
        for(let _i = 0; _i < this.courseTypes.length; _i++){
          let courseType = {
            text : this.courseTypes[_i].courseType,
            id : this.courseTypes[_i].courseTypeId,
          };
          this.courseTypesSelect.push(courseType);
        }
      },
      (error: HttpErrorResponse) => {
          this.toastr.error("Cannot get a courses types!");
      });
  }

  courseLevel : CourseLevel[] =[];
  courseLevelSelect : Select[] = [];
  courseTypeIdSelected : number;
  difficultyLevelIdSelected : number;
  getDifficulty(courseTypeId : any){
    this.service.getLevels() .subscribe((data: CourseLevel[]) => {
      console.log(data);
        this.courseLevel = data;
        let temporarySelect : Select[] = [];
        for(let _i = 0; _i < this.courseLevel.length; _i++){
          let courseLevel = {
            text : this.courseLevel[_i].courseLevel,
            id : this.courseLevel[_i].courseLevelId,
          };
          temporarySelect.push(courseLevel);
        }
        this.courseLevelSelect = temporarySelect;
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Cannot get a courses levels!");
      });
    this.courseTypeIdSelected = courseTypeId;
  }

  doSelectDifficulty(difficultyId : any){
    this.difficultyLevelIdSelected = difficultyId;
  }

  public ngxControl = new FormControl();

  private _ngxDefaultTimeout;
  private _ngxDefaultInterval;
  private _ngxDefault;


  public doNgxDefault1(): any {
    return this._ngxDefault;
  }

  public doNgxDefault2(): any {
    return this._ngxDefault;
  }

  public doNgxDefault3(): any {
    return this._ngxDefault;
  }

  public inputTyped = (source: string, text: string) => console.log('SingleDemoComponent.inputTyped', source, text);

  public doFocus = () => console.log('SingleDemoComponent.doFocus');

  public doBlur = () => console.log('SingleDemoComponent.doBlur');

  public doOpen = () => console.log('SingleDemoComponent.doOpen');

  public doClose = () => console.log('SingleDemoComponent.doClose');

  public doSelect = (value: any) => console.log('SingleDemoComponent.doSelect', value);

  public doRemove = (value: any) => console.log('SingleDemoComponent.doRemove', value);

  public doSelectOptions = (options: INgxSelectOption[]) => console.log('SingleDemoComponent.doSelectOptions', options);

}

