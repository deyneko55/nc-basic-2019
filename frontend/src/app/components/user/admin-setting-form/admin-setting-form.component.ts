import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ModalDirective} from "ngx-bootstrap";
import {AccordionPanelComponent} from 'ngx-bootstrap/accordion';
import {FormControl, NgForm} from "@angular/forms";
import {INgxSelectOption} from "ngx-select-ex";
import {TrainerService} from "../../../shared/trainer/trainer.service";
import {ManagerService} from "../../../shared/manager/manager.service";
import {Profile} from "../../../shared/profile/profile.model";
import {HttpErrorResponse} from "@angular/common/http";
import {ToastrService} from "ngx-toastr";
import {User} from "../../../shared/user/user.model";
import {UserService} from "../../../shared/user/user.service";
import {ProfileService} from "../../../shared/profile/profile.service";
import {Select} from "./select.model";
import {NewsService} from "../../../shared/news/news.service";
import {News} from "../../../shared/news/news.model";
import {Course} from "../../../shared/course/course.model";
import {CourseService} from "../../../shared/course/course.service";
import {CuorseType} from "../../../shared/course/cuorse-type.model";
import {CourseWithIdsFormRequest} from "../../../shared/course/course-with-ids-form-request.model";
import {CourseLevel} from "../../../shared/course/course-level.model";
import {LessonService} from "../../../shared/lessons/lesson.service";
import {PreentStatus} from "../../../shared/lessons/preent-status.model";
import {DomSanitizer} from "@angular/platform-browser";
import {Slider} from "../../../shared/news/slider.model";
import {SetNewsFoSlider} from "../../../shared/news/set-news-fo-slider.model";
import {AttendanceService} from "../../../shared/attendance/attendance.servise";
import {Level} from "../../../shared/trainer/level.model";
import {AbsenceReason} from "../../../shared/attendance/absence-reason.model";


@Component({
  selector: 'admin-setting-form',
  templateUrl: './admin-setting-form.component.html',
})
export class AdminSettingForm implements OnDestroy, OnInit {
  @ViewChild('addAbsenceReasonModal')
  public addAbsenceReasonModal: ModalDirective;
  @ViewChild('addTypeOfCourseModal')
  public addTypeOfCourseModal: ModalDirective;
  @ViewChild('addDifficultyLevelModal')
  public addDifficultyLevelModal: ModalDirective;
  @ViewChild('addQualificationLevelModal')
  public addQualificationLevelModal: ModalDirective;

  constructor(private newsService: NewsService, private toastr: ToastrService, private attendanceService : AttendanceService,
              private  courseService: CourseService, private sanitizer: DomSanitizer, private lessonsService: LessonService,
              private  trainerService : TrainerService) { }

  formData: CuorseType[] = [];

  newsList: News[] = [];

  getAllNews() {
    this.newsService.getAllNews().subscribe((data: News[]) => {
        this.newsList = data;
        for (let i = 0; i < data.length; i++) {
          let byteArrayImage: any;
          let imageData: any;
          let sanitizeImageData: any;
          byteArrayImage = data[i]["photo"];
          if (byteArrayImage != null) {
            imageData = 'data:image/png;base64,' + byteArrayImage;
            sanitizeImageData = this.sanitizer.bypassSecurityTrustUrl(imageData);
            this.newsList[i].picture = sanitizeImageData;
          }
        }
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Cannot get an information!");
      });
  }


  public newsId: string;
  public courseId: string;
  public groupId: string;
  public userId: string;
  conditionAddForm: boolean = true;
  sliderSettingPanel = false;
  reasonForAbsencePanel = true;
  typeOfCoursePanel = true;
  difficultyLevelPanel = true;
  qualificationLevelPanel = true;

  closeAll() {
    this.sliderSettingPanel = true;
    this.reasonForAbsencePanel = true;
    this.typeOfCoursePanel = true;
    this.difficultyLevelPanel = true;
    this.qualificationLevelPanel = true;
  }

  public ngxControl = new FormControl();

  private _ngxDefaultTimeout;
  private _ngxDefaultInterval;
  private _ngxDefault;


  slider:Slider[] = [];
  getNewsFoSlider() {
  this.newsService.getNewsForSlider().subscribe((data: Slider[]) => {
      this.slider = data;
    },
    (error: HttpErrorResponse) => {
      this.toastr.error("Cannot get slider!");
    });
  }


  presentStatus: PreentStatus[] = [];
  getAllPresenceStatuses() {
    this.lessonsService.getAllPresenceStatuses().subscribe((data: PreentStatus[]) => {
        this.presentStatus = data;
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Cannot get a courses types!");
      });
  }

  absenceReason: AbsenceReason[] = [];
  getAllAbsenceReason(){
    this.attendanceService.getAllAbsenceReason().subscribe((data: AbsenceReason[]) => {
        this.absenceReason = data;
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Cannot get a absence reasons!");
      });
  }

  set: string = '';
  courseTypes: CuorseType[] = [];
  getCourseTypes() {
    this.courseService.getCourseTypes().subscribe((data: CuorseType[]) => {
        this.courseTypes = data;
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Cannot get a courses types!");
      });
  }

  courseLevels: CourseLevel[] = [];

  getCourseLevels() {
    this.courseService.getLevels().subscribe((data: CourseLevel[]) => {
        this.courseLevels = data;
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Cannot get a courses levels!");
      });
  }

  qualificationLevels : Level[] =[];
  getQualificationLevels(){
    this.trainerService.getQualificationLevels().subscribe((data: Level[]) => {
      console.log(this.qualificationLevels);
        this.qualificationLevels = data;
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Cannot get a qualification levels!");
      });
  }


  ngOnInit() {
    this.getNews();
    this.getCourseTypes();
    this.getCourseLevels();
    this.getAllNews();
    this.getNewsFoSlider();
    this.getQualificationLevels();
    this.getAllAbsenceReason();
  }

  newAbsenceReason : string = '';
  addNewAbsenceReason(asenceReason : string){
    this.attendanceService.addNewAbsenceReason(asenceReason).subscribe((data: any) => {
        this.getAllAbsenceReason();
        this.addAbsenceReasonModal.hide();
        this.toastr.success("Added new reason for absence!");
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("failed to add new missing reason!");
      });
    this.newAbsenceReason = '';
  }

  newTypeOfCourse : string = '';
  addNewTypeOfCourse(typeOfCourse : string){
    this.courseService.createNewCourseType(typeOfCourse).subscribe((data: any) => {
        this.getCourseTypes();
        this.addTypeOfCourseModal.hide();
        this.toastr.success("Added new type of course!");
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("failed to add new type of course!");
      });
    this.newTypeOfCourse = '';
  }

  newDifficultyLevel : string = '';
  addNewDifficultyLevel(difficultyLevel : string){
    this.courseService.createNewDofficultuLevel(difficultyLevel).subscribe((data: any) => {
        this.getCourseLevels();
        this.addDifficultyLevelModal.hide();
        this.toastr.success("Added new difficulty level!");
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("failed to difficulty level!");
      });
    this.newDifficultyLevel = '';
  }

  newQualificationLevel : string = '';
  addNewQualificationLevel(qualificationLevel : string){
    this.trainerService.addNewQualificationLevel(qualificationLevel).subscribe((data: any) => {
        this.getQualificationLevels();
        this.addQualificationLevelModal.hide();
        this.toastr.success("Added new qualification level!");
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("failed to add new qualification level!");
      });
    this.newQualificationLevel = '';
  }


  newsListSelect: Select[] = [];
  newsSelect: Select;
  getNews() {
    this.newsService.getAllNews().subscribe((data: any) => {
        this.newsService.newsList = data as News[];
        for (let _i = 0; _i < this.newsService.newsList.length; _i++) {
          this.newsSelect = {
            text: this.newsService.newsList[_i].header,
            id: this.newsService.newsList[_i].newsPostId
          };
          this.newsListSelect.push(this.newsSelect);
        }
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to get news!");
      });
  }

  setSlider(sliderNumber: number , newsId : any){
    let setSlider : SetNewsFoSlider = {
      sliderId : sliderNumber,
      newsId : newsId,
    };
    this.newsService.setNewsForSlider(setSlider).subscribe((data: any) => {
      this.getNewsFoSlider();
        this.toastr.success("slider " + sliderNumber + " changed!");
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to get news!");
      });
  }

  ngOnDestroy(): void {
    clearTimeout(this._ngxDefaultTimeout);
    clearInterval(this._ngxDefaultInterval);
  }

  public doNgxDefault1(): any {
    return this._ngxDefault;
  }

  public doNgxDefault2(): any {
    return this._ngxDefault;
  }

  public doNgxDefault3(): any {
    return this._ngxDefault;
  }

  public inputTyped = (source: string, text: string) => console.log('SingleDemoComponent.inputTyped', source, text);

  public doFocus = () => console.log('SingleDemoComponent.doFocus');

  public doBlur = () => console.log('SingleDemoComponent.doBlur');

  public doOpen = () => console.log('SingleDemoComponent.doOpen');

  public doClose = () => console.log('SingleDemoComponent.doClose');

  profileId: string;

  doSelect(value: any) {
    this.profileId = value;
  }


  public doRemove = (value: any) => console.log('SingleDemoComponent.doRemove', value);

  public doSelectOptions = (options: INgxSelectOption[]) => console.log('SingleDemoComponent.doSelectOptions', options);

}

