import {Component, ViewChild} from '@angular/core';
import {GroupService} from "../../../shared/group/group.service";
import {Group} from "../../../shared/group/group.model";
import {ModalDirective, PageChangedEvent} from "ngx-bootstrap";
import {CourseService} from "../../../shared/course/course.service";
import {ToastrService} from "ngx-toastr";
import {RoleService} from "../../../shared/user/role.service";
import {NgForm} from "@angular/forms";
import {HttpErrorResponse} from "@angular/common/http";
import {Router} from "@angular/router";
import {GroupFilter} from "../../../shared/group/group-filter.model";
import {Course} from "../../../shared/course/course.model";
import {TrainerService} from "../../../shared/trainer/trainer.service";
import {UserService} from "../../../shared/user/user.service";

@Component({
  selector: 'groups-form',
  templateUrl: './groups-form.component.html',
  //styleUrls: ['../css/all.css', ]
})
export class GroupsForm {

  @ViewChild('groupModal') public groupModal: ModalDirective;
  @ViewChild('deleteConfirmModal') public deleteConfirmModal: ModalDirective;

  groupFilter: GroupFilter = new GroupFilter();

  constructor(private service: GroupService, private router: Router, private courseService: CourseService,
              private roleService: RoleService, private toastr: ToastrService , private trainerService : TrainerService,
              private userSevice : UserService) {
  }

  conditionAddForm: boolean = true;
  sliceViaPaginationGroupList: Group[];

  ngOnInit() {
    this.getCourses();
    this.getGroups();
  }

  getGroups(){
    if(this.roleService.roleMatch(['ADMIN'])){
      this.getAllGroups();
    }
    if(this.roleService.roleMatch(['TRAINER'])) {
      this.getTrainerGroups();
    }
    if(this.roleService.roleMatch(['EMPLOYEE'])||this.roleService.roleMatch(['MANAGER'])){
      this.getEmployeeGroups();
    }
  }

  getEmployeeGroups(){
    this.userSevice.getGroupsByUserId(localStorage.getItem('constant_userId')).subscribe((data: any) => {
        this.service.groupsList = data as Group[];
        this.sliceViaPaginationGroupList = this.service.groupsList.slice(0, 10);
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to get groups!");
      });
  };

  getCourses(){
    this.courseService.getCourses().subscribe((data: Course[]) => {
        this.courseService.coursesList = data ;
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to get Courses!");
      });
  }

  pageChanged(event: PageChangedEvent): void {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.sliceViaPaginationGroupList = this.service.groupsList.slice(startItem, endItem);
  }

  resetForm(form?: NgForm) {
    if (form != null)
      form.resetForm();
    this.service.formData = {
      nameGroup: '',
      courseId: null
    }
  }

  getGroupsByFilters(form: NgForm) {
    if (form.value.name !== undefined) {
      this.service.getGroupsByFilters(form.value).subscribe((data: any) => {
          this.service.groupsList = data as Group[];
          this.sliceViaPaginationGroupList = this.service.groupsList.slice(0, 10);
        },
        (error: HttpErrorResponse) => {
          this.toastr.error("Not possible to get information!");
        });
    } else {
      this.getGroups();
    }
  }

  getAllGroups() {
    this.service.getGroups().subscribe((data: any) => {
        this.service.groupsList = data as Group[];
        this.sliceViaPaginationGroupList = this.service.groupsList.slice(0, 10);
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to get groups!");
      });
  }

  getTrainerGroups(){
    this.trainerService.getTrainerGroups(localStorage.getItem('constant_userId')).subscribe((data: any) => {
        this.service.groupsList = data as Group[];
        this.sliceViaPaginationGroupList = this.service.groupsList.slice(0, 10);
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to get groups!");
      });
  }

  populateForm(group: Group) {
    this.service.formData = Object.assign({}, group);
  }

  goToGroup(groupId: number) {
    this.router.navigate(['/groups/' + groupId]);
  }

  onSubmit(form: NgForm) {
    this.groupModal.hide();
    if (this.conditionAddForm)
      this.insertGroup(form);
    else
      this.updateGroup(form);
  }

  getNameCourseByIdCourse(id: number): string {
    if (id != null && this.courseService.coursesList.length != 0) {

      return this.courseService.coursesList.find(x => x.courseId == id).courseName;
    }
  }

  updateGroup(form: NgForm) {
    this.service.updateGroup(form.value).subscribe((data: any) => {
        this.toastr.warning(data.Message);
        this.service.getGroups();
      },
      (error: HttpErrorResponse) => {
        if (error.status === 400) {
          this.toastr.error(error.error.Message);
        } else {
          this.toastr.error("Cannot updated a group!");
        }
      });
  }

  insertGroup(form: NgForm) {
    this.service.insertGroup(form.value).subscribe((data: any) => {
        this.toastr.success(data.Message);
        this.service.getGroups();
      },
      (error: HttpErrorResponse) => {
        if (error.status === 400) {
          this.toastr.error(error.error.Message);
        } else {
          this.toastr.error("Cannot inserted a group!");
        }
      });
  }

}

