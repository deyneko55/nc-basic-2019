import {Component, ViewChild} from '@angular/core';
import {BsDatepickerConfig, ModalDirective, PageChangedEvent} from "ngx-bootstrap";
import {CourseService} from "../../../shared/course/course.service";
import {RoleService} from "../../../shared/user/role.service";
import {ToastrService} from "ngx-toastr";
import {FormControl, NgForm} from "@angular/forms";
import {HttpErrorResponse} from "@angular/common/http";
import {Course} from "../../../shared/course/course.model";
import {Router} from "@angular/router";
import {CourseFilter} from "../../../shared/course/course-filter";
import {INgxSelectOption} from "ngx-select-ex";
import {Profile} from "../../../shared/profile/profile.model";
import {DesiredTime} from "../../../shared/desired-time/desired-time.model";
import {DayWeek} from "../../../shared/desired-time/day-week.model";
import {DomSanitizer} from "@angular/platform-browser";
import {ProfileService} from "../../../shared/profile/profile.service";
import {DesiredTimeService} from "../../../shared/desired-time/desired-time.service";
import {SetDesiredTime} from "../../../shared/desired-time/set-desired-time.model";

@Component({
  selector: 'calendar-form',
  templateUrl: './calendar-form.component.html',
})
export class CalendarForm {
  profile: Profile = new Profile();
  userDesiredTimesList: DesiredTime[] = [];
  days: DayWeek[] = [];
  day: DayWeek = new DayWeek();


  constructor(private sanitizer: DomSanitizer, private service: ProfileService, private roleService: RoleService,
              private toastr: ToastrService, private  desiredTimeService: DesiredTimeService) {
  }

  ngOnInit() {
    this.getTime();
  }

  userDesiredTime: SetDesiredTime = {
    userId: +localStorage.getItem('constant_userId'),
    userSchedule: '2019-04-0'
  };

  selectTime(time: string, dayWeek: string) {
    this.userDesiredTime.userSchedule = this.userDesiredTime.userSchedule + dayWeek + 'T' + time;
    this.isDesiredTime(time, dayWeek);
    for (let selectedTime of this.days[+dayWeek - 1].times) {
      if (selectedTime.name === time) {
        selectedTime.isSelect = !selectedTime.isSelect;
      }
    }
  }

  uploadTime() {
    this.desiredTimeService.setUserDesiredTime(this.userDesiredTime).subscribe((data: any) => {
        console.log(data);
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Cannot add desired time!");
      });
    this.userDesiredTime.userSchedule = '2019-04-0';
  }

  getTime() {
    this.desiredTimeService.getUserDesiredTime(localStorage.getItem('constant_userId')).subscribe((data: any) => {
        this.userDesiredTimesList = data as DesiredTime[];
        for (let _i = 1; _i < 8; _i++) {
          this.day = {
            times: [{name: "08:00", isSelect: this.isDesiredTime("08:00", [_i].toString())},
              {name: "10:00", isSelect: this.isDesiredTime("10:00", [_i].toString())},
              {name: "12:00", isSelect: this.isDesiredTime("12:00", [_i].toString())},
              {name: "14:00", isSelect: this.isDesiredTime("14:00", [_i].toString())},
              {name: "16:00", isSelect: this.isDesiredTime("16:00", [_i].toString())},
              {name: "18:00", isSelect: this.isDesiredTime("18:00", [_i].toString())},
              {name: "20:00", isSelect: this.isDesiredTime("20:00", [_i].toString())},],
          };
          this.days.push(this.day);
        }
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to get information!");
      });
  }


  isDesiredTime(time: string, dayWeek: string) {
    for (let _i = 0; _i < this.userDesiredTimesList.length; _i++) {
      if (('2019-04-0' + dayWeek + 'T' + time + ':00') === this.userDesiredTimesList[_i].userSchedule) {
        return true;
      }
    }
    return false;
  }

  deleteDesiredTime(time: string, dayWeek: string){
    for (let _i = 0; _i < this.userDesiredTimesList.length; _i++) {
      if (('2019-04-0' + dayWeek + 'T' + time + ':00') === this.userDesiredTimesList[_i].userSchedule) {
        this.desiredTimeService.deleteUserDesiredTime(this.userDesiredTimesList[_i].id).subscribe((data: any) => {
            console.log(data);
          },
          (error: HttpErrorResponse) => {
            this.toastr.error("Cannot delete desired time!");
          });
      }
    }
    this.userDesiredTime.userSchedule = '2019-04-0';
  }


}

