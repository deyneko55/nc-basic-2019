import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {Header} from "./components/header/header.component";
import {UserProfileForm} from "./components/user/user-profile-form/user-profile-form.component";
import {UsersForm} from "./components/user/users-form/users-form.component";
import {GroupsForm} from "./components/user/groups-form/groups-form.component";
import {CoursesForm} from "./components/user/courses-form/courses-form.component";
import {AuthGuard} from "./auth/auth.guard";
import {CoursePageComponent} from "./components/course-page/course-page.component";
import {GroupPageComponent} from "./components/group-page/group-page.component";
import {NewsForm} from "./components/news-form/news-form.component";
import {NewsPageComponent} from "./components/news-page/news-page.component";
import {AdminSettingForm} from "./components/user/admin-setting-form/admin-setting-form.component";
import {RegistrationForm} from "./components/landing-page/registration-form/registration-form.component";
import {DashboardsForm} from "./components/user/dashboards-form/dashboards-form.component";
import {NotFoundComponent} from "./errors/not-found/not-found.component";
import {HomeForm} from "./components/user/home-form/home-form.component";
import {ChatsForm} from "./components/chats-form/chats-form.component";
import {CalendarForm} from "./components/user/calendar-form/calendar-form.component";
import {AttendanceForm} from "./components/user/attendance-form/attendance-form.component";
import {NotificationForm} from "./components/notification-form/notification-form.component";


const routes: Routes = [
  {path: '', component: Header},
  {path: 'check/registration', component: RegistrationForm},
  {path: 'users/:id', component: UserProfileForm /*, canActivate: [AuthGuard]*/},
  {path: 'users', component: UsersForm /*, canActivate: [AuthGuard]*/},
  {path: 'groups', component: GroupsForm /*, canActivate: [AuthGuard]*/},
  {path: 'groups/:id', component: GroupPageComponent/* , canActivate: [AuthGuard]*/},
  {path: 'courses', component: CoursesForm /*, canActivate: [AuthGuard]*/},
  {path: 'courses/:id', component: CoursePageComponent /*, canActivate: [AuthGuard]*/},
  {path: 'news', component: NewsForm /*, canActivate: [AuthGuard]*/},
  {path: 'news/:id', component: NewsPageComponent /*, canActivate: [AuthGuard]*/},
  {path: 'setting', component: AdminSettingForm /*, canActivate: [AuthGuard]*/},
  {path: 'dashboards', component: DashboardsForm /*, canActivate: [AuthGuard]*/},
  // {path: '**', component: NotFoundComponent},
  {path: 'home', component: HomeForm /*, canActivate: [AuthGuard]*/},
  {path: 'chats', component: ChatsForm /*, canActivate: [AuthGuard]*/},
  {path: 'calendar', component: CalendarForm /*, canActivate: [AuthGuard]*/},
  {path: 'attendance', component: AttendanceForm /*, canActivate: [AuthGuard]*/},
  {path: 'notifications', component: NotificationForm /*, canActivate: [AuthGuard]*/},

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
