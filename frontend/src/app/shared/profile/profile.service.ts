import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import  {Profile} from "./profile.model";
import {ToastrService} from "ngx-toastr";
import {PhotoModel} from "./photo.model";

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  readonly rootUrl = 'http://localhost:8085/api';
  constructor(private http: HttpClient, private toastr: ToastrService) { }

  formData: Profile = new Profile();


  postFile(fileToUpload: File) {
    const formData: FormData = new FormData();
    formData.append('photo', fileToUpload);
    return this.http.post(this.rootUrl + '/photos/' + +localStorage.getItem('userId') +'/upload', formData);
  }

  updateProfile(profile: Profile) {
    const body: Profile = {
      userId: +localStorage.getItem('userId'),
      username: profile.username,
      firstName: profile.firstName,
      lastName: profile.lastName,
      email: profile.email,
      roleId: profile.roleId,
    };
    return this.http.patch(this.rootUrl + '/users', body);
  }
  getPhoto(){
    return this.http.get(this.rootUrl + '/photos/' + localStorage.getItem('userId'));
  }
  refreshInfo() {
    const id = +localStorage.getItem('userId');
    if(id) {
      return this.http.get(this.rootUrl + '/users/' + id);
    }
    else {
      this.toastr.error("Bad id");
    }
  };

  getProfile(id : number){
    return this.http.get(this.rootUrl + '/users/' + id);
  }
}
