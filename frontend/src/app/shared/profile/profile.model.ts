export class Profile {
  userId : number;
  firstName: string;
  lastName : string;
  username : string;
  email : string;
  roleId : number;
}
