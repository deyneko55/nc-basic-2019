import {HttpClient} from "@angular/common/http";
import {ToastrService} from "ngx-toastr";
import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  readonly rootUrl = 'http://localhost:8085/api';
  constructor(private http: HttpClient, private toastr: ToastrService) { }

  getUnreadNotifications(){
    return this.http.get(this.rootUrl + '/notifications/unread/' + +localStorage.getItem('constant_userId'));
  }

  getAllUserNotifications(){
    return this.http.get(this.rootUrl + '/notifications/' + +localStorage.getItem('constant_userId'));
  }

  getUserLastNotifications(){
    return this.http.get(this.rootUrl + '/notifications/last/' + +localStorage.getItem('constant_userId'));
  }

  markAllAsRead(userId: number){
    return this.http.patch(this.rootUrl + '/notifications/' + +localStorage.getItem('constant_userId'), userId);
  }

  wantToCourse(userId : number,  courseId : number){
    const body = {
      userId : userId,
      courseId : courseId,
    };
    return this.http.post(this.rootUrl + '/courses/join-course-request' ,body );
  }
}
