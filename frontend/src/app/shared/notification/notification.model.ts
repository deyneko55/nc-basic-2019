export class NotificationModel {
  notificationId: number;
  recipientId: number;
  content: string;
  isRead: boolean;
}
