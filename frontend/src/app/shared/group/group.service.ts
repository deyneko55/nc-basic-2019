import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Group} from "./group.model";
import {GroupFilter} from "./group-filter.model";
import {CreateGroup} from "./create-group.model";
import {LessonService} from "../lessons/lesson.service";
import {SetUserToGrop} from "../user/set-user-to-group.nodel";

@Injectable({
  providedIn: 'root'
})
export class GroupService {
  readonly rootUrl = 'http://localhost:8085/api';
  formData : CreateGroup = new CreateGroup();
  groupsList : Group[] = [];
  constructor(private http: HttpClient) { }

  getGroups(){
    return this.http.get(this.rootUrl + "/groups");
  }

  refreshInfo(id : string){
    return this.http.get(this.rootUrl + "/groups/" + +id);
  }
  getMembersInfo(id : string){
    return this.http.get(this.rootUrl + "/groups/" + +id+ "/members");
  }
  getGroupsByFilters(group : GroupFilter){
      return this.http.get(this.rootUrl + "/groups/search/" + group.name);
  }
  updateGroup(group : Group){
    const body: Group = {
      id: group.id,
      nameGroup: group.nameGroup,
      courseId: group.courseId,
      trainerId : null,
      trainerName : null,
    };
    return this.http.patch(this.rootUrl + "/groups/" + body.id,body);
  }
  getChatByGroupId(groupId: number) {
    if (groupId != null) {
      return this.http.get(this.rootUrl + '/chats/groups/' + groupId);
    }
  }

 setUserToGroup(setUserToGroup : SetUserToGrop){
   return this.http.post(this.rootUrl  + "/groups/add-user", setUserToGroup);
 }

  insertGroup(formData : CreateGroup){
    return this.http.post(this.rootUrl  + "/groups/new-group", formData);
  }
}
