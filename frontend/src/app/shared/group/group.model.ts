export class Group {
  id : number;
  nameGroup : string;
  courseId : number;
  trainerId : number;
  trainerName : string;
}
