export class TrainerInfo {
  userId : number;
  login : string;
  email : string;
  firstName : string;
  lastName : string;
  roleId : number;
  avatar : FormData;
  qualificationName : string;
}
