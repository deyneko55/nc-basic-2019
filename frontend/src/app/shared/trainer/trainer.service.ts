import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Trainer} from "./trainer-group.model";
import {Profile} from "../profile/profile.model";
import {Feedback} from "./feedback.model";

@Injectable({
  providedIn: 'root'
})
export class TrainerService {
  readonly rootUrl = 'http://localhost:8085/api';
  trainers: Profile[] = [];
  constructor(private http: HttpClient) { }

  getAllTrainers(){
    return this.http.get(this.rootUrl + "/trainers");
  }

  insertTrainerToGroup(trainer : Trainer){
    return this.http.put(this.rootUrl + "/admin/assign-trainer", trainer);
  }

  getTrainersByCourse(courseId : string){
    return this.http.get(this.rootUrl + "/trainers/courses/" + courseId);
  }

  getTrainersLevels(){
    return this.http.get(this.rootUrl + "/trainers/levels");
  }

  getTrainersWithLevels(){
    return this.http.get(this.rootUrl + "/trainers/show-levels");
  }

  getTrainersCourse(trainerId: string){
    return this.http.get(this.rootUrl +  '/trainers/' + trainerId + '/courses');
  }

  getTrainerGroups(trainerId : string){
    return this.http.get(this.rootUrl +  '/trainers/' + trainerId + '/groups');
  }

  getTrainerUsers(trainerId: string){
    return this.http.get(this.rootUrl + '/trainers/' +trainerId);
  }

  getQualificationLevels(){
    return this.http.get(this.rootUrl + '/trainers/levels');
  }
  addNewQualificationLevel(newQualificationLevel : string){
    const body = {
      qualificationLevel : newQualificationLevel,
    };
    return this.http.post(this.rootUrl + '/trainers/new-level' , body);
  }

  getTrainerInfo(trainerId : string){
    return this.http.get(this.rootUrl + '/trainers/' + trainerId + '/info');
  }

  setFeedback(feedback : Feedback){
    return this.http.post(this.rootUrl + '/trainers/new-feedback' , feedback);
  }

  getUserFeedback(userId : string){
    return this.http.get(this.rootUrl + '/users/' + userId + '/feedback' );
  }

  setQualificationLevel(userId : number, qualificationId : number){
    const body = {
      userId : userId,
      qualificationId : qualificationId,
    };
    return this.http.patch(this.rootUrl + '/trainers/updatequalification', body);
  }

}

