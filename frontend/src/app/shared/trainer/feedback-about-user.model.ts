export class FeedbackAboutUser {
  content : string;
  groupName : string;
  courseName : string;
  fullName : string;
}
