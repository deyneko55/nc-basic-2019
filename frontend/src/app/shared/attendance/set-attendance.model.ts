export class SetAttendance {
  userId : number;
  lessonId : number;
  explanation : string;
  presenceStatusId : number;
  absenceReasonId : number;
}
