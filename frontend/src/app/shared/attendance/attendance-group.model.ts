export  class AttendanceGroup {
  groupId:number;
  lessonName : string;
  lessonDate : string;
  userFullName : string;
  userPresenceStatus: string;
  userId : number;
}
