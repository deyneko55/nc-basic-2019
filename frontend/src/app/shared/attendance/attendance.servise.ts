import {Injectable, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {CreateGroup} from "../group/create-group.model";
import {SetAttendance} from "./set-attendance.model";

@Injectable({
  providedIn: 'root'
})
export class AttendanceService {
  readonly rootUrl = 'http://localhost:8085/api';
  constructor(private http: HttpClient) { }

  getGroupAttendance(groupId: string){
    return this.http.get(this.rootUrl + '/groups/attendance/' +groupId );
  }

  downloadCourseReport(courseId : string){
    return this.http.get('http://localhost:8085/api/admin/download/attendance-report/' + courseId , { responseType:"blob"});
  }

  downloadGroupReport(groupId : string){
    return this.http.get('http://localhost:8085/api/groups/download/attendance-report/' + groupId , { responseType:"blob"});
  }

  addNewAbsenceReason(AbsenceReason : string){
    const body = {
      absenceReasonName : AbsenceReason
    };
    return this.http.post(this.rootUrl + '/absence-reasons/new-absence-reason', body);
  }

  getAllAbsenceReason(){
    return this.http.get(this.rootUrl + '/absence-reasons');
  }

  setAttendance(setAttendance :SetAttendance){
    return this.http.post( this.rootUrl + '/lessons/status', setAttendance);
  }
}
