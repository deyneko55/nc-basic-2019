import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Profile} from "../profile/profile.model";
import {SetManager} from "./set-manager.model";
import {UserFilter} from "../user/user-filter.model";
import {ManagerResponse} from "./manager-response.model";

@Injectable({
  providedIn: 'root'
})
export class ManagerService {
  readonly rootUrl = 'http://localhost:8085/api';
  managers: Profile[] = [];
  constructor(private http: HttpClient) { }

  getAllManagers(){
    return this.http.get(this.rootUrl + "/managers");
  }
  getAllMenegersUsers(managerId: string){
    return this.http.get(this.rootUrl + "/managers/" + managerId);
  }

  setManager(userManager : SetManager){
    const body: SetManager = {
      userId: userManager.userId,
      managerId : userManager.managerId,
    };
    return this.http.post(this.rootUrl + "/managers", body);
  }

  getUserManager(userId : string){
    return this.http.get(this.rootUrl + '/managers/search/' + userId );
  }
}
