import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Course} from "./course.model";
import {CourseFilter} from "./course-filter";
import {AddEmploee} from "./add-emploee.motel";
import {CourseWithIdsFormRequest} from "./course-with-ids-form-request.model";

@Injectable({
  providedIn: 'root'
})
export class CourseService {
  readonly rootUrl = 'http://localhost:8085/api';
  formData1 : CourseWithIdsFormRequest = new CourseWithIdsFormRequest();
  newCourse : CourseWithIdsFormRequest[] = [];
  formData : Course = new Course();
  coursesList : Course[] = [];
  allCoursesList : Course[] = [];
  constructor(private http: HttpClient) { }

  getCourses(){
    return this.http.get(this.rootUrl + "/courses");
  }

  getCoursesByFilters(course : CourseFilter){
      return this.http.get(this.rootUrl + "/courses/search/" + course.name);
  }

  updateCourse(course : Course){
    const body: Course = {
      courseId: course.courseId,
      courseName: course.courseName,
      courseType: course.courseType,
      startDate : course.startDate,
      endDate: course.endDate,
      difficultyLevel: course.difficultyLevel,
    };
    return this.http.patch(this.rootUrl +'/courses',body);
  }

  getUsersByCourse(courseId){
    return this.http.get(this.rootUrl + "/courses/" + +courseId + "/users");
  }

  insertNewCourse(formData : CourseWithIdsFormRequest){
    const body : CourseWithIdsFormRequest = {
      courseName : formData.courseName,
      startDate : formData.startDate,
      endDate : formData.endDate,
      courseTypeId : formData.courseTypeId,
      difficultyLevelId : formData.difficultyLevelId,
    };
    return this.http.post(this.rootUrl  + "/courses/new", body);
  }

  insertCourse(formData : Course){
    return this.http.post(this.rootUrl  + "/courses/new-course", formData);
  }

  refreshInfo(id : number) {
    return this.http.get(this.rootUrl + '/courses/' + id);
  };

  getGroupsOfCourse(courseId : string){
    return this.http.get(this.rootUrl + "/courses/" + +courseId +"/groups");
  }

  getCourseTypes(){
    return this.http.get(this.rootUrl + "/courses/types" );
  }

  addEmployee(addEmploe : AddEmploee){
    const body : AddEmploee = {
      userId : addEmploe.userId,
      courseId : addEmploe.courseId,
    };
    return this.http.post(this.rootUrl + '/courses/bookcourse' , body);
  }

  getUsersByCourseType(courseTypeId : number){
    return this.http.get(this.rootUrl + '/courses/statistics/courseTypes/' + +courseTypeId + '/users');
  }

  getGroupsByCourseType(courseTypeId : number){
    return this.http.get(this.rootUrl + '/courses/statistics/courseTypes/' + +courseTypeId + '/groups');
  }

  getUnallocatedUsers(courseId : string){
    return this.http.get(this.rootUrl + '/courses/desiredcourses/' + courseId + '/users');
  }
  getLevelsByCourse(courseTypeId: string){
    return this.http.get(this.rootUrl + '/courses/type/' +courseTypeId +'/levels');
  }
  getLevels(){
    return this.http.get(this.rootUrl + '/courses/levels');
  }


  createNewCourseType(typeName : string){
    const body = {
      typeName : typeName,
    };
    return this.http.post(this.rootUrl + '/courses/new-type' , body);
  }

  createNewDofficultuLevel(nameOfLevel : string){
    const body = {
      nameOfLevel : nameOfLevel,
    };
    return this.http.post(this.rootUrl + '/courses/new-difficulty-level' , body);
  }
}
