export class Course {
  courseId : number;
  courseName : string;
  courseType : string;
  startDate: string;
  endDate : string;
  difficultyLevel : string;
}
