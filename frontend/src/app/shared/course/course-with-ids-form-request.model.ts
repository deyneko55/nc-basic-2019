export class CourseWithIdsFormRequest {
  courseName: string;
  startDate: string;
  endDate: string;
  courseTypeId: number;
  difficultyLevelId: number;
}
