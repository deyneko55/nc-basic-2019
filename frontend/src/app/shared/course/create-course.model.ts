export class CreateCourse {
  courseName : string;
  courseType : string;
  startDate: Date;
  endDate : Date;
  difficultyLevel : string;
}
