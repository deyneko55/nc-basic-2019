export class SendMessageModel {
  userId: number;
  content: string;
}
