import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {ChatModel} from './chat.model';
import {Injectable} from '@angular/core';
import {ChatMessage} from './message.model';
import {ToastrService} from "ngx-toastr";
import {NotificationModel} from "./notification.model";
import {text} from "d3-fetch";

@Injectable()
export class WebSocketService {
  readonly rootUrl = 'http://localhost:8085/api';
  readonly webSocketChatsUrl = 'http://localhost:8085/chats/';
  readonly webSocketNotificationsUrl = 'http://localhost:8085/notifications/';
  private stompClient: any;
  private stompNotificationsClient: any;
  private disabled = true;
  private currentChat: number;
  messages: ChatMessage[] = [];
  private connected = false;


  constructor(private http: HttpClient, private toastr: ToastrService) {
  }

  setConnected(connected: boolean) {
    this.disabled = !connected;

    if (connected) {
      console.log('connected');
    }
  }

  connect(chatId: number) {
    const _this = this;
    let id = +localStorage.getItem('constant_userId');
    _this.messages = null;
    _this.messages = [];
    const socket = new SockJS(this.webSocketChatsUrl);
    this.stompClient = Stomp.over(socket);
    this.stompClient.reconnectDelay = 1000;
    //if (this.disabled === false) {
    //  this.disconnect();
    //}
    this.currentChat = chatId;
    this.stompClient.connect({}, frame => {
      this.stompClient.send(
        '/' + this.currentChat + '/messages/' + id,
        {},
        JSON.stringify({'chatId': this.currentChat, 'userId': id})
      );
      // receive chat history
      _this.stompClient.subscribe('/chats/' + chatId + '/' + id, message => {
        _this.receiveMessage(JSON.parse(message.body));
      });
      // receive new message
      _this.stompClient.subscribe('/chats/' + chatId, message => {
        _this.receiveMessage(JSON.parse(message.body));
      });
    });
  }

  connectToNotifications() {
    const _this = this;
    const socket = new SockJS(this.webSocketNotificationsUrl);
    this.stompNotificationsClient = Stomp.over(socket);
    this.stompNotificationsClient.reconnectDelay = 1000;
    this.stompNotificationsClient.connect({}, frame => {
      // receive notification
      this.stompNotificationsClient.subscribe('/notifications/' + +localStorage.getItem('constant_userId'), notification => {
        this.receiveNotification(JSON.parse(notification.body));
      });
    });
  }

  receiveNotification(notification: NotificationModel) {
    this.toastr.info(notification.content);
  }


  receiveMessage(message: ChatMessage) {
    this.messages.unshift(message);
    this.messages = this.messages.sort(function (a, b) {
      return a.messageId - b.messageId;
    });
  }

  createPrivateChat(senderId: number, recipientId: number, chatName: string) {
    return this.http.post(this.rootUrl + '/chats/new-private-chat', {senderId, recipientId, chatName});
  }

  getUserInfo() {
    let id = +localStorage.getItem('constant_userId');
    if (id != null) {
      return this.http.get(this.rootUrl + '/users/' + id);
    }
  }

  disconnectFromNotifications() {
    if (this.stompNotificationsClient != null) {
      this.stompNotificationsClient.disconnect();
    }
  }

  disconnect() {
    if (this.stompClient != null) {
      this.stompClient.disconnect();
    }

    this.setConnected(false);
    console.log('Disconnected!');
  }

  getUserChats() {
    return this.http.get(this.rootUrl + '/users/' + +localStorage.getItem('constant_userId') + '/chats');
  }

  sendMessage(content, userId, username) {
    this.stompClient.send(
      '/send',
      {},
      JSON.stringify({content: content, chatId: this.currentChat, userId: userId, username: username})
    );
  }
}
