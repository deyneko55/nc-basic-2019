export class ChatMessage {
  messageId: number;
  messageSender: string;
  createDate: string;
  userId: number;
  chatId: number;
  content: string;
  status: boolean;
}
