import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {User} from "./user.model";
import {Login} from "./login.model";
import {ToastrService} from "ngx-toastr";
import {UserFilter} from "./user-filter.model";
import {Router} from "@angular/router";
import {Profile} from "../profile/profile.model";
import {Observable} from "rxjs";
import {Email} from "./email.model";
import {UserEditModel} from "./user-edit.model";

@Injectable({
  providedIn: 'root'
})
export class UserService {
  readonly rootUrl = 'http://localhost:8085/api';

  constructor(private http: HttpClient) {
  }

  token: string = '';
  tokenDefault: string = 'token';
  user: User = new User();
  usersProfileList: Profile[] = [];
  editUser: UserEditModel = new UserEditModel();

  createRegistrationLink(email: string) {
    const body: Email = {
      email: email
    };
    return this.http.post(this.rootUrl + '/admin/registration-link', body);
  }


  updateUser(user: UserEditModel) {
    return this.http.patch(this.rootUrl + "/users", user);
  }

  checkValidToken() {
    if (this.token == this.tokenDefault)
      return false;
    return this.http.get(this.rootUrl + '/users/registration/' + this.token);
  }

  registerUser(user: User) {
    return this.http.post(this.rootUrl + '/users', user);
  }

  registerUserByToken(user: User, token: string) {
    const body = {
      username: user.username,
      password: user.password,
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
      roleId: user.roleId,
      registrationToken: token,
    };
    return this.http.post(this.rootUrl + '/users/registration', body);
  }

  getUsers(): Observable<Profile[]> {
    return this.http.get<Profile[]>(this.rootUrl + '/users');
  }

  getAllUsers() {
    return this.http.get(this.rootUrl + "/users");
  }

  getGroupMates(userId: number) {
    return this.http.get(this.rootUrl + "/groups/group-mates/" + userId);
  }

  getUsersByFilters(user: UserFilter) {
    const body: UserFilter = {
      username: user.username,
      roleIds: user.roleIds,
    };
    return this.http.post(this.rootUrl + "/users/search", body);
  }

  userAunthentication(userName: string, passWord: string) {
    const loginRequestBody: Login = {
      username: userName,
      password: passWord
    };
    let reqHeader = new HttpHeaders({'No-Auth': 'True'});
    return this.http.post(this.rootUrl + '/auth/sign-in', loginRequestBody, {headers: reqHeader});
  }

  getDeleteUser(userId: number) {
    return this.http.delete(this.rootUrl + "/users/" + userId);
  }

  getCoursesByUserId(userId: string) {
    return this.http.get(this.rootUrl + '/courses/user-courses/' + userId);
  }

  getGroupsByUserId(userId: string) {
    return this.http.get(this.rootUrl + '/groups/user-groups/' + userId);
  }

  passwordRecovery(email: Email) {
    return this.http.post(this.rootUrl + "/auth/password-recovery", email);
  }

  getStatistik(managerId : string){
    return this.http.get(this.rootUrl + '/users/manager/' + managerId + '/subordinates');
  }
}
