export class UserEditModel {
  userId : number;
  username : string;
  firstName : string;
  lastName : string;
  email : string;
}
