import {Injectable} from '@angular/core';
import {Profile} from "../profile/profile.model";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {ToastrService} from "ngx-toastr";
import {Router} from "@angular/router";
import {forEach} from "@angular/router/src/utils/collection";
import {Role} from "./role.model";

@Injectable({
  providedIn: 'root'
})
export class RoleService {

  readonly rootUrl = 'http://localhost:8085/api';
  rolesList : Role[] = [];
  constructor(private http: HttpClient, private toastr: ToastrService, private router: Router) {
  }

  userMatch(): boolean {
    if (localStorage.getItem('userId') != localStorage.getItem('constant_userId'))
      return true;
    return false;
  }

  getRoles(){
    return this.http.get(this.rootUrl + '/roles').subscribe((data:any)=>{
      this.rolesList = data as Role[];
      },
      (error: HttpErrorResponse) => {
        this.toastr.error("Not possible to get information!");
      });
  }


  getRoleName(id:number) {
    for (let key in this.rolesList) {
      if(this.rolesList[key].id == id)
        return this.rolesList[key].name;
    }
    return 'not found';
  }

  roleMatch(allowedRoles : any): boolean{
    let isMatch = false;
    let userRole: string = localStorage.getItem('userRoles');
    allowedRoles.forEach((element: string) => {
      if(userRole.indexOf(element) > -1){
        isMatch = true;
        return false;
      }
    });
    return isMatch;
  }
}
