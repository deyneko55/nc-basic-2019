export class UserStatistick {
  firstName : string;
  lastName : string;
  username : string;
  visitedLessons : number;
  missedLessons : number;
}
