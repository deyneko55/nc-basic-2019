export class Lesson {
  id:number;
  courseId:number;
  lessonName:string;
  lessonDate : string;
  lessonStatus : string;
}
