import {Injectable, OnInit} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {Lesson} from "./lesson.model";
import {CreateLesson} from "./create-lesson.model";
import {CreateGroup} from "../group/create-group.model";
import {SetLessonsDateFoGroupModel} from "./setLessons-date-fo-group.model";

@Injectable({
  providedIn: 'root'
})
export class LessonService {
  readonly rootUrl = 'http://localhost:8085/api/lessons';
  constructor(private http: HttpClient) { }
  lessons : Lesson[] =[];
  formData : CreateLesson = new CreateLesson();

  getGroupLessons(groupId : number){
    return this.http.get(this.rootUrl + '/schedule/' +groupId);
  }
  addLesson(lesson : CreateLesson){
    return this.http.post(this.rootUrl + '/new-lesson' , lesson);
  }

  getCourseLessons(courseId : string){
    return this.http.get( 'http://localhost:8085/api/courses/' +  courseId + '/lessons')
  }

  downloadMateril(fileId : string){
    return this.http.get('http://localhost:8085/api/files/download/lesson-materials' , {
      params: {
        fileId: fileId,
      }});
  }
  setDateForLessonOfGroup(setDate : SetLessonsDateFoGroupModel){
    const body : SetLessonsDateFoGroupModel = {
      lessonId : setDate.lessonId,
      groupId : setDate.groupId,
      lessonDate : setDate.lessonDate,
    };
    console.log(body);
    return this.http.patch(this.rootUrl + '/set-date-for-group' , body);
  }

  getAllPresenceStatuses() {
    return this.http.get(this.rootUrl + "/presence-statuses" );
  }

  canselLesson(lessonId : number ,groupId : number){
    console.log(lessonId , groupId);
    return this.http.patch(this.rootUrl + '/cancel-lesson' ,  {} , {
      params: {
        lessonId: lessonId.toString(),
        groupId: groupId.toString(),
      }})  ;
  }

  uploadFile(fileToUpload: File, lessonId : number) {
    console.log(File , lessonId);
    const formData: FormData = new FormData();
    formData.append('file', fileToUpload);
    return this.http.post('http://localhost:8085/api/files/upload/' + lessonId.toString(), formData )
  }

  getLessonsFile(lessonId : number){
    return this.http.get(this.rootUrl + '/files/' +  lessonId)
  }



  getFile(fileId : number){
    return this.http.get('http://localhost:8085/api/files/' +  fileId.toString())
  }
}
