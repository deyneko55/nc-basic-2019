export class FileModel {
  fileId : number;
  fileName : string;
  fileType : string;
  lesson_file : any;
}
