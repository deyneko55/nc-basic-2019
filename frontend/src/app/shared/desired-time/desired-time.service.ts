import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {ToastrService} from "ngx-toastr";
import {Role} from "../user/role.model";
import {DesiredTime} from "./desired-time.model";
import {SetDesiredTime} from "./set-desired-time.model";
import {DeleteDesiredTime} from "./delete-desired-time.model";


@Injectable({
  providedIn: 'root'
})
export class DesiredTimeService {

  readonly rootUrl = 'http://localhost:8085/api/desiredschedules/';
  constructor(private http: HttpClient, private toastr: ToastrService) {
  }



  getUserDesiredTime(userId : string){
    return this.http.get(this.rootUrl  + userId);
  }

  setUserDesiredTime(userDesiredTime : SetDesiredTime){
    const body : SetDesiredTime ={
      userId : userDesiredTime.userId,
      userSchedule : userDesiredTime.userSchedule
    };
    return this.http.post(this.rootUrl + 'settime',  body);
  }

  deleteUserDesiredTime(desiredTimeId : number){
    return this.http.delete(this.rootUrl +  desiredTimeId);
  }

  getAllDesiredTimeForCourse(courseId : string){
    return this.http.get(this.rootUrl + 'courses/' + courseId);
  }

}


