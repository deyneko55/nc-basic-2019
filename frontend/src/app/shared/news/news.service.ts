import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ToastrService} from "ngx-toastr";
import {News} from "./news.model";
import {AddNew} from "./add-new.model";
import {SetNewsFoSlider} from "./set-news-fo-slider.model";

@Injectable({
  providedIn: 'root'
})
export class NewsService {
  readonly rootUrl = 'http://localhost:8085/api';
  newsList : News[] = [];
  news : News = new News();
  constructor(private http: HttpClient, private toastr: ToastrService) { }

  getAllNews(){
    return this.http.get(this.rootUrl + '/news' , {
      params: {
        userId: localStorage.getItem('constant_userId'),
      }});
  }
  getNews(newsId : string){
    return this.http.get(this.rootUrl + '/news/' + +newsId , {
      params: {
        userId: localStorage.getItem('constant_userId'),
      }});
  }
  createNews(news : AddNew) {
    return this.http.post(this.rootUrl + '/admin/news', news);
  }
  updateNews(body : News){
    return this.http.patch(this.rootUrl + '/news', + body);
  }
  postPhotoNews(fileToUpload: File, newsId : number){
    console.log(fileToUpload, newsId);
    const formData: FormData = new FormData();
    formData.append('picture', fileToUpload);
    console.log(newsId);
    const value : number = newsId;
    console.log(value);
    return this.http.post(this.rootUrl + '/admin/upload/' + newsId, formData);
  }

  getNewsForSlider(){
    return this.http.get(this.rootUrl + '/news/sliders');
  }

  setNewsForSlider(setSlider : SetNewsFoSlider){
    return this.http.patch(this.rootUrl + "/news/sliders/update" ,setSlider );
  }

  setView(userId : number, newsId : number){
    const body = {
      newsId : newsId,
      userId : userId,
    };
    return this.http.patch(this.rootUrl + '/news/set-viewed' , body);
  }


  setLike(userId : number, newsId : number, isLiked : boolean){
    const body = {
      newsId : newsId,
      userId : userId,
      isLiked : isLiked,
    };
    return this.http.patch(this.rootUrl + '/news/set-like' , body);
  }
}


