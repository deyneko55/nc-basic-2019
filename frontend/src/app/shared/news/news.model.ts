export class News {
  newsPostId : number;
  header: string;
  description : string;
  content : string;
  picture : FormData;
  numberOfLikes: number;
  numberOfViews: number;
  likedByCurrentUser : boolean;
}
