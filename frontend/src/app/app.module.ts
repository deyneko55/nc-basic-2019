import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpClientModule , HTTP_INTERCEPTORS} from '@angular/common/http';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import {NgxSelectModule} from "ngx-select-ex";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import { NgxLoadingModule } from 'ngx-loading';

import {Header} from './components/header/header.component';
import {LoginForm} from './components/landing-page/login-form/login-form.component';
import {RegistrationForm} from './components/landing-page/registration-form/registration-form.component';
import {ResetPasswordForm} from './components/landing-page/reset-password-form/reset-password-form.component';
import {UserService} from './shared/user/user.service';
import {RoleService} from "./shared/user/role.service";
import {UserHeader} from './components/user/user-header/user-header.component';
import {WrapperContainer} from './components/user/user-header/wrapper-container/wrapper-container.component';
import {UsersForm} from './components/user/users-form/users-form.component';
import {UserProfileForm} from "./components/user/user-profile-form/user-profile-form.component";
import {CoursesForm} from "./components/user/courses-form/courses-form.component";
import {GroupsForm} from "./components/user/groups-form/groups-form.component";
import {ForbiddenComponent} from './errors/forbidden/forbidden.component';
import {ToastrModule} from 'ngx-toastr';
import {ProfileService} from "./shared/profile/profile.service";


import {BsDatepickerModule, BsDropdownModule, CarouselModule, ModalModule, PaginationModule} from 'ngx-bootstrap'
import {CheckSpacesDirective} from "./directives/checkSpaces.directive";
import {DeleteSpacesDirective} from "./directives/deleteSpaces.directive";
import {CourseService} from "./shared/course/course.service";
import {GroupService} from "./shared/group/group.service";
import { GroupPageComponent } from './components/group-page/group-page.component';
import { CoursePageComponent } from './components/course-page/course-page.component';
import {NewsForm} from "./components/news-form/news-form.component";
import {NewsPageComponent} from "./components/news-page/news-page.component";
import {AuthInterceptor} from "./auth/auth.interceptor";
import {AdminSettingForm} from "./components/user/admin-setting-form/admin-setting-form.component";

import { CollapseModule } from 'ngx-bootstrap/collapse';
import { AccordionModule, AccordionConfig } from 'ngx-bootstrap/accordion';
import {DashboardsForm} from "./components/user/dashboards-form/dashboards-form.component";
import {InfoAuthenticationContainer} from "./components/landing-page/user-modal-form/authentication-container/authentication-container.component";
import { TabsModule } from 'ngx-bootstrap/tabs';

import {DashboardsTrainerForm} from "./components/user/dashboards-form/dashboards-trainer-form/dashboards-trainer-form.component";
import {DashboardsAdminForm} from "./components/user/dashboards-form/dashboards-admin-form/dashboards-admin-form.component";
import {DashboardsManagerForm} from "./components/user/dashboards-form/dashboards-manager-form/dashboards-manager-form.component";
import {DashboardsEmployeeForm} from "./components/user/dashboards-form/dashboards-employee-form/dashboards-employee-form.component";
import {TrainerService} from "./shared/trainer/trainer.service";
import {ManagerService} from "./shared/manager/manager.service";
import {HomeForm} from "./components/user/home-form/home-form.component";
import { NotFoundComponent } from './errors/not-found/not-found.component';
import {ChatsForm} from "./components/chats-form/chats-form.component";
import {NewsService} from "./shared/news/news.service";
import {WebSocketService} from "./shared/chat/web-socket.service";
import {CalendarForm} from "./components/user/calendar-form/calendar-form.component";
import {AttendanceService} from "./shared/attendance/attendance.servise";
import {AttendanceForm} from "./components/user/attendance-form/attendance-form.component";
import {NotificationForm} from "./components/notification-form/notification-form.component";
import {NotificationService} from "./shared/notification/notification.service";

@NgModule({
  declarations: [
    AppComponent,
    Header,
    LoginForm,
    RegistrationForm,
    ResetPasswordForm,
    UserHeader,
    WrapperContainer,
    UserProfileForm,
    UsersForm,
    CoursesForm,
    CheckSpacesDirective,
    DeleteSpacesDirective,
    GroupsForm,
    ForbiddenComponent,
    InfoAuthenticationContainer,
    GroupPageComponent,
    CoursePageComponent,
    NewsForm,
    NewsPageComponent,
    AdminSettingForm,
    DashboardsForm,
    DashboardsTrainerForm,
    DashboardsAdminForm,
    DashboardsManagerForm,
    NotFoundComponent,
    DashboardsEmployeeForm,
    HomeForm,
    ChatsForm,
    CalendarForm,
    AttendanceForm,
    NotificationForm,
  ],
  imports: [
    BrowserModule,
    NgxChartsModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NgxSelectModule,
    ModalModule.forRoot(),
    BsDatepickerModule.forRoot(),
    CarouselModule.forRoot(),
    BsDropdownModule.forRoot(),
    AccordionModule.forRoot(),
    ToastrModule.forRoot({
      preventDuplicates: true
    }),
    CollapseModule.forRoot(),
    TabsModule.forRoot(),
    PaginationModule.forRoot(),
    NgxLoadingModule.forRoot({})

  ],
  providers: [UserService, RoleService, ProfileService, GroupService, CourseService, TrainerService, ManagerService,
    WebSocketService,NewsService,AttendanceService, NotificationService,
    {provide: AccordionConfig, useValue: {closeOthers: true}},
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},],
  bootstrap: [AppComponent]
})
export class AppModule {
}
